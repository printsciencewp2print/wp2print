<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://woo.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 9.2
 *
 * @var bool $show_downloads Controls whether the downloads table should be rendered.
 */

defined( 'ABSPATH' ) || exit;

global $itn_enabled;

$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited

if ( ! $order ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();

if ( $show_downloads ) {
	wc_get_template(
		'order/order-downloads.php',
		array(
			'downloads'  => $downloads,
			'show_title' => true,
		)
	);
}
$itn_enabled = print_products_woocommerce_is_itn_enabled();
$thcolspan = '';
if ($itn_enabled) { $thcolspan = ' colspan="2"'; }
?>
<section class="woocommerce-order-details">
	<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>

	<h2 class="woocommerce-order-details__title"><?php esc_html_e( 'Order details', 'woocommerce' ); ?></h2>

	<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

		<thead>
			<tr>
				<th class="woocommerce-table__product-name product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<?php if ($itn_enabled) { ?><th class="woocommerce-table__product-table product-tracking"><?php echo print_products_woocommerce_get_itn_label(); ?></th><?php } ?>
				<th class="woocommerce-table__product-table product-total"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
			do_action( 'woocommerce_order_details_before_order_table_items', $order );

			foreach ( $order_items as $item_id => $item ) {
				$product = $item->get_product();

				wc_get_template(
					'order/order-details-item.php',
					array(
						'order'              => $order,
						'item_id'            => $item_id,
						'item'               => $item,
						'show_purchase_note' => $show_purchase_note,
						'purchase_note'      => $product ? $product->get_purchase_note() : '',
						'product'            => $product,
					)
				);
			}

			do_action( 'woocommerce_order_details_after_order_table_items', $order );
			?>
		</tbody>

		<tfoot>
			<?php
			foreach ( $order->get_order_item_totals() as $key => $total ) {
				?>
					<tr>
						<th scope="row"<?php echo $thcolspan; ?>><?php echo esc_html( $total['label'] ); ?></th>
						<td><?php echo wp_kses_post( $total['value'] ); ?></td>
					</tr>
					<?php
			}
			?>
			<?php if ( $order->get_customer_note() ) : ?>
				<tr>
					<th<?php echo $thcolspan; ?>><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					<td><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
				</tr>
			<?php endif; ?>
		</tfoot>
	</table>

	<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
</section>

<?php
/**
 * Action hook fired after the order details.
 *
 * @since 4.4.0
 * @param WC_Order $order Order data.
 */
do_action( 'woocommerce_after_order_details', $order );

if ( $show_customer_details ) {
	wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
}
$rcp_colors = array('Cyan', 'Magenta', 'Yellow', 'Black', 'Other');
if (print_products_license_allow_flexo_plate()) { ?>
<div style="display:none;">
	<div id="reorder-color-popup" class="reorder-color-popup" data-item-id="" data-other-value="<?php _e('Other', 'wp2print'); ?>" data-other-error="<?php _e('Please enter color', 'wp2print'); ?>.">
		<h3><?php _e('Re-order details', 'wp2print'); ?></h3>
		<div class="rcp-row">
			<label class="rcp-tracking"><?php echo print_products_woocommerce_get_itn_label(); ?>: <span></span></label>
		</div>
		<div class="rcp-row">
			<label><?php _e('Color', 'wp2print'); ?>:</label>
			<select name="rcp_color" class="rcp-color" onchange="reorder_color_change(this)">
			<?php foreach ($rcp_colors as $rcp_color) { ?>
				<option value="<?php echo $rcp_color; ?>"><?php echo $rcp_color; ?></option>
			<?php } ?>
			</select>
		</div>
		<div class="rcp-row other-row">
			<label><?php _e('Please enter color', 'wp2print'); ?>:</label>
			<input type="text" name="rcp_other" class="rcp-other">
		</div>
		<input type="button" value="<?php _e('Continue', 'wp2print'); ?>" class="button" onclick="reorder_color_continue()">
	</div>
</div>
<?php } ?>