<?php
global $wpdb, $current_user;

$is_superuser = print_products_users_groups_is_superuser($current_user->ID);
if ($is_superuser) {
	$group_ids = explode(';', get_user_meta($current_user->ID, '_superuser_group', true));
	$request_uri = $_SERVER['REQUEST_URI'];
	if (strpos($request_uri, '?')) { $request_uri = substr($request_uri, 0, strpos($request_uri, '?')); }
	?>
	<div class="wrap ma-group-membership-wrap">
		<?php if (isset($_GET['saved']) && $_GET['saved'] == 'true') { ?>
			<div class="notice-success"><p><?php _e('Group Membership were saved successfully.', 'wp2print') ?></p></div>
		<?php } ?>
		<form method="POST" class="magm-form">
		<input type="hidden" name="print_products_group_membership_action" value="true">
		<input type="hidden" name="redirect_url" value="<?php echo $request_uri; ?>">
		<?php foreach($group_ids as $group_id) {
			$group_data = print_products_users_groups_data($group_id);
			$group_users = get_users(array(
				'meta_key'     => '_user_group',
				'meta_value'   => $group_id
			));
			$users = array();
			if ($group_users) {
				foreach($group_users as $group_user) {
					$users[] = $group_user->ID;
				}
			}
			?>
			<div class="magm-group-line" style="margin-bottom:20px;">
				<h4 style="margin-bottom:5px;"><?php _e('Group', 'wp2print'); ?>: <?php echo $group_data->group_name; ?></h4>
				<select name="gusers[<?php echo $group_id; ?>][]" class="group-users" multiple="multiple" style="width:100%;">
					<?php if ($users && count($users)) { ?>
						<?php foreach ($users as $guser) {
							$userdata = get_userdata($guser);
							if ($userdata) { ?>
								<option value="<?php echo $userdata->ID; ?>" selected="selected"><?php echo $userdata->user_login; ?> (<?php echo $userdata->display_name; ?>)</option>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
		<?php } ?>
		<div class="ma-gm-submit">
			<input type="submit" value="<?php _e('Save', 'wp2print'); ?>" class="button button-primary">
		</div>
		</form>
		<script>
		jQuery(document).ready(function() {
			jQuery('.magm-form .group-users').select2({ajax: {multiple:true, url:'<?php echo site_url('/?AjaxAction=group-select-user'); ?>', dataType: 'json'}});
		});
		</script>
	</div>
<?php } ?>