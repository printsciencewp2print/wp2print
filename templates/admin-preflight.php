<?php
$print_products_preflight_profiles = get_option("print_products_preflight_profiles");
?>
<div class="wrap wp2print-create-order preflight-wrap">
	<h2><?php _e('Preflight Analysis', 'wp2print'); ?></h2>
	<div class="p-analysis">
		<p style="margin:0 0 8px; font-weight:700;"><?php _e('Please select preflight profile', 'wp2print'); ?>:</p>
		<div class="p-profiles" style="margin-bottom:15px;" data-error="<?php _e('Please select preflight profile', 'wp2print'); ?>.">
			<select name="p_profile" style="width:auto; max-width:100%;">
				<option value=""><?php _e('Select profile', 'wp2print'); ?></option>
				<?php if ($print_products_preflight_profiles) { ?>
					<?php foreach ($print_products_preflight_profiles as $pid => $pprofile) { ?>
						<option value="<?php echo $pid; ?>"<?php if ($pprofile['def'] == 1) { echo ' SELECTED'; } ?>><?php echo $pprofile['name']; ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</div>
		<?php include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-preflight-upload.php'; ?>
	</div>
	<div class="preflight-files">
		<table class="preflight-files-list"></table>
	</div>
	<div class="preflight-process">
		<div class="pp-result"></div>
	</div>
</div>
<div class="preflight-default" style="display:none;">
	<table>
		<tr>
			<td>{FNAME}</td>
			<td><input type="button" value="<?php _e('Analyze', 'wp2print'); ?>" class="button button-primary" onclick="wp2print_preflight_analize(this, '{FNM}', '{FURL}', '1');"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="a-loading" style="margin:5px; display:none;"></td>
		</tr>
	</table>
</div>