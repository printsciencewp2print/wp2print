<?php
$file_upload_max_size = get_option('print_products_file_upload_max_size');
$file_upload_target = get_option("print_products_file_upload_target");
$amazon_s3_settings = get_option("print_products_amazon_s3_settings");

if (!$file_upload_max_size) { $file_upload_max_size = 2; }
if (!$artwork_file_count) { $artwork_file_count = 25; }
if (!is_array($artwork_afile_types)) { $artwork_afile_types = array('all'); }
if (!count($artwork_afile_types)) { $artwork_afile_types = array('all'); }

$umime_types = '';
if (!in_array('all', $artwork_afile_types)) {
	$umime_types = '{title : "Specific files", extensions : "'.implode(',', $artwork_afile_types).'"}';
	$umime_types = str_replace('jpg/jpeg', 'jpg,jpeg', $umime_types);
}

$upload_to = 'host';
$plupload_url = get_bloginfo('url').'/index.php?ajaxupload=artwork&sessid='.session_id();
if ($file_upload_target == 'amazon' && $amazon_s3_settings['s3_access_key'] && $amazon_s3_settings['s3_secret_key']) {
	$upload_to = 'amazon';

	$s3_data = print_products_amazon_s3_get_data($amazon_s3_settings, $file_upload_max_size, $po_number);
	$s3path = $s3_data['s3path'];
	$is_s3_region = $s3_data['is_s3_region'];
	$plupload_url = $s3_data['amazon_url'];
	$amazon_file_url = $s3_data['amazon_file_url'];
	$multiparams = $s3_data['multiparams'];
}
?>
<?php if (isset($_GET['uploaded']) && $_GET['uploaded'] == '1') { ?>
	<div class="order-add-files-success" style="margin-bottom:20px;border:1px solid #339900; padding:10px; color:#339900;">
		<?php _e('Files were successfully added.', 'wp2print'); ?>
	</div>
<?php } else { ?>
	<div class="order-add-files-wrap">
		<p><?php if ($po_number) { ?><strong><?php _e('PO Number', 'wp2print'); ?>:</strong> <?php echo $po_number; ?><?php } else { ?><strong><?php _e('Order ID', 'wp2print'); ?>:</strong> <?php echo $order_id; ?><?php } ?></p>
		<div style="margin-bottom:20px;"><strong><?php echo $order_item_name; ?></strong><?php print_products_product_attributes_list_html($order_item_data); ?></div>
		<div style="margin-bottom:20px;<?php if (!$artwork_files) { echo ' display:none;'; } ?>"><strong><?php _e('Files', 'wp2print'); ?>:</strong>
			<div class="files-list">
				<?php if ($artwork_files) { $artwork_files_list = explode(';', $artwork_files); ?>
					<?php foreach($artwork_files_list as $furl) { ?>
						<?php echo basename($furl); ?><br>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
		<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>" class="order-add-files-form">
		<input type="hidden" name="order_add_files" value="true">
		<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
		<input type="hidden" name="order_item_id" value="<?php echo $order_item_id; ?>">
		<input type="hidden" name="artwork_files" class="artwork-files" value="<?php echo $artwork_files; ?>">
		<a href="#upload-files" class="woocommerce-button button view oaf-upload-btn"><?php _e('Upload files', 'wp2print'); ?></a>
		</form>
	</div>
	<div style="position:absolute;left:-20000px;">
		<div id="upload-artwork" class="upload-artwork-block print-products-area" style="margin:30px 30px 0; border:1px solid #C1C1C1; padding:20px; width:600px; height:400px;">
			<p style="margin:0 0 12px;"><?php _e('Please select artwork files', 'wp2print'); ?>:</p>
			<div id="filelist" class="ua-files-list" style="padding:10px 0; border-top:1px solid #C1C1C1; border-bottom:1px solid #C1C1C1;">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
			<div id="uacontainer" class="artwork-buttons">
				<a id="pickfiles" href="javascript:;" class="artwork-select"><?php _e('Select files', 'wp2print'); ?></a>
				<a id="uploadfiles" href="javascript:;" class="artwork-upload"><?php _e('Upload files', 'wp2print'); ?></a>
				<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="upload-loading" style="display:none;">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/plupload.full.min.js?ver=3.1.2"></script>
	<script type="text/javascript">
	<!--
	var omf_item_id = 0;
	jQuery(document).ready(function() {
		jQuery('.oaf-upload-btn').click(function(){
			omf_item_id = jQuery(this).attr('rel');
			jQuery('#filelist').html('').hide();
			jQuery('#uploadfiles').hide();
			jQuery('#continuebtn').hide();
			jQuery.colorbox({inline:true, href:"#upload-artwork"});
			return false;
		});
		jQuery('#continuebtn').click(function(){
			jQuery.colorbox.close();
			return false;
		});
		var ufilecount = <?php echo $artwork_file_count; ?>;
		var ufilenum = 0;
		var uploader = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			file_data_name: 'file',
			browse_button : 'pickfiles', // you can pass an id...
			container: document.getElementById('uacontainer'), // ... or DOM Element itself
			flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
			silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
			drop_element: document.getElementById('upload-artwork'), // ... or DOM Element itself
			url : '<?php echo $plupload_url; ?>',
			dragdrop: true,
			filters : {
				max_file_size : '<?php echo $file_upload_max_size; ?>mb',
				mime_types: [<?php echo $umime_types; ?>]
			},
			<?php if ($upload_to == 'amazon') { ?>
			multipart: true,
			<?php echo $multiparams; ?>
			<?php } ?>
			init: {
				PostInit: function() {
					jQuery('#filelist').html('').hide();
					jQuery('#uploadfiles').hide();
					jQuery('#continuebtn').hide();

					document.getElementById('uploadfiles').onclick = function() {
						uploader.start();
						jQuery('#uploadfiles').attr('disabled', 'disabled');
						jQuery('.upload-loading').show();
						jQuery('#continuebtn').hide();
						return false;
					};
				},
				FilesAdded: function(up, files) {
					var ucounterror = false;
					jQuery('#filelist').show();
					plupload.each(files, function(file) {
						file.name = wp2print_clear_fname(file.name);
						ufilenum++;
						if (ufilenum <= ufilecount) {
							document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
						} else {
							ucounterror = true;
						}
					});
					jQuery('#uploadfiles').removeAttr('disabled');
					jQuery('#uploadfiles').show();
					if (ucounterror) {
						alert("<?php _e('Max files count is', 'wp2print'); ?> "+ufilecount);
					}
				},
				UploadProgress: function(up, file) {
					document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
				},
				<?php if ($upload_to == 'amazon') { ?>
				BeforeUpload: function(up, file) {
					var regex = /(?:\.([^.]+))?$/;
					var ext = regex.exec(file.name)[1];
					if (ext == 'pdf') {
						up.settings.multipart_params['Content-Type'] = 'application/pdf';
					} else {
						up.settings.multipart_params['Content-Type'] = file.type;
					}
					up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
					<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
					up.settings.multipart_params['Content-Disposition'] = 'attachment';
				},
				<?php } ?>
				FileUploaded: function(up, file, response) {
					<?php if ($upload_to == 'amazon') { ?>
						var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
					<?php } else { ?>
						var ufileurl = response['response'];
					<?php } ?>
					if (ufileurl != '') {
						var artworkfiles = jQuery('.order-add-files-form .artwork-files').val();
						if (artworkfiles != '') { artworkfiles += ';'; }
						artworkfiles += ufileurl;
						jQuery('.order-add-files-form .artwork-files').val(artworkfiles);
					}
				},
				UploadComplete: function(files) {
					jQuery('.upload-loading').hide();
					jQuery('.order-add-files-form').submit();
				},
				Error: function(up, err) {
					alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
				}
			}
		});
		uploader.init();
	});
	//--></script>
<?php } ?>