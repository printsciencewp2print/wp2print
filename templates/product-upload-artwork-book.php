<?php
$file_upload_max_size = get_option('print_products_file_upload_max_size');
$file_upload_target = get_option("print_products_file_upload_target");
$amazon_s3_settings = get_option("print_products_amazon_s3_settings");

if (!$file_upload_max_size) { $file_upload_max_size = 2; }

$upload_to = 'host';
$uploader_url = get_bloginfo('url').'/index.php?ajaxupload=artwork&sessid='.session_id();
if ($file_upload_target == 'amazon' && $amazon_s3_settings['s3_access_key'] && $amazon_s3_settings['s3_secret_key']) {
	$upload_to = 'amazon';
	$s3_data = print_products_aec_amazon_s3_get_data($amazon_s3_settings, $file_upload_max_size);
	$uploader_url = $s3_data['amazon_url'];
	$amazon_file_url = $s3_data['amazon_file_url'];
}
?>
	<script type="text/javascript">var platform = false;</script>
	<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/pdf-parser-book.js?ver=<?php echo time(); ?>" data-optimized="0" data-no-optimize="1" data-no-defer="1"></script>
	<script type="text/javascript" src="https://wp2printapp.s3.amazonaws.com/20210111/universaluploader.gzip.js" data-optimized="0" data-no-optimize="1" data-no-defer="1"></script>
	<script type="text/javascript" src="https://wp2printapp.s3.amazonaws.com/20170919/language_<?php echo print_products_get_language_code(); ?>.js" data-optimized="0" data-no-optimize="1" data-no-defer="1"></script>
	<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/platform.js" data-optimized="0" data-no-optimize="1" data-no-defer="1"></script>
	<script type="text/javascript">
	<!--
	var parsednmb = 0;
	var files_data = [];
	var amazon_file_url = '<?php echo $amazon_file_url; ?>';
	var uploader_message = aec_uploader_message();
	if (uploader_message == 0 || uploader_message == 32) {
		universalUploader.init({
			serialNumber: "0081831741461771532272179149278921871210198",
			uploaders: "drag-and-drop",
			singleUploader : true,
			renderTabHeader: false,
			fileFilter_types:'pdf',
			width: '100%',
			height: '300',
			holder: 'universalUploader_holder',
			imagesPath : 'https://d207ec9xtrf2ml.cloudfront.net/jscripts/multipow10052015/universal/images/',
			url: '<?php echo $uploader_url; ?>',
			<?php if ($upload_to == 'amazon') {
				echo $s3_data['amazonS3_params'];
			} ?>
		});
		

		universalUploader.bindEventListener("Init", function (inited){
			if(inited) {
				jQuery('#uploadButton_drag-and-drop').addClass('uuUploadButton');
				jQuery('#browseButton_drag-and-drop span span').html('<?php _e('Select files', 'wp2print'); ?>');
				jQuery('#universalUploader_holder #tabs_container').css('height', 'auto');
				jQuery('#universalUploader_holder #drag-and-drop_content').css('height', 'auto');
			} else {
				alert("UniversalUploader failed to init!");
			}
		});
		universalUploader.bindEventListener("FilesAdded", function (uploaderId, files){
			not_uploaded = true;
			jQuery('.add-cart-form .simple-add-btn').hide();
			jQuery('.upload-pdf-processing').show();

			files_number1 = universalUploader.Html5.files.length;
			jQuery('.upload-pdf-processing ul li.tx').remove();
			pdf_parsing_process();
		});
		universalUploader.bindEventListener("UploadComplete", function (uploaderId, file){
			not_uploaded = false;
			if (autosubmit) {
				jQuery('form.add-cart-form').submit();
				autosubmit = false;
			}
		});
		universalUploader.bindEventListener("FileUploadComplete", function (uploaderId, file, response){
			<?php if ($upload_to == 'amazon') { ?>
				var ufileurl = amazon_file_url + file.name;
			<?php } else { ?>
				var ufileurl = response;
			<?php } ?>
			if (ufileurl != '') {
				var artworkfiles = jQuery('.add-cart-form .artwork-files').val();
				if (artworkfiles != '') { artworkfiles += ';'; }
				artworkfiles += ufileurl;
				jQuery('.add-cart-form .artwork-files').val(artworkfiles);
			}
		});
		universalUploader.bindEventListener("FilesRemoved", function (uploaderId, files){
			jQuery('.add-cart-form .artwork-files').val('');
			jQuery('.add-cart-form .simple-add-btn').hide();
			if (jQuery('.add-cart-form .quantity-bw').length) { jQuery('.add-cart-form .quantity-bw').val('0'); }
			if (jQuery('.add-cart-form .quantity-cl').length) { jQuery('.add-cart-form .quantity-cl').val('0'); }
			if (jQuery('.add-cart-form .smatrix-size').length) {
				jQuery('.add-cart-form .smatrix-size option').removeAttr('selected').trigger('change');
				if (jQuery('.add-cart-form .smatrix-size').parents('.attr-box').hasClass('attr-radio-box')) {
					jQuery('.add-cart-form .smatrix-size').parents('.attr-box').find('.a-radio input').eq(0).attr('checked', 'checked');
				}
				adetect_size_label();
			}
			jQuery('.label-quantity-bw').html('0');
			jQuery('.label-quantity-cl').html('0');
			parsednmb = 0;
			files_data = [];
		});
	}
	if (uploader_message != 0) {
		if (uploader_message == 'ie') {
			jQuery('#universalUploader_holder').hide();
		}
		jQuery('.uploader-warn-message .warn-message-'+uploader_message).show();
		jQuery('.uploader-warn-message').show();
	}
	function pdf_parsing_process() {
		var ftotal = universalUploader.Html5.files.length;
		if (parsednmb < ftotal) {
			var file = universalUploader.Html5.files[parsednmb];
			files_data[parsednmb] = {};
			files_data[parsednmb].name = file.name;
			jQuery('.upload-pdf-processing ul').append('<li class="tx afile-'+parsednmb+'"><?php _e('Analyzing file', 'wp2print'); ?>: '+file.name+' (<?php _e('processing page', 'wp2print'); ?> <span class="pp-cp">1</span> <?php _e('of', 'wp2print'); ?> <span class="pp-tp">1</span>)</li>');
			parse_pdf_file(file, parsednmb);
			parsednmb++;
		} else {
			pdf_parsing_complete();
		}
	}
	function pdf_parsing_complete() {
		jQuery('.upload-pdf-processing').hide();

		var fl_w = 0;
		var fl_h = 0;
		var fl_same_size = true;
		var fl_size_error = false;
		var bw_pages = 0;
		var cl_pages = 0;
		var init_message = "<?php _e('Page {X} of document {Y} is not an allowed size.', 'wp2print'); ?>";
		var size_messages = [];
		if (book_type == 3) { // Count detection only
			for (var f=0; f<files_data.length; f++) {
				var file_data = files_data[f];
				bw_pages = bw_pages + file_data.bw_pages;
				cl_pages = cl_pages + file_data.cl_pages;
			}
			bw_pages = bw_pages + cl_pages;
			pdf_parsing_set_pages(bw_pages, cl_pages);
			jQuery('.add-cart-form .simple-add-btn').show();
		} else {
			for (var f=0; f<files_data.length; f++) {
				var file_data = files_data[f];
				if (!file_data.same_size) {
					fl_same_size = false;
				}
				if (fl_w == 0) {
					fl_w = file_data.width;
					fl_h = file_data.height;
				} else if (file_data.width != fl_w || file_data.height != fl_h) {
					fl_same_size = false;
				}
				var spage = pdf_parsing_check_pages_size(file_data);
				if (spage) {
					fl_size_error = true;
					var wmessage = init_message.replace('{X}', spage);
					size_messages[f] = wmessage.replace('{Y}', file_data.name);
				}
				bw_pages = bw_pages + file_data.bw_pages;
				cl_pages = cl_pages + file_data.cl_pages;
			}
			if (book_type == 2) { bw_pages = bw_pages + cl_pages; }

			if (!fl_same_size) {
				alert("<?php _e('The dimensions of all the pages in your files are not the same', 'wp2print'); ?>.");
			} else if (fl_size_error) {
				alert("<?php _e('This product accepts these document sizes', 'wp2print'); ?>: "+pdf_parsing_get_sizes_name()+"\n\n"+size_messages.join("\n\n"));
			} else {
				pdf_parsing_set_size(fl_w, fl_h);
				pdf_parsing_set_pages(bw_pages, cl_pages);
				jQuery('.add-cart-form .simple-add-btn').show();
			}
		}
		matrix_calculate_price();
	}
	function pdf_parsing_get_sizes_name() {
		var sizes_name = '';
		if (jQuery('.print-attributes .smatrix-size').length) {
			jQuery('.print-attributes .smatrix-size option').each(function(){
				if (sizes_name != '') { sizes_name += ', '; }
				sizes_name += jQuery(this).html();
			});
		}
		return sizes_name;
	}
	function pdf_parsing_check_pages_size(file_data) {
		var pdf_pg = 0;
		if (jQuery('.print-attributes .smatrix-size').length) {
			var sizes_size = [];
			jQuery('.print-attributes .smatrix-size option').each(function(){
				var twidth = parseFloat(jQuery(this).data('width'));
				var theight = parseFloat(jQuery(this).data('height'));
				sizes_size[sizes_size.length] = twidth+';'+theight;
			});
			for (var p=0; p<file_data.pages.length; p++) {
				var psize_a = file_data.pages[p].split(';');
				var psize_matched = false;
				for (var s=0; s<sizes_size.length; s++) {
					var ssize_a = sizes_size[s].split(';');

					var wdiff = Math.abs(psize_a[0] - ssize_a[0]);
					var hdiff = Math.abs(psize_a[1] - ssize_a[1]);
					var wdiff2 = Math.abs(psize_a[0] - ssize_a[1]);
					var hdiff2 = Math.abs(psize_a[1] - ssize_a[0]);
					if ((wdiff <= 1 && hdiff <= 1) || (wdiff2 <= 1 && hdiff2 <= 1)) {
						psize_matched = true;
					}
				}
				if (pdf_pg == 0 && !psize_matched) { pdf_pg = p + 1; }
			}
		}
		return pdf_pg;
	}
	function pdf_parsing_set_size(file_width, file_height) {
		if (jQuery('.print-attributes .smatrix-size').length) {
			var aradio = false;
			jQuery('.print-attributes .smatrix-size option').removeAttr('selected');
			if (jQuery('.print-attributes .smatrix-size').parents('.attr-box').hasClass('attr-radio-box')) {
				aradio = true;
				jQuery('.print-attributes .smatrix-size').parents('.attr-box').find('.a-radio input').eq(0).attr('checked', 'checked');
			}
			jQuery('.print-attributes .smatrix-size option').each(function(){
				var tid = jQuery(this).attr('value');
				var twidth = parseFloat(jQuery(this).data('width'));
				var theight = parseFloat(jQuery(this).data('height'));

				var wdiff = Math.abs(file_width - twidth);
				var hdiff = Math.abs(file_height - theight);
				var wdiff2 = Math.abs(file_width - theight);
				var hdiff2 = Math.abs(file_height - twidth);

				if ((wdiff <= 1 && hdiff <= 1) || (wdiff2 <= 1 && hdiff2 <= 1)) {
					jQuery(this).prop('selected', true);
					jQuery('.print-attributes .smatrix-size').trigger('change');
					if (aradio) {
						jQuery('.print-attributes .smatrix-size').parents('.attr-box').find('.a-radio input[value="'+tid+'"]').attr('checked', 'checked').trigger('click');
					}
				}
			});
			adetect_size_label();
		}
	}
	function pdf_parsing_set_pages(bw_pages, cl_pages) {
		if (jQuery('.numbers-list .quantity-bw').length) {
			if (jQuery('.numbers-list .quantity-bw').hasClass('quantity-list')) {
				jQuery('.numbers-list .quantity-bw option').removeAttr('selected');
				jQuery('.numbers-list .quantity-bw option[value="'+bw_pages+'"]').prop('selected', true);
			} else {
				jQuery('.numbers-list .quantity-bw').val(bw_pages);
			}
			jQuery('.label-quantity-bw').html(bw_pages);
		}
		if (book_type == 1 && jQuery('.numbers-list .quantity-cl').length) {
			if (jQuery('.numbers-list .quantity-cl').hasClass('quantity-list')) {
				jQuery('.numbers-list .quantity-cl option').removeAttr('selected');
				jQuery('.numbers-list .quantity-cl option[value="'+cl_pages+'"]').prop('selected', true);
			} else {
				jQuery('.numbers-list .quantity-cl').val(cl_pages);
			}
			jQuery('.label-quantity-cl').html(cl_pages);
		}
	}
	function aec_uploader_message() {
		if (platform) {
			var bname = platform.name;
			if (bname == 'IE') {
				var bver = parseFloat(platform.version);
				if (bver < 12) {
					return 'ie';
				}
			} else if (bname == 'Firefox' || bname == 'Chrome') {
				var osbit = platform.os.architecture + '';
				if (platform.os.family == 'OS X') { osbit = '64'; }
				if (osbit == '32') {
					return 32;
				}
			}
		}
		return 0;
	}
	function adetect_size_label() {
		if (jQuery('.print-attributes .smatrix-size').length) {
			jQuery('.print-attributes .label-size').html(jQuery('.print-attributes .smatrix-size option:selected').html());
		}
	}
	adetect_size_label();
	//--></script>
	<?php do_action('print_products_upload_artwork_script'); ?>
