<?php
global $wpdb, $current_user;
$orders_per_page = 20;
$print_products_prodview_options = get_option("print_products_prodview_options");
$oistatuses = print_products_oistatus_get_list();
$approval_statuses = print_products_get_approval_statuses();
$oiemployees = print_products_vendor_get_vendors();
$oivendors = print_products_vendor_get_vendors_array();

$print_products_oistatus_options = get_option("print_products_oistatus_options");
$ois_list = array();
if (isset($print_products_oistatus_options['list']) && is_array($print_products_oistatus_options['list'])) {
	$ois_list = $print_products_oistatus_options['list'];
}

$pvpage = 1;
if (isset($_GET['pvpage'])) { $pvpage = (int)$_GET['pvpage']; }

$stransit = '';
if (isset($_GET['_vendor_company']) && $_GET['_vendor_company']) {
	$stransit .= '&_vendor_company='.$_GET['_vendor_company'];
}
if (isset($_GET['_vendor_employee']) && $_GET['_vendor_employee']) {
	$stransit .= '&_vendor_employee='.$_GET['_vendor_employee'];
}
if (isset($_GET['s']) && strlen($_GET['s'])) {
	$stransit .= '&s='.$_GET['s'];
}
$order = 'DESC';
$v_order = 'ASC';
if (isset($_GET['order']) && strlen($_GET['order'])) {
	if ($_GET['order'] == 'ASC') { $v_order = 'DESC'; }
	$order = $_GET['order'];
	$stransit .= '&order='.$_GET['order'];
}

$wc_orders = print_products_production_view_get_orders($order, $pvpage, $orders_per_page);

$wc_orders_total = $wpdb->get_var("SELECT FOUND_ROWS()");
$wc_orders_total_pages = ceil($wc_orders_total / $orders_per_page);
$colspan = 5;
if (print_products_vendor_show_prodview_ccompany_column()) { $colspan++; }
if (print_products_create_order_show_on_orders()) { $colspan++; }
if (print_products_vendor_show_prodview_employee_column()) { $colspan++; }
if (print_products_vendor_show_prodview_vendor_column()) { $colspan++; }
if (print_products_oirsdate_show_prodview_shipdate_column()) { $colspan++; }
?>
<style>.wp2print-production-view .min100{min-width:100px;}</style>
<div class="wrap wp2print-production-view">
	<h2><?php _e('Production View', 'wp2print'); ?></h2>
	<form class="sqh-search-form">
		<?php if (isset($_GET['post_type'])) { ?><input type="hidden" name="post_type" value="<?php echo $_GET['post_type']; ?>"><?php } ?>
		<input type="hidden" name="page" value="print-products-production-view">
		<p>
			<input id="post-search-input" type="text" name="s" value="<?php if (isset($_GET['s'])) { echo $_GET['s']; } ?>" style="vertical-align:middle;">
			<?php if (!in_array('vendor', $current_user->roles)) { ?>
				<?php print_products_vendor_filter_vendor_dropdown(); ?>
				<?php print_products_vendor_filter_employee_dropdown(); ?>
			<?php } ?>
			<input id="search-submit" type="submit" class="button" value="<?php _e('Search', 'wp2print'); ?>" style="vertical-align:middle;">
		</p>
	</form>
	<table class="wp-list-table widefat" width="100%">
		<thead>
			<tr>
				<th colspan="<?php echo $colspan; ?>">&nbsp;</th>
				<th colspan="<?php echo count($ois_list); ?>" style="text-align:center;"><?php _e('Production Status', 'wp2print'); ?></th>
				<th>&nbsp;</th>
			</tr>
			<tr>
				<th class="nwrap"><a href="admin.php?page=print-products-production-view&order=<?php echo $v_order; ?>" class="s-order"><?php _e('OrderID', 'wp2print'); ?></a></th>
				<th class="min100"><?php _e('Date', 'wp2print'); ?></th>
				<th><?php _e('Status', 'wp2print'); ?></th>
				<?php if (print_products_vendor_show_prodview_ccompany_column()) { ?>
					<th class="min100"><?php _e('Customer Company', 'wp2print'); ?></th>
				<?php } ?>
				<th style="text-align:center;"><span class="icon-approval"></span></th>
				<?php if (print_products_create_order_show_on_orders()) { ?>
					<th title="<?php _e('File collection email', 'wp2print'); ?>"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/icon-disk.png" title="<?php _e('File collection email', 'wp2print'); ?>" class="icon-disk"></th>
				<?php } ?>
				<th class="min100" style="min-width:120px;"><?php _e('Item', 'wp2print'); ?></th>
				<?php if (print_products_vendor_show_prodview_employee_column()) { ?>
					<th class="min100"><?php _e('Employee', 'wp2print'); ?></th>
				<?php } ?>
				<?php if (print_products_vendor_show_prodview_vendor_column()) { ?>
					<th class="min100"><?php _e('Vendor', 'wp2print'); ?></th>
				<?php } ?>
				<?php if (print_products_oirsdate_show_prodview_shipdate_column()) { ?>
					<th><?php _e('Required ship date', 'wp2print'); ?></th>
				<?php } ?>
				<?php foreach($ois_list as $ois_data) { ?>
					<th style="text-align:center;width:7%;"><?php echo $ois_data['name']; ?></th>
				<?php } ?>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php if ($wc_orders) { ?>
				<?php foreach($wc_orders as $wc_order) {
					$order_id = $wc_order->order_id;
					$order = wc_get_order($order_id);
					if ($order->get_type() == 'shop_order') {
						$order_date = $order->get_date_created();
						$order_items = $order->get_items();
						$order_company = $order->get_shipping_company();
						if (!strlen($order_company)) { $order_company = $order->get_billing_company(); }
						foreach($order_items as $item_id => $order_item) {
							$item_status = wc_get_order_item_meta($item_id, '_item_status', true);
							?>
							<tr class="ois-order-<?php echo $order_id; ?>">
								<td><a href="<?php echo print_products_woocommerce_get_order_edit_url($order_id); ?>"><?php echo $order_id; ?></a></td>
								<td><?php echo wc_format_datetime($order_date); ?></td>
								<td class="o-status"><?php echo wc_get_order_status_name($order->get_status()); ?></td>
								<?php if (print_products_vendor_show_prodview_ccompany_column()) { ?>
									<td class="o-ccompany"><?php echo $order_company; ?></td>
								<?php } ?>
								<td class="column-approval">
									<?php print_products_oistatus_approval_status($item_id, $approval_statuses); ?>
								</td>
								<?php if (print_products_create_order_show_on_orders()) {
									$co_add_files = $order->get_meta('_co_add_files', true); ?>
									<td class="column-fce"><?php if (strlen($co_add_files)) { ?><mark class="<?php if ($co_add_files == 'added') { echo 'approved'; } else { echo 'awaiting'; } ?>"></mark><?php } ?></td>
								<?php } ?>
								<td>
									<?php echo $order_item->get_name(); ?>
								</td>
								<?php if (print_products_vendor_show_prodview_employee_column()) { ?>
									<td class="column-employee">
										<?php $item_vendor_employee = (int)wc_get_order_item_meta($item_id, '_item_vendor_employee', true); ?>
										<?php if (isset($oiemployees[$item_vendor_employee])) { echo $oiemployees[$item_vendor_employee]; } else { echo '&nbsp;'; } ?>
									</td>
								<?php } ?>
								<?php if (print_products_vendor_show_prodview_vendor_column()) { ?>
									<td class="column-vendor">
										<?php $item_vendor = (int)wc_get_order_item_meta($item_id, '_item_vendor', true); ?>
										<?php if (isset($oivendors[$item_vendor])) { echo $oivendors[$item_vendor]; } ?>
									</td>
								<?php } ?>
								<?php if (print_products_oirsdate_show_prodview_shipdate_column()) { ?>
									<td class="column-shipdate">
										<?php
										$item_rsdate = wc_get_order_item_meta($item_id, '_item_rsdate', true);
										$rsd_bgcolour = '';
										$rsd_color = '';
										if (strlen($item_rsdate)) {
											$rsd_bgcolour = print_products_oirsdate_get_time_colour($item_rsdate);
											$arsdate = explode(' ', $item_rsdate);
											if (count($arsdate) > 2) {
												$item_rsdate = $arsdate[0].'<br>'.$arsdate[1].' '.$arsdate[2];
											} else {
												$item_rsdate = implode('<br>', $arsdate);
											}
											if (strlen($rsd_bgcolour)) {
												if (!wc_hex_is_light($rsd_bgcolour)) {
													$rsd_color = '#FFFFFF';
												}
											}
										}
										?>
										<div style="padding:2px 5px;<?php if (strlen($rsd_bgcolour)) { echo 'background:'.$rsd_bgcolour.';'; if (strlen($rsd_color)) { echo 'color:'.$rsd_color; } } ?>"><?php echo $item_rsdate; ?></div>
									</td>
								<?php } ?>
								<?php foreach($ois_list as $ois_data) {
									$oiskey = print_products_oistatus_get_slug($ois_data['name']);
									$class = '';
									if ($oiskey == $item_status) { $class = ' active'; } ?>
									<td class="ois-graph ois-graph-<?php echo $item_id; ?>"><div class="ois ois-<?php echo $oiskey; ?><?php echo $class; ?>" style="background:<?php echo $ois_data['color']; ?>;"></div></td>
								<?php } ?>
								<td>
									<?php if (print_products_vendor_allow_for_item($item_id)) { ?>
										<select name="ois" class="ois-ldd-<?php echo $item_id; ?>" onchange="wp2print_ois_change(<?php echo $order_id; ?>, <?php echo $item_id; ?>, 'pview')">
											<option value="">-- <?php _e('Status', 'wp2print'); ?> --</option>
											<?php foreach($oistatuses as $ois_key => $ois_val) { ?>
												<option value="<?php echo $ois_key; ?>"<?php if ($ois_key == $item_status) { echo ' SELECTED'; } ?>><?php echo $ois_val; ?></option>
											<?php } ?>
										</select>
									<?php } else { ?>
										&nbsp;
									<?php } ?><br>
									<div class="ois-success"><?php _e('Updated.', 'wp2print'); ?></div>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td colspan="<?php echo count($ois_list) + 7; ?>"><?php _e('No orders found.', 'wp2print'); ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?php if ($wc_orders_total_pages > 1) { ?>
		<div class="tablenav bottom">
			<div class="tablenav-pages">
				<span class="displaying-num"><?php echo $wc_orders_total; ?> <?php _e('items', 'wp2print'); ?></span>

				<?php if ($pvpage > 1) { ?>
					<a href="admin.php?page=print-products-production-view&pvpage=<?php echo ($pvpage - 1); ?><?php echo $stransit; ?>" class="prev-page button"><span aria-hidden="true">&lsaquo;</span></a>
				<?php } else { ?>
					<span aria-hidden="true" class="tablenav-pages-navspan button disabled">&lsaquo;</span>
				<?php } ?>
				<span class="paging-input" id="table-paging"><span class="tablenav-paging-text"><?php echo $pvpage; ?> <?php _e('of', 'wp2print'); ?> <span class="total-pages"><?php echo $wc_orders_total_pages; ?></span></span></span>
				<?php if (($pvpage + 1) <= $wc_orders_total_pages) { ?>
					<a href="admin.php?page=print-products-production-view&pvpage=<?php echo ($pvpage + 1); ?><?php echo $stransit; ?>" class="next-page button"><span aria-hidden="true">&rsaquo;</span></a>
				<?php } else { ?>
					<span aria-hidden="true" class="tablenav-pages-navspan button disabled">&rsaquo;</span>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
</div>
