<?php
global $wpdb;
$qo_per_page = 20;
$qo_page = 1;
if (isset($_GET['qopage'])) { $qo_page = (int)$_GET['qopage']; }

$qo_start = ($qo_page - 1) * $qo_per_page;

$where = '';
$stransit = '';
if (isset($_GET['s']) && strlen(trim($_GET['s']))) {
	$s = trim($_GET['s']);
	$where = " WHERE u.display_name LIKE '%".$s."%' OR u.user_email LIKE '%".$s."%'";

	$order_ids = array();
	$quote_orders_items = $wpdb->get_results(sprintf("SELECT qi.* FROM %sprint_products_quotes_items qi LEFT JOIN %sposts p ON p.ID = qi.product_id WHERE p.post_title LIKE '".$s."' OR p.post_content LIKE '".$s."'", $wpdb->prefix, $wpdb->prefix, '%'.$s.'%', '%'.$s.'%'));
	if ($quote_orders_items) {
		foreach($quote_orders_items as $quote_orders_item) {
			if (!in_array($quote_orders_item->order_id, $order_ids)) {
				$order_ids[] = $quote_orders_item->order_id;
			}
		}
		$where .= " OR q.order_id IN (".implode(',', $order_ids).")";
	}
	$stransit = 's='.$s.'&';
}
$quote_orders = $wpdb->get_results(sprintf("SELECT SQL_CALC_FOUND_ROWS q.*, u.display_name FROM %sprint_products_quotes q LEFT JOIN %s u ON u.ID = q.user_id %s ORDER BY q.order_id DESC LIMIT %s, %s", $wpdb->prefix, $wpdb->users, $where, $qo_start, $qo_per_page));
$quote_orders_total = $wpdb->get_var("SELECT FOUND_ROWS()");
$quote_orders_pages = 1;
if ($quote_orders_total) {
	$quote_orders_pages = ceil($quote_orders_total / $qo_per_page);
}
?>
<style>.wp2print-sq-history table td { vertical-align:middle; }</style>
<?php if (isset($_GET['resent'])) { ?>
	<div class="updated notice is-dismissible">
		<p><?php _e('Quote email was successfully sent.', 'wp2print'); ?></p>
	</div>
<?php } else if (isset($_GET['converted'])) { $wcoid = $_GET['wcoid']; ?>
	<div class="updated notice is-dismissible">
		<p><?php _e('Quote was successfully converted to order', 'wp2print'); ?> <?php echo '<a href="'.print_products_woocommerce_get_order_edit_url($wcoid).'">'.$wcoid.'</a>'; ?>.</p>
	</div>
<?php } ?>
<div class="wrap wp2print-create-order wp2print-sq-history">
	<h2><?php _e('Send Quote history', 'wp2print'); ?></h2>
	<?php if (isset($_GET['view']) && $_GET['view']) {
		$order_id = $_GET['view'];
		$order = print_products_send_quote_get_order($order_id);
		$customer_id = $order->user_id;
		$customer_data = get_userdata($customer_id);
		$order_items = print_products_send_quote_get_order_items($order_id);
		$img = 'icon-no.svg'; if ($order->status == 1) { $img = 'icon-yes.svg'; }
		?>
		<div class="create-order-wrap">
			<div class="sq-order">
				<div class="sq-o-line"><?php _e('Customer', 'wp2print'); ?>: <strong><?php echo $customer_data->display_name; ?> (<?php echo $customer_data->user_email; ?>)</strong></div>
				<?php if (strlen($order->reference)) { ?>
					<div class="sq-o-line"><?php _e('Reference', 'wp2print'); ?>: <strong><?php echo $order->reference; ?></strong></div>
				<?php } ?>
				<div class="sq-o-line">
					<table cellspacing="0" cellpadding="0">
						<tr>
							<td><?php _e('Product', 'wp2print'); ?></td>
							<td><?php _e('Quantity', 'wp2print'); ?></td>
							<td><?php _e('Subtotal', 'wp2print'); ?></td>
						</tr>
						<?php if ($order_items) { $subtotal = 0; ?>
							<?php foreach ($order_items as $order_item) {
								$product_id = $order_item->product_id;
								$product = wc_get_product($product_id);
								$quantity = $order_item->quantity;
								$product_price = $order_item->price;
								$additional = unserialize($order_item->additional);

								$product_name = $product->get_name();
								if (print_products_is_custom_product($product_id) && isset($additional['cptype']) && strlen($additional['cptype'])) {
									$product_name = $additional['cptype'];
								}
								if ($product_price) { $subtotal = $subtotal + $product_price; }
								?>
								<tr>
									<td><strong><?php echo $product_name; ?></strong><?php print_products_send_quote_product_data_html($order_item); ?></td>
									<td><?php echo $quantity; ?></td>
									<td><?php echo wc_price($product_price); ?></td>
								</tr>
							<?php } ?>
						<?php } ?>
						<tr>
							<td colspan="2">&nbsp;</td>
							<td><?php echo wc_price($subtotal); ?></td>
						</tr>
						<?php if ($order->discount) { $discount = (float)$order->discount; ?>
						<tr>
							<td colspan="2" style="font-weight:700;"><?php _e('Discount', 'wp2print'); ?>:</td>
							<td style="font-weight:700;"><?php echo wc_price($discount); ?></td>
						</tr>
						<tr>
							<td colspan="2" style="font-weight:700;"><?php _e('Total', 'wp2print'); ?>:</td>
							<td style="font-weight:700;"><?php echo wc_price($subtotal - $discount); ?></td>
						</tr>
						<?php } ?>
					</table>
				</div>
				<div class="sq-o-line"><?php _e('Converted', 'wp2print'); ?>: <img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL . 'images/' . $img; ?>" alt="" style="width:18px;margin-bottom:-4px;"></div>
				<?php if (strlen($order->expire_date)) { ?>
					<div class="sq-o-line"><?php _e('Expired', 'wp2print'); ?>: <strong><?php echo $order->expire_date; ?></strong></div>
				<?php } ?>
				<div class="sq-o-line"><?php _e('Created', 'wp2print'); ?>: <strong><?php echo $order->created; ?></strong></div>
			</div>
		</div>
		<a href="admin.php?page=print-products-send-quote-history" class="button" style="margin-top:10px;"><?php _e('Back', 'wp2print'); ?></a>
	<?php } else { ?>
		<form class="sqh-search-form">
			<input type="hidden" name="page" value="print-products-send-quote-history">
			<p>
				<input id="post-search-input" type="text" name="s" value="<?php if (isset($_GET['s'])) { echo $_GET['s']; } ?>">
				<input id="search-submit" type="submit" class="button" value="<?php _e('Search', 'wp2print'); ?>">
			</p>
		</form>
		<table class="wp-list-table widefat" width="100%">
			<thead>
				<tr>
					<th><?php _e('ID', 'wp2print'); ?></th>
					<th><?php _e('Customer', 'wp2print'); ?></th>
					<th><?php _e('Product', 'wp2print'); ?></th>
					<th style="text-align:center;"><?php _e('Qty', 'wp2print'); ?></th>
					<th><?php _e('Price', 'wp2print'); ?></th>
					<th style="text-align:center; width:140px;"><?php _e('Converted', 'wp2print'); ?></th>
					<th><?php _e('Convert to order', 'wp2print'); ?></th>
					<th><?php _e('Quote email', 'wp2print'); ?></th>
					<th><?php _e('Created', 'wp2print'); ?></th>
					<th><?php _e('Expired', 'wp2print'); ?></th>
					<th>&nbsp;</th>
				</tr>
				<?php if ($quote_orders) { ?>
					<?php foreach ($quote_orders as $quote_order) {
						$user_id = $quote_order->user_id;
						$subtotal = 0;
						$shipp_cost = '';
						$tax_rate = 0;
						$qitems = array();
						$quote_order_items = $wpdb->get_results(sprintf("SELECT qi.*, p.post_title FROM %sprint_products_quotes_items qi LEFT JOIN %sposts p ON p.ID = qi.product_id WHERE qi.order_id = %s", $wpdb->prefix, $wpdb->prefix, $quote_order->order_id));
						if ($quote_order_items) {
							foreach($quote_order_items as $quote_order_item) {
								$product_id = $quote_order_item->product_id;

								if (!$tax_rate) {
									$tax_rate = print_products_send_quote_get_tax_rate($product_id, $user_id);
								}

								if ($quote_order_item->additional) {
									$additional = unserialize($quote_order_item->additional);
									if (!$shipp_cost && isset($additional['shipping_cost']) && $additional['shipping_cost']) {
										$shipp_cost = $additional['shipping_cost'];
									}
								}
								$qitems['name'][] = $quote_order_item->post_title;
								$qitems['quantity'][] = $quote_order_item->quantity;
								$qitems['price'][] = wc_price($quote_order_item->price);
								$subtotal = $subtotal + $quote_order_item->price;
							}
						}
						?>
						<tr>
							<td><?php echo $quote_order->order_id; ?></td>
							<td><?php echo $quote_order->display_name; ?></td>
							<td><?php if (isset($qitems['name'])) { echo implode('<br>', $qitems['name']); } ?></td>
							<td style="text-align:center;"><?php if (isset($qitems['name'])) { echo implode('<br>', $qitems['quantity']); } ?></td>
							<td><?php if (isset($qitems['name'])) { echo implode('<br>', $qitems['price']); } ?></td>
							<td style="text-align:center;position:relative;"><?php $img = 'icon-no.svg'; if ($quote_order->status == 1) { $img = 'icon-yes.svg'; } ?><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL . 'images/' . $img; ?>" alt="" style="width:18px;margin-top:2px;"><?php if ($quote_order->status == 1 && $quote_order->wc_order_id) { $wc_order = wc_get_order($quote_order->wc_order_id); if ($wc_order) { echo ' <div class="sq-wcoid"><a href="'.print_products_woocommerce_get_order_edit_url($quote_order->wc_order_id).'">'.$quote_order->wc_order_id.'</a></div>'; }} ?></td>
							<td><?php if (!$quote_order->wc_order_id) { ?><a href="#convert" class="button sqh-convert-to-order" data-oid="<?php echo $quote_order->order_id; ?>" data-subtotal="<?php echo $subtotal; ?>" data-shipping="<?php echo $shipp_cost; ?>" data-tax-rate="<?php echo $tax_rate; ?>"><?php _e('Convert to order', 'wp2print'); ?></a><?php } ?></td>
							<td><?php if ($quote_order->status != 1) { ?><a href="#resend" class="button sqh-resend-email" data-oid="<?php echo $quote_order->order_id; ?>"><?php _e('Not sent yet', 'wp2print'); ?></a><?php } ?></td>
							<td><?php echo date('Y-m-d', strtotime($quote_order->created)); ?></td>
							<td><?php if (strlen($quote_order->expire_date)) { echo $quote_order->expire_date; } else { echo '-'; } ?></td>
							<td><a href="admin.php?page=print-products-send-quote-history&view=<?php echo $quote_order->order_id; ?>" class="fai-view" title="<?php _e('View', 'wp2print'); ?>"><?php _e('View', 'wp2print'); ?></a> <a href="#duplicate" class="fai-duplicate sqh-duplicate" data-oid="<?php echo $quote_order->order_id; ?>" title="<?php _e('Duplicate', 'wp2print'); ?>"><?php _e('Duplicate', 'wp2print'); ?></a><?php if ($quote_order->saved == 1) { ?> <a href="#edit" class="fai-envelope sqh-edit" data-oid="<?php echo $quote_order->order_id; ?>" title="<?php _e('Edit', 'wp2print'); ?>"><?php } ?></td>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td colspan="11"><?php _e('No quote orders.', 'wp2print'); ?></td>
					</tr>
				<?php } ?>
			</thead>
			<tbody>
			</tbody>
		</table>
		<?php if ($quote_orders_pages > 1) { ?>
			<div class="tablenav bottom">
				<div class="tablenav-pages">
					<span class="displaying-num"><?php echo $quote_orders_total; ?> <?php _e('items', 'wp2print'); ?></span>

					<?php if ($qo_page > 1) { ?>
						<a href="admin.php?page=print-products-send-quote-history&<?php echo $stransit; ?>qopage=<?php echo ($qo_page - 1); ?>" class="prev-page button"><span aria-hidden="true">&lsaquo;</span></a>
					<?php } else { ?>
						<span aria-hidden="true" class="tablenav-pages-navspan button disabled">&lsaquo;</span>
					<?php } ?>
					<span class="paging-input" id="table-paging"><span class="tablenav-paging-text"><?php echo $qo_page; ?> <?php _e('of', 'wp2print'); ?> <span class="total-pages"><?php echo $quote_orders_pages; ?></span></span></span>
					<?php if (($qo_page + 1) <= $quote_orders_pages) { ?>
						<a href="admin.php?page=print-products-send-quote-history&<?php echo $stransit; ?>qopage=<?php echo ($qo_page + 1); ?>" class="next-page button"><span aria-hidden="true">&rsaquo;</span></a>
					<?php } else { ?>
						<span aria-hidden="true" class="tablenav-pages-navspan button disabled">&rsaquo;</span>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
		<div style="display:none;">
			<form method="POST" class="sqh-form">
			<input type="hidden" name="sqh_action" class="sqh-action">
			<input type="hidden" name="order_id" class="sqh-order-id">
			</form>
		</div>
		<div style="display:none;">
			<div id="sqh-convert-popup" style="padding:30px;height:200px;">
				<h3 style="margin-top:0;"><?php _e('Order shipping & tax', 'wp2print'); ?></h3>
				<form method="POST" class="sqh-convert-form">
				<input type="hidden" name="sqh_action" value="convert">
				<input type="hidden" name="order_id" class="sqh-order-id">
				<table>
					<tr>
						<td style="width:80px;"><?php _e('Shipping', 'wp2print'); ?>:</td>
						<td><input type="text" name="shipping" class="sqh-shipping"></td>
					</tr>
					<tr>
						<td><?php _e('Apply Tax', 'wp2print'); ?>:</td>
						<td><select name="apply_tax" onchange="wp2print_sqh_apply_tax(this)">
							<option value=""><?php _e('Do not apply sales tax', 'wp2print'); ?></option>
							<option value="products"><?php _e('Apply sales tax to products only', 'wp2print'); ?></option>
							<option value="shipping"><?php _e('Apply sales tax to shipping only', 'wp2print'); ?></option>
							<option value="both"><?php _e('Apply sales tax to products and shipping', 'wp2print'); ?></option>
						</select></td>
					</tr>
					<tr class="sqh-convert-tax-field" style="display:none;">
						<td><?php _e('Tax', 'wp2print'); ?>:</td>
						<td><input type="text" name="tax" class="sqh-tax"></td>
					</tr>
					<tr class="sqh-convert-shipping-tax-field" style="display:none;">
						<td nowrap><?php _e('Shipping Tax', 'wp2print'); ?>:</td>
						<td><input type="text" name="shipping_tax" class="sqh-shipping-tax"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" value="<?php _e('Submit', 'wp2print'); ?>" class="button button-primary"></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
	<?php } ?>
</div>