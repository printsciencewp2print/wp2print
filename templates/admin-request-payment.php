<?php
$step = isset($_GET['step']) ? $_GET['step'] : 1;
$request_payment_data = print_products_request_payment_get_order_data();
if ($step != '1' && $step != 'completed' && empty($request_payment_data)) {
	wp_redirect('admin.php?page=print-products-request-payment');
	exit;
}
$print_products_request_payment_options = (array)get_option("print_products_request_payment_options");
$rapid_user_search = print_products_is_rapid_user_search();
?>
<div class="wrap wp2print-create-order">
	<h2><?php _e('Request payment', 'wp2print'); ?></h2>
	<?php if (isset($request_payment_data['product_id']) && !$request_payment_data['product_id']) { ?>
		<p class="form-field"><?php _e('Please select product for payments.', 'wp2print'); ?></p>
	<?php return; } ?>
	<div style="float:right; margin:-35px 5px 0 0;"><a href="<?php if (current_user_can('manage_options')) { echo 'admin.php?'; } else { echo 'edit.php?post_type=shop_order&'; } ?>page=print-products-request-payment-history" class="button"><?php _e('Request payment history', 'wp2print'); ?></a></div>
	<form method="POST" action="admin.php?page=print-products-request-payment&step=<?php if ($step != 'completed') { echo $step + 1; } ?>" class="send-quote-form" onsubmit="return request_payment_process(<?php echo $step; ?>);" data-error-required="<?php _e('Please fill required field(s).', 'wp2print'); ?>" data-error-sure="<?php _e('Are you sure?', 'wp2print'); ?>">
		<input type="hidden" name="print_products_request_payment_action" value="process">
		<div class="create-order-wrap">
			<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
			<?php //////////////////////////////////////////////// STEP 1 /////////////////////////////////////////////////////////// ?>
			<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
			<?php if ($step == '1') { ?>
				<input type="hidden" name="process_step" value="1">
				<div class="co-step-title"><?php _e('Step', 'wp2print'); ?> 1: <?php _e('Select customer', 'wp2print'); ?></div>
				<?php $adminusers = get_users(array('role__in' => array('administrator', 'sales'))); ?>
				<p class="form-field">
					<label><?php _e('Sender', 'wp2print'); ?>:</label>
					<select name="order_sender" class="order-sender">
						<option value="">-- <?php _e('Select', 'wp2print'); ?> --</option>
						<?php foreach($adminusers as $adminuser) { ?>
							<option value="<?php echo $adminuser->user_email; ?>"<?php if (isset($request_payment_data['sender']) && $adminuser->user_email == $request_payment_data['sender']) { echo ' SELECTED'; } ?>><?php echo $adminuser->display_name; ?> (<?php echo $adminuser->user_email; ?>)</option>
						<?php } ?>
					</select>
				</p>
				<p class="form-field">
					<label><?php _e('Customer', 'wp2print'); ?>: <span class="req">*</span></label>
					<select name="order_customer" class="order-customer">
						<option value="">-- <?php _e('Select', 'wp2print'); ?> --</option>
						<?php if ($rapid_user_search == 1) { ?>
							<?php if (isset($request_payment_data['customer']) && $request_payment_data['customer']) {
								$userdata = get_userdata($send_quote_data['customer']);
								if ($userdata) { ?>
									<option value="<?php echo $userdata->ID; ?>" selected="selected"><?php echo $userdata->display_name; ?> (<?php _e('Email', 'wp2print'); ?>: <?php echo $userdata->user_email; ?>)</option>
								<?php } ?>
							<?php } ?>
						<?php } else { ?>
							<?php $wpusers = get_users(array('orderby' => 'display_name', 'order' => 'asc')); ?>
							<?php foreach($wpusers as $wpuser) {
								$first_name = get_user_meta($wpuser->ID, 'first_name', true);
								$last_name = get_user_meta($wpuser->ID, 'last_name', true);
								$billing_company = get_user_meta($wpuser->ID, 'billing_company', true);
								$user_email = get_user_meta($wpuser->ID, 'billing_email', true);
								if (!strlen($user_email)) { $user_email = $wpuser->user_email; }
								$name = $wpuser->display_name;
								if (strlen($first_name)) {
									$name = $first_name.' '.$last_name;
								}
								$company = '';
								if (strlen($billing_company)) {
									$company = '; '.__('Company', 'wp2print').': '.$billing_company;
								} ?>
								<option value="<?php echo $wpuser->ID; ?>"<?php if (isset($request_payment_data['customer']) && $wpuser->ID == $request_payment_data['customer']) { echo ' SELECTED'; } ?>><?php echo $name; ?> (<?php _e('Email', 'wp2print'); ?>: <?php echo $user_email; ?><?php echo $company; ?>)</option>
							<?php } ?>
						<?php } ?>
					</select>
					<script>
					<!--
					jQuery(document).ready(function() {
					<?php if ($rapid_user_search == 1) { ?>
						jQuery('.send-quote-form .order-customer').select2({ajax: {url:'<?php echo site_url('/?AjaxAction=request-payment-select-user'); ?>', dataType: 'json'}});
					<?php } else { ?>
						jQuery('.send-quote-form .order-customer').select2();
					<?php } ?>
					});
					//--></script>
				</p>
				<p class="form-field">
					<input type="button" value="<?php _e('Create new user account', 'wp2print'); ?>" class="button sq-add-user-btn">
				</p>
				<p class="submit"><input type="submit" value="<?php _e('Continue', 'wp2print'); ?>" class="button button-primary"></p>
			<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
			<?php //////////////////////////////////////////////// STEP 2 /////////////////////////////////////////////////////////// ?>
			<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
			<?php } else if ($step == '2') {
				$product_id = (int)$request_payment_data['product_id'];
				$product_name = get_the_title($product_id);
				$paybill_options = get_post_meta($product_id, '_paybill_options', true);
				if (!is_array($paybill_options)) { $paybill_options = array('invoice-number-label' => __('Invoice number', 'wp2print'), 'amount-label' => __('Amount', 'wp2print')); }
				if (isset($request_payment_data['order_id']) && $request_payment_data['order_id']) { $product_name = __('Extra payment for Order', 'wp2print').' '.$request_payment_data['order_id']; }
				?>
				<input type="hidden" name="process_step" value="2">
				<div class="co-step-title"><?php _e('Step', 'wp2print'); ?> 2: <?php _e('Product settings', 'wp2print'); ?></div>
				<p class="form-field">
					<label><?php _e('Product', 'wp2print'); ?>: <span><?php echo $product_name; ?></span></label>
				</p>
				<p class="form-field">
					<label><?php echo $paybill_options['invoice-number-label']; ?>: </label>
					<input type="text" name="order_invoice_number" value="<?php if (isset($request_payment_data['invoice_number'])) { echo $request_payment_data['invoice_number']; } ?>">
					<input type="hidden" name="order_invoice_number_label" value="<?php echo $paybill_options['invoice-number-label']; ?>">
				</p>
				<p class="form-field">
					<label><?php echo $paybill_options['amount-label']; ?>: </label>
					<input type="text" name="order_amount" value="<?php if (isset($request_payment_data['amount'])) { echo $request_payment_data['amount']; } ?>">
					<input type="hidden" name="order_amount_label" value="<?php echo $paybill_options['amount-label']; ?>">
				</p>
				<p class="form-field">
					<label><?php _e('Add sales taxes', 'wp2print'); ?>:</label>
					<select name="tax">
						<option value="0"><?php _e('No', 'wp2print'); ?></option>
						<option value="1"<?php if (isset($request_payment_data['tax']) && $request_payment_data['tax'] == 1) { echo ' SELECTED'; } ?>><?php _e('Yes', 'wp2print'); ?></option>
					</select>
				</p>
				<?php include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-request-payment-upload.php'; ?>
				<p class="submit" style="border-top:1px solid #C1C1C1;padding-top:20px;">
					<input type="button" value="<?php _e('Back', 'wp2print'); ?>" class="button" onclick="window.location.href='admin.php?page=print-products-request-payment&step=1';">
					<input type="submit" value="<?php _e('Continue', 'wp2print'); ?>" class="button button-primary">
				</p>
			<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
			<?php //////////////////////////////////////////////// STEP 3 /////////////////////////////////////////////////////////// ?>
			<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
			<?php } else if ($step == '3') {
				$customer_data = get_userdata($request_payment_data['customer']);
				$product_id = $request_payment_data['product_id'];
				$product_name = get_the_title($product_id);
				$excl_suffix = get_option('woocommerce_price_display_excl_suffix');
				if (isset($request_payment_data['order_id']) && $request_payment_data['order_id']) { $product_name = __('Extra payment for Order', 'wp2print').' '.$request_payment_data['order_id']; }
				?>
				<input type="hidden" name="process_step" value="send">
				<div class="co-step-title"><?php _e('Step', 'wp2print'); ?> 3: <?php _e('Quote Confirmation', 'wp2print'); ?></div>
				<div class="co-confirmation">
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td class="co-head"><?php _e('Customer', 'wp2print'); ?>:</td>
							<td class="co-value"><span class="co-edit"><a href="admin.php?page=print-products-request-payment&step=1"><?php _e('edit', 'wp2print'); ?></a></span>
							<strong><?php echo $customer_data->display_name; ?> (<?php echo $customer_data->user_email; ?>)</strong></td>
						</tr>
						<tr>
							<td class="co-head"><?php _e('Product', 'wp2print'); ?>:</td>
							<td class="co-value"><strong><?php echo $product_name; ?></strong></td>
						</tr>
						<tr>
							<td class="co-head"><?php echo $request_payment_data['invoice_number_label']; ?>:</td>
							<td class="co-value"><span class="co-edit"><a href="admin.php?page=print-products-request-payment&step=2"><?php _e('edit', 'wp2print'); ?></a></span>
							<strong><?php echo $request_payment_data['invoice_number']; ?></strong></td>
						</tr>
						<tr>
							<td class="co-head"><?php echo $request_payment_data['amount_label']; ?><?php if ($excl_suffix) { echo ' ('.$excl_suffix.')'; } ?>:</td>
							<td class="co-value"><span class="co-edit"><a href="admin.php?page=print-products-request-payment&step=2"><?php _e('edit', 'wp2print'); ?></a></span>
							<strong><?php echo wc_price($request_payment_data['amount']); ?></strong></td>
						</tr>
						<?php if (isset($request_payment_data['tax']) && $request_payment_data['tax'] == 1) { ?>
						<tr>
							<td class="co-head"><?php _e('Add sales taxes', 'wp2print'); ?>:</td>
							<td class="co-value"><span class="co-edit"><a href="admin.php?page=print-products-request-payment&step=2"><?php _e('edit', 'wp2print'); ?></a></span>
							<strong><?php _e('Yes', 'wp2print'); ?></strong></td>
						</tr>
						<?php } ?>
						<?php if (strlen($request_payment_data['pdffile'])) { ?>
						<tr>
							<td class="co-head"><?php _e('PDF of invoice', 'wp2print'); ?>:</td>
							<td class="co-value"><span class="co-edit"><a href="admin.php?page=print-products-request-payment&step=2"><?php _e('edit', 'wp2print'); ?></a></span>
							<strong><?php echo '<a href="'.print_products_get_amazon_file_url($request_payment_data['pdffile']).'" target="_blank">'.basename($request_payment_data['pdffile']).'</a>'; ?></strong></td>
						</tr>
						<?php } ?>
						<tr>
							<td class="co-head" style="line-height:25px;"><?php _e('Email Options', 'wp2print'); ?>:</td>
							<td class="co-value" style="line-height:25px;">
								<input type="text" name="email_subject" value="<?php echo $print_products_request_payment_options['email_subject']; ?>" style="width:100%;">
								<textarea name="email_message" style="width:100%; height:200px;"><?php echo $print_products_request_payment_options['email_message']; ?></textarea>
							</td>
						</tr>
					</table>
				</div>
				<p class="submit" style="text-align:center;">
					<input type="submit" value="<?php _e('Send Request Payment', 'wp2print'); ?>" class="button button-primary button-create">
				</p>
			<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
			<?php //////////////////////////////////////////////// STEP COMPLETED /////////////////////////////////////////////////// ?>
			<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
			<?php } else if ($step == 'completed') {
				$order_id = $_GET['order'];
				$order = print_products_request_payment_get_order($order_id);
				$customer_id = $order->user_id;
				$product_id = $order->product_id;
				$product_name = get_the_title($product_id);
				$additional = unserialize($order->additional);
				$customer_data = get_userdata($customer_id);
				if (isset($additional['order_id']) && $additional['order_id']) { $product_name = __('Extra payment for Order', 'wp2print').' '.$additional['order_id']; }
				?>
				<h3 style="margin-top:0px;"><?php _e('Request payment was successfully sent.', 'wp2print'); ?></h3>
				<div class="co-order">
					<ul>
						<li><?php _e('Customer', 'wp2print'); ?>: <strong><?php echo $customer_data->display_name; ?> (<?php echo $customer_data->user_email; ?>)</strong></li>
						<li style="line-height:22px;"><?php _e('Product', 'wp2print'); ?>: <strong><?php echo $product_name; ?></strong></li>
						<li style="line-height:22px;"><?php _e('Invoice number', 'wp2print'); ?>: <strong><?php echo $additional['invoice_number']; ?></strong></li>
						<li style="line-height:22px;"><?php _e('Amount', 'wp2print'); ?>: <strong><?php echo wc_price($order->price); ?></strong></li>
						<?php if ($order->tax == 1) { ?><li style="line-height:22px;"><?php _e('Add sales taxes', 'wp2print'); ?>: <strong><?php _e('Yes', 'wp2print'); ?></strong></li><?php } ?>
						<?php if (strlen($order->pdffile)) { ?><li style="line-height:22px;"><?php _e('PDF of invoice', 'wp2print'); ?>: <strong><?php echo basename($order->pdffile); ?></strong></li><?php } ?>
					</ul>
				</div>
			<?php } ?>
			<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
		</div>
	</form>
</div>
<div style="display:none;">
	<div id="sq-add-user" class="sq-add-user">
		<h2><?php _e('Create new user account', 'wp2print'); ?></h2>
		<form method="POST" class="sq-add-user-form" onsubmit="return wp2print_sqau_submit();" data-required="<?php _e('Please fill required fields.', 'wp2print'); ?>">
			<table class="form-table">
				<tr>
					<th><label><?php _e('Username', 'wp2print'); ?>:</label> <span style="color:#FF0000;">*</span></th>
					<td><input type="text" name="sqau_username" class="sqau-username"></td>
				</tr>
				<tr>
					<th><label><?php _e('Email', 'wp2print'); ?>:</label> <span style="color:#FF0000;">*</span></th>
					<td><input type="email" name="sqau_email" class="sqau-email"></td>
				</tr>
				<tr>
					<th><label><?php _e('First Name', 'wp2print'); ?>:</label></th>
					<td><input type="text" name="sqau_fname" class="sqau-fname"></td>
				</tr>
				<tr>
					<th><label><?php _e('Last Name', 'wp2print'); ?>:</label></th>
					<td><input type="text" name="sqau_lname" class="sqau-lname"></td>
				</tr>
				<tr>
					<th><label><?php _e('Password', 'wp2print'); ?>:</label> <span style="color:#FF0000;">*</span></th>
					<td><input type="text" name="sqau_pass" class="sqau-pass" value="<?php echo wp_generate_password(24); ?>"></td>
				</tr>
			</table>
			<div class="sq-add-user-error"></div>
			<p class="submit">
				<input type="submit" value="<?php _e('Add New User', 'wp2print'); ?>" class="button button-primary">
			</p>
			<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="sq-add-user-loading">
		</form>
	</div>
</div>