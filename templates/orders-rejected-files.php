<?php
global $wp, $wpdb, $current_user;

$view_order_id = (isset($_GET['view']) ? $_GET['view'] : '');

$the_order = false;
if ($view_order_id) {
	$the_order = wc_get_order($view_order_id);
}
if ($the_order) { $afnum = 0; ?>
	<div class="wrap orders-rejected-files-wrap">
		<?php if ($the_order && $the_order->get_customer_id() == $current_user->ID) { ?>
			<form method="POST" action="<?php echo site_url('index.php'); ?>" class="orders-rejected-files-form">
			<div class="omf-success"></div>
			<table class="orf-main-table" border="0">
				<tr>
					<td colspan="4"><h4><?php _e('Order #', 'wp2print'); ?>: <?php echo $view_order_id; ?></h4></td>
				</tr>
				<tr style="background:#F4F4F4 !important;">
					<td><strong><?php _e('Item', 'wp2print'); ?></strong></td>
					<td><strong><?php _e('Rejected file', 'wp2print'); ?></strong></td>
					<td><strong><?php _e('Uploaded file', 'wp2print'); ?></strong></td>
					<td style="width:80px;"><strong><?php _e('Actions', 'wp2print'); ?></strong></td>
				</tr>
				<?php foreach ($the_order->get_items('line_item') as $item_id => $item) {
					$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s' AND atcaction = 'artwork'", $wpdb->prefix, $item_id));
					if ($order_item_data) {
						$artwork_rejected_files = $order_item_data->artwork_rejected;
						if ($artwork_rejected_files && strlen($artwork_rejected_files)) {
							$artwork_rejected_files = unserialize($artwork_rejected_files);
							$afnum = count($artwork_rejected_files);
							if ($afnum) { ?>
							<tr style="border-top:1px solid #C1C1C1;">
								<td valign="top"><span style="font-weight:700;"><?php echo $item['name']; ?></span>
									<?php print_products_product_attributes_list_html($order_item_data); ?>
								</td>
								<td valign="top" class="rf-line">
									<?php foreach ($artwork_rejected_files as $artwork_rejected_file) { $arfkey = md5($artwork_rejected_file); ?>
										<div><a href="<?php echo print_products_get_amazon_file_url($artwork_rejected_file); ?>" target="_blank"><?php echo basename($artwork_rejected_file); ?></a></div>
									<?php } ?>
								</td>
								<td valign="top" class="rf-line">
									<?php foreach ($artwork_rejected_files as $artwork_rejected_file) { $arfkey = md5($artwork_rejected_file); ?>
										<div class="files-list-<?php echo $item_id; ?>-<?php echo $arfkey; ?>" data-uploaded="">&nbsp;</div>
									<?php } ?>
								</td>
								<td valign="top" class="rf-line">
									<?php foreach ($artwork_rejected_files as $artwork_rejected_file) { $arfkey = md5($artwork_rejected_file); ?>
										<div style="white-space:nowrap;"><a href="#upload-file" class="woocommerce-button button view orf-upload-btn" rel="<?php echo $item_id; ?>-<?php echo $arfkey; ?>"><?php _e('Upload file', 'wp2print'); ?></a><input type="hidden" name="artworkfile[]" class="artwork-file-<?php echo $item_id; ?>-<?php echo $arfkey; ?>"></div>
									<?php } ?>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				<?php if ($afnum == 0) { ?>
					<tr>
						<td colspan="4"><?php _e('No rejected files', 'wp2print'); ?>.</td>
					</tr>
				<?php } ?>
			</table>
			<input type="hidden" name="order_id" value="<?php echo $view_order_id; ?>" class="order-id">
			</form>
			<?php include('orders-rejected-files-upload.php'); ?>
		<?php } else { ?>
			<p><?php _e("You aren't allowed to view this order.", 'wp2print'); ?></p>
		<?php } ?>
	</div>
	<?php
} else {
	$rejected_files_orders = false;
	$rejected_items = array();
	$user_orders = wc_get_orders(array('customer_id' => $current_user->ID));
	if ($user_orders) {
		foreach($user_orders as $user_order) {
			$order_id = $user_order->get_id();
			$order_items = $user_order->get_items('line_item');
			$rejected_files = false;
			if ($order_items) {
				foreach($order_items as $item_id => $order_item) {
					$pp_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = %s AND atcaction = 'artwork'", $wpdb->prefix, $item_id));
					if ($pp_item_data) {
						$afnum = 0;
						$artwork_rejected_files = $pp_item_data->artwork_rejected;
						if ($artwork_rejected_files && strlen($artwork_rejected_files)) {
							$artwork_rejected_files = unserialize($artwork_rejected_files);
							$afnum = count($artwork_rejected_files);
						}
						if ($afnum) {
							$rejected_files = true;
							if (isset($rejected_items[$order_id])) { $rejected_items[$order_id]++; } else { $rejected_items[$order_id] = 1; }
						}
					}
				}
			}
			if ($rejected_files) {
				$rejected_files_orders[] = $user_order;
			}
		}
	}
	?>
	<div class="wrap orders-awaiting-approval-wrap">
		<table class="woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
			<thead>
				<tr>
					<th scope="col" class="manage-column" style="width:60px;"><?php _e('Order', 'wp2print'); ?></th>
					<th scope="col" class="manage-column" style="text-align:center;"><?php _e('Items rejected files', 'wp2print'); ?></th>
					<th scope="col" class="manage-column"><?php _e('Date', 'wp2print'); ?></th>
					<th scope="col" class="manage-column" style="width:130px;"><?php _e('Actions', 'wp2print'); ?></th>
				</tr>
			</thead>
			<tbody id="the-list">
				<?php if ($rejected_files_orders) {
					foreach($rejected_files_orders as $rejected_files_order) {
						$order_id = $rejected_files_order->get_id();
						$order_date = $rejected_files_order->get_date_created(); ?>
						<tr>
							<td><a href="?view=<?php echo $order_id; ?>">#<?php echo $order_id; ?></a></td>
							<td style="text-align:center;"><?php echo $rejected_items[$order_id]; ?></td>
							<td><?php echo date('M j, Y', strtotime($order_date)); ?></td>
							<td><a href="?view=<?php echo $order_id; ?>" class="woocommerce-button button view"><?php _e('Add files', 'wp2print'); ?></a></td>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td colspan="4"><?php _e('No orders.', 'wp2print'); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
<?php } ?>