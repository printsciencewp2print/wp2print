<?php
$file_upload_max_size = get_option('print_products_file_upload_max_size');
$file_upload_target = get_option("print_products_file_upload_target");
$amazon_s3_settings = get_option("print_products_amazon_s3_settings");

if (!$file_upload_max_size) { $file_upload_max_size = 2; }

$upload_to = 'host';
$plupload_url = get_bloginfo('url').'/index.php?ajaxupload=artwork&sessid='.session_id();
if ($file_upload_target == 'amazon' && $amazon_s3_settings['s3_access_key'] && $amazon_s3_settings['s3_secret_key']) {
	$upload_to = 'amazon';

	$s3_data = print_products_amazon_s3_get_data($amazon_s3_settings, $file_upload_max_size);
	$s3path = $s3_data['s3path'];
	$is_s3_region = $s3_data['is_s3_region'];
	$plupload_url = $s3_data['amazon_url'];
	$amazon_file_url = $s3_data['amazon_file_url'];
	$multiparams = $s3_data['multiparams'];
	$ftypes = print_products_user_files_get_types();
}
?>
	<div id="upload-artwork" class="upload-artwork-block" style="border:1px solid #C1C1C1; padding:10px; background:#FFF;">
		<table>
		<tr>
		<td><?php _e('File type', 'wp2print'); ?>:</td>
		<td>
			<select name="ftype">
				<?php foreach($ftypes as $tkey => $tname) { ?>
					<option value="<?php echo $tkey; ?>"><?php echo $tname; ?></option>
				<?php } ?>
			</select>&nbsp;&nbsp;</td>
		<td><?php _e('File', 'wp2print'); ?>:</td>
		<td><div id="filelist" class="ua-files-list"></div></td>
		<td>&nbsp;</td>
		<td>
			<div id="uacontainer" class="artwork-buttons">
				<a id="pickfiles" href="javascript:;" class="button artwork-select"><?php _e('Select file', 'wp2print'); ?></a>
				<a id="uploadfiles" href="javascript:;" class="button artwork-upload" style="display:none;"><?php _e('Upload file', 'wp2print'); ?></a>
				<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="upload-loading" style="margin:6px 0 0 4px; display:none;">
			</div>
		</td>
		</tr>
		</table>
	</div>
	<input type="hidden" name="artworkfiles" class="artwork-files">
	<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/plupload.full.min.js?ver=3.1.2"></script>
	<script type="text/javascript">
	<!--
	var uploader = false;
	jQuery(document).ready(function() {
		uploader = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			file_data_name: 'file',
			browse_button : 'pickfiles', // you can pass an id...
			container: document.getElementById('uacontainer'), // ... or DOM Element itself
			flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
			silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
			drop_element: document.getElementById('upload-artwork'), // ... or DOM Element itself
			url : '<?php echo $plupload_url; ?>',
			dragdrop: true,
			filters : {
				max_file_size : '<?php echo $file_upload_max_size; ?>mb'
			},
			<?php if ($upload_to == 'amazon') { ?>
			multipart: true,
			<?php echo $multiparams; ?>
			<?php } ?>
			init: {
				PostInit: function() {
					<?php if (!strlen($artworkfiles)) { ?>jQuery('#filelist').html('').hide();<?php } ?>
					document.getElementById('uploadfiles').onclick = function() {
						uploader.start();
						jQuery('.upload-loading').show();
						return false;
					};
				},
				FilesAdded: function(up, files) {
					jQuery('#filelist').show();
					plupload.each(files, function(file) {
						file.name = wp2print_clear_fname(file.name);
						document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
					});
					jQuery('#uploadfiles').show();
				},
				UploadProgress: function(up, file) {
					document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
				},
				<?php if ($upload_to == 'amazon') { ?>
				BeforeUpload: function(up, file) {
					var regex = /(?:\.([^.]+))?$/;
					var ext = regex.exec(file.name)[1];
					if (ext == 'pdf') {
						up.settings.multipart_params['Content-Type'] = 'application/pdf';
					} else {
						up.settings.multipart_params['Content-Type'] = file.type;
					}
					up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
					<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
					up.settings.multipart_params['Content-Disposition'] = 'attachment';
				},
				<?php } ?>
				FileUploaded: function(up, file, response) {
					<?php if ($upload_to == 'amazon') { ?>
						var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
					<?php } else { ?>
						var ufileurl = response['response'];
					<?php } ?>
					if (ufileurl != '') {
						var artworkfiles = jQuery('.user-files-form .artwork-files').val();
						if (artworkfiles != '') { artworkfiles += ';'; }
						artworkfiles += ufileurl;
						jQuery('.user-files-form .artwork-files').val(artworkfiles);
					}
				},
				UploadComplete: function(files) {
					jQuery('.upload-loading').hide();
					jQuery('.user-files-form').submit();
				},
				Error: function(up, err) {
					alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
				}
			}
		});
		uploader.init();
		jQuery('.uf-list .uf-delete').click(function(){
			var fid = jQuery(this).data('file_id');
			var emessage = jQuery('.user-files-delete-form').data('emessage');
			var dconfirm = confirm(emessage);
			if (dconfirm) {
				jQuery('.user-files-delete-form .file-id').val(fid);
				jQuery('.user-files-delete-form').submit();
			}
			return false;
		});
	});
	//--></script>
