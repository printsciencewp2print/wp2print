<?php
global $wpdb, $current_user;
$user_group = print_products_users_groups_get_user_group($current_user->ID);
if (!$user_group) {
	$suser_group = print_products_users_groups_is_superuser($user_id);
	$user_group = print_products_users_groups_data($suser_group);
}
$group_products = false;
$group_products_id = print_products_users_groups_get_products($user_group);
if ($group_products_id) {
	$query = new WC_Product_Query(array('include' => $group_products_id, 'orderby' => 'title', 'order' => 'asc', 'limit' => -1));
	$group_products = $query->get_products();
}
?>
<div class="wrap ma-inventory-tracking-wrap">
	<?php if ($group_products) { ?>
		<table>
			<tr>
				<th><?php _e('Product', 'wp2print'); ?></th>
				<th><?php _e('Status', 'wp2print'); ?></th>
				<th><?php _e('Count', 'wp2print'); ?></th>
				<th><?php _e('Low-stock threshold', 'wp2print'); ?></th>
			</tr>
			<?php foreach ($group_products as $group_product) { ?>
				<tr>
					<td><a href="<?php echo get_permalink($group_product->get_id()); ?>" target="_blank"><?php echo $group_product->get_name(); ?></a></td>
					<td><?php if ($group_product->get_manage_stock()) { echo ucfirst($group_product->get_stock_status()); } else { _e('Inventory not tracked', 'wp2print'); } ?></td>
					<td><?php echo $group_product->get_stock_quantity(); ?></td>
					<td><?php echo $group_product->get_low_stock_amount(); ?></td>
				</tr>
			<?php } ?>
		</table>
	<?php } else { ?>
		<p><?php _e('No private store products.', 'wp2print'); ?></p>
	<?php } ?>
</div>
