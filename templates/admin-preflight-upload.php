<?php
$file_upload_target = get_option("print_products_file_upload_target");
$amazon_s3_settings = get_option("print_products_amazon_s3_settings");
$email_options = get_option("print_products_email_options");

$original_path = 'preflight/originals';

$upload_to = 'host';
$plupload_url = get_bloginfo('url').'/index.php?ajaxupload=artwork&sessid='.session_id();
if ($file_upload_target == 'amazon' && $amazon_s3_settings['s3_access_key'] && $amazon_s3_settings['s3_secret_key']) {
	$upload_to = 'amazon';

	$s3_data = print_products_amazon_s3_get_data($amazon_s3_settings, $file_upload_max_size, $original_path);
	$s3path = $s3_data['s3path'];
	$is_s3_region = $s3_data['is_s3_region'];
	$plupload_url = $s3_data['amazon_url'];
	$amazon_file_url = $s3_data['amazon_file_url'];
	$multiparams = $s3_data['multiparams'];
}
?>
<div id="upload-artwork" class="order-upload-pdf">
	<p style="margin:0 0 8px; font-weight:700;"><?php _e('Please select PDF file(s)', 'wp2print'); ?>:</p>
	<div id="filelist" class="ua-files-list" style="padding:10px 0; border-top:1px solid #C1C1C1; border-bottom:1px solid #C1C1C1;">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
	<div id="uacontainer" class="artwork-buttons">
		<a id="pickfiles" href="javascript:;" class="button artwork-select"><?php _e('Select files', 'wp2print'); ?></a>
		<a id="uploadfiles" href="javascript:;" class="button button-primary" style="display:none;"><?php _e('Upload files', 'wp2print'); ?></a>
	</div>
</div>
<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/plupload.full.min.js?ver=3.1.2"></script>
<script type="text/javascript">
<!--
jQuery(document).ready(function() {
	var ufnum = 0;
	var uploader = new plupload.Uploader({
		runtimes : 'html5,flash,silverlight,html4',
		file_data_name: 'file',
		browse_button : 'pickfiles', // you can pass an id...
		container: document.getElementById('uacontainer'), // ... or DOM Element itself
		flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
		silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
		drop_element: document.getElementById('upload-artwork'), // ... or DOM Element itself
		url : '<?php echo $plupload_url; ?>',
		dragdrop: true,
		filters : {
			mime_types: [{title : "Specific files", extensions : "pdf"}]
		},
		<?php if ($upload_to == 'amazon') { ?>
		multipart: true,
		<?php echo $multiparams; ?>
		<?php } ?>
		init: {
			PostInit: function() {
				jQuery('#filelist').html('').hide();
				jQuery('#uploadfiles').hide();

				document.getElementById('uploadfiles').onclick = function() {
					uploader.start();
					jQuery('#uploadfiles').attr('disabled', 'disabled');
					return false;
				};
			},
			FilesAdded: function(up, files) {
				var ucounterror = false;
				jQuery('#filelist').show();
				plupload.each(files, function(file) {
					file.name = wp2print_clear_fname(file.name);
					document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
				});
				jQuery('#uploadfiles').removeAttr('disabled').show();
			},
			UploadProgress: function(up, file) {
				document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
			},
			<?php if ($upload_to == 'amazon') { ?>
			BeforeUpload: function(up, file) {
				var regex = /(?:\.([^.]+))?$/;
				up.settings.multipart_params['Content-Type'] = 'application/pdf';
				up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
				<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
				up.settings.multipart_params['Content-Disposition'] = 'attachment';
			},
			<?php } ?>
			FileUploaded: function(up, file, response) {
				<?php if ($upload_to == 'amazon') { ?>
					var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
				<?php } else { ?>
					var ufileurl = response['response'];
				<?php } ?>
				if (ufileurl != '') {
					var html = jQuery('.preflight-default table tbody').html();
					html = html.replace('{FNAME}', file.name);
					html = html.replace('{FNM}', file.name);
					html = html.replace('{FURL}', ufileurl);
					jQuery('.preflight-files-list').append(html);
					ufnum++;
				}
			},
			UploadComplete: function(files) {
				jQuery('.preflight-files').slideDown();
			},
			Error: function(up, err) {
				alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
			}
		}
	});
	uploader.init();
});
//--></script>
