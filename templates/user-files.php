<?php
global $current_user, $wpdb;
$admin_user_files = print_products_user_files_get_admin_uploaded_files($current_user->ID);
$user_uploaded_files = print_products_user_files_get_uploaded_files($current_user->ID);
?>
<div class="wrap user-files-wrap">
	<?php if ($admin_user_files) { ?>
		<div class="uf-box">
			<label style="font-weight:700;"><?php _e('Admin uploaded files', 'wp2print'); ?>:</label>
			<table cellspacing="0" cellpadding="0" style="width:100%;">
				<?php foreach ($admin_user_files as $admin_user_file) { ?>
				<tr>
					<td style="padding-left:0;"><a href="<?php echo print_products_get_amazon_file_url($admin_user_file->file_url); ?>" target="_blank"><?php echo basename($admin_user_file->file_url); ?></a><td>
					<td style="width:150px;"><?php echo $admin_user_file->created; ?></td>
				</tr>
				<?php } ?>
			</table>
		</div>
	<?php } ?>
	<div class="uf-box">
		<label style="font-weight:700;"><?php _e('My files', 'wp2print'); ?>:</label>
		<?php if ($user_uploaded_files) { ?>
			<table cellspacing="0" cellpadding="0" style="width:100%;">
				<?php foreach ($user_uploaded_files as $user_uploaded_file) { ?>
				<tr>
					<td style="padding-left:0;"><a href="<?php echo print_products_get_amazon_file_url($user_uploaded_file['file_url']); ?>" target="_blank"><?php echo $user_uploaded_file['file_name']; ?></a><td>
					<td style="width:150px;"><?php echo $user_uploaded_file['created']; ?></td>
				</tr>
				<?php } ?>
			</table>
		<?php } else { ?>
			<p><?php _e('No uploaded files.', 'wp2print'); ?></p>
		<?php } ?>
	</div>
</div>