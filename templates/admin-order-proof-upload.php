<?php
$file_upload_max_size = get_option('print_products_file_upload_max_size');
$file_upload_target = get_option("print_products_file_upload_target");
$amazon_s3_settings = get_option("print_products_amazon_s3_settings");
$email_options = get_option("print_products_email_options");

$upload_to = 'host';
$plupload_url = get_bloginfo('url').'/index.php?ajaxupload=artwork&sessid='.session_id();
if ($file_upload_target == 'amazon' && $amazon_s3_settings['s3_access_key'] && $amazon_s3_settings['s3_secret_key']) {
	$upload_to = 'amazon';

	$s3_data = print_products_amazon_s3_get_data($amazon_s3_settings, $file_upload_max_size);
	$s3path = $s3_data['s3path'];
	$is_s3_region = $s3_data['is_s3_region'];
	$plupload_url = $s3_data['amazon_url'];
	$amazon_file_url = $s3_data['amazon_file_url'];
	$multiparams = $s3_data['multiparams'];
}
?>
<div style="display:none;">
	<div id="upload-artwork" class="order-upload-pdf" style="margin:30px 30px 0; border:1px solid #C1C1C1; padding:20px; width:600px; height:500px;">
		<p style="margin:0 0 12px;"><?php _e('Please select file(s)', 'wp2print'); ?>:</p>
		<div id="filelist" class="ua-files-list" style="padding:10px 0; border-top:1px solid #C1C1C1; border-bottom:1px solid #C1C1C1;">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
		<div id="uacontainer" class="artwork-buttons">
			<a id="pickfiles" href="javascript:;" class="button artwork-select"><?php _e('Select files', 'wp2print'); ?></a>
		</div>
		<div class="clear"></div>
		<form method="POST" class="order-proof-form">
		<input type="hidden" name="orders_proof_action" value="send">
		<input type="hidden" name="order_id" class="proof-order-id">
		<input type="hidden" name="item_id" class="proof-item-id">
		<input type="hidden" name="proof_files" class="proof-files">
		<div class="order-proof-email">
			<input type="text" name="email_subject" value="<?php echo stripslashes($email_options['order_proof_subject']); ?>" class="op-email-subject" placeholder="<?php _e('Email Subject', 'wp2print'); ?>" title="<?php _e('Email Subject', 'wp2print'); ?>">
			<textarea name="email_message" class="op-email-message" placeholder="<?php _e('Email Message', 'wp2print'); ?>" title="<?php _e('Email Message', 'wp2print'); ?>"><?php echo stripslashes($email_options['order_proof_message']); ?></textarea>
		</div>
		<?php if (isset($email_options['order_proof_send_sms']) && $email_options['order_proof_send_sms'] == 1) { ?>
			<div class="order-proof-sms">
				<p style="margin:15px 0 0 0;"><?php _e('SMS Message', 'wp2print'); ?></p>
				<textarea name="sms_message" class="op-sms-message" placeholder="<?php _e('SMS Message', 'wp2print'); ?>" title="<?php _e('SMS Message', 'wp2print'); ?>" style="height:100px;"><?php echo stripslashes($email_options['order_proof_sms']); ?></textarea>
			</div>
		<?php } ?>
		<div class="clear"></div>
		<div class="order-proof-submit">
			<a id="uploadfiles" href="javascript:;" class="button button-primary"><?php _e('Send proof', 'wp2print'); ?></a>
		</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/plupload.full.min.js?ver=3.1.2"></script>
<script type="text/javascript">
<!--
jQuery(document).ready(function() {
	var uploader = new plupload.Uploader({
		runtimes : 'html5,flash,silverlight,html4',
		file_data_name: 'file',
		browse_button : 'pickfiles', // you can pass an id...
		container: document.getElementById('uacontainer'), // ... or DOM Element itself
		flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
		silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
		drop_element: document.getElementById('upload-artwork'), // ... or DOM Element itself
		url : '<?php echo $plupload_url; ?>',
		dragdrop: true,
		<?php if ($upload_to == 'amazon') { ?>
		multipart: true,
		<?php echo $multiparams; ?>
		<?php } ?>
		init: {
			PostInit: function() {
				jQuery('#filelist').html('').hide();
				//jQuery('#uploadfiles').hide();

				document.getElementById('uploadfiles').onclick = function() {
					uploader.start();
					jQuery('#uploadfiles').attr('disabled', 'disabled');
					return false;
				};
			},
			FilesAdded: function(up, files) {
				var ucounterror = false;
				jQuery('#filelist').show();
				plupload.each(files, function(file) {
					file.name = wp2print_clear_fname(file.name);
					document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
				});
				jQuery('#uploadfiles').removeAttr('disabled');
				jQuery('#uploadfiles').show();
			},
			UploadProgress: function(up, file) {
				document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
			},
			<?php if ($upload_to == 'amazon') { ?>
			BeforeUpload: function(up, file) {
				var regex = /(?:\.([^.]+))?$/;
				var ext = regex.exec(file.name)[1];
				if (ext == 'pdf') {
					up.settings.multipart_params['Content-Type'] = 'application/pdf';
				} else {
					up.settings.multipart_params['Content-Type'] = file.type;
				}
				up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
				<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
				up.settings.multipart_params['Content-Disposition'] = 'attachment';
			},
			<?php } ?>
			FileUploaded: function(up, file, response) {
				<?php if ($upload_to == 'amazon') { ?>
					var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
				<?php } else { ?>
					var ufileurl = response['response'];
				<?php } ?>
				if (ufileurl != '') {
					var prooffiles = jQuery('.order-proof-form .proof-files').val();
					if (prooffiles != '') { prooffiles += ';'; }
					prooffiles += ufileurl;
					jQuery('.order-proof-form .proof-files').val(prooffiles);
				}
			},
			UploadComplete: function(files) {
				jQuery('form.order-proof-form').submit();
			},
			Error: function(up, err) {
				alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
			}
		}
	});
	uploader.init();
});
//--></script>
