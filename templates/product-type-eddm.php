<?php
global $product, $wpdb, $print_products_settings, $attribute_names, $terms_names, $attribute_types, $attribute_imgs, $wp2print_attribute_images;

$product_id = $product->get_id();

$woocommerce_calc_taxes = get_option('woocommerce_calc_taxes');
$woocommerce_prices_include_tax = get_option('woocommerce_prices_include_tax');
$price_display_incl_suffix = get_option('woocommerce_price_display_suffix');
$price_display_excl_suffix = get_option('woocommerce_price_display_excl_suffix');
$print_products_plugin_options = get_option('print_products_plugin_options');
$print_products_accuzip_api_options = get_option("print_products_accuzip_api_options");

$artwork_source = get_post_meta($product_id, '_artwork_source', true);
$artwork_allow_later = get_post_meta($product_id, '_artwork_allow_later', true);
$artwork_file_count = (int)get_post_meta($product_id, '_artwork_file_count', true);
$artwork_afile_types = get_post_meta($product_id, '_artwork_afile_types', true);
$product_shipping_weights = unserialize(get_post_meta($product_id, '_product_shipping_weights', true));
$product_display_weight = get_post_meta($product_id, '_product_display_weight', true);
$product_display_price = get_post_meta($product_id, '_product_display_price', true);
$attribute_labels = (array)get_post_meta($product_id, '_attribute_labels', true);
$attribute_display = (array)get_post_meta($product_id, '_attribute_display', true);
$attribute_as_radio = (array)get_post_meta($product_id, '_attribute_as_radio', true);
$use_production_speed = (int)get_post_meta($product_id, '_use_production_speed', true);
$production_speed_label = get_post_meta($product_id, '_production_speed_label', true);
$production_speed_options = get_post_meta($product_id, '_production_speed_options', true);
$production_speed_sd_data = get_post_meta($product_id, '_production_speed_sd_data', true);
$order_min_price = (float)get_post_meta($product_id, '_order_min_price', true);
$order_max_price = (float)get_post_meta($product_id, '_order_max_price', true);
$personalize_sc_product_id = get_post_meta($product_id, '_personalize_sc_product_id', true);
$max_price_message = $print_products_plugin_options['max_price_message'];

//$product_shipping_base_quantity = (int)get_post_meta($product_id, '_product_shipping_base_quantity', true);
$product_shipping_base_quantity = 1;

if (!strlen($production_speed_label)) { $production_speed_label = __('Production speed', 'wp2print'); }
if (!$product_display_price || $woocommerce_calc_taxes != 'yes') { $product_display_price = 'excl'; }

$size_attribute = $print_products_settings['size_attribute'];
$colour_attribute = $print_products_settings['colour_attribute'];
$material_attribute = $print_products_settings['material_attribute'];
$page_count_attribute = $print_products_settings['page_count_attribute'];
$postage_attribute = (int)$print_products_settings['postage_attribute'];

$attributes = $wpdb->get_results(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies ORDER BY attribute_order, attribute_label", $wpdb->prefix));
print_products_price_matrix_attr_names_init($attributes);

$eddm_target_options = print_products_get_eddm_target_options();

$min_routes_quantity = 0;
if (isset($print_products_accuzip_api_options['min_quantity'])) {
	$min_routes_quantity = (int)$print_products_accuzip_api_options['min_quantity'];
}

$nrfields = array('agent_or_mailer_signing_statement', 'agent_or_mailer_company', 'agent_or_mailer_phone', 'agent_or_mailer_email', 'mailing_agent_mailer_id', 'mailing_agent_crid', 'mailing_agent_edoc_sender_crid', 'permit_holder_mailer_id', 'permit_holder_crid');

$is_modify = false;
$eddm_target_customer = 0;
$quantity_val = 0;
if (isset($_GET['modify']) && strlen($_GET['modify'])) {
	$cart_item_key = $_GET['modify'];
	$prod_cart_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_cart_data WHERE cart_item_key = '%s'", $wpdb->prefix, $cart_item_key));
	if ($prod_cart_data) {
		$is_modify = true;
		$quantity_val = $prod_cart_data->quantity;
		$product_attributes = unserialize($prod_cart_data->product_attributes);
		$additional = unserialize($prod_cart_data->additional);
		$eddm_target_customer = (int)$additional['eddm_target_customer'];
	}
}
$product_type_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s ORDER BY mtype, sorder", $wpdb->prefix, $product_id));

$size_dimensions = array();
$eddm_data = print_products_get_eddm_data();
if ($eddm_data && $eddm_data['success'] && isset($eddm_data['eddm_url']) && strlen($eddm_data['eddm_url'])) { ?>
	<div class="print-products-area product-attributes" style="margin:0 0 15px 0;">
		<form method="POST" class="add-cart-form" onsubmit="return products_add_cart_action();">
			<div class="eddm-top cf">
				<div class="eddm-select-wrap" style="float:right;">
					<a href="#select-routes" class="button alt eddm-select-routes-btn"><?php _e('Select routes', 'wp2print'); ?></a>
				</div>
				<div class="eddm-quantity-wrap">
					<label style="line-height:2.2">Quantity: <span>0</span></label>
				</div>
			</div>
			<?php if ($product_type_matrix_types) { ?>
				<?php $sattrex = 0; $mtypecount = array();
				foreach($product_type_matrix_types as $product_type_matrix_type) {
					$mtype_id = $product_type_matrix_type->mtype_id;
					$mtype = $product_type_matrix_type->mtype;
					$mattributes = unserialize($product_type_matrix_type->attributes);
					$materms = unserialize($product_type_matrix_type->aterms);
					$numbers = explode(',', $product_type_matrix_type->numbers);
					$num_style = $product_type_matrix_type->num_style;
					$num_type = $product_type_matrix_type->num_type;
					$ltext_attr = (int)$product_type_matrix_type->ltext_attr;
					$min_qmailed = (int)$product_type_matrix_type->min_qmailed;

					if ($mattributes) { $mattributes = print_products_sort_attributes($mattributes); ?>
						<?php if ($mtype == 0) { // simple matrix ?>
							<div class="matrix-type-simple" data-mtid="<?php echo $mtype_id; ?>" data-ntp="<?php echo $num_type; ?>">
								<ul class="product-attributes-list print-attributes">
									<?php foreach($mattributes as $mattribute) {
										$matype = $attribute_types[$mattribute];
										$aterms = $materms[$mattribute];
										$aval = '';
										if ($is_modify) {
											$avals = explode(':', $product_attributes[$anmb]);
											$akey = $avals[0];
											$aval = $avals[1];
											$anmb++;
										}
										$is_radio = false;
										if (isset($attribute_as_radio[$mattribute]) && $attribute_as_radio[$mattribute]) { $is_radio = true; }
										if ($matype == 'text') { ?>
											<li class="matrix-attribute-<?php echo $mtype_id; ?>-<?php echo $mattribute; ?>">
												<label><?php echo print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]); ?>:</label><br />
												<div class="attr-box">
													<input type="text" name="sattribute[<?php echo $mattribute; ?>]" class="smatrix-attr smatrix-attr-text" value="<?php echo $aval; ?>" data-aid="<?php echo $mattribute; ?>" onchange="matrix_calculate_price();" onblur="matrix_calculate_price();"><?php print_products_attribute_help_icon($mattribute); ?>
												</div>
											</li>
											<?php
										} else {
											if ($aterms) {
												$aimg = $attribute_imgs[$mattribute];
												$aterms = print_products_get_attribute_terms($aterms);
												$attr_class = '';
												if ($mattribute == $size_attribute) { $attr_class = ' smatrix-size'; }
												if ($mattribute == $colour_attribute) { $attr_class = ' smatrix-colour'; }
												if ($mattribute == $material_attribute) { $attr_class = ' smatrix-material'; }
												if ($mattribute == $page_count_attribute) { $attr_class = ' smatrix-pagecount'; }

												if ($mattribute == $size_attribute) {
													foreach($aterms as $aterm_id => $aterm_name) {
														$dimensions = get_term_meta($aterm_id, '_dimensions', true);
														if ($dimensions && is_array($dimensions)) {
															$size_dimensions[$aterm_id] = $dimensions['width'].'x'.$dimensions['height'];
														}
													}
												}

												$do_not_display = (isset($attribute_display[$mattribute]) ? (int)$attribute_display[$mattribute] : 0);
												if ($do_not_display) {
													if (!$is_modify) { $aterms_keys = array_keys($aterms); $aval = $aterms_keys[0]; } ?>
													<input type="hidden" name="sattribute[<?php echo $mattribute; ?>]" class="smatrix-attr<?php echo $attr_class; ?>" value="<?php echo $aval; ?>" data-aid="<?php echo $mattribute; ?>">
												<?php } else { ?>
													<li class="matrix-attribute-<?php echo $mtype_id; ?>-<?php echo $mattribute; ?>">
														<label><?php echo print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]); ?>:</label><br />
														<div class="attr-box<?php if ($is_radio) { echo ' attr-radio-box'; } ?>">
															<?php if ($is_radio) { ?>
																<div class="a-radio a-radio-<?php echo $mtype_id; ?>-<?php echo $mattribute; ?>">
																	<?php foreach($aterms as $aterm_id => $aterm_name) { if (!strlen($aval)) { $aval = $aterm_id; } ?>
																		<label><input type="radio" name="rattribute[<?php echo $mtype_id.$mattribute; ?>]" value="<?php echo $aterm_id; ?>" rel="smatrix-attr-<?php echo $mtype_id.$mattribute; ?>" onclick="matrix_aradio(this);"<?php if ($aval == $aterm_id) { echo ' CHECKED'; } ?>><span class="button alt"><?php echo $aterm_name; ?></span></label>
																	<?php } ?>
																</div>
															<?php } ?>
															<?php if ($is_radio) { ?><div style="display:none;"><?php } ?>
															<select name="sattribute[<?php echo $mattribute; ?>]" class="smatrix-attr smatrix-attr-<?php echo $mtype_id.$mattribute; ?><?php echo $attr_class; ?>" data-aid="<?php echo $mattribute; ?>" onchange="matrix_calculate_price();<?php if ($aimg) { ?> matrix_attribute_image(this, <?php echo $mattribute; ?>, <?php echo $mtype_id; ?>);<?php } ?>">
																<?php foreach($aterms as $aterm_id => $aterm_name) { ?>
																	<option value="<?php echo $aterm_id; ?>"<?php if ($aval == $aterm_id) { echo ' SELECTED'; } ?>><?php echo $aterm_name; ?></option>
																<?php } ?>
															</select>
															<?php if ($is_radio) { ?></div><?php } ?>
															<?php print_products_attribute_help_icon($mattribute); ?>
														</div>
														<?php if ($aimg) { $showai = false; $ainmb = 1;
															foreach($aterms as $aterm_id => $aterm_name) {
																if ($wp2print_attribute_images[$aterm_id]) { $showai = true; }
															}
															if ($showai) { ?>
																<div class="attribute-images attribute-images-<?php echo $mattribute; ?>">
																	<ul>
																		<?php foreach($aterms as $aterm_id => $aterm_name) {
																			if ($wp2print_attribute_images[$aterm_id]) { ?>
																				<li><img src="<?php echo print_products_get_thumb($wp2print_attribute_images[$aterm_id], 100, 80, true) ?>" class="attribute-image-<?php echo $aterm_id; ?><?php if ($ainmb == 1) { echo ' active'; } ?>" rel="matrix-attribute-<?php echo $mtype_id; ?>-<?php echo $mattribute; ?>"></li>
																			<?php } ?>
																		<?php $ainmb++; } ?>
																	</ul>
																</div>
															<?php } ?>
														<?php } ?>
													</li>
												<?php } ?>
											<?php } ?>
										<?php } ?>
									<?php } ?>
								</ul>
							</div>
						<?php } else { // finishing matrix ?>
							<div class="matrix-type-finishing" data-mtid="<?php echo $mtype_id; ?>" data-ntp="<?php echo $num_type; ?>">
								<ul class="product-attributes-list finishing-attributes">
								<?php foreach($mattributes as $mattribute) {
									$matype = $attribute_types[$mattribute];
									$aterms = $materms[$mattribute];
									$aval = '';
									if ($is_modify) {
										$avals = explode(':', $product_attributes[$anmb]);
										$akey = $avals[0];
										$aval = $avals[1];
										$anmb++;
									}
									$is_radio = false;
									if (isset($attribute_as_radio[$mattribute]) && $attribute_as_radio[$mattribute]) { $is_radio = true; }
									if ($matype == 'text') { ?>
										<li class="matrix-attribute-<?php echo $mtype_id; ?>-<?php echo $mattribute; ?>">
											<label><?php echo print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]); ?>:</label><br />
											<div class="attr-box">
												<input type="text" name="fattribute[<?php echo $mattribute; ?>]" class="fmatrix-attr" value="<?php echo $aval; ?>" data-aid="<?php echo $mattribute; ?>" onchange="matrix_calculate_price();" onblur="matrix_calculate_price();"><?php print_products_attribute_help_icon($mattribute); ?>
											</div>
										</li>
										<?php
									} else {
										if ($aterms) {
											$aimg = $attribute_imgs[$mattribute];
											$aterms = print_products_get_attribute_terms($aterms);
											$do_not_display = (isset($attribute_display[$mattribute]) ? (int)$attribute_display[$mattribute] : 0);
											if ($do_not_display) {
												if (!$is_modify) { $aterms_keys = array_keys($aterms); $aval = $aterms_keys[0]; } ?>
												<input type="hidden" name="fattribute[<?php echo $mattribute; ?>]" class="fmatrix-attr" value="<?php echo $aval; ?>" data-aid="<?php echo $mattribute; ?>">
											<?php } else { ?>
												<li class="matrix-attribute-<?php echo $mtype_id; ?>-<?php echo $mattribute; ?>">
													<label><?php echo print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]); ?>:</label><br />
													<div class="attr-box<?php if ($is_radio) { echo ' attr-radio-box'; } ?>">
														<?php if ($is_radio) { ?>
															<div class="a-radio a-radio-<?php echo $mtype_id; ?>-<?php echo $mattribute; ?>">
																<?php foreach($aterms as $aterm_id => $aterm_name) { if (!strlen($aval)) { $aval = $aterm_id; } ?>
																	<label><input type="radio" name="rattribute[<?php echo $mtype_id.$mattribute; ?>]" value="<?php echo $aterm_id; ?>" rel="fmatrix-attr-<?php echo $mtype_id.$mattribute; ?>" onclick="matrix_aradio(this);"<?php if ($aval == $aterm_id) { echo ' CHECKED'; } ?>><span class="button alt"><?php echo $aterm_name; ?></span></label>
																<?php } ?>
															</div>
														<?php } ?>
														<?php if ($is_radio) { ?><div style="display:none;"><?php } ?>
														<select name="fattribute[<?php echo $mattribute; ?>]" class="fmatrix-attr fmatrix-attr-<?php echo $mattribute; ?> fmatrix-attr-<?php echo $mtype_id.$mattribute; ?>" data-aid="<?php echo $mattribute; ?>" onchange="matrix_calculate_price();<?php if ($aimg) { ?> matrix_attribute_image(this, <?php echo $mattribute; ?>, <?php echo $mtype_id; ?>);<?php } ?>">
															<?php foreach($aterms as $aterm_id => $aterm_name) { ?>
																<option value="<?php echo $aterm_id; ?>"<?php if ($aval == $aterm_id) { echo ' SELECTED'; } ?>><?php echo $aterm_name; ?></option>
															<?php } ?>
														</select>
														<?php if ($is_radio) { ?></div><?php } ?>
														<?php print_products_attribute_help_icon($mattribute); ?>
													</div>
													<?php if ($aimg) { $showai = false; $ainmb = 1;
														foreach($aterms as $aterm_id => $aterm_name) {
															if ($wp2print_attribute_images[$aterm_id]) { $showai = true; }
														}
														if ($showai) { ?>
															<div class="attribute-images attribute-images-<?php echo $mattribute; ?>">
																<ul>
																	<?php foreach($aterms as $aterm_id => $aterm_name) {
																		if ($wp2print_attribute_images[$aterm_id]) { ?>
																			<li><img src="<?php echo print_products_get_thumb($wp2print_attribute_images[$aterm_id], 100, 80, true) ?>" class="attribute-image-<?php echo $aterm_id; ?><?php if ($ainmb == 1) { echo ' active'; } ?>" rel="matrix-attribute-<?php echo $mtype_id; ?>-<?php echo $mattribute; ?>"></li>
																		<?php } ?>
																	<?php $ainmb++; } ?>
																</ul>
															</div>
														<?php } ?>
													<?php } ?>
												</li>
											<?php } ?>
										<?php } ?>
									<?php } ?>
								<?php } ?>
								</ul>
							</div>
						<?php } ?>
						<?php
						$lmtype = $mtype;
					}
				} ?>
			<?php } ?>
			<div class="eddm-target-wrap">
				<ul class="product-attributes-list">
					<li>
						<label><?php echo print_products_attribute_label('eddmtcl', $attribute_labels, __('EDDM Target customer', 'wp2print')); ?>:</label><br />
						<div class="attr-box">
							<select name="eddm_target_customer" class="eddm-target-customer" onchange="matrix_show_quantity(); matrix_set_mailing_price(); matrix_calculate_price();">
								<?php foreach($eddm_target_options as $eto_key => $eto_val) { ?>
									<option value="<?php echo $eto_key; ?>"<?php if ($eto_key == $eddm_target_customer) { echo ' SELECTED'; } ?>><?php echo $eto_val; ?></option>
								<?php } ?>
							</select>
							<input type="hidden" name="eddm_target_customer_label" value="<?php echo print_products_attribute_label('eddmtcl', $attribute_labels, __('EDDM Target customer', 'wp2print')); ?>">
						</div>
					</li>
				</ul>
			</div>
			<?php if ($use_production_speed && $production_speed_options && is_array($production_speed_options) && count($production_speed_options)) { ?>
				<ul class="product-attributes-list">
					<li>
						<label><?php echo $production_speed_label; ?>:</label><br />
						<select name="production_speed" class="production-speed" onchange="matrix_production_speed(); matrix_calculate_price();">
							<?php foreach($production_speed_options as $okey => $pso) { ?>
								<option value="<?php echo $okey; ?>" data-p="<?php echo $pso['percent']; ?>"><?php echo $pso['label']; ?></option>
							<?php } ?>
						</select>
					</li>
					<?php if ($production_speed_sd_data && $production_speed_sd_data['show']) {
						if (!strlen($production_speed_sd_data['label'])) { $production_speed_sd_data['label'] = __('Shipment date', 'wp2print'); } ?>
						<li class="shipp-date-row">
							<label><?php echo $production_speed_sd_data['label']; ?>:&nbsp;</label>
							<?php foreach($production_speed_options as $okey => $pso) {
								$sd_val = print_products_get_shipping_date($pso['days'], $production_speed_sd_data['time'], $production_speed_sd_data['weekend']); ?>
								<span class="sd-val sd-<?php echo $okey; ?>"<?php if ($okey > 0) { echo ' style="display:none;"'; } ?>><?php echo $sd_val; ?></span>
							<?php } ?>
						</li>
					<?php } ?>
				</ul>
			<?php } ?>
			<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
			<?php $is_product_hidden_price = print_products_is_product_hidden_price($product_id); ?>
			<?php if ($is_product_hidden_price) { ?>
				<div class="product-price"><?php _e('Price', 'wp2print'); ?>: <?php echo print_products_get_nl_login_link(); ?></div>
			<?php } else { ?>
				<?php if ($product_display_price == 'both') { ?>
					<div class="product-price product-price-incl-tax price-incl-tax"><?php _e('Price', 'wp2print'); ?>: <span class="pprice">0.00</span> <?php echo $price_display_incl_suffix; ?><span class="price-unit"></span></div>
					<div class="product-price product-price-excl-tax price-excl-tax"><span class="pprice">0.00</span> <?php echo $price_display_excl_suffix; ?></div>
				<?php } else if ($product_display_price == 'incl') { ?>
					<div class="product-price price-incl-tax"><?php _e('Price', 'wp2print'); ?>: <span class="pprice">0.00</span> <?php echo $price_display_incl_suffix; ?><span class="price-unit"></span></div>
				<?php } else { ?>
					<div class="product-price price-excl-tax"><?php _e('Price', 'wp2print'); ?>: <span class="pprice">0.00</span> <?php echo $price_display_excl_suffix; ?><span class="price-unit"></span></div>
				<?php } ?>
			<?php } ?>

			<?php if ($product_display_weight) { ?><div class="product-weight"><?php _e('Weight', 'wp2print'); ?>: <span class="pweight">0</span> <?php echo print_products_get_weight_unit(); ?></div><?php } ?>
			<?php if (!$is_product_hidden_price) { ?>
				<div class="product-add-button cf">
					<input type="hidden" name="quantity" value="0" class="quantity">
					<input type="hidden" name="product_type" value="eddm" class="product-type">
					<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" class="product-id">
					<?php if ($is_modify) { ?>
						<input type="hidden" name="print_products_checkout_process_action" value="update-cart">
						<input type="hidden" name="cart_item_key" value="<?php echo $cart_item_key; ?>">
						<input type="submit" value="<?php _e('Update cart', 'wp2print'); ?>" class="single_add_to_cart_button <?php print_products_buttons_class(); ?> update-cart-btn" onclick="return products_add_cart_action();">
					<?php } else { ?>
						<input type="hidden" name="print_products_checkout_process_action" value="add-to-cart">
						<input type="hidden" name="add-to-cart" value="<?php echo $product_id; ?>">
						<?php if (strlen($artwork_source)) { ?>
							<?php if ($artwork_source == 'artwork' || $artwork_source == 'both') { ?>
								<input type="button" value="<?php _e('Upload your own design', 'wp2print'); ?>" class="single_add_to_cart_button <?php print_products_buttons_class(); ?> alt artwork-btn upload-artwork-btn ch-price" onclick="return products_add_cart_action();" disabled>
								<?php if ($artwork_allow_later) { ?>
									<button class="single_add_to_cart_button <?php print_products_buttons_class(); ?> alt artwork-btn simple-add-btn ch-price" onclick="return products_add_cart_action();" disabled><?php _e('Upload later', 'wp2print'); ?></button>
								<?php } ?>
							<?php } ?>
							<?php if (($artwork_source == 'design' || $artwork_source == 'both') && print_products_designer_installed()) {
								$personalizeclass = 'personalize';
								$window_type = personalize_get_window_type();
								if ($window_type == 'Modal Pop-up window') {
									$personalizeclass .= ' personalizep';
								}
								?>
								<button class="single_add_to_cart_button <?php print_products_buttons_class(); ?> alt design-online-btn <?php echo $personalizeclass; ?>" onclick="return products_add_cart_action();" disabled><?php _e('DESIGN ONLINE', 'wp2print'); ?></button>
							<?php } ?>
						<?php } else { ?>
							<input type="submit" value="<?php _e('ADD TO CART', 'wp2print'); ?>" class="single_add_to_cart_button button alt simple-add-btn" onclick="return products_add_cart_action();" disabled>
						<?php } ?>
					<?php } ?>
				</div>
			<?php } ?>
			<div class="product-na-text" style="display:none;"><?php _e('This product is not available with the set of options. Please choose another set of options.', 'wp2print'); ?></div>
			<input type="hidden" name="smparams" class="sm-params">
			<input type="hidden" name="fmparams" class="fm-params">
			<input type="hidden" name="atcaction" class="atc-action" value="artwork">
			<input type="hidden" name="artworkfiles" class="artwork-files">
			<input type="hidden" name="pprice" class="p-price">
			<input type="hidden" name="price_mailing" class="price-mailing">
			<input type="hidden" name="guid" value="<?php echo $eddm_data['guid']; ?>">
			<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
		</form>
		<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
	</div>
	<div style="position:absolute;left:-20000px;">
		<div id="eddm-iframe" class="eddm-iframe-wrap print-products-area" style="margin:25px 0 0 20px; width:1000px; height:605px;">
			<iframe src="<?php echo $eddm_data['eddm_url']; ?>" class="eddm-iframe" frameborder="0" style="width:1000px; height:530px; margin-left:-10px;"></iframe>
			<div class="eddm-iframe-buttons" style="text-align:right;padding-right:82px;">
				<a href="#save" class="button eddm-save"><?php _e('Save', 'wp2print'); ?></a>
				<a href="#cancel" class="button eddm-cancel"><?php _e('Cancel', 'wp2print'); ?></a>
			</div>
		</div>
	</div>
	<?php
	$smatrix = array();
	$fmatrix = array();
	foreach($product_type_matrix_types as $product_type_matrix_type) {
		$mtype_id = $product_type_matrix_type->mtype_id;
		$mtype = $product_type_matrix_type->mtype;
		$numbers = $product_type_matrix_type->numbers;

		$mnumbers[$mtype_id] = $numbers;

		$matrix_prices = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_prices WHERE mtype_id = %s", $wpdb->prefix, $mtype_id));
		if ($matrix_prices) {
			foreach($matrix_prices as $matrix_price) {
				$aterms = $matrix_price->aterms;
				$number = $matrix_price->number;
				$price = $matrix_price->price;

				if ($mtype == 1) {
					$fmatrix[$aterms.'-'.$number] = $price;
				} else {
					$smatrix[$aterms.'-'.$number] = $price;
				}
			}
		}
	}
	?>
	<script>
	<!--
	var price = 0;
	var order_min_price = <?php echo $order_min_price; ?>;
	var order_max_price = <?php echo $order_max_price; ?>;
	var postage_attribute = <?php echo $postage_attribute; ?>;
	var price_mailing = 0;
	var price_accuzip = 0;
	var residential_only_qty = 0;
	var residential_and_business_qty = 0;
	var min_routes_quantity = <?php echo $min_routes_quantity; ?>;
	var show_unit_price = <?php if ($unitpricetable && isset($unitpricetable['show_price'])) { echo (int)$unitpricetable['show_price']; } else { echo '0'; } ?>;

	var numbers_array = new Array();
	<?php foreach($mnumbers as $ntp => $narr) { ?>
	numbers_array[<?php echo $ntp; ?>] = '<?php echo $narr; ?>';
	<?php } ?>

	var smatrix = new Object();
	<?php foreach($smatrix as $mkey => $mval) { ?>
	smatrix['<?php echo $mkey; ?>'] = <?php echo $mval; ?>;
	<?php } ?>

	var fmatrix = new Object();
	<?php foreach($fmatrix as $mkey => $mval) { ?>
	fmatrix['<?php echo $mkey; ?>'] = <?php echo $mval; ?>;
	<?php } ?>

	var size_dimensions = [];
	<?php if (count($size_dimensions)) { foreach ($size_dimensions as $aid => $dval) { ?>
		size_dimensions[<?php echo $aid; ?>] = '<?php echo $dval; ?>';
	<?php }} ?>

	var shipping_base_quantity = <?php echo $product_shipping_base_quantity; ?>;
	var shipping_weights = new Object();
	<?php if (is_array($product_shipping_weights) && count($product_shipping_weights)) {
		foreach($product_shipping_weights as $mterm => $sterms) {
			foreach($sterms as $sterm => $weight) {
				if (is_array($weight)) {
					$pcterms = $weight;
					foreach($pcterms as $pcterm => $weight) { ?>
						shipping_weights['<?php echo $mterm.'-'.$sterm.'-'.$pcterm; ?>'] = <?php echo (float)$weight; ?>;
					<?php } ?>
				<?php } else { ?>
					shipping_weights['<?php echo $mterm.'-'.$sterm; ?>'] = <?php echo (float)$weight; ?>;
				<?php } ?>
			<?php }
		}
	} ?>

	matrix_single_add_to_cart_button(true);

	jQuery(document).ready(function() {
		setTimeout(function(){
			matrix_calculate_price();
			matrix_attribute_images_init();
			matrix_single_add_to_cart_button(false);
		});
		jQuery('.print-products-area .eddm-select-routes-btn').click(function(){
			jQuery.colorbox({inline:true, href:'#eddm-iframe'});
			matrix_buttons_action(true);
			return false;
		});
		jQuery('.print-products-area .eddm-save').click(function(){
			var mail_piece_size = 'LETTER';
			if (jQuery('.print-attributes .smatrix-size').length) {
				var size_val = jQuery('.print-attributes .smatrix-size').val();
				var size_dims = size_dimensions[size_val];
				if (size_dims != '') {
					size_dims = size_dims.split('x');
					var size_dimm1 = parseFloat(size_dims[0]);
					var size_dimm2 = parseFloat(size_dims[1]);
					var small_dimm = size_dimm1;
					var large_dimm = size_dimm2;
					if (size_dimm2 < size_dimm1) {
						small_dimm = size_dimm2;
						large_dimm = size_dimm1;
					}
					if (small_dimm > 6.125 || large_dimm > 10.5) {
						mail_piece_size = 'FLAT';
					}
				}
			}
			var eddm_atype = '<?php if (isset($print_products_accuzip_api_options['atype'])) { echo $print_products_accuzip_api_options['atype']; } ?>';
			var eddm_request_data = {'mail_piece_size':mail_piece_size, 'eddm_retail_off':'1'};
			<?php if (isset($print_products_accuzip_api_options['atype']) && $print_products_accuzip_api_options['atype'] == 'non-retail') {
				foreach ($nrfields as $nrfield) { if (isset($print_products_accuzip_api_options[$nrfield])) { ?>
					eddm_request_data.<?php echo $nrfield; ?> = '<?php echo $print_products_accuzip_api_options[$nrfield]; ?>';
			<?php }}} ?>
			jQuery('#eddm-iframe').addClass('loading');
			jQuery.get('https://cloud2.iaccutrace.com/servoy-service/rest_ws/ws_360/job/<?php echo $eddm_data['guid']; ?>/QUOTE/true', eddm_request_data,
				function(data) {
					jQuery('#eddm-iframe').removeClass('loading');
					price_accuzip = parseFloat(eddm_rclear(data.Estimated_Postage_Standard_Letter));
					if (mail_piece_size == 'FLAT') {
						price_accuzip = parseFloat(eddm_rclear(data.Estimated_Postage_Standard_Flat));
					}
					residential_only_qty = parseInt(eddm_rclear(data.Total_Residential));
					residential_and_business_qty = parseInt(eddm_rclear(data.Total_Possible));

					var eprocess = true;
					var quantity = matrix_get_quantity();
					if (min_routes_quantity > 0) {
						if (quantity < min_routes_quantity) {
							eprocess = false;
							alert("<?php _e('Minimum number of recipients is', 'wp2print'); ?> <?php echo $min_routes_quantity; ?>.");
						}
					}
					if (eddm_atype != 'non-retail' && quantity > 5000) {
						eprocess = false;
						alert("<?php _e('Maximum number of recipients is 5000', 'wp2print'); ?>");
					}
					if (eprocess) {
						matrix_show_quantity();
						matrix_set_mailing_price();
						matrix_calculate_price();
						jQuery.colorbox.close();
					}
				}
			);
			return false;
		});
		jQuery('.print-products-area .eddm-cancel').click(function(){
			jQuery.colorbox.close();
			return false;
		});
	});

	function eddm_rclear(v) {
		if (v != '') {
			v = v.replace('$', '');
			v = wp2print_replace(v, ',', '');
		}
		return v;
	}

	function matrix_set_mailing_price() {
		var eddm_target_customer = parseInt(jQuery('select.eddm-target-customer').val());
		if (eddm_target_customer == 1) {
			price_mailing = price_accuzip;
		} else {
			if (residential_only_qty) {
				price_mailing = price_accuzip * (residential_only_qty / residential_and_business_qty);
			}
		}
		jQuery('.add-cart-form .price-mailing').val(price_mailing);
	}

	function matrix_get_quantity() {
		var quantity = residential_only_qty;
		var eddm_target_customer = parseInt(jQuery('select.eddm-target-customer').val());
		if (eddm_target_customer == 1) {
			quantity = residential_and_business_qty;
		}
		return quantity;
	}

	function matrix_show_quantity() {
		var quantity = matrix_get_quantity();
		jQuery('.eddm-quantity-wrap label span').html(quantity);
		jQuery('.add-cart-form .quantity').val(quantity);
		if (quantity > 0) {
			matrix_buttons_action(false);
		} else {
			matrix_buttons_action(true);
		}
	}

	function matrix_buttons_action(d) {
		if (d) {
			jQuery('.product-add-button input[type="submit"]').attr('disabled', 'disabled');
			jQuery('.product-add-button input[type="button"]').attr('disabled', 'disabled');
			jQuery('.product-add-button button').attr('disabled', 'disabled');
		} else {
			jQuery('.product-add-button input[type="submit"]').removeAttr('disabled');
			jQuery('.product-add-button input[type="button"]').removeAttr('disabled');
			jQuery('.product-add-button button').removeAttr('disabled');
		}
	}

	function matrix_calculate_price() {
		var smparams = '';
		var fmparams = '';
		var weight_number = 0;
		var pdprice = '<?php echo $product_display_price; ?>';
		var na_price = false;

		price = 0;

		var quantity = matrix_get_quantity();

		// simple matrix
		jQuery('.matrix-type-simple').each(function(){
			var mtid = jQuery(this).attr('data-mtid');
			var ntp = jQuery(this).attr('data-ntp');
			var smval = ''; var psmval = ''; var smsep = '';
			var size_val = parseInt(jQuery(this).find('.print-attributes .smatrix-size').eq(0).val());
			var material_val = parseInt(jQuery(this).find('.print-attributes .smatrix-material').eq(0).val());
			var pagecount_val = parseInt(jQuery(this).find('.print-attributes .smatrix-pagecount').eq(0).val());
			var numbers = numbers_array[mtid].split(',');
			var min_number = parseInt(numbers[0]);

			var nmb_val = quantity;
			if (ntp == 5) {
				nmb_val = ltext.length;
			}

			jQuery(this).find('.print-attributes .smatrix-attr').each(function(){
				var aid = jQuery(this).attr('data-aid');
				var fval = jQuery(this).val();
				fval = matrix_aval(fval);
				smval += smsep + aid+':'+fval;
				if (!jQuery(this).hasClass('smatrix-attr-text')) {
					psmval += smsep + aid+':'+fval;
				}
				smsep = '-';
			});

			var nums = matrix_get_numbers(nmb_val, numbers);
			var smprice = matrix_get_price(smatrix, psmval, nmb_val, nums);
			if (smprice) {
				if (ntp == 5) {
					smprice = smprice * quantity;
				}
				price = price + smprice;
			}

			if (smparams != '') { smparams += ';'; }
			smparams += mtid+'|'+smval+'|'+nmb_val;

			var mtweight = matrix_shipping_get_weight(material_val, size_val, pagecount_val);
			weight_number = weight_number + mtweight;

			if (smprice == -1) { na_price = true; }
		});
		jQuery('.sm-params').val(smparams);

		// finishing matrix
		jQuery('.matrix-type-finishing').each(function(){
			var mtid = jQuery(this).attr('data-mtid');
			var ntp = jQuery(this).attr('data-ntp');
			var fmsize_aid = 0;
			var fmsize_val = 0;
			if (jQuery('.matrix-type-simple').find('.smatrix-size').length) {
				fmsize_aid = jQuery('.matrix-type-simple').find('.smatrix-size').attr('data-aid');
				fmsize_val = jQuery('.matrix-type-simple').find('.smatrix-size').val();
			}

			jQuery(this).find('.finishing-attributes .fmatrix-attr').each(function(){
				var fprice = 0;
				var aid = jQuery(this).attr('data-aid');
				var fval = jQuery(this).val();
				fval = matrix_aval(fval);
				var fmval = aid+':'+fval;

				if (fmsize_aid && aid != postage_attribute) {
					fmval = fmsize_aid+':'+fmsize_val+'-'+aid+':'+fval;
				}

				var nmb_val = quantity;

				var numbers = numbers_array[mtid].split(',');

				var nums = matrix_get_numbers(nmb_val, numbers);
				var fmprice = matrix_get_price(fmatrix, fmval, nmb_val, nums);

				if (fmprice) { price = price + fmprice; }

				if (fmparams != '') { fmparams += ';'; }
				fmparams += mtid+'|'+fmval+'|'+nmb_val;

				if (fmprice == -1) { na_price = true; }
			});
		});
		jQuery('.fm-params').val(fmparams);

		price = price + price_mailing;

		var show_weight = true;
		if (!na_price) {
			if (price < 0) { price = 0; }
			price = matrix_production_speed_price(price);
			price = matrix_user_discount_price(price);

			if (order_min_price > 0 && price < order_min_price) {
				price = order_min_price;
			}
			if (order_max_price > 0 && price > order_max_price) {
				jQuery('.add-cart-form .product-price').slideUp();
				jQuery('.product-add-button').slideUp();
				jQuery('.upt-container').slideUp();
				jQuery('.add-cart-form .product-weight').slideUp();
				jQuery('.email-quote-box').slideUp();
				jQuery.colorbox({inline:true, href:"#max-price-message"});
				show_weight = false;
			} else {
				jQuery('.add-cart-form .product-price').slideDown();
				jQuery('.product-add-button').slideDown();
				jQuery('.upt-container').slideDown();
				jQuery('.add-cart-form .product-weight').slideDown();
				jQuery('.email-quote-box').slideDown();
			}

			if (pdprice == 'both' || pdprice == 'incl') {
				jQuery.post('<?php echo site_url('index.php'); ?>',
					{
						AjaxAction: 'product-get-price-with-tax',
						product_id: <?php echo $product_id; ?>,
						price: price
					},
					function(data) {
						var pricewithtax = parseFloat(data);
						jQuery('.product-attributes .price-incl-tax .pprice').html(matrix_html_price(pricewithtax));
					}
				);
			}
			jQuery('.product-attributes .price-excl-tax .pprice').html(matrix_html_price(price));
			jQuery('.add-cart-form .product-na-text').hide();
		} else {
			jQuery('.add-cart-form .product-add-button').slideUp();
			jQuery('.add-cart-form .product-na-text').slideDown();
			jQuery('.product-attributes .product-price .pprice').html('N/A');
		}

		jQuery('.add-cart-form .p-price').val(price);
		if (show_weight) {
			matrix_shipping_weight(weight_number);
		}
		matrix_designer_check();
		matrix_unit_price(price, quantity);
	}

	function matrix_unit_price(p, q) {
		var unit_label = '<?php if ($unitpricetable && isset($unitpricetable['utext'])) { echo $unitpricetable['utext']; } ?>';
		if (show_unit_price == 1) {
			var uprice = '';
			if (p) {
				var upval = p / q;
				uprice = '('+matrix_html_price(upval)+'/'+unit_label+')';
			}
			jQuery('.product-attributes .product-price .price-unit').html(uprice);
		}
	}

	function matrix_production_speed_price(p) {
		if (jQuery('select.production-speed').length && p > 0) {
			var psp = parseFloat(jQuery('select.production-speed option:selected').attr('data-p'));
			if (psp > 0) {
				var pp = (p / 100) * psp;
				p = p + pp;
			}
		}
		return p;
	}

	function matrix_production_speed() {
		var psval = jQuery('select.production-speed').val();
		jQuery('.shipp-date-row .sd-val').hide();
		jQuery('.shipp-date-row .sd-'+psval).fadeIn(100);
	}

	function matrix_user_discount_price(p) {
		var udiscount = <?php echo print_products_user_discount_get_discount_amount(); ?>;
		if (udiscount) {
			var discount_price = (p / 100) * udiscount;
			p = p - discount_price;
		}
		return p;
	}

	function matrix_html_price(price) {
		var price_decimals = <?php echo wc_get_price_decimals(); ?>;
		var currency_symbol = '<?php echo get_woocommerce_currency_symbol(); ?>';
		var currency_pos = '<?php echo get_option('woocommerce_currency_pos'); ?>';
		var fprice = matrix_format_price(price.toFixed(price_decimals));
		if (currency_pos == 'left') {
			return currency_symbol + fprice;
		} else if (currency_pos == 'right') {
			return fprice + currency_symbol;
		} else if (currency_pos == 'left_space') {
			return currency_symbol + ' ' + fprice;
		} else if (currency_pos == 'right_space') {
			return fprice + ' ' + currency_symbol;
		}
	}
	function matrix_format_price(p) {
		var decimal_sep = '<?php echo wc_get_price_decimal_separator(); ?>';
		var thousand_sep = '<?php echo wc_get_price_thousand_separator(); ?>';
		var pparts = p.toString().split('.');
		pparts[0] = pparts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousand_sep);
		return pparts.join(decimal_sep);
	}

	function matrix_shipping_get_weight(material_val, size_val, pagecount_val) {
		var product_weight = 0;
		var swkey = material_val+'-'+size_val;
		if (pagecount_val) {
			swkey = swkey+'-'+pagecount_val;
		}
		if (shipping_weights[swkey]) {
			product_weight = shipping_weights[swkey];
		}
		return product_weight;
	}

	function matrix_shipping_weight(product_weight) {
		if (product_weight > 0) {
			jQuery('.product-weight .pweight').html(product_weight.toFixed(1));
			jQuery('.product-weight').animate({height:'show'}, 100);
		} else {
			jQuery('.product-weight').animate({height:'hide'}, 100);
		}
		matrix_update_theme_shipping_weight(product_weight);
	}

	function matrix_update_theme_shipping_weight(product_weight) {
		if (product_weight) {
			product_weight = product_weight.toFixed(1) + ' <?php echo print_products_get_weight_unit(); ?>';
		}
		jQuery('.woocommerce-product-attributes-item--weight .woocommerce-product-attributes-item__value').html(product_weight);
	}

	function matrix_attribute_images_init() {
		if (jQuery('.attribute-images').length) {
			jQuery('.matrix-type-simple .smatrix-attr').each(function(){
				jQuery(this).trigger('change');
			});
			jQuery('.matrix-type-finishing .fmatrix-attr').each(function(){
				jQuery(this).trigger('change');
			});
		}
	}

	function matrix_attribute_image(o, aid, mtp) {
		var aval = jQuery(o).val();
		jQuery('.matrix-attribute-'+mtp+'-'+aid+' .attribute-images-'+aid+' img').removeClass('active');
		jQuery('.matrix-attribute-'+mtp+'-'+aid+' .attribute-images-'+aid+' .attribute-image-'+aval).addClass('active');
	}

	function products_add_cart_action() {
		var pb_error = false;
		var quantity = matrix_get_quantity();
		if (!quantity) {
			alert("<?php _e('Please select routes', 'wp2print'); ?>.");
			return false;
		}
	}

	function matrix_designer_check() {
		<?php if ($personalize_sc_product_id && is_array($personalize_sc_product_id)) { ?>
			var scpids = [];
			var size_val = 0; if (jQuery('.print-attributes .smatrix-size').length) { size_val = parseInt(jQuery('.print-attributes .smatrix-size').eq(0).val()); }
			var colour_val = 0; if (jQuery('.print-attributes .smatrix-colour').length) { colour_val = parseInt(jQuery('.print-attributes .smatrix-colour').eq(0).val()); }
			var pcount_val = 0; if (jQuery('.print-attributes .smatrix-pagecount').length) { pcount_val = parseInt(jQuery('.print-attributes .smatrix-pagecount').eq(0).val()); }
			<?php foreach($personalize_sc_product_id as $akey1 => $adata1) {
				if (is_array($adata1)) {
					foreach($adata1 as $akey2 => $adata2) {
						if (is_array($adata2)) {
							foreach($adata2 as $akey3 => $adata3) {
								echo "scpids['".$akey1."-".$akey2."-".$akey3."'] = '".$adata3."';".chr(10);
							}
						} else {
							echo "scpids['".$akey1."-".$akey2."'] = '".$adata2."';".chr(10);
						}
					}
				} else {
					echo "scpids['".$akey1."'] = '".$adata1."';".chr(10);
				}
			} ?>
			if (jQuery('.design-online-btn').length) {
				var dbhide = true;
				var scpids_key = '';
				if (size_val && colour_val && pcount_val) {
					scpids_key = size_val+'-'+colour_val+'-'+pcount_val;
				} else if (size_val && colour_val) {
					scpids_key = size_val+'-'+colour_val;
				} else if (size_val && pcount_val) {
					scpids_key = size_val+'-'+pcount_val;
				}
				if (scpids_key) {
					if (scpids[scpids_key]) {
						dbhide = false;
					}
				}
				if (dbhide) {
					jQuery('.design-online-btn').fadeOut(200);
				} else {
					jQuery('.design-online-btn').fadeIn(200);
				}
			}
		<?php } ?>
	}
	//--></script>
	<?php include('product-upload-artwork.php'); ?>
	<div style="display:none;">
		<div id="max-price-message" class="max-price-message">
			<?php echo wpautop($max_price_message); ?>
		</div>
	</div>
<?php } else { ?>
	<p><?php _e('Routing service error.', 'wp2print'); ?> ERROR: <?php echo $eddm_data['error']; ?></p>
<?php } ?>
<?php do_action( 'print_products_after_product_options' ); ?>
