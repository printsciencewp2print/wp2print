<?php
global $current_user;
$show_uploaded_files = get_post_meta($product_id, '_show_uploaded_files', true);
$file_upload_max_size = get_option('print_products_file_upload_max_size');
$file_upload_target = get_option('print_products_file_upload_target');
$amazon_s3_settings = get_option('print_products_amazon_s3_settings');
$autoproof_options = get_option('print_products_autoproof_options');

$multiple_file_data = get_post_meta($product_id, '_multiple_file_data', true);

if (!$file_upload_max_size) { $file_upload_max_size = 2; }
if (!$artwork_file_count) { $artwork_file_count = 25; }
if (!is_array($artwork_afile_types)) { $artwork_afile_types = array('all'); }
if (!count($artwork_afile_types)) { $artwork_afile_types = array('all'); }

$umime_types = '';
if (!in_array('all', $artwork_afile_types)) {
	$umime_types = '{title : "Specific files", extensions : "'.implode(',', $artwork_afile_types).'"}';
	$umime_types = str_replace('jpg/jpeg', 'jpg,jpeg', $umime_types);
}

$upload_to = 'host';
$plupload_url = get_bloginfo('url').'/index.php?ajaxupload=artwork&sessid='.session_id();
if ($file_upload_target == 'amazon' && $amazon_s3_settings['s3_access_key'] && $amazon_s3_settings['s3_secret_key']) {
	$upload_to = 'amazon';

	$s3_data = print_products_amazon_s3_get_data($amazon_s3_settings, $file_upload_max_size);
	$s3path = $s3_data['s3path'];
	$is_s3_region = $s3_data['is_s3_region'];
	$plupload_url = $s3_data['amazon_url'];
	$amazon_file_url = $s3_data['amazon_file_url'];
	$multiparams = $s3_data['multiparams'];
}
?>
	<div style="position:absolute;left:-20000px;">
		<div id="upload-artwork-wrap" class="print-products-area" style="margin:30px; width:620px; height:350px; overflow:auto;position:relative;">
			<div id="upload-file-1" class="print-products-area" style="border:1px solid #C1C1C1; padding:20px;">
				<p style="margin:0;"><strong class="lbl-text"></strong> <span class="file-list"></span> <span class="file-uprocess"></span></p>
				<div class="artwork-buttons">
					<a id="select-button-1" href="#select-file" class="artwork-select"><?php _e('Select file', 'wp2print'); ?></a>
					<a id="upload-button-1" href="#upload-file" class="artwork-upload" style="display:none;"><?php _e('Upload file', 'wp2print'); ?></a>
					<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="upload-loading" style="display:none;">
					<input type="hidden" name="mfile1" class="ufile-url">
				</div>
			</div>
			<div id="upload-file-2" class="print-products-area" style="margin-top:20px; border:1px solid #C1C1C1; padding:20px; display:none;">
				<p style="margin:0;"><strong class="lbl-text"></strong> <span class="file-list"></span> <span class="file-uprocess"></span></p>
				<div class="artwork-buttons">
					<a id="select-button-2" href="#select-file" class="artwork-select"><?php _e('Select file', 'wp2print'); ?></a>
					<a id="upload-button-2" href="#upload-file" class="artwork-upload" style="display:none;"><?php _e('Upload file', 'wp2print'); ?></a>
					<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="upload-loading" style="display:none;">
					<input type="hidden" name="mfile2" class="ufile-url">
				</div>
			</div>
			<div id="upload-file-3" class="print-products-area" style="margin-top:20px; border:1px solid #C1C1C1; padding:20px; display:none;">
				<p style="margin:0;"><strong class="lbl-text"></strong> <span class="file-list"></span> <span class="file-uprocess"></span></p>
				<div class="artwork-buttons">
					<a id="select-button-3" href="#select-file" class="artwork-select"><?php _e('Select file', 'wp2print'); ?></a>
					<a id="upload-button-3" href="#upload-file" class="artwork-upload" style="display:none;"><?php _e('Upload file', 'wp2print'); ?></a>
					<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="upload-loading" style="display:none;">
					<input type="hidden" name="mfile3" class="ufile-url">
				</div>
			</div>
			<div id="upload-file-4" class="print-products-area" style="margin-top:20px; border:1px solid #C1C1C1; padding:20px; display:none;">
				<p style="margin:0;"><strong class="lbl-text"></strong> <span class="file-list"></span> <span class="file-uprocess"></span></p>
				<div class="artwork-buttons">
					<a id="select-button-4" href="#select-file" class="artwork-select"><?php _e('Select file', 'wp2print'); ?></a>
					<a id="upload-button-4" href="#upload-file" class="artwork-upload" style="display:none;"><?php _e('Upload file', 'wp2print'); ?></a>
					<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="upload-loading" style="display:none;">
					<input type="hidden" name="mfile4" class="ufile-url">
				</div>
			</div>
			<div id="uacontainer" class="artwork-buttons">
				<a id="continuebtn" href="javascript:;" class="artwork-continue"><?php _e('CONTINUE >>', 'wp2print'); ?></a>
				<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="upload-loading" style="display:none;">
				<?php do_action('print_products_upload_artwork_buttons'); ?>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/plupload.full.min.js?ver=3.1.2"></script>
	<script type="text/javascript">
	<!--
	jQuery(document).ready(function() {
		var mfu_usage = '<?php echo $multiple_file_data['usage']; ?>';
		var mfu_files_num = 1;
		var mfu_files_labels = []; mfu_files_labels[1] = ''; mfu_files_labels[2] = ''; mfu_files_labels[3] = ''; mfu_files_labels[4] = '';
		var mfu_colours_data = [];
		<?php if ($multiple_file_data['usage'] == 'colour') { foreach ($multiple_file_data as $mfdkey => $mfdata) { $fch = substr($mfdkey, 0, 1);
			if ($fch != 'u' && $fch != 'n' && $fch != 'l') { $num = (int)$mfdata['num']; ?>
			mfu_colours_data[<?php echo $mfdkey; ?>] = [];
			mfu_colours_data[<?php echo $mfdkey; ?>]['num'] = <?php echo $num; ?>;
			mfu_colours_data[<?php echo $mfdkey; ?>]['label1'] = '<?php if (isset($mfdata['label1'])) { echo $mfdata['label1']; } ?>';
			mfu_colours_data[<?php echo $mfdkey; ?>]['label2'] = '<?php if (isset($mfdata['label2'])) { echo $mfdata['label2']; } ?>';
			mfu_colours_data[<?php echo $mfdkey; ?>]['label3'] = '<?php if (isset($mfdata['label3'])) { echo $mfdata['label3']; } ?>';
			mfu_colours_data[<?php echo $mfdkey; ?>]['label4'] = '<?php if (isset($mfdata['label4'])) { echo $mfdata['label4']; } ?>';
		<?php }}} ?>
		function mfu_files_upload_init() {
			if (mfu_usage == 'colour') {
				var clrval = jQuery('.add-cart-form .smatrix-colour').eq(0).val();
				if (mfu_colours_data[clrval] != undefined) {
					mfu_files_num = mfu_colours_data[clrval]['num'];
					mfu_files_labels[1] = mfu_colours_data[clrval]['label1'];
					mfu_files_labels[2] = mfu_colours_data[clrval]['label2'];
					mfu_files_labels[3] = mfu_colours_data[clrval]['label3'];
					mfu_files_labels[4] = mfu_colours_data[clrval]['label4'];
				}
			} else {
				mfu_files_num = <?php echo (int)$multiple_file_data['num']; ?>;
				mfu_files_labels[1] = '<?php if (isset($multiple_file_data['label1'])) { echo $multiple_file_data['label1']; } ?>';
				mfu_files_labels[2] = '<?php if (isset($multiple_file_data['label2'])) { echo $multiple_file_data['label2']; } ?>';
				mfu_files_labels[3] = '<?php if (isset($multiple_file_data['label3'])) { echo $multiple_file_data['label3']; } ?>';
				mfu_files_labels[4] = '<?php if (isset($multiple_file_data['label4'])) { echo $multiple_file_data['label4']; } ?>';
			}
			jQuery('#upload-file-1 .lbl-text').html(mfu_files_labels[1]+':');
			jQuery('#upload-file-2 .lbl-text').html(mfu_files_labels[2]+':');
			jQuery('#upload-file-3 .lbl-text').html(mfu_files_labels[3]+':');
			jQuery('#upload-file-4 .lbl-text').html(mfu_files_labels[4]+':');
			jQuery('.add-cart-form .mfu-data').val(mfu_files_num+';'+mfu_files_labels[1]+';'+mfu_files_labels[2]+';'+mfu_files_labels[3]+';'+mfu_files_labels[4]);
			mfu_files_upload_clear();
		}
		jQuery('.upload-artwork-btn').click(function(){
			if (jQuery(this).hasClass('ch-price')) {
				if (price < 0) { return false; }
			}
			mfu_files_upload_init();
			if (mfu_files_num == 3) {
				jQuery('#upload-artwork-wrap').css('height', '505px');
			} else if (mfu_files_num == 4) {
				jQuery('#upload-artwork-wrap').css('height', '660px');
			}
			jQuery.colorbox({inline:true, href:"#upload-artwork-wrap"});
		});
		jQuery('#upload-button-1').click(function(){
			if (ufadded1) {
				uploader1.start();
			}
			return false;
		});
		jQuery('#upload-button-2').click(function(){
			if (ufadded2) {
				uploader2.start();
			}
			return false;
		});
		jQuery('#upload-button-3').click(function(){
			if (ufadded3) {
				uploader3.start();
			}
			return false;
		});
		jQuery('#upload-button-4').click(function(){
			if (ufadded4) {
				uploader4.start();
			}
			return false;
		});
		jQuery('#continuebtn').click(function(){
			jQuery('.atc-action').val('artwork');
			jQuery.colorbox.close();
			jQuery('.add-cart-form').submit();
			return false;
		});
		var ufilenum1 = 0;
		var ufilenum2 = 0;
		var ufilenum3 = 0;
		var ufilenum4 = 0;
		var ufadded1 = false;
		var ufadded2 = false;
		var ufadded3 = false;
		var ufadded4 = false;
		var uploader1 = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			file_data_name: 'file',
			browse_button : 'select-button-1', // you can pass an id...
			container: document.getElementById('upload-file-1'), // ... or DOM Element itself
			flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
			silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
			drop_element: document.getElementById('upload-file-1'), // ... or DOM Element itself
			url : '<?php echo $plupload_url; ?>',
			dragdrop: true,
			multi_selection: false,
			filters : {
				max_file_size : '<?php echo $file_upload_max_size; ?>mb',
				mime_types: [<?php echo $umime_types; ?>]
			},
			<?php if ($upload_to == 'amazon') { ?>
			multipart: true,
			<?php echo $multiparams; ?>
			<?php } ?>
			init: {
				PostInit: function() {
					jQuery('#continuebtn').hide();
				},
				FilesAdded: function(up, files) {
					var ucounterror = false;
					ufadded1 = true;
					plupload.each(files, function(file) {
						file.name = wp2print_clear_fname(file.name);
						jQuery('#upload-file-1 .file-list').html(file.name + ' (' + plupload.formatSize(file.size) + ')');
						jQuery('#upload-file-1 .file-uprocess').html('');
					});
					jQuery('#upload-button-1').show();
					jQuery('#continuebtn').hide();
				},
				UploadProgress: function(up, file) {
					jQuery('#upload-file-1 .file-uprocess').html(file.percent + '%');
				},
				<?php if ($upload_to == 'amazon') { ?>
				BeforeUpload: function(up, file) {
					var regex = /(?:\.([^.]+))?$/;
					var ext = regex.exec(file.name)[1];
					if (ext == 'pdf') {
						up.settings.multipart_params['Content-Type'] = 'application/pdf';
					} else {
						up.settings.multipart_params['Content-Type'] = file.type;
					}
					up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
					<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
					up.settings.multipart_params['Content-Disposition'] = 'attachment';
					jQuery('#upload-file-1 .upload-loading').show();
				},
				<?php } ?>
				FileUploaded: function(up, file, response) {
					<?php if ($upload_to == 'amazon') { ?>
						var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
					<?php } else { ?>
						var ufileurl = response['response'];
					<?php } ?>
					if (ufileurl != '') {
						jQuery('#upload-file-1 .ufile-url').val(ufileurl);
					}
				},
				UploadComplete: function(files) {
					ufadded1 = false;
					jQuery('#upload-file-1 .upload-loading').hide();
					jQuery('#upload-button-1').hide();
					wp2print_set_artwork_files();

					if (mfu_files_num > 1) {
						jQuery('#upload-file-2').slideDown(200);
					} else {
						jQuery('#continuebtn').show();
					}
				},
				Error: function(up, err) {
					alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
				}
			}
		});
		// -------------------------------------
		var uploader2 = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			file_data_name: 'file',
			browse_button : 'select-button-2', // you can pass an id...
			container: document.getElementById('upload-file-2'), // ... or DOM Element itself
			flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
			silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
			drop_element: document.getElementById('upload-file-2'), // ... or DOM Element itself
			url : '<?php echo $plupload_url; ?>',
			dragdrop: true,
			multi_selection: false,
			filters : {
				max_file_size : '<?php echo $file_upload_max_size; ?>mb',
				mime_types: [<?php echo $umime_types; ?>]
			},
			<?php if ($upload_to == 'amazon') { ?>
			multipart: true,
			<?php echo $multiparams; ?>
			<?php } ?>
			init: {
				PostInit: function() {
					jQuery('#continuebtn').hide();
				},
				FilesAdded: function(up, files) {
					var ucounterror = false;
					ufadded2 = true;
					plupload.each(files, function(file) {
						file.name = wp2print_clear_fname(file.name);
						jQuery('#upload-file-2 .file-list').html(file.name + ' (' + plupload.formatSize(file.size) + ')');
						jQuery('#upload-file-2 .file-uprocess').html('');
					});
					jQuery('#upload-button-2').show();
					jQuery('#continuebtn').hide();
				},
				UploadProgress: function(up, file) {
					jQuery('#upload-file-2 .file-uprocess').html(file.percent + '%');
				},
				<?php if ($upload_to == 'amazon') { ?>
				BeforeUpload: function(up, file) {
					var regex = /(?:\.([^.]+))?$/;
					var ext = regex.exec(file.name)[1];
					if (ext == 'pdf') {
						up.settings.multipart_params['Content-Type'] = 'application/pdf';
					} else {
						up.settings.multipart_params['Content-Type'] = file.type;
					}
					up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
					<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
					up.settings.multipart_params['Content-Disposition'] = 'attachment';
					jQuery('#upload-file-2 .upload-loading').show();
				},
				<?php } ?>
				FileUploaded: function(up, file, response) {
					<?php if ($upload_to == 'amazon') { ?>
						var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
					<?php } else { ?>
						var ufileurl = response['response'];
					<?php } ?>
					if (ufileurl != '') {
						jQuery('#upload-file-2 .ufile-url').val(ufileurl);
					}
				},
				UploadComplete: function(files) {
					ufadded2 = false;
					jQuery('#upload-file-2 .upload-loading').hide();
					jQuery('#upload-button-2').hide();
					wp2print_set_artwork_files();

					if (mfu_files_num > 2) {
						jQuery('#upload-file-3').slideDown(200);
					} else {
						jQuery('#continuebtn').show();
					}
				},
				Error: function(up, err) {
					alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
				}
			}
		});
		// -------------------------------------
		var uploader3 = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			file_data_name: 'file',
			browse_button : 'select-button-3', // you can pass an id...
			container: document.getElementById('upload-file-3'), // ... or DOM Element itself
			flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
			silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
			drop_element: document.getElementById('upload-file-3'), // ... or DOM Element itself
			url : '<?php echo $plupload_url; ?>',
			dragdrop: true,
			multi_selection: false,
			filters : {
				max_file_size : '<?php echo $file_upload_max_size; ?>mb',
				mime_types: [<?php echo $umime_types; ?>]
			},
			<?php if ($upload_to == 'amazon') { ?>
			multipart: true,
			<?php echo $multiparams; ?>
			<?php } ?>
			init: {
				PostInit: function() {
					jQuery('#continuebtn').hide();
				},
				FilesAdded: function(up, files) {
					var ucounterror = false;
					ufadded3 = true;
					plupload.each(files, function(file) {
						file.name = wp2print_clear_fname(file.name);
						jQuery('#upload-file-3 .file-list').html(file.name + ' (' + plupload.formatSize(file.size) + ')');
						jQuery('#upload-file-3 .file-uprocess').html('');
					});
					jQuery('#upload-button-3').show();
					jQuery('#continuebtn').hide();
				},
				UploadProgress: function(up, file) {
					jQuery('#upload-file-3 .file-uprocess').html(file.percent + '%');
				},
				<?php if ($upload_to == 'amazon') { ?>
				BeforeUpload: function(up, file) {
					var regex = /(?:\.([^.]+))?$/;
					var ext = regex.exec(file.name)[1];
					if (ext == 'pdf') {
						up.settings.multipart_params['Content-Type'] = 'application/pdf';
					} else {
						up.settings.multipart_params['Content-Type'] = file.type;
					}
					up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
					<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
					up.settings.multipart_params['Content-Disposition'] = 'attachment';
					jQuery('#upload-file-3 .upload-loading').show();
				},
				<?php } ?>
				FileUploaded: function(up, file, response) {
					<?php if ($upload_to == 'amazon') { ?>
						var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
					<?php } else { ?>
						var ufileurl = response['response'];
					<?php } ?>
					if (ufileurl != '') {
						jQuery('#upload-file-3 .ufile-url').val(ufileurl);
					}
				},
				UploadComplete: function(files) {
					ufadded3 = false;
					jQuery('#upload-file-3 .upload-loading').hide();
					jQuery('#upload-button-3').hide();
					wp2print_set_artwork_files();

					if (mfu_files_num > 3) {
						jQuery('#upload-file-4').slideDown(200);
					} else {
						jQuery('#continuebtn').show();
					}
				},
				Error: function(up, err) {
					alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
				}
			}
		});
		// -------------------------------------
		var uploader4 = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			file_data_name: 'file',
			browse_button : 'select-button-4', // you can pass an id...
			container: document.getElementById('upload-file-4'), // ... or DOM Element itself
			flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
			silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
			drop_element: document.getElementById('upload-file-4'), // ... or DOM Element itself
			url : '<?php echo $plupload_url; ?>',
			dragdrop: true,
			multi_selection: false,
			filters : {
				max_file_size : '<?php echo $file_upload_max_size; ?>mb',
				mime_types: [<?php echo $umime_types; ?>]
			},
			<?php if ($upload_to == 'amazon') { ?>
			multipart: true,
			<?php echo $multiparams; ?>
			<?php } ?>
			init: {
				PostInit: function() {
					jQuery('#continuebtn').hide();
				},
				FilesAdded: function(up, files) {
					var ucounterror = false;
					ufadded4 = true;
					plupload.each(files, function(file) {
						file.name = wp2print_clear_fname(file.name);
						jQuery('#upload-file-4 .file-list').html(file.name + ' (' + plupload.formatSize(file.size) + ')');
						jQuery('#upload-file-4 .file-uprocess').html('');
					});
					jQuery('#upload-button-4').show();
					jQuery('#continuebtn').hide();
				},
				UploadProgress: function(up, file) {
					jQuery('#upload-file-4 .file-uprocess').html(file.percent + '%');
				},
				<?php if ($upload_to == 'amazon') { ?>
				BeforeUpload: function(up, file) {
					var regex = /(?:\.([^.]+))?$/;
					var ext = regex.exec(file.name)[1];
					if (ext == 'pdf') {
						up.settings.multipart_params['Content-Type'] = 'application/pdf';
					} else {
						up.settings.multipart_params['Content-Type'] = file.type;
					}
					up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
					<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
					up.settings.multipart_params['Content-Disposition'] = 'attachment';
					jQuery('#upload-file-4 .upload-loading').show();
				},
				<?php } ?>
				FileUploaded: function(up, file, response) {
					<?php if ($upload_to == 'amazon') { ?>
						var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
					<?php } else { ?>
						var ufileurl = response['response'];
					<?php } ?>
					if (ufileurl != '') {
						jQuery('#upload-file-4 .ufile-url').val(ufileurl);
					}
				},
				UploadComplete: function(files) {
					ufadded2 = false;
					jQuery('#upload-file-4 .upload-loading').hide();
					jQuery('#upload-button-4').hide();
					wp2print_set_artwork_files();

					jQuery('#continuebtn').show();
				},
				Error: function(up, err) {
					alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
				}
			}
		});
		uploader1.init();
		uploader2.init();
		uploader3.init();
		uploader4.init();
	});
	function mfu_files_upload_clear() {
		for (var u=1; u<=4; u++) {
			jQuery('#upload-file-'+u+' .file-list').html('');
			jQuery('#upload-file-'+u+' .file-uprocess').html('');
			jQuery('#upload-file-'+u+' .ufile-url').val('');
		}
		jQuery('#upload-file-2').hide();
		jQuery('#upload-file-3').hide();
		jQuery('#upload-file-4').hide();
		jQuery('#continuebtn').hide();

		wp2print_set_artwork_files();
	}
	function wp2print_set_artwork_files() {
		var afiles = '';
		for (var u=1; u<=4; u++) {
			var furl = jQuery('#upload-file-'+u+' .ufile-url').val();
			if (furl != '') {
				if (afiles != '') { afiles = afiles + ';'; }
				afiles = afiles + furl;
			}
		}
		jQuery('.add-cart-form .artwork-files').val(afiles);
	}
	//--></script>
	<?php do_action('print_products_upload_artwork_script'); ?>
