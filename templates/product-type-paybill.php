<?php
global $product, $wpdb;
$currency_symbol = get_woocommerce_currency_symbol();
$product_id = $product->get_id();
$paybill_options = get_post_meta($product_id, '_paybill_options', true);
if (!is_array($paybill_options)) { $paybill_options = array('invoice-number-label' => __('Invoice number', 'wp2print'), 'amount-label' => __('Amount', 'wp2print')); }

$is_modify = false;
$pb_invoice_number = '';
$pb_amount = '';
if (isset($_GET['modify']) && strlen($_GET['modify'])) {
	$cart_item_key = $_GET['modify'];
	$prod_cart_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_cart_data WHERE cart_item_key = '%s'", $wpdb->prefix, $cart_item_key));
	if ($prod_cart_data) {
		$is_modify = true;
		$additional = unserialize($prod_cart_data->additional);
		$pb_invoice_number = $additional['invoice_number'];
		$pb_amount = $additional['amount'];
	}
}
?>
<div class="print-products-area product-attributes" style="margin:0 0 15px 0;">
	<form method="POST" class="add-cart-form" onsubmit="return products_add_cart_action();">
		<ul class="product-attributes-list pb-fields">
			<li>
				<label><?php echo $paybill_options['invoice-number-label']; ?>:</label><br />
				<input type="text" name="pb_invoice_number" class="pb-invoice-number" value="<?php echo $pb_invoice_number; ?>">
				<input type="hidden" name="pb_invoice_number_label" class="pb-invoice-number-label" value="<?php echo $paybill_options['invoice-number-label']; ?>">
			</li>
			<li>
				<label><?php echo $paybill_options['amount-label']; ?>:</label><br />
				<span class="pb-curr"><?php echo $currency_symbol; ?></span>
				<input type="text" name="pb_amount" class="pb-amount" value="<?php echo $pb_amount; ?>">
			</li>
		</ul>
	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
		<div class="product-add-button cf">
			<input type="hidden" name="quantity" value="1">
			<?php if ($is_modify) { ?>
				<input type="hidden" name="print_products_checkout_process_action" value="update-cart">
				<input type="hidden" name="product_type" value="paybill" class="product-type">
				<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" class="product-id">
				<input type="hidden" name="cart_item_key" value="<?php echo $cart_item_key; ?>">
				<input type="submit" value="<?php _e('Update cart', 'wp2print'); ?>" class="single_add_to_cart_button <?php print_products_buttons_class(); ?> update-cart-btn" onclick="return products_add_cart_action();">
			<?php } else { ?>
				<input type="hidden" name="print_products_checkout_process_action" value="add-to-cart">
				<input type="hidden" name="product_type" value="paybill" class="product-type">
				<input type="hidden" name="product_id" value="<?php echo $product_id; ?>" class="product-id">
				<input type="hidden" name="add-to-cart" value="<?php echo $product_id; ?>">
				<input type="submit" value="<?php _e('Pay bill', 'wp2print'); ?>" class="single_add_to_cart_button button alt simple-add-btn" onclick="return products_add_cart_action();">
			<?php } ?>
			<div class="pb-required"><?php _e('All fields are required.', 'wp2print'); ?></div>
		</div>
		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>
</div>
<script>
<!--
function products_add_cart_action() {
	var pb_error = false;
	var pb_invoice_number = jQuery('form.add-cart-form .pb-invoice-number').val();
	var pb_amount = jQuery('form.add-cart-form .pb-amount').val();
	if (pb_invoice_number == '') {
		pb_error = true;
		alert("<?php echo $paybill_options['invoice-number-label']; ?> <?php _e('is required field', 'wp2print'); ?>.");
	} else if (pb_amount == '') {
		pb_error = true;
		alert("<?php echo $paybill_options['amount-label']; ?> <?php _e('is required field', 'wp2print'); ?>.");
	} else {
		pb_amount = parseFloat(pb_amount);
		if (pb_amount <= 0) {
			pb_error = true;
			alert("<?php echo $paybill_options['amount-label']; ?> <?php _e('is incorrect', 'wp2print'); ?>.");
		}
	}
	if (pb_error) {
		return false;
	}
}
//--></script>