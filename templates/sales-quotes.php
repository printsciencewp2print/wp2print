<?php
global $current_user, $wpdb;

$qo_per_page = 20;
$qo_page = isset($_GET['sqpage']) ? (int)$_GET['sqpage'] : 1;
$qo_start = ($qo_page - 1) * $qo_per_page;

$quote_orders = $wpdb->get_results(sprintf("SELECT SQL_CALC_FOUND_ROWS * FROM %sprint_products_quotes WHERE user_id = %s ORDER BY order_id DESC LIMIT %s, %s", $wpdb->prefix, $current_user->ID, $qo_start, $qo_per_page));
$quote_orders_total = $wpdb->get_var("SELECT FOUND_ROWS()");
$quote_orders_pages = 1;
if ($quote_orders_total) {
	$quote_orders_pages = ceil($quote_orders_total / $qo_per_page);
}
$page_ul = $_SERVER['REQUEST_URI'];
if (strpos($page_ul, '?')) { $page_ul = substr($page_ul, 0, strpos($page_ul, '?')); }

$pay_now_text = __('Pay now', 'wp2print');
$print_products_send_quote_options = get_option("print_products_send_quote_options");
if (isset($print_products_send_quote_options['pay_now_text']) && strlen($print_products_send_quote_options['pay_now_text'])) {
	$pay_now_text = $print_products_send_quote_options['pay_now_text'];
}
?>
	<div class="wrap orders-awaiting-approval-wrap">
		<table class="woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
			<thead>
				<tr>
					<th scope="col" class="manage-column" style="width:60px;"><?php _e('QuoteID', 'wp2print'); ?></th>
					<th scope="col" class="manage-column"><?php _e('Product', 'wp2print'); ?></th>
					<th scope="col" class="manage-column"><?php _e('Qty', 'wp2print'); ?></th>
					<th scope="col" class="manage-column"><?php _e('Price', 'wp2print'); ?></th>
					<th scope="col" class="manage-column"><?php _e('Date created', 'wp2print'); ?></th>
					<th scope="col" class="manage-column"><?php _e('Date expired', 'wp2print'); ?></th>
					<th scope="col" class="manage-column" style="text-align:center;"><?php _e('Status', 'wp2print'); ?></th>
					<th scope="col" class="manage-column">&nbsp;</th>
				</tr>
			</thead>
			<tbody id="the-list">
				<?php if ($quote_orders) { $dnow = current_time('mysql');
					foreach($quote_orders as $quote_order) {
						$order_id = $quote_order->order_id;
						$expire_date = strlen($quote_order->expire_date) ? $quote_order->expire_date : '';
						$converted_order = 0;
						if ($quote_order->status == 1) {
							if ($quote_order->wc_order_id) {
								$wc_order = wc_get_order($quote_order->wc_order_id);
								if ($wc_order) {
									$converted_order = $quote_order->wc_order_id;
								}
							}
						}
						$allow_paynow = true;
						if (strlen($expire_date)) {
							if (strtotime($expire_date) < strtotime($dnow)) {
								$allow_paynow = false;
							}
						}

						$quote_order_items = $wpdb->get_results(sprintf("SELECT qi.*, p.post_title FROM %sprint_products_quotes_items qi INNER JOIN %sposts p ON p.ID = qi.product_id WHERE qi.order_id = %s", $wpdb->prefix, $wpdb->prefix, $order_id));
						if ($quote_order_items) {
							foreach($quote_order_items as $quote_order_item) { ?>
								<tr>
									<td>#<?php echo $order_id; ?></td>
									<td><?php echo $quote_order_item->post_title; ?></td>
									<td><?php echo $quote_order_item->quantity; ?></td>
									<td><?php echo wc_price($quote_order_item->price); ?></td>
									<td><?php echo date('Y-m-d', strtotime($quote_order->created)); ?></td>
									<td><?php if (strlen($expire_date)) { echo $expire_date; } else { echo '-'; } ?></td>
									<?php if ($quote_order->status == 1 && $converted_order) { ?>
										<td style="text-align:center;"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL . 'images/icon-yes.svg'; ?>" alt="" style="width:18px;"></td>
										<td><a href="<?php echo wc_get_account_endpoint_url( 'view-order' ) . $converted_order.'/'; ?>"><?php echo $converted_order; ?></a></td>
									<?php } else { ?>
										<?php if ($allow_paynow) { ?>
											<td style="text-align:center;">&nbsp;</td>
											<td><a href="<?php echo site_url('?qorder=true&uid='.md5($current_user->ID).'&oid='.md5($order_id)); ?>" class="woocommerce-button button" target="_blank"><?php echo $pay_now_text; ?></a></td>
										<?php } else { ?>
											<td style="text-align:center;"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL . 'images/icon-no.svg'; ?>" alt="" style="width:18px;"></td>
											<td>&nbsp;</td>
										<?php } ?>
									<?php } ?>
								</tr>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td colspan="8"><?php _e('No sales quotes.', 'wp2print'); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php if ($quote_orders_pages > 1) { ?>
			<div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
				<?php if ($qo_page > 1) { ?><a href="<?php echo site_url($page_ul); ?>?sqpage=<?php echo $qo_page - 1; ?>" class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button"><?php _e('Previous', 'wp2print'); ?></a><?php } ?>
				<?php if (($qo_page + 1) <= $quote_orders_pages) { ?><a href="<?php echo site_url($page_ul); ?>?sqpage=<?php echo $qo_page + 1; ?>" class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button"><?php _e('Next', 'wp2print'); ?></a><?php } ?>
			</div>
		<?php } ?>
	</div>
