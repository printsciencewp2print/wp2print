<?php
global $current_user;
$show_uploaded_files = get_post_meta($product_id, '_show_uploaded_files', true);
$file_upload_max_size = get_option('print_products_file_upload_max_size');
$file_upload_target = get_option("print_products_file_upload_target");
$amazon_s3_settings = get_option("print_products_amazon_s3_settings");
$autoproof_options = get_option("print_products_autoproof_options");
$preflight_options = get_post_meta($product_id, '_preflight_options', true);

if (!$file_upload_max_size) { $file_upload_max_size = 2; }
if (!$artwork_file_count) { $artwork_file_count = 25; }
if (!is_array($artwork_afile_types)) { $artwork_afile_types = array('all'); }
if (!count($artwork_afile_types)) { $artwork_afile_types = array('all'); }

$umime_types = '';
if (!in_array('all', $artwork_afile_types)) {
	$umime_types = '{title : "Specific files", extensions : "'.implode(',', $artwork_afile_types).'"}';
	$umime_types = str_replace('jpg/jpeg', 'jpg,jpeg', $umime_types);
}

$upload_to = 'host';
$plupload_url = get_bloginfo('url').'/index.php?ajaxupload=artwork&sessid='.session_id();
if ($file_upload_target == 'amazon' && $amazon_s3_settings['s3_access_key'] && $amazon_s3_settings['s3_secret_key']) {
	$upload_to = 'amazon';

	$s3_data = print_products_amazon_s3_get_data($amazon_s3_settings, $file_upload_max_size);
	$s3path = $s3_data['s3path'];
	$is_s3_region = $s3_data['is_s3_region'];
	$plupload_url = $s3_data['amazon_url'];
	$amazon_file_url = $s3_data['amazon_file_url'];
	$multiparams = $s3_data['multiparams'];
}
$autoproof_enabled = ($autoproof_options && isset($autoproof_options['enable']) && $autoproof_options['enable'] == 1) ? true : false;
?>
	<div style="position:absolute;left:-20000px;">
		<div id="upload-artwork" class="upload-artwork-block print-products-area" style="margin:30px; border:1px solid #C1C1C1; padding:10px 15px; width:620px; height:470px;overflow:auto;position:relative;">
			<p style="margin:0 0 12px;"><?php _e('Please select artwork files', 'wp2print'); ?>:</p>
			<div id="filelist" class="ua-files-list" style="padding:10px 0; border-top:1px solid #C1C1C1; border-bottom:1px solid #C1C1C1;">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
			<div class="pf-checking" style="display:none;"></div>
			<div class="artwork-message" style="display:none;"></div>
			<div id="uacontainer" class="artwork-buttons">
				<a id="pickfiles" href="javascript:;" class="artwork-select"><?php _e('Select files', 'wp2print'); ?></a>
				<a id="uploadfiles" href="javascript:;" class="artwork-upload"><?php _e('Upload files', 'wp2print'); ?></a>
				<a id="cancelbtn" href="javascript:;" class="artwork-cancel" style="display:none;"><?php _e('Cancel', 'wp2print'); ?></a>
				<a id="continuebtn" href="javascript:;" class="artwork-continue"><?php _e('CONTINUE >>', 'wp2print'); ?></a>
				<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="upload-loading" style="display:none;">
				<?php do_action('print_products_upload_artwork_buttons'); ?>
			</div>
			<?php if (is_user_logged_in() && $show_uploaded_files == 1) {
				$user_files = array();
				$admin_user_files = print_products_user_files_get_admin_uploaded_files($current_user->ID);
				$user_uploaded_files = print_products_user_files_get_uploaded_files($current_user->ID);
				if ($admin_user_files) {
					foreach($admin_user_files as $admin_user_file) {
						$user_files[] = array('file_url' => $admin_user_file->file_url, 'created' => $admin_user_file->created);
					}
				}
				if ($user_uploaded_files) {
					foreach($user_uploaded_files as $user_uploaded_file) {
						$user_files[] = array('file_url' => $user_uploaded_file['file_url'], 'created' => $user_uploaded_file['created']);
					}
				}
				if (count($user_files)) { ?>
					<div class="uu-files">
						<p style="margin:0 0 12px;"><?php _e('Use previously uploaded file', 'wp2print'); ?>:</p>
						<table cellspacing="0" cellpadding="0" style="width:100%;">
							<?php foreach($user_files as $user_file) { ?>
							<tr>
								<td><a href="<?php echo print_products_get_amazon_file_url($user_file['file_url']); ?>" class="uu-flink" target="_blank"><?php echo basename($user_file['file_url']); ?></a></td>
								<td nowrap><?php echo date('Y-m-d H:i', strtotime($user_file['created'])); ?></td>
								<td nowrap><a href="#use-file" class="button uu-use-file" data-file="<?php echo $user_file['file_url']; ?>"><?php _e('Select file', 'wp2print'); ?></a></td>
							</tr>
							<?php } ?>
						</table>
					</div>
				<?php }
				?>
			<?php } ?>
			<div class="continue-loading" style="position:absolute;left:0;top:0;right:0;bottom:0;z-index:10000;display:none;">
				<div class="c-loading" style="background:#FFF;opacity:0.95;width:100%;height:100%;padding:10px 15px;"></div>
				<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/loader.gif" style="position:fixed;z-index:100000;top:49%;left:49%;">
			</div>
		</div>
		<?php if ($autoproof_enabled) {
			$button_text = (isset($autoproof_options['button_text']) && $autoproof_options['button_text']) ? $autoproof_options['button_text'] : __('Approve', 'wp2print');
			?>
			<div id="autoproof-popup" class="autoproof-popup print-products-area" style="margin:30px; width:620px; height:450px;overflow:auto;">
				<div class="autoproof-gallery-wrap">
					<a href="#left" class="ag-left"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/s-left.png"></a>
					<div class="autoproof-gallery"></div>
					<a href="#right" class="ag-right"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/s-right.png"></a>
				</div>
				<div class="autoproof-footer">
					<?php echo wpautop($autoproof_options['acceptance_text']); ?>
					<div class="autoproof-buttons artwork-buttons">
						<a href="#cancel" class="autoproof-cancel"><?php _e('Cancel', 'wp2print'); ?></a>
						<a href="#confirm" class="autoproof-confirm"><?php echo $button_text; ?></a>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
	<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/pdf-parser.js?ver=1.0.15" data-optimized="0" data-no-optimize="1" data-no-defer="1"></script>
	<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/plupload.full.min.js?ver=3.1.2"></script>
	<script type="text/javascript">
	<!--
	var autoproof_snum = 1;
	var autoproof_pn_grabbed = true;
	var autoproof_page_nums = [];
	var autoproof_page = "<?php _e('Page', 'wp2print'); ?>";
	var autoproof_enabled = <?php if ($autoproof_enabled) { echo 'true'; } else { echo 'false'; } ?>;
	var autoproof_message = "<?php _e('Compiling proof of page %p of document %f.', 'wp2print'); ?>";
	var pf_customer_enable = <?php if ($preflight_options && isset($preflight_options['enable']) && $preflight_options['enable'] == 1) { echo '1'; } else { echo '0'; } ?>;
	var pf_prevent_cart = <?php if ($preflight_options && isset($preflight_options['prevent_cart']) && $preflight_options['prevent_cart'] == 1) { echo '1'; } else { echo '0'; } ?>;
	var pf_processed = true;
	var pf_psuccess = true;
	var pf_prnum = 0;
	var pf_files = [];
	jQuery(document).ready(function() {
		jQuery('.upload-artwork-btn').click(function(){
			if (jQuery(this).hasClass('ch-price')) {
				if (price < 0) { return false; }
			}
			jQuery.colorbox({inline:true, href:"#upload-artwork"});
		});
		jQuery('#uploadfiles').click(function(){
			if (ufadded) {
				uploader.start();
			}
			return false;
		});
		jQuery('#continuebtn').click(function(){
			jQuery('.atc-action').val('artwork');
			if (autoproof_enabled && matrix_is_pdf_exist()) {
				jQuery('.autoproof-gallery').html('');
				jQuery('#upload-artwork .continue-loading').show();
				var pn = 0, pg = 0, pimg = '', ffname = '';
				var pdfs = autoproof_get_pdf_files();
				var pdfs_pages = autoproof_get_pdf_pages(pdfs);
				jQuery.post(
					wp2print_siteurl,
					{
						ajax_autoproof: 'get-pdf-thumbs',
						pdf_files: pdfs,
						pdf_pages: pdfs_pages
					},
					function(data) {
						var ahtml = '';
						var pdf_data = JSON.parse(data);
						var pdf_thumbs = '';
						for (var f=0; f<pdf_data.length; f++) { pg = 0;
							for (var i=0; i<pdf_data[f].thumbs.length; i++) {
								pg = i + 1; pn = pn + 1;
								pimg = pdf_data[f].thumbs[i];
								if (pdf_thumbs != '') { pdf_thumbs += ';'; }
								pdf_thumbs += pimg;
								if (pn == 1) {
									jQuery('.autoproof-gallery').append('<div class="g-item g-item-'+pn+' i-active" data-fname="'+pdf_data[f].name+'" data-pg="'+pg+'"><a href="'+pimg+'" rel="prettyPhoto" data-rel="prettyPhoto[apg]"><img src="'+pimg+'" style="height:300px;" class="i-loading" data-iid="'+pn+'"></a><p>'+pdf_data[f].name+' ('+autoproof_page+' '+pg+')</p></div>');
									ffname = pdf_data[f].name;
								} else {
									jQuery('.autoproof-gallery').append('<div class="g-item g-item-'+pn+'" data-fname="'+pdf_data[f].name+'" data-pg="'+pg+'"><a href="'+pimg+'" rel="prettyPhoto" data-rel="prettyPhoto[apg]"><img src="'+pimg+'" style="height:300px;" class="i-loading" data-iid="'+pn+'"></a><p>'+pdf_data[f].name+' ('+autoproof_page+' '+pg+')</p></div>');
								}
							}
						}
						if (pdf_thumbs != '') {
							jQuery('form.add-cart-form').append('<input type="hidden" name="pdf_thumbs" class="pdf-thumbs">');
							jQuery('form.add-cart-form .pdf-thumbs').val(pdf_thumbs);
						}
						if (pn == 1) {
							jQuery('#autoproof-popup a.ag-left').css('visibility', 'hidden');
							jQuery('#autoproof-popup a.ag-right').css('visibility', 'hidden');
						} else {
							jQuery('#autoproof-popup a.ag-left').css('visibility', 'visible');
							jQuery('#autoproof-popup a.ag-right').css('visibility', 'visible');
						}
						var tlnum = 1;
						var amessage = autoproof_message.replace('%p', '1').replace('%f', ffname);
						jQuery('.continue-loading .c-loading').append('<div>'+amessage+'</div>');
						jQuery('.autoproof-gallery .g-item-1').addClass('tl-message');
						var aitimer = setInterval(function() {
							jQuery('.autoproof-gallery .g-item').each(function(){
								var pg_val = jQuery(this).data('pg');
								var fname_val = jQuery(this).data('fname');
								var tloaded = jQuery(this).hasClass('tl-message');

								var iobj = jQuery(this).find('img');
								var iid = iobj.attr('data-iid');
								var iclass = iobj.attr('class');
								var isrc = iobj.attr('src');

								if (tlnum == iid) {
									if (!tloaded) {
										amessage = autoproof_message.replace('%p', pg_val).replace('%f', fname_val);
										jQuery('.continue-loading .c-loading').append('<div>'+amessage+'</div>');
										jQuery('.autoproof-gallery .g-item-'+iid).addClass('tl-message');
									}
									var imgspinning = new Image();
									imgspinning.src = isrc;
									imgspinning.onload = function() {
										jQuery('.autoproof-gallery .g-item-'+iid+' img').attr('src', isrc).removeClass('i-loading');
										tlnum++;
									}
								}
							});
							if (!jQuery('.autoproof-gallery .i-loading').length) {
								clearInterval(aitimer);
								jQuery.colorbox({inline:true, href:"#autoproof-popup"});
								jQuery('#upload-artwork .continue-loading').hide();
								jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
									hook: 'data-rel',
									social_tools: false,
									theme: 'pp_woocommerce',
									horizontal_padding: 20,
									opacity: 0.8,
									deeplinking: false
								});
							}
						}, 2000);
					}
				);
			} else {
				jQuery.colorbox.close();
				jQuery('.add-cart-form').submit();
			}
			return false;
		});
		jQuery('#cancelbtn').click(function(){
			jQuery.colorbox.close();
			return false;
		});
		jQuery('#autoproof-popup .autoproof-confirm').click(function(){
			jQuery.colorbox.close();
			jQuery('.add-cart-form').submit();
			return false;
		});
		jQuery('#autoproof-popup .autoproof-cancel').click(function(){
			jQuery.colorbox.close();
			return false;
		});
		jQuery('#autoproof-popup a.ag-left').click(function(){
			var g_items = jQuery('.autoproof-gallery .g-item').length;
			autoproof_snum = autoproof_snum - 1;
			if (autoproof_snum < 1) { autoproof_snum = g_items; }
			jQuery('.autoproof-gallery .g-item').removeClass('i-active');
			jQuery('.autoproof-gallery .g-item-'+autoproof_snum).addClass('i-active');
			if (autoproof_snum == g_items) {
				jQuery('.autoproof-gallery .g-item-1').addClass('i-last-active');
			} else {
				jQuery('.autoproof-gallery .g-item-1').removeClass('i-last-active');
			}
			return false;
		});
		jQuery('#autoproof-popup a.ag-right').click(function(){
			var g_items = jQuery('.autoproof-gallery .g-item').length;
			autoproof_snum = autoproof_snum + 1;
			if (autoproof_snum > g_items) { autoproof_snum = 1; }
			jQuery('.autoproof-gallery .g-item').removeClass('i-active');
			jQuery('.autoproof-gallery .g-item-'+autoproof_snum).addClass('i-active');
			if (autoproof_snum == g_items) {
				jQuery('.autoproof-gallery .g-item-1').addClass('i-last-active');
			} else {
				jQuery('.autoproof-gallery .g-item-1').removeClass('i-last-active');
			}
			return false;
		});
		jQuery('.uu-files .uu-use-file').click(function(){
			var ufile = jQuery(this).data('file');
			jQuery('.add-cart-form .artwork-files').val(ufile);
			jQuery('#continuebtn').trigger('click');
			return false;
		});
		var ufilecount = <?php echo $artwork_file_count; ?>;
		var ufilenum = 0;
		var ufadded = false;
		var uploader = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			file_data_name: 'file',
			browse_button : 'pickfiles', // you can pass an id...
			container: document.getElementById('uacontainer'), // ... or DOM Element itself
			flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
			silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
			drop_element: document.getElementById('upload-artwork'), // ... or DOM Element itself
			url : '<?php echo $plupload_url; ?>',
			dragdrop: true,
			filters : {
				max_file_size : '<?php echo $file_upload_max_size; ?>mb',
				mime_types: [<?php echo $umime_types; ?>]
			},
			<?php if ($upload_to == 'amazon') { ?>
			multipart: true,
			<?php echo $multiparams; ?>
			<?php } ?>
			init: {
				PostInit: function() {
					jQuery('.add-cart-form .artwork-files').val('');
					jQuery('#filelist').html('').hide();
					jQuery('#uploadfiles').hide();
					jQuery('#cancelbtn').hide();
					jQuery('#continuebtn').hide();
				},
				FilesAdded: function(up, files) {
					var ucounterror = false;
					ufadded = true;
					jQuery('#filelist').show();
					plupload.each(files, function(file) {
						file.name = wp2print_clear_fname(file.name);
						ufilenum++;
						if (ufilenum <= ufilecount) {
							document.getElementById('filelist').innerHTML += '<div id="' + file.id + '" data-name="' + file.name + '" data-url="">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
						} else {
							ucounterror = true;
						}
					});
					jQuery('#uploadfiles').show();
					jQuery('#cancelbtn').hide();
					jQuery('#continuebtn').hide();
					jQuery('.pf-checking').hide();
					jQuery('.artwork-message').hide();
					if (ucounterror) {
						alert("<?php _e('Max files count is', 'wp2print'); ?> "+ufilecount);
					}
				},
				UploadProgress: function(up, file) {
					document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
				},
				<?php if ($upload_to == 'amazon') { ?>
				BeforeUpload: function(up, file) {
					var regex = /(?:\.([^.]+))?$/;
					var ext = regex.exec(file.name)[1];
					if (ext == 'pdf') {
						up.settings.multipart_params['Content-Type'] = 'application/pdf';
					} else {
						up.settings.multipart_params['Content-Type'] = file.type;
					}
					up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
					<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
					up.settings.multipart_params['Content-Disposition'] = 'attachment';
					jQuery('.upload-loading').show();
				},
				<?php } ?>
				FileUploaded: function(up, file, response) {
					<?php if ($upload_to == 'amazon') { ?>
						var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
					<?php } else { ?>
						var ufileurl = response['response'];
					<?php } ?>
					if (ufileurl != '') {
						var artworkfiles = jQuery('.add-cart-form .artwork-files').val();
						if (artworkfiles != '') { artworkfiles += ';'; }
						artworkfiles += ufileurl;
						jQuery('.add-cart-form .artwork-files').val(artworkfiles);
						jQuery('div#'+file.id).attr('data-url', ufileurl);
					}
					if (autoproof_enabled) {
						pdf_get_pages_number(file.name, ufileurl);
					}
				},
				UploadComplete: function(files) {
					ufadded = false;
					var pntimer = setInterval(function() {
						if (autoproof_pn_grabbed) {
							var show_continuebtn = true;
							if (jQuery('.add-cart-form .autofitter-action').length) {
								var ufiles = jQuery('.add-cart-form .artwork-files').val().split(';');
								if (ufiles.length == 1) {
									var furl = ufiles[0];
									var fext = furl.substring(furl.length - 3);
									if (fext == 'pdf' || fext == 'PDF') {
										show_continuebtn = false;
										jQuery('#pickfiles').hide(); jQuery('.moxie-shim').hide();
										jQuery('a.autofitter-btn').show();
									}
								}
							}
							if (show_continuebtn) {
								jQuery('.upload-loading').hide();
								jQuery('#uploadfiles').hide();
								if (pf_customer_enable == 1) {
									jQuery('#filelist div').each(function(){
										var fid = jQuery(this).attr('id');
										var fname = jQuery(this).attr('data-name');
										var furl = jQuery(this).attr('data-url');
										var fext = fname.substring(fname.length - 3);
										if (fext == 'pdf' || fext == 'PDF') {
											pf_files[pf_files.length] = fid+';'+furl+';'+fname;
										}
									});
									if (pf_files.length) {
										jQuery('#pickfiles').hide(); jQuery('.moxie-shim').hide();
										pf_prnum = 0;
										pdf_preflight_analize();
									} else {
										jQuery('#continuebtn').show();
									}
								} else {
									jQuery('#continuebtn').show();
								}
							}
							clearInterval(pntimer);
						}
					}, 1000);
				},
				Error: function(up, err) {
					alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
				}
			}
		});
		uploader.init();
	});
	function pdf_preflight_analize() {
		var aw = 0, ah = 0, szterm_id = 0;
		if (jQuery('.matrix-type-simple .print-attributes .smatrix-size').length) {
			szterm_id = jQuery('.matrix-type-simple .print-attributes .smatrix-size').val();
		}
		if (jQuery('.print-products-area input.width').length) {
			aw = jQuery('.print-products-area input.width').val();
		}
		if (jQuery('.print-products-area input.height').length) {
			ah = jQuery('.print-products-area input.height').val();
		}
		
		var pfdata = pf_files[pf_prnum].split(';');

		jQuery('.pf-checking').append('<div class="pf-ch-box pf-ch-box-'+pfdata[0]+'"><ul><li class="pfch-fname"><strong>'+pfdata[2]+'</strong> <span class="pfr-icon"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" alt=""></span></li><li class="pfch-note"><?php _e('File uploaded correctly, performing preflight analysis', 'wp2print'); ?></li></ul></div>').slideDown(100);

		jQuery.post(
			wp2print_siteurl,
			{
				preflight_process: 'true',
				atype: 'customer-analize',
				product_id: <?php echo $product_id; ?>,
				afile: pfdata[1],
				size_term_id: szterm_id,
				width: aw,
				height: ah
			},
			function(data) {
				var pficon = 'icon-no.svg';
				if (data) {
					var pfresult = JSON.parse(data);
					if (pfresult.status == 'PASS') {
						pficon = 'icon-yes.svg';
					} else {
						pf_psuccess = false;
					}

					var pfch_sf_icon = 'icon-yes.svg', pfch_mf_icon = 'icon-yes.svg', pfch_lr_icon = 'icon-yes.svg', pfch_ud_icon = 'icon-yes.svg';

					if (pfresult.small_font == '1') { pfch_sf_icon = 'icon-no.svg'; }
					if (pfresult.missing_font == '1') { pfch_mf_icon = 'icon-no.svg'; }
					if (pfresult.low_resolution == '1') { pfch_lr_icon = 'icon-no.svg'; }

					var pfch_html = '<li><?php _e('Checking for small fonts', 'wp2print'); ?> <span class="pfr-icon"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/'+pfch_sf_icon+'" alt="" style="width:18px;"></span></li>';
					pfch_html += '<li><?php _e('Checking for missing fonts', 'wp2print'); ?> <span class="pfr-icon"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/'+pfch_mf_icon+'" alt="" style="width:18px;"></span></li>';
					
					pfch_html += '<li><?php _e('Checking for low-resolution images', 'wp2print'); ?> <span class="pfr-icon"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/'+pfch_lr_icon+'" alt="" style="width:18px;"></span></li>';
					if (pfresult.required_dimensions != '') {
						pfch_html += '<li><?php _e('Required dimensions', 'wp2print'); ?>: <span class="pfr-size">'+pfresult.required_dimensions+'</span></li>';
					}
					if (pfresult.uploaded_dimensions != '') {
						if (pfresult.dimensions_incorrect == '1') { pfch_ud_icon = 'icon-no.svg'; }
						pfch_html += '<li><?php _e('Uploaded file dimensions', 'wp2print'); ?>: <span class="pfr-size">'+pfresult.uploaded_dimensions+'</span></li>';
						pfch_html += '<li><?php _e('Checking dimensions match', 'wp2print'); ?>: <span class="pfr-icon"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/'+pfch_ud_icon+'" alt="" style="width:18px;"></span></li>';
					}
					jQuery('.pf-checking .pf-ch-box-'+pfdata[0]+' ul li.pfch-note').remove();
					jQuery('.pf-checking .pf-ch-box-'+pfdata[0]+' ul').append(pfch_html);
				}
				jQuery('.pf-checking .pf-ch-box-'+pfdata[0]+' ul li.pfch-fname .pfr-icon').html('<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/'+pficon+'" alt="" style="width:20px;">');

				pf_prnum = pf_prnum + 1;
				if (pf_prnum >= pf_files.length) {
					if (pf_psuccess) {
						jQuery('#continuebtn').show();
					} else {
						var pf_message = "<?php _e("One of the files you are submitting is problematic. Click 'Cancel' if you would like to fix the problem yourself and submit later. Click 'Continue' to have us fix the problem.", 'wp2print'); ?>";
						if (pf_prevent_cart == 1) {
							pf_message = "<?php _e("Our system does not allow this item to be added to the shopping cart. Please fix the problem with the artwork files and submit later.", 'wp2print'); ?>";
						} else {
							jQuery('#continuebtn').show();
						}
						jQuery('#cancelbtn').show();
						jQuery('.artwork-message').html(pf_message).slideDown(100);
					}
				} else {
					pdf_preflight_analize();
				}
			}
		);
	}
	function pdf_get_pages_number(filename, fileurl) {
		var fext = filename.substring(filename.length - 3);
		if (fext == 'pdf' || fext == 'PDF') {
			autoproof_pn_grabbed = false;
			jQuery.post(
				wp2print_siteurl,
				{
					ajax_autoproof: 'get-pdf-url',
					pdf_url: fileurl
				},
				function(data) {
					PDFJS.getDocument(data).then(function(pdf) {
						autoproof_page_nums[fileurl] = pdf.numPages;
						autoproof_pn_grabbed = true;
					});
				}
			);
		}
	}
	function autoproof_get_pdf_pages(pdfs) {
		var pdf_pages = '';
		var pdfs_array = pdfs.split(';');
		for (var i=0; i<pdfs_array.length; i++) {
			if (pdf_pages != '') { pdf_pages = pdf_pages + ';'; }
			pdf_pages = pdf_pages + autoproof_page_nums[pdfs_array[i]];
		}
		return pdf_pages;
	}
	//--></script>
	<?php do_action('print_products_upload_artwork_script'); ?>
