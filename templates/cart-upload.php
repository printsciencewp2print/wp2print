<?php
global $current_user;
$cart = WC()->cart->get_cart();
$file_upload_max_size = get_option('print_products_file_upload_max_size');
$file_upload_target = get_option("print_products_file_upload_target");
$amazon_s3_settings = get_option("print_products_amazon_s3_settings");
$email_options = get_option("print_products_email_options");

if (!$file_upload_max_size) { $file_upload_max_size = 2; }

$upload_to = 'host';
$plupload_url = get_bloginfo('url').'/index.php?ajaxupload=artwork&sessid='.session_id();
if ($file_upload_target == 'amazon' && $amazon_s3_settings['s3_access_key'] && $amazon_s3_settings['s3_secret_key']) {
	$upload_to = 'amazon';

	$s3_data = print_products_amazon_s3_get_data($amazon_s3_settings, $file_upload_max_size);
	$s3path = $s3_data['s3path'];
	$is_s3_region = $s3_data['is_s3_region'];
	$plupload_url = $s3_data['amazon_url'];
	$amazon_file_url = $s3_data['amazon_file_url'];
	$multiparams = $s3_data['multiparams'];
}
if ($cart) { $is_cart_upload = false; ?>
<div style="position:absolute;left:-20000px;">
	<?php foreach ($cart as $cart_item_key => $values) {
		$product_id = apply_filters( 'woocommerce_cart_item_product_id', $values['product_id'], $values, $cart_item_key );
		$cart_upload_button = (int)get_post_meta($product_id, '_cart_upload_button', true);
		if ($cart_upload_button) { $is_cart_upload = true;
			$cart_upload_stage = (int)get_post_meta($product_id, '_cart_upload_stage', true);
			$cart_upload_artwork_label = get_post_meta($product_id, '_cart_upload_artwork_label', true);
			$cart_upload_artwork_required = (int)get_post_meta($product_id, '_cart_upload_artwork_required', true);
			$cart_upload_artwork_ftypes = get_post_meta($product_id, '_cart_upload_artwork_ftypes', true);
			$cart_upload_artwork_uploaded_files = (int)get_post_meta($product_id, '_cart_upload_artwork_uploaded_files', true);
			$cart_upload_database_label = get_post_meta($product_id, '_cart_upload_database_label', true);
			$cart_upload_database_required = (int)get_post_meta($product_id, '_cart_upload_database_required', true);
			$cart_upload_database_ftypes = get_post_meta($product_id, '_cart_upload_database_ftypes', true);
			$cart_upload_database_uploaded_files = (int)get_post_meta($product_id, '_cart_upload_database_uploaded_files', true);
			if (is_array($cart_upload_artwork_ftypes)) { $cart_upload_artwork_ftypes = implode(',', $cart_upload_artwork_ftypes); $cart_upload_artwork_ftypes = str_replace('jpg/jpeg', 'jpg,jpeg', $cart_upload_artwork_ftypes); }
			if (is_array($cart_upload_database_ftypes)) { $cart_upload_database_ftypes = implode(',', $cart_upload_database_ftypes); }
			$cu_stages = array(1);
			if ($cart_upload_stage == 2) { $cu_stages = array(1, 2); } else if ($cart_upload_stage == 1) { $cu_stages = array(2); }
			?>
			<div id="upload-artwork-<?php echo $cart_item_key; ?>" class="upload-artwork-block print-products-area upload-artwork-<?php echo $cart_item_key; ?>" style="margin:30px; border:1px solid #C1C1C1; padding:20px; width:600px; height:450px; overflow:auto;" data-upload-stage="<?php echo $cart_upload_stage; ?>" data-artwork-ftypes="<?php echo $cart_upload_artwork_ftypes; ?>" data-database-ftypes="<?php echo $cart_upload_database_ftypes; ?>" data-artwork-ufiles="<?php echo $cart_upload_artwork_uploaded_files; ?>" data-database-ufiles="<?php echo $cart_upload_database_uploaded_files; ?>">
				<?php if (!$cart_upload_artwork_required) { ?><div class="cu-skip-1" style="float:right;"><input type="button" value="<?php _e('Skip file upload', 'wp2print'); ?>" class="button" onclick="wp2print_cart_upload_skip(1)"></div><?php } ?>
				<div class="cu-continue-1" style="float:right;display:none;"><input type="button" value="<?php _e('Continue', 'wp2print'); ?>" class="button" onclick="wp2print_cart_upload_process()"></div>
				<?php if (strlen($cart_upload_artwork_label)) { ?><p class="cu-label-1" style="font-weight:700;margin:0 0 12px;"><?php echo $cart_upload_artwork_label; ?></p><?php } ?>

				<?php if (!$cart_upload_database_required) { ?><div class="cu-skip-2" style="float:right;"><input type="button" value="<?php _e('Skip file upload', 'wp2print'); ?>" class="button" onclick="wp2print_cart_upload_skip(2)"></div><?php } ?>
				<div class="cu-continue-2" style="float:right;display:none;"><input type="button" value="<?php _e('Continue', 'wp2print'); ?>" class="button" onclick="wp2print_cart_upload_process()"></div>
				<?php if (strlen($cart_upload_database_label)) { ?><p class="cu-label-2" style="font-weight:700;margin:0 0 12px;"><?php echo $cart_upload_database_label; ?></p><?php } ?>
				<p style="margin:0 0 12px;"><?php _e('Please select file(s)', 'wp2print'); ?>:</p>
				<div id="filelist-<?php echo $cart_item_key; ?>" class="ua-files-list" style="padding:10px 0; border-top:1px solid #C1C1C1; border-bottom:1px solid #C1C1C1;">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
				<?php foreach($cu_stages as $cu_stage) { ?>
					<div id="uacontainer-<?php echo $cart_item_key; ?>-<?php echo $cu_stage; ?>" class="artwork-buttons">
						<a id="pickfiles-<?php echo $cart_item_key; ?>-<?php echo $cu_stage; ?>" href="javascript:;" class="artwork-select"><?php _e('Select files', 'wp2print'); ?></a>
						<a id="uploadfiles-<?php echo $cart_item_key; ?>-<?php echo $cu_stage; ?>" href="javascript:;" class="artwork-upload"><?php _e('Upload files', 'wp2print'); ?></a>
						<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="upload-loading" style="display:none;">
					</div>
				<?php } ?>
				<?php if (is_user_logged_in() && ($cart_upload_artwork_uploaded_files == 1 || $cart_upload_database_uploaded_files == 1)) {
					$user_files = array();
					$admin_user_files = print_products_user_files_get_admin_uploaded_files($current_user->ID);
					$user_uploaded_files = print_products_user_files_get_uploaded_files($current_user->ID);
					if ($admin_user_files) {
						foreach($admin_user_files as $admin_user_file) {
							$user_files[] = array('file_url' => $admin_user_file->file_url, 'created' => $admin_user_file->created);
						}
					}
					if ($user_uploaded_files) {
						foreach($user_uploaded_files as $user_uploaded_file) {
							$user_files[] = array('file_url' => $user_uploaded_file['file_url'], 'created' => $user_uploaded_file['created']);
						}
					}
					$a_user_files = $user_files;
					$d_user_files = $user_files;
					if (count($user_files)) {
						if (strlen($cart_upload_artwork_ftypes)) {
							$aftypes = explode(',', $cart_upload_artwork_ftypes);
							foreach($a_user_files as $ufkey => $user_file) {
								$fname = basename($user_file['file_url']);
								$fext = substr($fname, strrpos($fname, '.') + 1);
								if (!in_array($fext, $aftypes)) {
									unset($a_user_files[$ufkey]);
								}
							}
						}
						if (strlen($cart_upload_database_ftypes)) {
							$aftypes = explode(',', $cart_upload_database_ftypes);
							foreach($d_user_files as $ufkey => $user_file) {
								$fname = basename($user_file['file_url']);
								$fext = substr($fname, strrpos($fname, '.') + 1);
								if (!in_array($fext, $aftypes)) {
									unset($d_user_files[$ufkey]);
								}
							}
						}
					}
					if (count($a_user_files) || count($d_user_files)) { ?>
						<?php if (count($a_user_files) && $cart_upload_artwork_uploaded_files == 1) { ?>
							<div class="uu-files cart-uu-files cart-uu-files-1">
								<p style="margin:0 0 12px;"><?php _e('Use previously uploaded file', 'wp2print'); ?>:</p>
								<table cellspacing="0" cellpadding="0" style="width:100%;">
									<?php foreach($a_user_files as $user_file) { ?>
									<tr>
										<td><a href="<?php echo print_products_get_amazon_file_url($user_file['file_url']); ?>" class="uu-flink" target="_blank"><?php echo basename($user_file['file_url']); ?></a></td>
										<td nowrap><?php echo date('Y-m-d H:i', strtotime($user_file['created'])); ?></td>
										<td nowrap><input type="button" class="button uu-use-file" data-file="<?php echo $user_file['file_url']; ?>" data-filename="<?php echo basename($user_file['file_url']); ?>" value="<?php _e('Select file', 'wp2print'); ?>"></td>
									</tr>
									<?php } ?>
								</table>
							</div>
						<?php } ?>
						<?php if (count($d_user_files) && $cart_upload_database_uploaded_files == 1) { ?>
							<div class="uu-files cart-uu-files cart-uu-files-2">
								<p style="margin:0 0 12px;"><?php _e('Use previously uploaded file', 'wp2print'); ?>:</p>
								<table cellspacing="0" cellpadding="0" style="width:100%;">
									<?php foreach($d_user_files as $user_file) { ?>
									<tr>
										<td><a href="<?php echo print_products_get_amazon_file_url($user_file['file_url']); ?>" class="uu-flink" target="_blank"><?php echo basename($user_file['file_url']); ?></a></td>
										<td nowrap><?php echo date('Y-m-d H:i', strtotime($user_file['created'])); ?></td>
										<td nowrap><input type="button" class="button uu-use-file" data-file="<?php echo $user_file['file_url']; ?>" data-filename="<?php echo basename($user_file['file_url']); ?>" value="<?php _e('Select file', 'wp2print'); ?>"></td>
									</tr>
									<?php } ?>
								</table>
							</div>
						<?php } ?>
					<?php }
					?>
				<?php } ?>
			</div>
			<form method="POST" id="cartuploadform-<?php echo $cart_item_key; ?>" class="cart-upload-form cart-upload-form-<?php echo $cart_item_key; ?>">
			<input type="hidden" name="cart_upload_action" value="save">
			<input type="hidden" name="cart_item_key" value="<?php echo $cart_item_key; ?>" class="cart-item-key">
			<input type="hidden" name="artwork_files" class="artwork-files">
			<input type="hidden" name="redirect_to" value="<?php the_permalink(); ?>">
			</form>
		<?php } ?>
	<?php } ?>
</div>
<?php if ($is_cart_upload) { ?>
<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/plupload.full.min.js?ver=3.1.2"></script>
<script type="text/javascript">
<!--
jQuery(document).ready(function() {
	jQuery('.uu-files .uu-use-file').click(function(){
		var ufile = jQuery(this).data('file');
		var ufilename = jQuery(this).data('filename');
		var artworkfiles = jQuery('.cart-upload-form-'+cart_upload_cikey+' .artwork-files').val();
		if (artworkfiles != '') { artworkfiles += ';'; }
		artworkfiles += ufile;
		jQuery('.cart-upload-form-'+cart_upload_cikey+' .artwork-files').val(artworkfiles);

		jQuery('#filelist-'+cart_upload_cikey).append('<div>' + ufilename + '</div>');
		jQuery('#filelist-'+cart_upload_cikey).fadeIn(300);
		jQuery(this).prop('disabled', 'disabled');

		jQuery('.upload-artwork-'+cart_upload_cikey+' .cu-skip-'+cart_upload_active).hide();
		jQuery('.upload-artwork-'+cart_upload_cikey+' .cu-continue-'+cart_upload_active).fadeIn(300);
		
		return false;
	});
});
var cart_upload_cikey = '';
var cart_upload_stage = '';
var cart_upload_active = 1;
function wp2print_cart_upload_button(cikey) {
	cart_upload_active = 1;
	cart_upload_cikey = cikey;
	cart_upload_stage = parseInt(jQuery('.upload-artwork-'+cikey).data('upload-stage'));
	var artwork_ftypes = jQuery('.upload-artwork-'+cikey).data('artwork-ftypes');
	var database_ftypes = jQuery('.upload-artwork-'+cikey).data('database-ftypes');
	jQuery('.cart-upload-form-'+cikey+' .artwork-files').val('');
	if (cart_upload_stage == 1) {
		cart_upload_active = 2;
		wp2print_cart_upload_init(cikey, database_ftypes);
	} else {
		wp2print_cart_upload_init(cikey, artwork_ftypes);
	}
	jQuery.colorbox({inline:true, href:".upload-artwork-"+cikey});
}
function wp2print_cart_upload_process() {
	if (cart_upload_active == 2) {
		var artworkfiles = jQuery('.cart-upload-form-'+cart_upload_cikey+' .artwork-files').val();
		if (artworkfiles != '') {
			if (jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).is(':visible')) {
				jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).trigger('click');
			} else {
				jQuery('.cart-upload-form-'+cart_upload_cikey).submit();
				jQuery.colorbox.close();
			}
		} else {
			if (jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).is(':visible')) {
				jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).trigger('click');
			} else {
				jQuery.colorbox.close();
			}
		}
	} else {
		if (cart_upload_stage == 2) {
			if (jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).is(':visible')) {
				jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).trigger('click');
			} else {
				wp2print_cart_upload_next();
			}
		} else {
			var artworkfiles = jQuery('.cart-upload-form-'+cart_upload_cikey+' .artwork-files').val();
			if (artworkfiles != '') {
				if (jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).is(':visible')) {
					jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).trigger('click');
				} else {
					jQuery('.cart-upload-form-'+cart_upload_cikey).submit();
					jQuery.colorbox.close();
				}
			} else {
				if (jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).is(':visible')) {
					jQuery('#uploadfiles-'+cart_upload_cikey+'-'+cart_upload_active).trigger('click');
				} else {
					jQuery.colorbox.close();
				}
			}
		}
	}
}
function wp2print_cart_upload_next() {
	cart_upload_active = 2;
	var database_ftypes = jQuery('.upload-artwork-'+cart_upload_cikey).data('database-ftypes');
	jQuery('.upload-artwork-'+cart_upload_cikey).hide();
	wp2print_cart_upload_init(cart_upload_cikey, database_ftypes);
	jQuery('.upload-artwork-'+cart_upload_cikey).fadeIn(300);
}
function wp2print_cart_upload_skip(n) {
	if (n == 2) {
		var artworkfiles = jQuery('.cart-upload-form-'+cart_upload_cikey+' .artwork-files').val();
		if (artworkfiles != '') {
			jQuery('.cart-upload-form-'+cart_upload_cikey).submit();
		}
		jQuery.colorbox.close();
	} else {
		if (cart_upload_stage == 2) {
			wp2print_cart_upload_next();
		} else {
			jQuery.colorbox.close();
		}
	}
}
function wp2print_cart_upload_continue(n) {
}
function wp2print_cart_upload_init(cikey, ftypes) {
	var upkey = cikey+'-'+cart_upload_active;
	var ufilters = { max_file_size : '<?php echo $file_upload_max_size; ?>mb' }
	if (ftypes != '') {
		var ufilters = { max_file_size : '<?php echo $file_upload_max_size; ?>mb', mime_types: [{'title' : 'Specific files', 'extensions' : ftypes}] }
	}

	jQuery('.upload-artwork-'+cikey+' .uu-files').hide();
	if (cart_upload_active == 2) {
		var dufiles = parseInt(jQuery('.upload-artwork-'+cikey).data('database-ufiles'));
		jQuery('.upload-artwork-'+cikey+' .cu-skip-1, .upload-artwork-'+cikey+' .cu-continue-1, .upload-artwork-'+cikey+' .cu-label-1').hide();
		jQuery('.upload-artwork-'+cikey+' .cu-skip-2, .upload-artwork-'+cikey+' .cu-label-2').show();
		jQuery('#uacontainer-'+cikey+'-1').hide();
		jQuery('#uacontainer-'+cikey+'-2').show();
		if (dufiles) {
			jQuery('.upload-artwork-'+cikey+' .cart-uu-files-2').show();
		}
	} else {
		var aufiles = parseInt(jQuery('.upload-artwork-'+cikey).data('artwork-ufiles'));
		jQuery('.upload-artwork-'+cikey+' .cu-skip-2, .upload-artwork-'+cikey+' .cu-continue-2, .upload-artwork-'+cikey+' .cu-label-2').hide();
		jQuery('.upload-artwork-'+cikey+' .cu-skip-1, .upload-artwork-'+cikey+' .cu-label-1').show();
		jQuery('#uacontainer-'+cikey+'-2').hide();
		jQuery('#uacontainer-'+cikey+'-1').show();
		if (aufiles) {
			jQuery('.upload-artwork-'+cikey+' .cart-uu-files-1').show();
		}
	}

	if (uploader) { uploader.destroy(); }

	var uploader = new plupload.Uploader({
		runtimes : 'html5,flash,silverlight,html4',
		file_data_name: 'file',
		browse_button : 'pickfiles-'+upkey, // you can pass an id...
		container: document.getElementById('uacontainer-'+upkey), // ... or DOM Element itself
		flash_swf_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.swf',
		silverlight_xap_url : '<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/Moxie.xap',
		drop_element: document.getElementById('upload-artwork-'+cikey), // ... or DOM Element itself
		url : '<?php echo $plupload_url; ?>',
		dragdrop: true,
		filters : ufilters,
		<?php if ($upload_to == 'amazon') { ?>
		multipart: true,
		<?php echo $multiparams; ?>
		<?php } ?>
		init: {
			PostInit: function() {
				jQuery('#filelist-'+cikey).html('').hide();
				jQuery('#uploadfiles-'+upkey).hide();

				document.getElementById('uploadfiles-'+upkey).onclick = function() {
					uploader.start();
					jQuery('#uploadfiles-'+upkey).attr('disabled', 'disabled');
					jQuery('#uacontainer-'+upkey+' .upload-loading').show();
					return false;
				};
			},
			FilesAdded: function(up, files) {
				var ucounterror = false;
				jQuery('#filelist-'+cikey).show();
				plupload.each(files, function(file) {
					file.name = wp2print_clear_fname(file.name);
					document.getElementById('filelist-'+cikey).innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
				});
				jQuery('#uploadfiles-'+upkey).removeAttr('disabled');
				jQuery('#uploadfiles-'+upkey).show();
				jQuery('.upload-artwork-'+cart_upload_cikey+' .cu-skip-'+cart_upload_active).hide();
				jQuery('.upload-artwork-'+cart_upload_cikey+' .cu-continue-'+cart_upload_active).fadeIn(300);
			},
			UploadProgress: function(up, file) {
				document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
			},
			<?php if ($upload_to == 'amazon') { ?>
			BeforeUpload: function(up, file) {
				var regex = /(?:\.([^.]+))?$/;
				var ext = regex.exec(file.name)[1];
				if (ext == 'pdf') {
					up.settings.multipart_params['Content-Type'] = 'application/pdf';
				} else {
					up.settings.multipart_params['Content-Type'] = file.type;
				}
				up.settings.multipart_params['key'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);
				<?php if (!$is_s3_region) { ?>up.settings.multipart_params['Filename'] = '<?php echo $s3path; ?>'+wp2print_clear_fname(file.name);<?php } ?>
				up.settings.multipart_params['Content-Disposition'] = 'attachment';
			},
			<?php } ?>
			FileUploaded: function(up, file, response) {
				<?php if ($upload_to == 'amazon') { ?>
					var ufileurl = '<?php echo $amazon_file_url; ?>'+file.name;
				<?php } else { ?>
					var ufileurl = response['response'];
				<?php } ?>
				if (ufileurl != '') {
					var artworkfiles = jQuery('.cart-upload-form-'+cikey+' .artwork-files').val();
					if (artworkfiles != '') { artworkfiles += ';'; }
					artworkfiles += ufileurl;
					jQuery('.cart-upload-form-'+cikey+' .artwork-files').val(artworkfiles);
				}
			},
			UploadComplete: function(files) {
				jQuery('#uacontainer-'+upkey+' .upload-loading').hide();
				jQuery('#uploadfiles-'+upkey).hide();
				wp2print_cart_upload_process();
			},
			Error: function(up, err) {
				alert("<?php _e('Upload error', 'wp2print'); ?>: "+err.message); // err.code
			}
		}
	});
	uploader.init();
}
//--></script>
<?php } ?>
<?php } ?>