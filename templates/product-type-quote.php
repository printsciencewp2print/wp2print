<?php
global $product;

$product_id = $product->get_id();

$quote_options = get_post_meta($product_id, '_quote_options', true);
if ($quote_options && is_array($quote_options) && isset($quote_options['button-link']) && strlen($quote_options['button-link'])) { ?>
	<div class="print-products-area" style="margin:15px 0;">
		<input type="button" value="<?php echo $quote_options['button-text']; ?>" class="button alt" onclick="window.location.href='<?php echo $quote_options['button-link']; ?>';">
	</div>
<?php } ?>