jQuery(document).ready(function() {
	jQuery('.product-thumbnail').show();

	jQuery('#wp2print_billing_addresses').change(function(){
		var akey = jQuery(this).val();
		if (akey > 0) {
			wp2print_set_billing_address(akey);
		}
		return false;
	});

	jQuery('#wp2print_shipping_addresses').change(function(){
		var akey = jQuery(this).val();
		if (akey > 0) {
			wp2print_set_shipping_address(akey);
		}
		return false;
	});
	if (jQuery('.print-products-area .product-attributes-list .a-help').length) {
		jQuery('.print-products-area .product-attributes-list .a-help').each(function(){
			var awidth = jQuery(this).parent().width() + 29;
			var aleft = awidth - 14;
			jQuery(this).find('.a-help-text').css('width', awidth+'px');
			jQuery(this).find('.a-help-text').css('margin-left', '-'+aleft+'px');
		});
	}
	jQuery('.print-products-area .product-attributes-list .a-help img').hover(
		function(){ jQuery(this).parent().find('.a-help-text').fadeIn(); },
		function(){ jQuery(this).parent().find('.a-help-text').hide(); }
	);
	jQuery('.ma-section-head').click(function(){
		var thisdiv = jQuery(this);
		var reldiv = thisdiv.attr('rel');
		if (jQuery('.'+reldiv).is(':visible')) {
			jQuery('.'+reldiv).slideUp(400, function(){
				thisdiv.removeClass('opened');
			});
		} else {
			jQuery('.'+reldiv).slideDown(400, function(){
				thisdiv.addClass('opened');
			});
		}
	});
	if (jQuery('.attribute-images').length) {
		jQuery('.attribute-images img').click(function(){
			var relto = jQuery(this).attr('rel');
			var iclass = jQuery(this).attr('class').replace(' active', '');
			var aterm_id = iclass.replace('attribute-image-', '');
			jQuery('.'+relto+' select option').removeAttr('selected');
			jQuery('.'+relto+' select option[value="'+aterm_id+'"]').prop('selected', true);
			jQuery('.'+relto+' select').trigger('change');
			matrix_image_aradio(relto, aterm_id);
			return false;
		});
		jQuery('.matrix-type-simple').each(function(){
			jQuery(this).find('.print-attributes .smatrix-attr').each(function(){
				var aid = jQuery(this).attr('data-aid');
				if (jQuery('.attribute-images-'+aid).length) {
					if (!jQuery('.attribute-images-'+aid+' img.active').length) {
						jQuery(this).trigger('change');
					}
				}
			});
		});
		jQuery('.matrix-type-finishing').each(function(){
			jQuery(this).find('.finishing-attributes .fmatrix-attr').each(function(){
				var aid = jQuery(this).attr('data-aid');
				if (jQuery('.attribute-images-'+aid).length) {
					if (!jQuery('.attribute-images-'+aid+' img.active').length) {
						jQuery(this).trigger('change');
					}
				}
			});
		});
	}
	if (jQuery('.attribute-colors').length) {
		jQuery('.attribute-colors span').click(function(){
			var relto = jQuery(this).attr('rel');
			var iclass = jQuery(this).attr('class').replace(' active', '');
			var aterm_id = iclass.replace('attribute-color-', '');
			jQuery('.'+relto+' select option').removeAttr('selected');
			jQuery('.'+relto+' select option[value="'+aterm_id+'"]').prop('selected', true);
			jQuery('.'+relto+' select').trigger('change');
			matrix_image_aradio(relto, aterm_id);
			return false;
		});
	}
	jQuery('.upload-artwork-btn').click(function(){ jQuery('.add-cart-form .atc-action').val('artwork'); });
	jQuery('.design-online-btn').click(function(){ jQuery('.add-cart-form .atc-action').val('design'); });
	jQuery('.simple-add-btn').click(function(){ jQuery('.add-cart-form .atc-action').val('artwork'); });

	var spiniconnum = 1;
	if (jQuery('.spinning-icon').length) {
		var sitimer = setInterval(function() {
			jQuery('.spinning-icon').each(function(){
				var thumb_src = jQuery(this).attr('data-thumb');
				var thisid = jQuery(this).attr('id');
				var imgspinning = new Image();
				imgspinning.src = thumb_src;
				imgspinning.onload = function() {
					jQuery('#'+thisid).attr('src', thumb_src);
					jQuery('#'+thisid).removeClass('spinning-icon').addClass('blitline-img');
					jQuery('#'+thisid).parent().attr('href', thumb_src);
				}
			});
			spiniconnum++;
			if (spiniconnum >= 6) {
				jQuery('.spinning-icon').each(function(){
					var icon_src = jQuery(this).attr('data-icon');
					var file_src = jQuery(this).attr('data-file');
					var thisid = jQuery(this).attr('id');
					var imgspinning = new Image();
					imgspinning.src = icon_src;
					imgspinning.onload = function() {
						jQuery('#'+thisid).attr('src', icon_src);
						jQuery('#'+thisid).removeClass('spinning-icon');
						jQuery('#'+thisid).parent().attr('href', file_src);
					}
				});
			}
			if (!jQuery('.spinning-icon').length || spiniconnum >= 6) {
				clearInterval(sitimer);
			}
		}, 5000);
	}
	var ww = jQuery(window).width();
	jQuery('.o-notes .o-notes-more').click(function(){
		var tdh = jQuery(this).parent().parent().height();
		jQuery('.o-notes-text').hide();
		jQuery(this).parent().find('.o-notes-text').css('top', tdh).fadeIn(300);
		return false;
	});
	jQuery('html').click(function(event) {
		if (jQuery(event.target).closest('.o-notes .o-notes-more').length === 0) {
			jQuery('.o-notes-text').hide();
		}
	});

	// reCaptcha
	if (jQuery('.g-recaptcha').length) {
		jQuery('form').each(function(){
			if (jQuery(this).find('.g-recaptcha').length) {
				jQuery(this).find('[type=submit]').attr('disabled', 'disabled');
			}
		});
	}
	wp2print_wcbof_init();
	jQuery('.wcbulkordertable .add-to-cart-single').on('click', function() {
		wp2print_wcbof_init();
	});
});

function wp2print_recaptcha() {
	jQuery('form').each(function(){
		if (jQuery(this).find('.g-recaptcha').length) {
			var grr = jQuery(this).find('.g-recaptcha-response').val();
			if (grr != '') {
				jQuery(this).find('[type=submit]').removeAttr('disabled');
			}
		}
	});
}

function reorder_error() {
	jQuery('.pp-reorder-error').slideDown();
}

function reorder_product_action(item_id, tp) {
	var rcolor = jQuery('.history-reorder-form-'+item_id+' .reorder-color').val();
	jQuery('.history-reorder-form-'+item_id+' .atc-action').val(tp);
	jQuery('.history-reorder-form-'+item_id+' .redesign-fld').val('false');
	if (tp == 'design') {
		jQuery('.history-reorder-form-'+item_id+' .redesign-fld').val('true');
	}
	if (rcolor == '1') {
		var tn = jQuery('td.product-tracking-'+item_id).html();
		jQuery('.reorder-color-popup .rcp-tracking span').html(tn);
		jQuery('.reorder-color-popup').data('item-id', item_id);
		jQuery.colorbox({inline:true, href:"#reorder-color-popup"});
	} else {
		jQuery('.history-reorder-form-'+item_id).submit();
	}
}

function reorder_color_change(o) {
	var otherval = jQuery('.reorder-color-popup').data('other-value');
	var cval = jQuery(o).val();
	if (cval == otherval) {
		jQuery('.reorder-color-popup .other-row').slideDown(200);
	} else {
		jQuery('.reorder-color-popup .other-row').slideUp(200);
	}
}

function reorder_color_continue() {
	var item_id = jQuery('.reorder-color-popup').data('item-id');
	var otherval = jQuery('.reorder-color-popup').data('other-value');
	var othererror = jQuery('.reorder-color-popup').data('other-error');
	var cval = jQuery('.reorder-color-popup .rcp-color').val();
	if (cval == otherval) {
		cval = jQuery('.reorder-color-popup .rcp-other').val();
	}
	if (cval) {
		jQuery('.history-reorder-form-'+item_id+' .reorder-color-value').val(cval);
		jQuery('.history-reorder-form-'+item_id).submit();
	} else {
		alert(othererror);
	}
}

function matrix_aradio(o) {
	var afield = jQuery(o).attr('rel');
	var oval = jQuery(o).val();
	if (jQuery(o).is(':checked')) {
		jQuery('.'+afield+' option').removeAttr('selected');
		jQuery('.'+afield+' option[value="'+oval+'"]').prop('selected', true);
		jQuery('.'+afield).trigger('change');
	}
}

function matrix_image_aradio(orel, aterm_id) {
	var rarray = orel.split('-');
	var mtype_id = rarray[2];
	var mattr_id = rarray[3];
	if (jQuery('.a-radio-'+mtype_id+'-'+mattr_id).length) {
		jQuery('.a-radio-'+mtype_id+'-'+mattr_id+' input').removeAttr('checked');
		jQuery('.a-radio-'+mtype_id+'-'+mattr_id+' input[value="'+aterm_id+'"]').prop('checked', true);
	}
}

function matrix_get_numbers(num, numbers) {
	var lastnum = num;
	var matrix_numbers = new Array();
	matrix_numbers[0] = 0;
	matrix_numbers[1] = 0;
	if (num > 0) {
		for (var i=0; i<numbers.length; i++) {
			anumb = parseFloat(numbers[i]);
			if (num < anumb) {
				matrix_numbers[0] = lastnum;
				matrix_numbers[1] = anumb;
				return matrix_numbers;
			} else if (num == anumb) {
				matrix_numbers[0] = anumb;
				matrix_numbers[1] = anumb;
				return matrix_numbers;
			}
			lastnum = anumb;
		}
		if (numbers.length == 1) {
			matrix_numbers[0] = numbers[0];
		} else {
			matrix_numbers[0] = numbers[numbers.length - 2];
		}
		matrix_numbers[1] = lastnum;
	}
	return matrix_numbers;
}

function matrix_get_price(pmatrix, mval, nmb, nums) {
	var p = 0;
	var min_nmb = nums[0];
	var max_nmb = nums[1];
	if (nmb == min_nmb && nmb < max_nmb) {
		mval = mval + '-' + max_nmb;
		if (pmatrix[mval]) {
			p = (pmatrix[mval] / max_nmb) * nmb;
		}
	} else if (nmb == min_nmb && nmb == max_nmb) {
		mval = mval + '-' + nmb;
		if (pmatrix[mval]) {
			p = pmatrix[mval];
		}
	} else if (nmb > min_nmb && nmb < max_nmb) {
		var min_mval = mval + '-' + min_nmb;
		var max_mval = mval + '-' + max_nmb;
		if (pmatrix[min_mval] && pmatrix[max_mval]) {
			p = pmatrix[min_mval] + (nmb - min_nmb) * (pmatrix[max_mval] - pmatrix[min_mval]) / (max_nmb - min_nmb);
		}
	} else if (nmb > min_nmb && nmb > max_nmb) {
		var min_mval = mval + '-' + min_nmb;
		var max_mval = mval + '-' + max_nmb;
		if (pmatrix[min_mval] && pmatrix[max_mval]) {
			if (min_nmb == max_nmb) {
				p = pmatrix[max_mval] * nmb;
			} else {
				p = pmatrix[max_mval] + (nmb - max_nmb) * (pmatrix[max_mval] - pmatrix[min_mval]) / (max_nmb - min_nmb);
			}
		}
	}
	return p;
}

function matrix_aval(val) {
	if (val) {
		if (val.indexOf('-') > -1) { val = wp2print_replace(val, '-', '{dc}'); }
		if (val.indexOf(':') > -1) { val = wp2print_replace(val, ':', '{cc}'); }
		if (val.indexOf('|') > -1) { val = wp2print_replace(val, '|', '{vc}'); }
	}
	return val;
}

function matrix_get_corrected_num(nmb, nums) {
	var min_nmb = nums[0];
	var max_nmb = nums[1];
	if (nmb == min_nmb && nmb < max_nmb) {
		return min_nmb;
	} else if (nmb == min_nmb && nmb == max_nmb) {
		return nmb;
	} else if (nmb > min_nmb && nmb < max_nmb) {
		var min_diff = nmb - min_nmb;
		var max_diff = max_nmb - nmb;
		if (min_diff < max_diff) {
			return min_nmb;
		} else {
			return max_nmb;
		}
	} else if (nmb > min_nmb && nmb > max_nmb) {
		return max_nmb;
	}
}

function matrix_is_pdf_exist() {
	var pdfs = autoproof_get_pdf_files();
	if (pdfs != '') {
		return true;
	}
	return false;
}

function autoproof_get_pdf_files() {
	var pdf_files = '';
	var artworkfiles = jQuery('.add-cart-form .artwork-files').val();
	if (artworkfiles != '') {
		var fext = '', fsep = '';
		var alist = artworkfiles.split(';');
		for (var f=0; f<alist.length; f++) {
			fext = alist[f].substring(alist[f].length - 3);
			if (fext == 'pdf' || fext == 'PDF') {
				pdf_files = pdf_files + fsep + alist[f];
				fsep = ';';
			}
		}
	}
	return pdf_files;
}

function wp2print_set_billing_address(akey) {
	var avals = wp2print_billing_address[akey].split('|');
	jQuery('#billing_first_name').val(avals[0]);
	jQuery('#billing_last_name').val(avals[1]);
	jQuery('#billing_company').val(avals[2]);
	jQuery('#billing_country').val(avals[3]).change();
	jQuery("#billing_country_chosen").find('span').html(jQuery('#billing_country option[value="'+avals[3]+'"]').text());
	jQuery('#billing_address_1').val(avals[4]);
	jQuery('#billing_address_2').val(avals[5]);
	jQuery('#billing_city').val(avals[6]);
	jQuery('#billing_state').val(avals[7]);
	jQuery('#billing_postcode').val(avals[8]);
	jQuery('#billing_phone').val(avals[9]);

	jQuery('#group_billing_email').val(avals[10]);
	jQuery('#group_billing_nosend').val(avals[11]);

	jQuery('#billing_state option').removeAttr('selected');
	jQuery('#billing_state option[value="'+avals[7]+'"]').prop('selected', true);
	var sname = jQuery('#billing_state option[value="'+avals[7]+'"]').text();
	jQuery('#select2-billing_state-container').html(sname);
}

function wp2print_set_shipping_address(akey) {
	var avals = wp2print_shipping_address[akey].split('|');
	jQuery('#shipping_first_name').val(avals[0]);
	jQuery('#shipping_last_name').val(avals[1]);
	jQuery('#shipping_company').val(avals[2]);
	jQuery('#shipping_country').val(avals[3]).change();
	jQuery("#shipping_country_chosen").find('span').html(jQuery('#shipping_country option[value="'+avals[3]+'"]').text());
	jQuery('#shipping_address_1').val(avals[4]);
	jQuery('#shipping_address_2').val(avals[5]);
	jQuery('#shipping_city').val(avals[6]);
	jQuery('#shipping_state').val(avals[7]);
	jQuery('#shipping_postcode').val(avals[8]);

	jQuery('#shipping_state option').removeAttr('selected');
	jQuery('#shipping_state option[value="'+avals[7]+'"]').prop('selected', true);
	var sname = jQuery('#shipping_state option[value="'+avals[7]+'"]').text();
	jQuery('#select2-shipping_state-container').html(sname);
}

function wp2print_trim(str) {
	if (str != 'undefined') {
		return str.replace(/^\s+|\s+$/g,"");
	} else {
		return '';
	}
}

function wp2print_wwof(pid, o) {
	var n = jQuery(o).val();
	jQuery(o).parent().find('input[name="quantity"]').val(n);
	jQuery('.wp2print-wwof-prices-'+pid+' span.price').hide();
	jQuery('.wp2print-wwof-prices-'+pid+' span.price-qty-'+n).fadeIn();
}

function wp2print_wcbof(pid, o) {
	var n = jQuery(o).val();
	jQuery(o).parent().find('input.product_qty').val(n).trigger('change');
	jQuery('.wp2print-wwof-prices-'+pid+' span.price').hide();
	jQuery('.wp2print-wwof-prices-'+pid+' span.price-qty-'+n).fadeIn();
}

function wp2print_wcbof_init() {
	if (jQuery('.wcbulkordertable select.wp2print-wwof-qty').length)	{
		setTimeout(function(){
			jQuery('.wcbulkordertable select.wp2print-wwof-qty').each(function(){
				var n = parseInt(jQuery(this).val());
				if (n) {
					jQuery(this).parent().find('input.product_qty').val(n).trigger('change');
				}
			});
		}, 1000);
	}
}

function wp2print_email_quote() {
	var eqerror = '';
	var email = wp2print_trim(jQuery('.email-quote-box .eq-email').val());
	if (email == '') {
		eqerror = 'empty';
	} else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email)) {
		eqerror = 'incorrect';
	}
	jQuery('.email-quote-box .eq-errors, .email-quote-box .eq-errors span').hide();
	if (eqerror == '') {
		var ptype = jQuery('.add-cart-form input.product-type').val();
		wp2print_email_quote_request(email, ptype);
		jQuery('.email-quote-box .eq-success').slideDown();
		setTimeout(function(){ jQuery('.email-quote-box .eq-success').slideUp(); }, 5000);
	} else {
		jQuery('.email-quote-box .eq-errors .error-'+eqerror).show();
		jQuery('.email-quote-box .eq-errors').slideDown();
	}
}

function wp2print_email_quote_request(email, tp) {
	var site_url = jQuery('.email-quote-box .email-quote-form').attr('action');
	var product_id = jQuery('.add-cart-form input.product-id').val();
	var quantity = jQuery('.add-cart-form .quantity').val();
	var smparams = jQuery('.add-cart-form .sm-params').val();
	var fmparams = jQuery('.add-cart-form .fm-params').val();
	var price = jQuery('.add-cart-form .p-price').val();
	var fnonce = jQuery('.email-quote-form .form-nonce').val();

	var pspeed = '';
	if (jQuery('select.production-speed').length) {
		pspeed = jQuery('select.production-speed').val();
	}

	var rparams = {AjaxAction: 'email-quote-send', email: email, product_id: product_id, product_type: tp, quantity: quantity, smparams: smparams, fmparams: fmparams, pspeed: pspeed, price: price, form_nonce: fnonce};

	if (tp == 'area') {
		rparams.width = jQuery('.add-cart-form .width').val();
		rparams.height = jQuery('.add-cart-form .height').val();
	} else if (tp == 'box') {
		rparams.width = jQuery('.add-cart-form .width').val();
		rparams.height = jQuery('.add-cart-form .height').val();
		rparams.length = jQuery('.add-cart-form .length').val();
	} else if (tp == 'book') {
		var pagesqty = '';
		jQuery('.add-cart-form .quantity').each(function(){
			if (pagesqty != '') { pagesqty += ';'; }
			pagesqty += jQuery(this).val();
		});
		rparams.booksqty = jQuery('.add-cart-form .books-quantity').val();
		rparams.pagesqty = pagesqty;
	} else if (tp == 'aec' || tp == 'aecsimple') {
		rparams.project_name = jQuery('.add-cart-form .aec-project-name').val();
		rparams.total_price = jQuery('.add-cart-form .aec-total-price').val();
		rparams.total_area = jQuery('.add-cart-form .aec-total-area').val();
		rparams.total_pages = jQuery('.add-cart-form .aec-total-pages').val();
	} else if (tp == 'aecbwc') {
		rparams.project_name = jQuery('.add-cart-form .aec-project-name').val();
		rparams.total_price = jQuery('.add-cart-form .aec-total-price').val();
		rparams.total_area = jQuery('.add-cart-form .aec-total-area').val();
		rparams.total_pages = jQuery('.add-cart-form .aec-total-pages').val();
		rparams.area_bw = jQuery('.add-cart-form .aec-area-bw').val();
		rparams.pages_bw = jQuery('.add-cart-form .aec-pages-bw').val();
		rparams.area_cl = jQuery('.add-cart-form .aec-area-cl').val();
		rparams.pages_cl = jQuery('.add-cart-form .aec-pages-cl').val();
	}

	jQuery.post(site_url, rparams);
}

function wp2print_replace(s, f, r) {
	return s.replace(new RegExp(f, 'gi'), r);
}

function wp2print_clear_fname(s) {
	s = s.toLowerCase();
	s = s.replace(/ /g, '');
	s = s.replace(/[&\/\\#,+()$~%'":*?<>{}^@!]/g, '');
	s = wp2print_transliterate(s);
	s = s.replace(/[^\x00-\x7F]/g, '');
	s = encodeURI(s);
	s = s.replace(/[%]/g, '');
	return s;
}

var wp2print_transliterate = function(text) {
    text = text
        .replace(/\u042A/g, '-')
        .replace(/\u0451/g, 'yo')
        .replace(/\u0439/g, 'i')
        .replace(/\u0446/g, 'ts')
        .replace(/\u0443/g, 'u')
        .replace(/\u043A/g, 'k')
        .replace(/\u0435/g, 'e')
        .replace(/\u043D/g, 'n')
        .replace(/\u0433/g, 'g')
        .replace(/\u0448/g, 'sh')
        .replace(/\u0449/g, 'sch')
        .replace(/\u0437/g, 'z')
        .replace(/\u0445/g, 'h')
        .replace(/\u044A/g, '-')
        .replace(/\u0410/g, 'a')
        .replace(/\u0444/g, 'f')
        .replace(/\u044B/g, 'i')
        .replace(/\u0432/g, 'v')
        .replace(/\u0430/g, 'a')
        .replace(/\u043F/g, 'p')
        .replace(/\u0440/g, 'r')
        .replace(/\u043E/g, 'o')
        .replace(/\u043B/g, 'l')
        .replace(/\u0434/g, 'd')
        .replace(/\u0436/g, 'zh')
        .replace(/\u044D/g, 'e')
        .replace(/\u042C/g, '-')
        .replace(/\u044F/g, 'a')
        .replace(/\u0447/g, 'ch')
        .replace(/\u0441/g, 's')
        .replace(/\u043C/g, 'm')
        .replace(/\u0438/g, 'i')
        .replace(/\u0442/g, 't')
        .replace(/\u044C/g, '-')
        .replace(/\u0431/g, 'b')
        .replace(/\u044E/g, 'u');

    return text;
};

if (jQuery('.woocommerce-MyAccount-content .ois-order-status').length) {
	jQuery('.woocommerce-MyAccount-content mark.order-number').parent().remove();
}

function matrix_printess() {
	if (jQuery('#printess-customize-button').length && jQuery('.matrix-type-simple').length) {
		var mtobj = jQuery('.matrix-type-simple').eq(0);
		var pt_templ_key = '';
		if (mtobj.find('.print-attributes .smatrix-size').length) {
			pt_templ_key = parseInt(mtobj.find('.print-attributes .smatrix-size').eq(0).val());
		}
		if (mtobj.find('.print-attributes .smatrix-colour').length) {
			if (pt_templ_key != '') { pt_templ_key = pt_templ_key + '-'; }
			pt_templ_key = pt_templ_key + parseInt(mtobj.find('.print-attributes .smatrix-colour').eq(0).val());
		}
		if (mtobj.find('.print-attributes .smatrix-pagecount').length) {
			if (pt_templ_key != '') { pt_templ_key = pt_templ_key + '-'; }
			pt_templ_key = pt_templ_key + parseInt(mtobj.find('.print-attributes .smatrix-pagecount').eq(0).val());
		}
		if (pt_templ_key) {
			if (pt_templates[pt_templ_key] != undefined) {
				pt_template_name = pt_templates[pt_templ_key];
			}
		}
	}
}