jQuery(document).ready(function() {
	wp2print_colors_picker('.fld-color');
	jQuery('.pmo-attributes .atermitem').click(function(){
		var ao = jQuery(this).attr('rel');
		var achecked = false;
		jQuery('.pmo-attributes .atermitem').each(function(){
			if (jQuery(this).is(':checked')) {
				achecked = true;
			}
		});
		if (achecked) {
			jQuery('.'+ao).prop('checked', true);
		} else {
			jQuery('.'+ao).removeAttr('checked');
		}
	});
	jQuery('#product-type').change(function(){
		print_products_check_tax_select();
	});
	print_products_check_tax_select();
	print_products_num_type();

	if (jQuery('.help-icon').length) {
		jQuery('.wp2print-wrap').tooltip();
	}

	// disable select checkbox for rejected orders
	if (jQuery('mark.rejected-prod').length) {
		jQuery('mark.rejected-prod').parent().prev().find('input').attr('disabled', 'disabled');
	}
	if (jQuery('mark.await-approval').length) {
		jQuery('mark.await-approval').parent().prev().find('input').attr('disabled', 'disabled');
	}
	if (jQuery('.order_data_column_container #order_status').length) {
		var ostatus = jQuery('.order_data_column_container #order_status').val();
		if (ostatus == 'wc-rejected-prod' || ostatus == 'wc-await-approval') {
			jQuery('.order_data_column_container #order_status').attr('disabled', 'disabled');
			jQuery('#woocommerce-order-actions').hide();
		}
	}

	// show General tab for wp2print products
	if (jQuery('ul.product_data_tabs').length) {
		jQuery('p._tax_status_field').parent().addClass('show_if_fixed show_if_book show_if_area show_if_box show_if_aec');
		jQuery('select#product-type').change();
	}
	jQuery('.apply-round-up').click(function(){
		if (jQuery(this).is(':checked')) {
			jQuery('table.round-up-discount').show();
		} else {
			jQuery('table.round-up-discount').hide();
		}
	});
	if (jQuery('select.bq-style').length) {
		print_products_bq_style();
		print_products_pq_style();
	}
	jQuery('.order-send-proof').click(function(){
		var oid = jQuery(this).attr('data-oid');
		var iid = jQuery(this).attr('data-iid');
		jQuery('.order-proof-form .proof-order-id').val(oid);
		jQuery('.order-proof-form .proof-item-id').val(iid);
		jQuery.colorbox({inline:true, href:"#upload-artwork"});
		return false;
	});
	if (jQuery('#order_data')) {
		if (order_edit_btn) {
			jQuery('#order_data').prepend('<input type="button" class="button button-primary order-edit-btn" value="'+order_edit_label+'">');
			jQuery('#order_data .order-edit-btn').click(function(){
				jQuery('.co-edit-order-form').submit();
			});
		}
		jQuery('#order_data').prepend('<input type="button" class="button button-primary order-print-btn" value="'+order_print_label+'" onclick="window.print();">');
	}
	if (jQuery('.i-v-date').length) {
		jQuery('.i-v-date').datepicker({dateFormat:'yy-mm-dd'});
	}
	if (jQuery('.pp-sq-edate').length) {
		jQuery('.pp-sq-edate').datepicker({dateFormat:'yy-mm-dd'});
	}
	if (jQuery('.item_rsdate').length) {
		var reqtime = parseInt(jQuery('.item_rsdate').data('reqtime'));
		var tformat = jQuery('.item_rsdate').data('tformat');
		if (reqtime == 1) {
			jQuery('.item-rsdate').datetimepicker({
				timeFormat: tformat,
				dateFormat : 'yy-mm-dd',
				onSelect:function(){
					var sdate = jQuery(this).val();
					var oid = jQuery(this).attr('data-order-id');
					var iid = jQuery(this).attr('data-item-id');
					if (sdate) {
						wp2print_oirsdate_change(oid, iid, sdate);
					}
				}
			});
		} else {
			jQuery('.item-rsdate').datepicker({dateFormat:'yy-mm-dd',
				onSelect:function(){
					var sdate = jQuery(this).val();
					var oid = jQuery(this).attr('data-order-id');
					var iid = jQuery(this).attr('data-item-id');
					if (sdate) {
						wp2print_oirsdate_change(oid, iid, sdate);
					}
				}
			});
		}
	}

	jQuery('.order-item-vendor').change(function(){
		var oval = jQuery(this).val();
		var orel = jQuery(this).attr('rel');

		if (oval) {
			jQuery('.v-order-item-'+orel+' .order-vendor-address').slideDown();
		} else {
			jQuery('.v-order-item-'+orel+' .order-vendor-address').slideUp();
		}
	});
	jQuery('.order-vendor-address .ovendor-address').click(function(){
		var orel = jQuery(this).attr('rel');
		var ova = jQuery('.v-order-item-'+orel+' .order-vendor-address .ovendor-address:checked').val();
		if (ova == 'vendor') {
			jQuery('.v-order-item-'+orel+' .order-vendor-address .customer-address .address-line').slideUp();
			jQuery('.v-order-item-'+orel+' .order-vendor-address .vendor-address .address-line').slideDown();
		} else {
			jQuery('.v-order-item-'+orel+' .order-vendor-address .vendor-address .address-line').slideUp();
			jQuery('.v-order-item-'+orel+' .order-vendor-address .customer-address .address-line').slideDown();
		}
	});
	jQuery('.users-groups-form .delete-theme-logo').click(function(){
		var confmess = jQuery(this).data('confirm');
		var d = confirm(confmess);
		if (d) {
			jQuery('.users-groups-form .theme-logo').val('');
			jQuery('.users-groups-form').submit();
		}
	});
	// order item status
	jQuery('.ois-list .ois-name').click(function(){
		var orel = jQuery(this).attr('rel');
		if (jQuery('.ois-row-'+orel+' .ois-form').is(':visible')) {
			jQuery('.ois-row-'+orel+' .ois-form').slideUp();
		} else {
			jQuery('.ois-row-'+orel+' .ois-form').slideDown();
		}
	});
	wp2print_colors_picker('.ois-color');
	// send quote
	send_quote_cproduct();
	jQuery('.send-quote-form .sq-add-user-btn').click(function(){
		jQuery.colorbox({inline:true, href:"#sq-add-user"});
	});
	// create order
	create_order_cproduct();
	jQuery('.create-order-form .co-add-user-btn').click(function(){
		jQuery.colorbox({inline:true, href:"#co-add-user"});
	});
	jQuery('.create-order-form .shipping-types input').click(function(){
		if (jQuery(this).val() == 'Shipping') {
			jQuery('.create-order-form .co-shipping-address').slideDown(300);
			jQuery('.create-order-form .copy-billing').fadeIn();
		} else {
			jQuery('.create-order-form .co-shipping-address').slideUp(200);
			jQuery('.create-order-form .copy-billing').hide();
		}
	});
	// order show old artwork files
	jQuery('.show-old-files').click(function(){
		var ofl = jQuery(this).parents('.product-attributes-list').find('.old-files-list');
		if (ofl.is(':visible')) {
			ofl.slideUp();
		} else {
			ofl.slideDown();
		}
		return false;
	});
	jQuery('.show-rejected-files').click(function(){
		var ofl = jQuery(this).parents('.product-attributes-list').find('.rejected-files-list');
		if (ofl.is(':visible')) {
			ofl.slideUp();
		} else {
			ofl.slideDown();
		}
		return false;
	});
	if (jQuery('input#_manage_stock').length) {
		jQuery('input#_manage_stock').parent().addClass('show_if_fixed').show();
	}
	// send quote
	jQuery('.send-quote-form .button-save').click(function(){
		jQuery('.send-quote-form .process-action').val('save');
	});
	jQuery('.send-quote-form .button-create').click(function(){
		jQuery('.send-quote-form .process-action').val('send');
	});
	// send quote history
	jQuery('.sqh-convert-to-order').click(function(){
		var oid = jQuery(this).attr('data-oid');
		var subtotal = parseFloat(jQuery(this).attr('data-subtotal'));
		var shipp_cost = parseFloat(jQuery(this).attr('data-shipping'));
		var tax_rate = parseFloat(jQuery(this).attr('data-tax-rate'));
		var tax_value = 0;
		if (subtotal && tax_rate) {
			tax_value = (subtotal / 100) * tax_rate;
			tax_value = tax_value.toFixed(2);
		}

		jQuery('form.sqh-convert-form .sqh-order-id').val(oid);
		jQuery('form.sqh-convert-form .sqh-shipping').val(shipp_cost);
		jQuery('form.sqh-convert-form .sqh-tax').val(tax_value).data('tax-rate', tax_rate);
		if (shipp_cost) {
			jQuery('form.sqh-convert-form .sqh-shipping').trigger('keyup');
		}
		jQuery.colorbox({inline:true, href:"#sqh-convert-popup"});
		return false;
	});
	jQuery('form.sqh-convert-form .sqh-shipping').keyup(function(){
		var shipping = parseFloat(jQuery(this).val());
		var tax_rate = parseFloat(jQuery('form.sqh-convert-form .sqh-tax').data('tax-rate'));
		var tax_value = 0;
		if (shipping && tax_rate) {
			tax_value = (shipping / 100) * tax_rate;
			tax_value = tax_value.toFixed(2);
		}
		jQuery('form.sqh-convert-form .sqh-shipping-tax').val(tax_value);
	});
	jQuery('.sqh-resend-email').click(function(){
		var oid = jQuery(this).attr('data-oid');
		jQuery('form.sqh-form .sqh-action').val('resend');
		jQuery('form.sqh-form .sqh-order-id').val(oid);
		jQuery('form.sqh-form').submit();
		return false;
	});
	jQuery('.sqh-duplicate').click(function(){
		var oid = jQuery(this).attr('data-oid');
		jQuery('form.sqh-form .sqh-action').val('duplicate');
		jQuery('form.sqh-form .sqh-order-id').val(oid);
		jQuery('form.sqh-form').submit();
		return false;
	});
	jQuery('.sqh-edit').click(function(){
		var oid = jQuery(this).attr('data-oid');
		jQuery('form.sqh-form .sqh-action').val('edit');
		jQuery('form.sqh-form .sqh-order-id').val(oid);
		jQuery('form.sqh-form').submit();
		return false;
	});
	// request payment history
	jQuery('.rph-resend-email').click(function(){
		var oid = jQuery(this).attr('data-oid');
		jQuery('form.rph-form .rph-action').val('resend');
		jQuery('form.rph-form .rph-order-id').val(oid);
		jQuery('form.rph-form').submit();
		return false;
	});
	jQuery('.rph-duplicate').click(function(){
		var oid = jQuery(this).attr('data-oid');
		jQuery('form.rph-form .rph-action').val('duplicate');
		jQuery('form.rph-form .rph-order-id').val(oid);
		jQuery('form.rph-form').submit();
		return false;
	});
	jQuery('.rph-delete').click(function(){
		var dcmess = jQuery('.rph-table').attr('data-confirm');
		var d = confirm(dcmess);
		if (d) {
			var oid = jQuery(this).attr('rel');
			jQuery('form.rph-form .rph-action').val('delete');
			jQuery('form.rph-form .rph-order-id').val(oid);
			jQuery('form.rph-form').submit();
		}
		return false;
	});
	
	// orders list
	if (jQuery('.ois-block').length) {
		var oilines = [];
		jQuery('.ois-block .oil-line').each(function(){
			var oi = parseInt(jQuery(this).attr('rel'));
			var h = jQuery(this).height();
			var mt2 = h / 2;
			var mt3 = h / 2.3;
			jQuery('.oil-item-'+oi).height(h);
			jQuery('.oil-item-'+oi+' span').css('padding-top', mt2+'px');
			jQuery('.oil-item-'+oi+' span.mrk').css('padding-top', mt3+'px');
		});
	}

	// order details
	jQuery('.eddm-purchase-wrap .eddm-purchase-btn').click(function(){
		var item_id = jQuery(this).data('item-id');
		var guid = jQuery(this).data('guid');
		var drop_zip = jQuery(this).data('zipcode');
		var piece_height = jQuery(this).data('piece-height');
		var piece_length = jQuery(this).data('piece-length');
		var thickness_value = jQuery(this).data('thickness-value');
		var weight_value = jQuery(this).data('weight-value');
		var residential_only = jQuery(this).data('residential-only');
		var mail_piece_size = 'LETTER';
		var eddm_prepared_for_crid = jQuery('input.eddm-prepared-for-crid').val();
		var eddm_ddu = jQuery('input.eddm-ddu').val();

		eddm_ddu = eddm_ddu.replace(new RegExp(',', 'gi'), ';');

		var rerror = false;
		if (eddm_prepared_for_crid == '') {
			rerror = true;
			alert(jQuery('input.eddm-prepared-for-crid').data('emessage'));
		} else if (eddm_ddu == '') {
			rerror = true;
			alert(jQuery('input.eddm-ddu').data('emessage'));
		}

		if (!rerror) {
			jQuery(this).addClass('disabled');
			jQuery('.eddm-purchase-wrap-'+item_id+' .ep-loading').show();
			jQuery.get(
				'https://cloud2.iaccutrace.com/servoy-service/rest_ws/ws_360/v2_0/job/'+guid+'/QUOTE/true',
				{},
				function(data) {
					if (data.success) {
						jQuery.ajax({
							url: 'https://cloud2.iaccutrace.com/servoy-service/rest_ws/ws_360/v2_0/job/'+guid+'/QUOTE',
							type: 'PUT',
							data: '{"presort_class":"STANDARD MAIL (EDDM)","mail_piece_size":"'+mail_piece_size+'","drop_zip":"'+drop_zip+'","piece_height":"'+piece_height+'","piece_length":"'+piece_length+'","thickness_value":"'+thickness_value+'","thickness_based_on":"1","weight_value":"'+weight_value+'","weight_unit":"OUNCES","residential_only":"'+residential_only+'","eddm_retail_off":"1","prepared_for_crid":'+eddm_prepared_for_crid+',"DDU":'+eddm_ddu+'}',
							success: function(response) {
								jQuery.get(
									'https://cloud2.iaccutrace.com/servoy-service/rest_ws/ws_360/v2_0/job/'+guid+'/CASS-NCOA-DUPS_01-PRESORT',
									{},
									function(data) {
										if (data.success) {
											var eddm_finish_timeout = setInterval(function(){
												jQuery.get(
													'https://cloud2.iaccutrace.com/servoy-service/rest_ws/ws_360/v2_0/job/'+guid+'/QUOTE',
													{},
													function(data) {
														if (data.task_name == 'FINISHED') {
															clearTimeout(eddm_finish_timeout);
															print_products_eddm_finished(item_id);
														}
													}
												);
											}, 2000);
										}
									}
								);
							}
						});
					}
				}
			);
		}
		return false;
	});
	// categories list
	jQuery('.wp-list-table .cat-groups-link').click(function(){
		var cat_id = jQuery(this).data('id');
		jQuery('.cat-groups-form .category-id').val(cat_id);
		jQuery('.cat-groups-form input.category-groups').prop('checked', false);
		jQuery('.cat-groups-form input.gc-'+cat_id).prop('checked', true);
		jQuery.colorbox({inline:true, href:"#catgroups-popup"});
		return false;
	});
});

function wp2print_colors_picker(c) {
	jQuery(c).each(function(){
		var cval = jQuery(this).val();
		jQuery(this).minicolors({control: 'saturation', defaultValue: cval, format: 'hex', swatches: ['#f44336','#2196f3','#4caf50','#ffeb3b','#ff9800','#795548','#9e9e9e']});
	});
}

function print_products_eddm_finished(item_id) {
	jQuery('.eddm-purchase-wrap-'+item_id+' .ep-loading').hide();
	jQuery('.eddm-purchase-wrap-'+item_id+' .eddm-purchase-btn').hide();
	jQuery('.eddm-purchase-wrap-'+item_id+' .eddm-download-link').show();
	jQuery.post(
		wp2print_adminurl,
		{
			eddm_update_item: 'true',
			item_id: item_id
		}
	);
}

function print_products_select_taxonomy(element) {
	jQuery('#print_products_sort_order_form').submit();
}

function print_products_check_tax_select() {
	var product_type = jQuery('#product-type').val();
	if (product_type == 'fixed' || product_type == 'area' || product_type == 'book' || product_type == 'box' || product_type == 'aec') {
		jQuery('._tax_status_field').parent().show();
		jQuery('li.inventory_tab').addClass('show_if_fixed show_if_area show_if_book show_if_aec').show();
		setTimeout(function(){
			jQuery('li.inventory_tab').addClass('show_if_fixed show_if_area show_if_book show_if_aec').show();
		}, 2000);
	}
}

function print_products_serialize(mixed_value) {
	var _utf8Size = function (str) {
		var size = 0,
			i = 0,
			l = str.length,
			code = '';
		for (i = 0; i < l; i++) {
			code = str.charCodeAt(i);
			if (code < 0x0080) {
				size += 1;
			} else if (code < 0x0800) {
				size += 2;
			} else {
				size += 3;
			}
		}
		return size;
	};
	var _getType = function (inp) {
		var type = typeof inp,
			match;
		var key;

		if (type === 'object' && !inp) {
			return 'null';
		}
		if (type === "object") {
			if (!inp.constructor) {
				return 'object';
			}
			var cons = inp.constructor.toString();
			match = cons.match(/(\w+)\(/);
			if (match) {
				cons = match[1].toLowerCase();
			}
			var types = ["boolean", "number", "string", "array"];
			for (key in types) {
				if (cons == types[key]) {
					type = types[key];
					break;
				}
			}
		}
		return type;
	};
	var type = _getType(mixed_value);
	var val, ktype = '';

	switch (type) {
	case "function":
		val = "";
		break;
	case "boolean":
		val = "b:" + (mixed_value ? "1" : "0");
		break;
	case "number":
		val = (Math.round(mixed_value) == mixed_value ? "i" : "d") + ":" + mixed_value;
		break;
	case "string":
		val = "s:" + _utf8Size(mixed_value) + ":\"" + mixed_value + "\"";
		break;
	case "array":
	case "object":
		val = "a";
		var count = 0;
		var vals = "";
		var okey;
		var key;
		for (key in mixed_value) {
			if (mixed_value.hasOwnProperty(key)) {
				ktype = _getType(mixed_value[key]);
				if (ktype === "function") {
					continue;
				}

				okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
				vals += this.print_products_serialize(okey) + this.print_products_serialize(mixed_value[key]);
				count++;
			}
		}
		val += ":" + count + ":{" + vals + "}";
		break;
	case "undefined":
		// undefined
	default:
		val = "N";
		break;
	}
	if (type !== "object" && type !== "array") {
		val += ";";
	}
	return val;
}

function print_products_num_type() {
	var ntype = jQuery('.num-type').val();
	jQuery('.numbers-label .nlabel').hide();
	jQuery('.numbers-label .nlabel-'+ntype).show();
	if (ntype == 5) {
		jQuery('.ltext-attr-tr').show();
	} else {
		jQuery('.ltext-attr-tr').hide();
	}
	if (ntype == 6) {
		jQuery('.minimum-quantity-mailed-tr').show();
	} else {
		jQuery('.minimum-quantity-mailed-tr').hide();
	}
}

function print_products_bq_style() {
	var bq_style = jQuery('select.bq-style').val();
	if (bq_style == 1) {
		jQuery('.bq-numbers-tr').show();
		jQuery('.bq-min-tr').hide();
	} else {
		jQuery('.bq-numbers-tr').hide();
		jQuery('.bq-min-tr').show();
	}
}

function print_products_pq_style() {
	var pq_style = jQuery('select.pq-style').val();
	if (pq_style == 1) {
		jQuery('.pq-numbers-tr').show();
		jQuery('.pq-defval-tr').hide();
	} else {
		jQuery('.pq-numbers-tr').hide();
		jQuery('.pq-defval-tr').show();
	}
}

function print_products_group_address_add(atype) {
	jQuery('.group-address-form')[0].reset();
	jQuery('.group-address-form .ga-action').val('add');
	jQuery('.group-address-form .ga-type').val(atype);
	jQuery('.group-address-form .ga-add-title').show();
	jQuery('.group-address-form .ga-edit-title').hide();
	jQuery('.group-address-form .ga-error').hide();
	if (atype == 'billing') {
		jQuery('.group-address-form .ga-nosend').prop('checked', false);
		jQuery('.group-address-form .ga-phone-email').show();
		jQuery('.group-address-form .ga-nosend-line').show();
	} else {
		jQuery('.group-address-form .ga-phone-email').hide();
		jQuery('.group-address-form .ga-nosend-line').hide();
	}
	jQuery('.group-address-form .ga-country option').removeAttr('selected');
	print_products_group_address_country_change();
}

function print_products_group_address_edit(akey, atype) {
	var relobj = '.'+atype+'-'+akey;
	jQuery('.group-address-form')[0].reset();
	jQuery('.group-address-form .ga-action').val('edit');
	jQuery('.group-address-form .ga-type').val(atype);
	jQuery('.group-address-form .ga-rel').val(akey);
	jQuery('.group-address-form .ga-add-title').hide();
	jQuery('.group-address-form .ga-edit-title').show();
	jQuery('.group-address-form .ga-error').hide();

	var country = jQuery(relobj+' .a-country').val();
	var state = jQuery(relobj+' .a-state').val();

	jQuery('.group-address-form .ga-label').val(jQuery(relobj+' .a-label').val());
	jQuery('.group-address-form .ga-fname').val(jQuery(relobj+' .a-fname').val());
	jQuery('.group-address-form .ga-lname').val(jQuery(relobj+' .a-lname').val());
	jQuery('.group-address-form .ga-company').val(jQuery(relobj+' .a-company').val());
	jQuery('.group-address-form .ga-country option[value="'+country+'"]').prop('selected', true);
	jQuery('.group-address-form .ga-address').val(jQuery(relobj+' .a-address').val());
	jQuery('.group-address-form .ga-address2').val(jQuery(relobj+' .a-address2').val());
	jQuery('.group-address-form .ga-city').val(jQuery(relobj+' .a-city').val());
	jQuery('.group-address-form .ga-zip').val(jQuery(relobj+' .a-zip').val());
	if (atype == 'billing') {
		var nosend_val = jQuery(relobj+' .a-nosend').val();
		var nosend_ch = false; if (nosend_val == '1') { nosend_ch = true; }
		jQuery('.group-address-form .ga-phone').val(jQuery(relobj+' .a-phone').val());
		jQuery('.group-address-form .ga-email').val(jQuery(relobj+' .a-email').val());
		jQuery('.group-address-form .ga-nosend').prop('checked', nosend_ch);
		jQuery('.group-address-form .ga-phone-email').show();
		jQuery('.group-address-form .ga-nosend-line').show();
	} else {
		jQuery('.group-address-form .ga-phone-email').hide();
		jQuery('.group-address-form .ga-nosend-line').hide();
	}

	print_products_group_address_country_change();
	if (jQuery('.group-address-form .ga-state-'+country).length) {
		jQuery('.group-address-form .ga-state-'+country+' option[value="'+state+'"]').prop('selected', true);
	} else {
		jQuery('.group-address-form .ga-state-text').val(state);
	}
}

function print_products_group_address_delete(akey) {
	var delmessage = jQuery('.group-addresses-content').attr('data-dmessage');
	var d = confirm(delmessage);
	if (d) {
		jQuery('.group-addresses-content .'+akey).remove();
	}
}

function print_products_group_address_country_change() {
	var country = jQuery('.group-address-form .ga-country').val();
	if (country != '') {
		if (jQuery('.group-address-form .ga-state-'+country).length) {
			jQuery('.group-address-form .ga-state option').removeAttr('selected');

			jQuery('.group-address-form .ga-state-'+country).show();
			jQuery('.group-address-form .ga-state-text').hide();
		} else {
			jQuery('.group-address-form .ga-state').hide();
			jQuery('.group-address-form .ga-state-text').show();
		}
	} else {
		jQuery('.group-address-form .ga-state').hide();
		jQuery('.group-address-form .ga-state-text').show();
	}
}

function print_products_group_address_save() {
	var error = false;
	var gaaction = jQuery('.group-address-form .ga-action').val();
	var gatype = wp2print_trim(jQuery('.group-address-form .ga-type').val());
	var garel = wp2print_trim(jQuery('.group-address-form .ga-rel').val());
	var label = wp2print_trim(jQuery('.group-address-form .ga-label').val());
	var fname = wp2print_trim(jQuery('.group-address-form .ga-fname').val());
	var lname = wp2print_trim(jQuery('.group-address-form .ga-lname').val());
	var company = wp2print_trim(jQuery('.group-address-form .ga-company').val());
	var country = wp2print_trim(jQuery('.group-address-form .ga-country').val());
	var address = wp2print_trim(jQuery('.group-address-form .ga-address').val());
	var address2 = wp2print_trim(jQuery('.group-address-form .ga-address2').val());
	var city = wp2print_trim(jQuery('.group-address-form .ga-city').val());
	var zip = wp2print_trim(jQuery('.group-address-form .ga-zip').val());
	if (gatype == 'billing') {
		var phone = wp2print_trim(jQuery('.group-address-form .ga-phone').val());
		var email = wp2print_trim(jQuery('.group-address-form .ga-email').val());
		var nosend = 0;
		var nosend_ch = jQuery('.group-address-form .ga-nosend').is(':checked');
		if (nosend_ch) { nosend = 1; }
	}

	var state = wp2print_trim(jQuery('.group-address-form .ga-state-text').val());
	if (jQuery('.group-address-form .ga-state-'+country).length) {
		state = jQuery('.group-address-form .ga-state-'+country).val();
	}

	jQuery('.group-address-form .ga-error').hide();

	if (label == '') { error = true; }
	if (fname == '') { error = true; }
	if (lname == '') { error = true; }
	if (company == '') { error = true; }
	if (country == '') { error = true; }
	if (address == '') { error = true; }
	if (city == '') { error = true; }
	if (state == '') { error = true; }
	if (zip == '') { error = true; }
	if (gatype == 'billing') {
		if (phone == '') { error = true; }
		if (email == '') { error = true; }
	}

	if (error) {
		jQuery('.group-address-form .ga-error').slideDown();
	} else {
		if (gaaction == 'edit') {
			var arel = '.'+gatype+'-'+garel;
			jQuery(arel+' .a-line').html(label);
			jQuery(arel+' .a-label').val(label);
			jQuery(arel+' .a-fname').val(fname);
			jQuery(arel+' .a-lname').val(lname);
			jQuery(arel+' .a-company').val(company);
			jQuery(arel+' .a-country').val(country);
			jQuery(arel+' .a-address').val(address);
			jQuery(arel+' .a-address2').val(address2);
			jQuery(arel+' .a-city').val(city);
			jQuery(arel+' .a-state').val(state);
			jQuery(arel+' .a-zip').val(zip);
			if (gatype == 'billing') {
				jQuery(arel+' .a-phone').val(phone);
				jQuery(arel+' .a-email').val(email);
				jQuery(arel+' .a-nosend').val(nosend);
			}
		} else {
			var akey = new Date().getTime();
			var elabel = jQuery('.group-address-form').attr('data-edit');
			var dlabel = jQuery('.group-address-form').attr('data-delete');
			var ahtml = '<tr class="billing-'+akey+'">';
			ahtml += '<td><input type="checkbox" name="'+gatype+'_addresses['+akey+'][active]" value="1"></td>';
			ahtml += '<td class="a-line">'+label+'</td>';
			ahtml += '<td align="right"><a href="#TB_inline?width=400&height=535&inlineId=group-address-form" class="thickbox" onclick="print_products_group_address_edit(\''+akey+'\', \'billing\');">'+elabel+'</a>&nbsp;|&nbsp;<a href="#delete" class="delete-addr" onclick="print_products_group_address_delete(\'billing-'+akey+'\'); return false;">'+dlabel+'</a>';
			ahtml += '<div class="a-info" style="display:none;">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][label]" value="'+label+'" class="a-fname">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][fname]" value="'+fname+'" class="a-fname">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][lname]" value="'+lname+'" class="a-lname">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][company]" value="'+company+'" class="a-company">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][country]" value="'+country+'" class="a-country">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][address]" value="'+address+'" class="a-address">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][address2]" value="'+address2+'" class="a-address2">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][city]" value="'+city+'" class="a-city">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][state]" value="'+state+'" class="a-state">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][zip]" value="'+zip+'" class="a-zip">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][phone]" value="'+phone+'" class="a-phone">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][email]" value="'+email+'" class="a-email">';
			ahtml += '<input type="hidden" name="'+gatype+'_addresses['+akey+'][nosend]" value="'+nosend+'" class="a-nosend">';
			ahtml += '</div></td>';
			ahtml += '</tr>';

			if (jQuery('.ga-'+gatype+'-addresses table .a-noaddress').length) {
				jQuery('.ga-'+gatype+'-addresses table .a-noaddress').remove();
			}
			jQuery('.ga-'+gatype+'-addresses table').append(ahtml);
		}
		self.parent.tb_remove();
	}
	return false;
}

function wp2print_trim(str) {
	if (str != 'undefined') {
		return str.replace(/^\s+|\s+$/g,"");
	} else {
		return '';
	}
}

function create_order_process(step) {
	var error = false;
	if (step == 1) {
		var customer = jQuery('.create-order-form .order-customer').val();
		if (customer == '') {
			error = true;
		}
	} else if (step == 2) {
		var afilled = true;
		var stval = jQuery('.create-order-form .shipping-types input:checked').val();
		jQuery('.create-order-form .co-billing-address .form-field .f-required').each(function(){
			var fval = jQuery(this).val();
			if (jQuery(this).attr('id') == 'billing_address_2') {
				fval = 'FILLED';
			}
			if (fval == '') {
				afilled = false;
			}
		});
		if (stval == 'Shipping') {
			jQuery('.create-order-form .co-shipping-address .form-field .f-required').each(function(){
				var fval = jQuery(this).val();
				if (jQuery(this).attr('id') == 'shipping_address_2') {
					fval = 'FILLED';
				}
				if (fval == '') {
					afilled = false;
				}
			});
		}
		if (!afilled) {
			error = true;
		}
	} else if (step == 3) {
		var quantity = jQuery('.create-order-form .quantity').val();
		if (quantity == '') {
			error = true;
		}
	} else if (step == 4) {
		var subtotal = parseFloat(jQuery('.create-order-form .p-price').val());
		var total = parseFloat(jQuery('.create-order-form .total-price').val());
		if ((subtotal == '' || subtotal == 0) && (total == '' || total == 0)) {
			error = true;
		}
	}
	if (error) {
		var error_message = jQuery('.create-order-form').attr('data-error-required');
		alert(error_message);
		return false;
	}
}

function create_order_copy_billing() {
	jQuery('.co-shipping-address #shipping_company').val(jQuery('.co-billing-address #billing_company').val());
	jQuery('.co-shipping-address #shipping_address_1').val(jQuery('.co-billing-address #billing_address_1').val());
	jQuery('.co-shipping-address #shipping_address_2').val(jQuery('.co-billing-address #billing_address_2').val());
	jQuery('.co-shipping-address #shipping_city').val(jQuery('.co-billing-address #billing_city').val());
	jQuery('.co-shipping-address #shipping_postcode').val(jQuery('.co-billing-address #billing_postcode').val());

	var country = jQuery('.co-billing-address #billing_country').val();
	var country_name = jQuery('.co-billing-address #billing_country option:selected').text();
	jQuery('.co-shipping-address #shipping_country option').removeAttr('selected');
	if (country) {
		jQuery('.co-shipping-address #shipping_country option[value="'+country+'"]').prop('selected', true);
		jQuery('.co-shipping-address #select2-shipping_country-container').html(country_name);
		jQuery('.co-shipping-address #shipping_country').trigger('change');
	}
	var state = jQuery('.co-billing-address #billing_state').val();
	var state_name = state;
	if (jQuery('.co-billing-address #billing_state').length) {
		state_name = jQuery('.co-billing-address #billing_state option:selected').text();
	}
	if (jQuery('.co-shipping-address select#shipping_state').length) {
		jQuery('.co-shipping-address select#shipping_state option[value="'+state+'"]').prop('selected', true);
		jQuery('.co-shipping-address #select2-shipping_state-container').html(state_name);
	} else {
		jQuery('.co-shipping-address #shipping_state').val(state);
	}
	return false;
}

function matrix_get_numbers(num, numbers) {
	var lastnum = num;
	var matrix_numbers = new Array();
	matrix_numbers[0] = 0;
	matrix_numbers[1] = 0;
	if (num > 0) {
		for (var i=0; i<numbers.length; i++) {
			anumb = parseInt(numbers[i]);
			if (num < anumb) {
				matrix_numbers[0] = lastnum;
				matrix_numbers[1] = anumb;
				return matrix_numbers;
			} else if (num == anumb) {
				matrix_numbers[0] = anumb;
				matrix_numbers[1] = anumb;
				return matrix_numbers;
			}
			lastnum = anumb;
		}
		if (numbers.length == 1) {
			matrix_numbers[0] = numbers[0];
		} else {
			matrix_numbers[0] = numbers[numbers.length - 2];
		}
		matrix_numbers[1] = lastnum;
	}
	return matrix_numbers;
}

function matrix_get_price(pmatrix, mval, nmb, nums) {
	var p = 0;
	var min_nmb = nums[0];
	var max_nmb = nums[1];
	if (nmb == min_nmb && nmb < max_nmb) {
		mval = mval + '-' + max_nmb;
		if (pmatrix[mval]) {
			p = (pmatrix[mval] / max_nmb) * nmb;
		}
	} else if (nmb == min_nmb && nmb == max_nmb) {
		mval = mval + '-' + nmb;
		if (pmatrix[mval]) {
			p = pmatrix[mval];
		}
	} else if (nmb > min_nmb && nmb < max_nmb) {
		var min_mval = mval + '-' + min_nmb;
		var max_mval = mval + '-' + max_nmb;
		if (pmatrix[min_mval] && pmatrix[max_mval]) {
			p = pmatrix[min_mval] + (nmb - min_nmb) * (pmatrix[max_mval] - pmatrix[min_mval]) / (max_nmb - min_nmb);
		}
	} else if (nmb > min_nmb && nmb > max_nmb) {
		var min_mval = mval + '-' + min_nmb;
		var max_mval = mval + '-' + max_nmb;
		if (pmatrix[min_mval] && pmatrix[max_mval]) {
			if (min_nmb == max_nmb) {
				p = pmatrix[max_mval] * nmb;
			} else {
				p = pmatrix[max_mval] + (nmb - max_nmb) * (pmatrix[max_mval] - pmatrix[min_mval]) / (max_nmb - min_nmb);
			}
		}
	}
	return p;
}

function matrix_set_tax() {
	var tax_rate = parseFloat(jQuery('.create-order-form .tax-price').attr('data-rate'));
	if (tax_rate) {
		var subtotal = parseFloat(jQuery('.create-order-form .p-price').val());
		if (subtotal > 0) {
			var tax_price = (subtotal / 100) * tax_rate;
			jQuery('.create-order-form .tax-price').val(tax_price.toFixed(2));
		}
	}
}

function matrix_set_shipping_tax(tax_rate) {
	if (tax_rate) {
		var shipping = parseFloat(jQuery('.create-order-form .shipping-price').val());
		if (shipping > 0) {
			var shipping_tax_price = (shipping / 100) * tax_rate;
			jQuery('.create-order-form .shipping-tax-price').val(shipping_tax_price.toFixed(2));
		}
	}
}

function matrix_set_prices() {
	var subtotal = parseFloat(jQuery('.create-order-form .p-price').val());
	var shipping = parseFloat(jQuery('.create-order-form .shipping-price').val());
	var shipping_tax = parseFloat(jQuery('.create-order-form .shipping-tax-price').val());
	var total = subtotal + shipping + shipping_tax;
	jQuery('.create-order-form .total-price').val(total.toFixed(2));
}

function matrix_aval(val) {
	val = val.replace('-', '{dc}');
	val = val.replace(':', '{cc}');
	val = val.replace('|', '{vc}');
	return val;
}

function sva_save_address() {
	jQuery('form.sva-form').submit();
}

// send quote
function send_quote_cproduct() {
	var pid = parseInt(jQuery('.send-quote-form select.order-product').val());
	var cpid = parseInt(jQuery('.send-quote-form').attr('data-custom-product'));
	if (pid == cpid) {
		jQuery('.send-quote-form .sq-cproduct-type').slideDown();
	} else {
		jQuery('.send-quote-form .sq-cproduct-type').slideUp();
	}
}

function send_quote_add_product() {
	var pid = parseInt(jQuery('.send-quote-form select.order-product').val());
	if (!pid) {
		alert(jQuery('.send-quote-form .co-add-product').attr('data-error'));
		return false;
	}
	jQuery('.send-quote-form .product-action').val('add');
	jQuery('.send-quote-form').submit();
	return true;
}

function send_quote_duplicate_product(pkey) {
	jQuery('.send-quote-form .product-key').val(pkey);
	jQuery('.send-quote-form .product-action').val('duplicate');
	jQuery('.send-quote-form').submit();
}

function send_quote_delete_product(pkey) {
	var d = confirm(jQuery('.send-quote-form').attr('data-error-sure'));
	if (d) {
		jQuery('.send-quote-form .product-key').val(pkey);
		jQuery('.send-quote-form .product-action').val('delete');
		jQuery('.send-quote-form').submit();
	}
	return false;
}

function send_quote_process(step) {
	var error = false;
	if (step == 1) {
		var customer = jQuery('.send-quote-form .order-customer').val();
		if (customer == '') {
			error = true;
		}
	} else if (step == 2) {
		var product_action = jQuery('.send-quote-form .product-action').val();
		if (product_action == 'attributes') {
			var quantity = jQuery('.send-quote-form .quantity').val();
			if (quantity == '') {
				error = true;
			} else {
				if (jQuery('.send-quote-form .artwork-files').length) {
					var artworkfiles = jQuery('.send-quote-form .artwork-files').val();
					if (artworkfiles == '' && squploaded) {
						var ctext = jQuery('.send-quote-form').attr('data-confirm-wfiles');
						var cnf = confirm(ctext);
						if (!cnf) {
							jQuery('div.moxie-shim input[type="file"]').trigger('click');
							return false;
						}
					} else {
						if (!squploaded) {
							jQuery('.upload-loading').show();
							uploader.start();
							return false;
						}
					}
				}
			}
		}
	}
	if (error) {
		var error_message = jQuery('.send-quote-form').attr('data-error-required');
		alert(error_message);
		return false;
	}
}

// request payment
function request_payment_process(step) {
	var error = false;
	if (step == 1) {
		var customer = jQuery('.send-quote-form .order-customer').val();
		var product = jQuery('.send-quote-form .order-product').val();
		if (customer == '') {
			error = true;
		}
		if (product == '') {
			error = true;
		}
	} else if (step == 2) {
		if (!squploaded) {
			jQuery('.upload-loading').show();
			uploader.start();
			return false;
		}
	}
	if (error) {
		var error_message = jQuery('.send-quote-form').attr('data-error-required');
		alert(error_message);
		return false;
	}
}

// create order
function create_order_cproduct() {
	var pid = parseInt(jQuery('.create-order-form select.order-product').val());
	var cpid = parseInt(jQuery('.create-order-form').attr('data-custom-product'));
	if (pid == cpid) {
		jQuery('.create-order-form .co-cproduct-type').slideDown();
	} else {
		jQuery('.create-order-form .co-cproduct-type').slideUp();
	}
}

function create_order_add_product() {
	var pid = parseInt(jQuery('.create-order-form select.order-product').val());
	if (!pid) {
		alert(jQuery('.create-order-form .co-add-product').attr('data-error'));
		return false;
	}
	jQuery('.create-order-form .product-action').val('add');
	jQuery('.create-order-form').submit();
	return true;
}

function create_order_duplicate_product(pkey) {
	jQuery('.create-order-form .product-key').val(pkey);
	jQuery('.create-order-form .product-action').val('duplicate');
	jQuery('.create-order-form').submit();
}

function create_order_delete_product(pkey) {
	var d = confirm(jQuery('.create-order-form').attr('data-error-sure'));
	if (d) {
		jQuery('.create-order-form .product-key').val(pkey);
		jQuery('.create-order-form .product-action').val('delete');
		jQuery('.create-order-form').submit();
	}
	return false;
}

var cpc_num = 0;
var cpc_url = '';
var cpc_products = '';
function wp2print_check_product_config() {
	cpc_num = 0;
	cpc_url = jQuery('.ch-product-conf-form').attr('action');
	cpc_products = jQuery('.ch-product-conf-form .ch-pids').val().split(';');
	jQuery('.ch-product-conf').html('');
	jQuery('.ch-product-conf-form .button').hide();
	jQuery('.ch-analyzing').show();
	wp2print_check_product_config_process();
}

function wp2print_check_product_config_process() {
	if (cpc_num < cpc_products.length) {
		jQuery.post(
			cpc_url,
			{
				AjaxAction: 'check-product-config',
				product_id: cpc_products[cpc_num]
			},
			function(data) {
				jQuery('.ch-product-conf').append(data);
				cpc_num++;
				wp2print_check_product_config_process();
			}
		);
	} else {
		jQuery('.ch-analyzing').hide();
		jQuery('.ch-product-conf-form .button').show();
		var cpc_html = jQuery('.ch-product-conf').html();
		jQuery.post(
			cpc_url,
			{
				AjaxAction: 'check-product-send-result',
				result_html: cpc_html
			},
			function(data) {
				jQuery('.ch-product-conf').append(data);
			}
		);
	}
}

function wp2print_pso_check(o) {
	var psoch = jQuery(o).is(':checked');
	if (psoch) {
		jQuery('.pso-list').slideDown();
	} else {
		jQuery('.pso-list').slideUp();
	}
}

function wp2print_pso_add() {
	var trnum = parseInt(jQuery('.pso-data').attr('data-num'));
	var tr_html = jQuery('.pso-data table tbody').html();
	tr_html = tr_html.replace(new RegExp('{N}', 'gi'), trnum);
	jQuery('.pso-list table.pso-list-table').append(tr_html);
	wp2print_pso_renum();
	trnum = trnum + 1;
	jQuery('.pso-data').attr('data-num', trnum)
}

function wp2print_pso_remove(n) {
	var rmessage = jQuery('.pso-data').attr('data-rmessage');
	var rem = confirm(rmessage);
	if (rem) {
		jQuery('.pso-list table.pso-list-table tr.pso-tr'+n).remove();
		wp2print_pso_renum();
	}
}

function wp2print_pso_renum() {
	var n = 1;
	if (jQuery('.pso-list table.pso-list-table tr.pso-tr').length) {
		jQuery('.pso-list table.pso-list-table tr.pso-tr').each(function(){
			jQuery(this).find('td').eq(0).html(n);
			n++;
		});
	}
	
}

function wp2print_pso_sd_check(o) {
	var sdch = jQuery(o).is(':checked');
	if (sdch) {
		jQuery('.pso-sd-data').slideDown();
	} else {
		jQuery('.pso-sd-data').slideUp();
	}
}

function wp2print_ois_add() {
	var form_html = jQuery('.ois-hidden').html();
	var fnmb = parseInt(jQuery('.ois-list').attr('data-nmb'));
	form_html = form_html.replace(new RegExp('{N}', 'gi'), fnmb);
	form_html = form_html.replace('ois-color-fld', 'ois-color-select');
	jQuery('.ois-list').append(form_html);
	wp2print_colors_picker('.ois-color-select');
	jQuery('.ois-list .ois-row-'+fnmb+' .ois-form').slideDown();
	fnmb = fnmb + 1;
	jQuery('.ois-list').attr('data-nmb', fnmb);
}

function wp2print_ois_delete(o) {
	var dm = jQuery('.ois-list').attr('data-message');
	var d = confirm(dm);
	if (d) {
		jQuery('.ois-list .ois-row-'+o).remove();
	}
}

var oistm = 0;
function wp2print_ois_change(order_id, item_id, ptp) {
	var item_status = jQuery('.ois-ldd-'+item_id).val();
	var oitems = jQuery('.ois-order-'+order_id+' select').length;
	var allow_popup = parseInt(jQuery('.ois-popup').attr('data-allow-popup'));
	if (ptp == 'pview') { oitems = jQuery('.ois-order-'+order_id).length; }

	jQuery('.ois-popup h2 span').html(order_id);
	jQuery('.ois-popup').attr('data-order-id', order_id);
	jQuery('.ois-popup').attr('data-item-id', item_id);
	jQuery('.ois-popup').attr('data-status', item_status);
	jQuery('.ois-popup').attr('data-ptp', ptp);
	oistm = 0;
	if (allow_popup == 1) {
		if (oitems > 1) {
			oistm = 500;
			jQuery.colorbox({inline:true, href:"#ois-popup"});
		} else {
			wp2print_ois_submit();
		}
	} else {
		wp2print_ois_submit();
	}
}

function wp2print_ois_network_change(blog_id, order_id, item_id, ptp) {
	var oitems = jQuery('.ois-order-'+blog_id+'-'+order_id+' select').length;
	var item_status = jQuery('.ois-order-'+blog_id+'-'+order_id+' .ois-ldd-'+item_id).val();
	jQuery('.ois-popup h2 span').html(order_id);
	jQuery('.ois-popup').attr('data-blog-id', blog_id);
	jQuery('.ois-popup').attr('data-order-id', order_id);
	jQuery('.ois-popup').attr('data-item-id', item_id);
	jQuery('.ois-popup').attr('data-status', item_status);
	jQuery('.ois-popup').attr('data-ptp', ptp);
	oistm = 0;

	if (oitems > 1) {
		oistm = 500;
		jQuery.colorbox({inline:true, href:"#ois-popup"});
	} else {
		wp2print_ois_network_submit();
	}
}

function wp2print_ois_submit() {
	var order_id = jQuery('.ois-popup').attr('data-order-id');
	var item_id = jQuery('.ois-popup').attr('data-item-id');
	var istatus = jQuery('.ois-popup').attr('data-status');
	var ptp = jQuery('.ois-popup').attr('data-ptp');
	var ioption = 0;
	if (jQuery('.ois-popup .ois-i-options input').eq(1).is(':checked')) { ioption = 1; }
	jQuery.colorbox.close();
	if (ioption == 1) {
		jQuery('.ois-order-'+order_id+' select option').removeAttr('selected');
		jQuery('.ois-order-'+order_id+' select option[value="'+istatus+'"]').prop('selected', true);
	}
	jQuery('.ois-popup .ois-i-options input').eq(0).trigger('click')

	jQuery('.ois-order-'+order_id+' select').attr('disabled', 'disabled');
	jQuery.post(wp2print_adminurl,
		{
			OisAjaxAction: 'change-oistatus-from-list',
			order_id: order_id,
			item_id: item_id,
			ioption: ioption,
			item_status: istatus
		},
		function(data) {
			jQuery('.ois-order-'+order_id+' select').removeAttr('disabled');
			if (ioption == 1) {
				jQuery('.ois-order-'+order_id+' .ois-success').slideDown();
			} else {
				if (ptp == 'detail') {
					jQuery('.ois-order-'+order_id+' .ois-success-'+item_id).slideDown();
				} else {
					jQuery('.ois-order-'+order_id+' .ois-success').slideDown();
				}
			}
			setTimeout(function(){ jQuery('.ois-order-'+order_id+' .ois-success').slideUp(); }, 2000);
			if (ptp == 'pview') {
				if (ioption == 1) {
					jQuery('.ois-order-'+order_id+' .ois-graph .ois').removeClass('active');
					jQuery('.ois-order-'+order_id+' .ois-graph .ois-'+istatus).addClass('active');
				} else {
					jQuery('.ois-graph-'+item_id+' .ois').removeClass('active');
					jQuery('.ois-graph-'+item_id+' .ois-'+istatus).addClass('active');
				}
				if (data != '') {
					jQuery('.ois-order-'+order_id+' .o-status').html(data);
				}
			}
		}
	);
	setTimeout(function(){
		wp2print_ois_tracking(order_id, item_id, istatus, 0);
	}, oistm);
}

function wp2print_ois_network_submit() {
	var blog_id = jQuery('.ois-popup').attr('data-blog-id');
	var order_id = jQuery('.ois-popup').attr('data-order-id');
	var item_id = jQuery('.ois-popup').attr('data-item-id');
	var istatus = jQuery('.ois-popup').attr('data-status');
	var ptp = jQuery('.ois-popup').attr('data-ptp');
	var ioption = 0;
	if (jQuery('.ois-popup .ois-i-options input').eq(1).is(':checked')) { ioption = 1; }
	jQuery.colorbox.close();
	if (ioption == 1) {
		jQuery('.ois-order-'+blog_id+'-'+order_id+' select option').removeAttr('selected');
		jQuery('.ois-order-'+blog_id+'-'+order_id+' select option[value="'+istatus+'"]').prop('selected', true);
	}
	jQuery('.ois-popup .ois-i-options input').eq(0).trigger('click')

	jQuery('.ois-order-'+blog_id+'-'+order_id+' select').attr('disabled', 'disabled');
	jQuery.post(wp2print_adminurl,
		{
			OisAjaxAction: 'change-oistatus-from-list',
			blog_id: blog_id,
			order_id: order_id,
			item_id: item_id,
			ioption: ioption,
			item_status: istatus
		},
		function(data) {
			jQuery('.ois-order-'+blog_id+'-'+order_id+' select').removeAttr('disabled');
			if (ioption == 1) {
				jQuery('.ois-order-'+blog_id+'-'+order_id+' .ois-success').slideDown();
			} else {
				if (ptp == 'detail') {
					jQuery('.ois-order-'+blog_id+'-'+order_id+' .ois-success-'+item_id).slideDown();
				} else {
					jQuery('.ois-order-'+blog_id+'-'+order_id+' .ois-success').slideDown();
				}
			}
			setTimeout(function(){ jQuery('.ois-order-'+blog_id+'-'+order_id+' .ois-success').slideUp(); }, 2000);
			if (ptp == 'pview') {
				if (ioption == 1) {
					jQuery('.ois-order-'+blog_id+'-'+order_id+' .ois-graph td').removeClass('active');
					jQuery('.ois-order-'+blog_id+'-'+order_id+' .ois-graph td.ois-'+istatus).addClass('active');
				} else {
					jQuery('.ois-order-'+blog_id+'-'+order_id+' .ois-graph-'+item_id+' td').removeClass('active');
					jQuery('.ois-order-'+blog_id+'-'+order_id+' .ois-graph-'+item_id+' td.ois-'+istatus).addClass('active');
				}
				if (data != '') {
					jQuery('.ois-order-'+blog_id+'-'+order_id+' .o-status').html(data);
				}
			}
		}
	);
	setTimeout(function(){
		wp2print_ois_tracking(order_id, item_id, istatus, blog_id);
	}, oistm);
}

function wp2print_ois_tracking(order_id, item_id, istatus, blog_id) {
	if (jQuery('#ois-tracking-popup').length && istatus != '') {
		var shipped_status = jQuery('#ois-tracking-popup').attr('data-status');
		if (shipped_status == istatus) {
			jQuery('.ois-tracking-popup').attr('data-order-id', order_id);
			jQuery('.ois-tracking-popup').attr('data-item-id', item_id);
			jQuery('.ois-tracking-popup').attr('data-blog-id', blog_id);
			jQuery.colorbox({inline:true, href:"#ois-tracking-popup"});
		}
	}
}

function wp2print_ois_tracking_submit(se) {
	var order_id = jQuery('.ois-tracking-popup').attr('data-order-id');
	var order_item_id = jQuery('.ois-tracking-popup').attr('data-item-id');
	var blog_id = jQuery('.ois-tracking-popup').attr('data-blog-id');
	var tracking_company = jQuery('.ois-tracking-popup .tracking-company').val();
	var tracking_numbers = jQuery('.ois-tracking-popup .tracking-numbers').val();
	jQuery.colorbox.close();
	jQuery.post(wp2print_adminurl,
		{
			OisAjaxAction: 'oistatus-submit-tracking-info',
			order_id: order_id,
			item_id: order_item_id,
			blog_id: blog_id,
			tracking_company: tracking_company,
			tracking_numbers: tracking_numbers,
			send_email: se
		}
	);
	var defcompany = jQuery('.ois-tracking-popup .tracking-company').attr('data-defcompany');
	jQuery('.ois-tracking-popup .tracking-company option').removeAttr('selected');
	jQuery('.ois-tracking-popup .tracking-numbers').val('');
	if (defcompany != '') {
		jQuery('.ois-tracking-popup .tracking-company option[value="'+defcompany+'"]').prop('selected', true);
	}
}

function wp2print_sqau_submit() {
	var username = jQuery('.sq-add-user-form .sqau-username').val();
	var email = jQuery('.sq-add-user-form .sqau-email').val();
	var fname = jQuery('.sq-add-user-form .sqau-fname').val();
	var lname = jQuery('.sq-add-user-form .sqau-lname').val();
	var pass = jQuery('.sq-add-user-form .sqau-pass').val();

	jQuery('.sq-add-user-form .sq-add-user-error').hide();
	if (username == '' || email == '' || pass == '') {
		var err = jQuery('.sq-add-user-form').attr('data-required');
		jQuery('.sq-add-user-form .sq-add-user-error').html(err).fadeIn();
	} else {
		jQuery('.sq-add-user-form .sq-add-user-loading').show();
		jQuery('.sq-add-user-form .button-primary').attr('disabled', 'disabled');
		jQuery.post(wp2print_adminurl,
			{
				AjaxAction: 'send-quote-add-user',
				u_username: username,
				u_email: email,
				u_fname: fname,
				u_lname: lname,
				u_pass: pass
			},
			function(data) {
				jQuery('.sq-add-user-form .sq-add-user-loading').hide();
				var r_data = data.split(';');
				if (r_data[0] == 'success') {
					jQuery('.send-quote-form .order-customer').append('<option value="'+r_data[1]+'">'+r_data[2]+'</option>');
					jQuery('.send-quote-form .order-customer option').removeAttr('selected');
					jQuery('.send-quote-form .order-customer option[value="'+r_data[1]+'"]').prop('selected', true);
					jQuery.colorbox.close();
					jQuery('.sq-add-user-form .form-table input').val('');
				} else {
					jQuery('.sq-add-user-form .sq-add-user-error').html(r_data[1]).fadeIn();
				}
				jQuery('.sq-add-user-form .button-primary').removeAttr('disabled');
			}
		);
	}
	return false;
}

function wp2print_coau_submit() {
	var username = jQuery('.co-add-user-form .coau-username').val();
	var email = jQuery('.co-add-user-form .coau-email').val();
	var fname = jQuery('.co-add-user-form .coau-fname').val();
	var lname = jQuery('.co-add-user-form .coau-lname').val();
	var pass = jQuery('.co-add-user-form .coau-pass').val();

	jQuery('.co-add-user-form .sq-add-user-error').hide();
	if (username == '' || email == '' || pass == '') {
		var err = jQuery('.co-add-user-form').attr('data-required');
		jQuery('.co-add-user-form .sq-add-user-error').html(err).fadeIn();
	} else {
		jQuery('.co-add-user-form .sq-add-user-loading').show();
		jQuery('.co-add-user-form .button-primary').attr('disabled', 'disabled');
		jQuery.post(wp2print_adminurl,
			{
				AjaxAction: 'create-order-add-user',
				u_username: username,
				u_email: email,
				u_fname: fname,
				u_lname: lname,
				u_pass: pass
			},
			function(data) {
				jQuery('.co-add-user-form .sq-add-user-loading').hide();
				var r_data = data.split(';');
				if (r_data[0] == 'success') {
					jQuery('.create-order-form .order-customer').append('<option value="'+r_data[1]+'">'+r_data[2]+'</option>');
					jQuery('.create-order-form .order-customer option').removeAttr('selected');
					jQuery('.create-order-form .order-customer option[value="'+r_data[1]+'"]').prop('selected', true);
					jQuery.colorbox.close();
					jQuery('.co-add-user-form .form-table input').val('');
				} else {
					jQuery('.co-add-user-form .sq-add-user-error').html(r_data[1]).fadeIn();
				}
				jQuery('.co-add-user-form .button-primary').removeAttr('disabled');
			}
		);
	}
	return false;
}

function wp2print_group_use_printshop() {
	var use_printshop = jQuery('.users-groups-form .use-printshop').is(':checked');
	if (use_printshop) {
		jQuery('.users-groups-form .use-privatestore').removeAttr('checked');
		jQuery('.users-groups-form .printshop-theme-settings').show();
		jQuery('.users-groups-form .printshop-theme-menus').show();
	} else {
		jQuery('.users-groups-form .printshop-theme-settings').hide();
		jQuery('.users-groups-form .printshop-theme-menus').hide();
	}
}

function wp2print_group_use_privatestore() {
	var use_privatestore = jQuery('.users-groups-form .use-privatestore').is(':checked');
	if (use_privatestore) {
		jQuery('.users-groups-form .use-printshop').removeAttr('checked');
		jQuery('.users-groups-form .printshop-theme-settings').hide();
		jQuery('.users-groups-form .printshop-theme-menus').show();
	} else {
		jQuery('.users-groups-form .printshop-theme-menus').hide();
	}
}

// vendor companies
function wp2print_ppvc_clear_form() {
	jQuery('.pp-vc-wrap .pp-vc-form input[type="text"]').val('');
	jQuery('.pp-vc-wrap .pp-vc-form select option').removeAttr('selected');
	jQuery('.pp-vc-wrap .pp-vc-form input[type="checkbox"]').removeAttr('checked');
}

function wp2print_ppvc_add() {
	var last_cid = parseInt(jQuery('.pp-vc-wrap .pp-vc-form').attr('data-last-cid'));
	var cid = last_cid + 1;
	wp2print_ppvc_clear_form();
	jQuery('.pp-vc-wrap .pp-vc-form').attr('data-company-id', cid).attr('data-atype', 'add');
	jQuery('.pp-vc-wrap .pp-vc-form input.vc-send').prop('checked', true);
	jQuery('.pp-vc-wrap .pp-vc-form').slideDown();
	return false;
}

function wp2print_ppvc_edit(cid) {
	var c_name = jQuery('.vc-'+cid+' .vc-data .c-name').html();
	var c_address1 = jQuery('.vc-'+cid+' .vc-data .c-address1').html();
	var c_address2 = jQuery('.vc-'+cid+' .vc-data .c-address2').html();
	var c_city = jQuery('.vc-'+cid+' .vc-data .c-city').html();
	var c_postcode = jQuery('.vc-'+cid+' .vc-data .c-postcode').html();
	var c_state = jQuery('.vc-'+cid+' .vc-data .c-state').html();
	var c_country = jQuery('.vc-'+cid+' .vc-data .c-country').html();
	var c_email = jQuery('.vc-'+cid+' .vc-data .c-email').html();
	var c_send = jQuery('.vc-'+cid+' .vc-data .c-send').html();
	var c_employees = jQuery('.vc-'+cid+' .vc-data .c-employees').html();
	var c_access = jQuery('.vc-'+cid+' .vc-data .c-access').html();

	wp2print_ppvc_clear_form();

	jQuery('.pp-vc-wrap .pp-vc-form').attr('data-company-id', cid).attr('data-atype', 'update');
	jQuery('.pp-vc-wrap .pp-vc-form .vc-name').val(c_name);
	jQuery('.pp-vc-wrap .pp-vc-form .vc-address1').val(c_address1);
	jQuery('.pp-vc-wrap .pp-vc-form .vc-address2').val(c_address2);
	jQuery('.pp-vc-wrap .pp-vc-form .vc-city').val(c_city);
	jQuery('.pp-vc-wrap .pp-vc-form .vc-postcode').val(c_postcode);
	jQuery('.pp-vc-wrap .pp-vc-form .vc-state').val(c_state);
	jQuery('.pp-vc-wrap .pp-vc-form .vc-country option[value="'+c_country+'"]').prop('selected', true);
	jQuery('.pp-vc-wrap .pp-vc-form .vc-email').val(c_email);

	if (c_send == 1) {
		jQuery('.pp-vc-wrap .pp-vc-form input.vc-send').prop('checked', true);
	} else {
		jQuery('.pp-vc-wrap .pp-vc-form input.vc-send').removeAttr('checked');
	}

	if (c_access != '') {
		var c_access_vals = c_access.split(';');
		for (var a=0; a<c_access_vals.length; a++) {
			jQuery('.pp-vc-wrap .pp-vc-form input.vc-access-'+c_access_vals[a]).prop('checked', true);
		}
	}

	if (c_employees != '') {
		var c_earray = c_employees.split(',');
		for (var e=0; e<c_earray.length; e++) {
			jQuery('.pp-vc-wrap .pp-vc-form .vc-elist input[value="'+c_earray[e]+'"]').prop('checked', true);
		}
	}

	jQuery('.pp-vc-wrap .pp-vc-form').slideDown();
	return false;
}

function wp2print_ppvc_save() {
	var vc_error = '';
	var atype = jQuery('.pp-vc-wrap .pp-vc-form').attr('data-atype');
	var vc_id = jQuery('.pp-vc-wrap .pp-vc-form').attr('data-company-id');
	var vc_name = jQuery('.pp-vc-wrap .pp-vc-form .vc-name').val();
	var vc_address1 = jQuery('.pp-vc-wrap .pp-vc-form .vc-address1').val();
	var vc_address2 = jQuery('.pp-vc-wrap .pp-vc-form .vc-address2').val();
	var vc_city = jQuery('.pp-vc-wrap .pp-vc-form .vc-city').val();
	var vc_postcode = jQuery('.pp-vc-wrap .pp-vc-form .vc-postcode').val();
	var vc_state = jQuery('.pp-vc-wrap .pp-vc-form .vc-state').val();
	var vc_country = jQuery('.pp-vc-wrap .pp-vc-form .vc-country').val();
	var vc_email = jQuery('.pp-vc-wrap .pp-vc-form .vc-email').val();
	var vc_send = 0;
	var vc_access = '';

	if (jQuery('.pp-vc-wrap .pp-vc-form .vc-send').is(':checked')) { vc_send = 1; }

	var vc_access_label = '';
	jQuery('.pp-vc-wrap .pp-vc-form .vc-access').each(function(){
		if (jQuery(this).is(':checked')) {
			var aval = jQuery(this).val();
			if (vc_access != '') { vc_access += ';'; vc_access_label += ', '; }
			vc_access += aval;
			vc_access_label += jQuery(this).parent().find('.alabel-'+aval).html();
		}
	});

	var vc_send_label = jQuery('.pp-vc-wrap').attr('data-no');
	if (vc_send == 1) {
		var vc_send_label = jQuery('.pp-vc-wrap').attr('data-yes');
	}

	var vc_employees = '';
	var vc_employees_name = '';
	jQuery('.pp-vc-wrap .pp-vc-form .vc-elist input').each(function(){
		if (jQuery(this).is(':checked')) {
			if (vc_employees != '') { vc_employees += ','; vc_employees_name += ', '; }
			vc_employees += jQuery(this).val();
			vc_employees_name += jQuery(this).attr('data-name');
		}
	});

	var vc_address = vc_address1;
	if (vc_address2 != '') { vc_address += ', '+vc_address2; }
	if (vc_city != '') { vc_address += ', '+vc_city; }
	if (vc_state != '') { vc_address += ', '+vc_state; }
	if (vc_postcode != '') { vc_address += ' '+vc_postcode; }
	if (vc_country != '') { vc_address += ', '+vc_country; }

	if (vc_name == '') {
		vc_error = jQuery('.pp-vc-wrap .pp-vc-form .vc-name').attr('data-error');
	} else if (vc_email == '') {
		vc_error = jQuery('.pp-vc-wrap .pp-vc-form .vc-email').attr('data-error');
	}
	if (vc_error != '') {
		alert(vc_error);
	} else {
		jQuery.post(wp2print_adminurl,
			{
				AjaxAction: 'vendor-company-action',
				vc_atype: atype,
				vc_id: vc_id,
				vc_name: vc_name,
				vc_address1: vc_address1,
				vc_address2: vc_address2,
				vc_city: vc_city,
				vc_postcode: vc_postcode,
				vc_state: vc_state,
				vc_country: vc_country,
				vc_email: vc_email,
				vc_send: vc_send,
				vc_employees: vc_employees,
				vc_access: vc_access
			}
		);
		if (atype == 'update') {
			jQuery('.vc-'+vc_id+' .lc-name').html(vc_name);
			jQuery('.vc-'+vc_id+' .lc-address').html(vc_address);
			jQuery('.vc-'+vc_id+' .lc-email').html(vc_email);
			jQuery('.vc-'+vc_id+' .lc-send').html(vc_send_label);
			jQuery('.vc-'+vc_id+' .lc-employees').html(vc_employees_name);
			jQuery('.vc-'+vc_id+' .lc-access').html(vc_access_label);

			jQuery('.vc-'+vc_id+' .vc-data .c-name').html(vc_name);
			jQuery('.vc-'+vc_id+' .vc-data .c-address1').html(vc_address1);
			jQuery('.vc-'+vc_id+' .vc-data .c-address2').html(vc_address2);
			jQuery('.vc-'+vc_id+' .vc-data .c-city').html(vc_city);
			jQuery('.vc-'+vc_id+' .vc-data .c-postcode').html(vc_postcode);
			jQuery('.vc-'+vc_id+' .vc-data .c-state').html(vc_state);
			jQuery('.vc-'+vc_id+' .vc-data .c-country').html(vc_country);
			jQuery('.vc-'+vc_id+' .vc-data .c-email').html(vc_email);
			jQuery('.vc-'+vc_id+' .vc-data .c-send').html(vc_send);
			jQuery('.vc-'+vc_id+' .vc-data .c-employees').html(vc_employees);
			jQuery('.vc-'+vc_id+' .vc-data .c-access').html(vc_access);
		} else {
			var del_label = jQuery('.pp-vc-wrap').attr('data-del-label');

			var vc_tr_html = '<tr class="vc-'+vc_id+'">';
			vc_tr_html += '<td class="vc-nm"><a href="#edit" onclick="return wp2print_ppvc_edit('+vc_id+');" class="lc-name">'+vc_name+'</a>';
			vc_tr_html += '<div class="vc-data" style="display:none;"><span class="c-name">'+vc_name+'</span><span class="c-address1">'+vc_address1+'</span><span class="c-address2">'+vc_address2+'</span><span class="c-city">'+vc_city+'</span><span class="c-postcode">'+vc_postcode+'</span><span class="c-state">'+vc_state+'</span><span class="c-country">'+vc_country+'</span><span class="c-email">'+vc_email+'</span><span class="c-send">'+vc_send+'</span><span class="c-employees">'+vc_employees+'</span><span class="c-access">'+vc_access+'</span></div></td>';
			vc_tr_html += '<td class="lc-address">'+vc_address+'</td>';
			vc_tr_html += '<td class="lc-email">'+vc_email+'</td>';
			vc_tr_html += '<td class="lc-send">'+vc_send_label+'</td>';
			vc_tr_html += '<td class="lc-employees">'+vc_employees_name+'</td>';
			vc_tr_html += '<td class="lc-access">'+vc_access_label+'</td>';
			vc_tr_html += '<td class="vc-del"><a href="#delete" onclick="return wp2print_ppvc_delete('+vc_id+');">'+del_label+'</a></td></tr>';

			if (jQuery('.pp-vc-wrap .pp-vc-table .vc-no-records').length) {
				jQuery('.pp-vc-wrap .pp-vc-table .vc-no-records').remove();
			}
			jQuery('.pp-vc-wrap .pp-vc-table').append(vc_tr_html);
			jQuery('.pp-vc-wrap .pp-vc-form').attr('data-last-cid', vc_id);
		}
		jQuery('.pp-vc-wrap .pp-vc-form').slideUp();
		setTimeout(function(){ wp2print_ppvc_clear_form(); }, 1000);
	}
}

function wp2print_ppvc_delete(cid) {
	var dmessage = jQuery('.pp-vc-wrap').attr('data-del-error');
	var d = confirm(dmessage);
	if (d) {
		jQuery.post(wp2print_adminurl,
			{
				AjaxAction: 'vendor-company-action',
				vc_atype: 'delete',
				vc_id: cid
			}
		);
		jQuery('.pp-vc-wrap .pp-vc-table .vc-'+cid).remove();
	}
	return false;
}

function wp2print_vendor_unassign(iid) {
	var mess = jQuery('.oiv-employee-'+iid).attr('data-confirm');
	var cnf = confirm(mess);
	if (cnf) {
		jQuery.post(wp2print_adminurl,
			{
				AjaxAction: 'oi-vendor-unassign',
				item_id: iid
			}
		);
		jQuery('.oiv-employee-'+iid).slideUp();
	}
	return false;
}

function user_orders_reorder(order_id, item_id) {
	jQuery('.user-orders-form .uoa-order-id').val(order_id);
	jQuery('.user-orders-form .uoa-item-id').val(item_id);
	jQuery('.user-orders-form').submit();
	return false;
}

function wp2print_oirsdate_change(order_id, item_id, sdate) {
	var oitems = jQuery('.item-rsdate').length;
	jQuery('.oirsdate-popup').attr('data-order-id', order_id);
	jQuery('.oirsdate-popup').attr('data-item-id', item_id);
	jQuery('.oirsdate-popup').attr('data-sdate', sdate);
	if (oitems > 1) {
		jQuery.colorbox({inline:true, href:"#oirsdate-popup"});
	} else {
		wp2print_oirsdate_submit();
	}
}

function wp2print_oirsdate_submit() {
	var order_id = jQuery('.oirsdate-popup').attr('data-order-id');
	var item_id = jQuery('.oirsdate-popup').attr('data-item-id');
	var sdate = jQuery('.oirsdate-popup').attr('data-sdate');

	var poption = 0;
	if (jQuery('.oirsdate-popup input').eq(1).is(':checked')) { poption = 1; }

	jQuery.colorbox.close();

	if (poption == 1) {
		jQuery('.item-rsdate').val(sdate);
	}
	jQuery('.oirsdate-popup input').eq(0).trigger('click');

	jQuery.post(wp2print_adminurl,
		{
			OirsdateAjaxAction: 'change-oirsdate',
			order_id: order_id,
			item_id: item_id,
			item_sdate: sdate,
			poption: poption
		},
		function(data) {
			if (poption == 1) {
				jQuery('.oirsdate-success').slideDown();
			} else {
				jQuery('.oirsdate-success-'+item_id).slideDown();
			}
			setTimeout(function(){ jQuery('.oirsdate-success').slideUp(); }, 2000);
		}
	);
}

function wp2print_employee_assign_to_me(order_id, item_id) {
	jQuery('.vatm-popup').attr('data-order-id', order_id);
	jQuery('.vatm-popup').attr('data-item-id', item_id);
	if (jQuery('.employee-assign-table .e-oitem').length > 1) {
		jQuery.colorbox({inline:true, href:"#vatm-popup"});
	} else {
		wp2print_employee_assign_to_me_submit();
	}
	
}

function wp2print_employee_assign_to_me_submit() {
	var order_id = jQuery('.vatm-popup').attr('data-order-id');
	var item_id = jQuery('.vatm-popup').attr('data-item-id');
	var vendor_id = jQuery('.vatm-popup').attr('data-vendor-id');
	var vendor_company_id = jQuery('.vatm-popup').attr('data-company-id');

	var poption = 0;
	if (jQuery('.vatm-popup input').eq(1).is(':checked')) { poption = 1; }

	jQuery.colorbox.close();

	jQuery('.vatm-popup input').eq(0).trigger('click');

	if (poption == 1) {
		jQuery('.employee-assign-table .order-item-employee option[value="'+vendor_id+'"]').prop('selected', true);
		jQuery('.employee-assign-table .e-oitem .button').hide();
		if (vendor_company_id) {
			jQuery('.vendor-assign-table .order-item-vendor option[value="'+vendor_company_id+'"]').prop('selected', true);
		}
		jQuery('.employee-assign-table .oive-success').slideDown();
	} else {
		jQuery('.employee-assign-table .e-oitem-'+item_id+' .order-item-employee option[value="'+vendor_id+'"]').prop('selected', true);
		jQuery('.employee-assign-table .e-oitem-'+item_id+' .button').hide();
		if (vendor_company_id) {
			jQuery('.vendor-assign-table .v-order-item-'+item_id+' .order-item-vendor option[value="'+vendor_company_id+'"]').prop('selected', true);
		}
		jQuery('.employee-assign-table .e-oitem-'+item_id+' .oive-success').slideDown();
	}

	jQuery.post(wp2print_adminurl,
		{
			AjaxAction: 'vendor-assign-to-me',
			order_id: order_id,
			item_id: item_id,
			poption: poption
		},
		function(data) {
			setTimeout(function(){ jQuery('.employee-assign-table .oive-success').slideUp(); }, 2000);
		}
	);
}

function wp2print_pts_calculate() {
	var dunit = jQuery('.term-dimensions-wrap').data('dimm-unit');
	var w = parseFloat(jQuery('.term-dimensions-wrap .dimm-width').val());
	var h = parseFloat(jQuery('.term-dimensions-wrap .dimm-height').val());
	if (w) {
		jQuery('.term-dimensions-pts-wrap .pts-width').val(wp2print_unit_to_pts(w, dunit));
	}
	if (h) {
		jQuery('.term-dimensions-pts-wrap .pts-height').val(wp2print_unit_to_pts(h, dunit));
	}
}

function wp2print_unit_to_pts(num, dimension_unit) {
	if (num) {
		if (dimension_unit == 'm') {
			num = num * 2835;
		} else if (dimension_unit == 'dm') {
			num = num * 283.464567;
		} else if (dimension_unit == 'cm') {
			num = num * 28.3464567;
		} else if (dimension_unit == 'mm') {
			num = num * 2.8346456693;
		} else if (dimension_unit == 'in') {
			num = num * 72;
		} else if (dimension_unit == 'ft') {
			num = num * 864;
		} else if (dimension_unit == 'yd') {
			num = num * 2592;
		}
		num = Math.floor(num);
	}
	return num;
}

// preflight profiles
function wp2print_pfp_clear_form() {
	jQuery('.pfp-wrap .pfp-form input[type="text"]').val('');
	jQuery('.pfp-wrap .pfp-form select option').removeAttr('selected');
	jQuery('.pfp-wrap .pfp-form input[type="checkbox"]').prop('checked', false);
}

function wp2print_pfp_add() {
	var last_pid = parseInt(jQuery('.pfp-wrap .pfp-form').attr('data-last-pid'));
	var pid = last_pid + 1;
	wp2print_pfp_clear_form();
	jQuery('.pfp-wrap .pfp-form').attr('data-profile-id', pid).attr('data-atype', 'add');
	jQuery('.pfp-wrap .pfp-form').slideDown();
	return false;
}

function wp2print_pfp_edit(pid) {
	var p_name = jQuery('.pfp-'+pid+' .pfp-data .p-name').html();
	var p_wlimit = jQuery('.pfp-'+pid+' .pfp-data .p-wlimit').html();
	var p_flimit = jQuery('.pfp-'+pid+' .pfp-data .p-flimit').html();
	var p_slimit = jQuery('.pfp-'+pid+' .pfp-data .p-slimit').html();
	var p_pcode = jQuery('.pfp-'+pid+' .pfp-data .p-pcode').html();
	var p_lang = jQuery('.pfp-'+pid+' .pfp-data .p-lang').html();
	var p_bleed = jQuery('.pfp-'+pid+' .pfp-data .p-bleed').html();
	var p_tolerance = jQuery('.pfp-'+pid+' .pfp-data .p-tolerance').html();
	var p_fmismatch = jQuery('.pfp-'+pid+' .pfp-data .p-fmismatch').html();
	var p_ftrimbox = jQuery('.pfp-'+pid+' .pfp-data .p-ftrimbox').html();
	var p_def = jQuery('.pfp-'+pid+' .pfp-data .p-def').html();

	wp2print_pfp_clear_form();

	jQuery('.pfp-wrap .pfp-form').attr('data-profile-id', pid).attr('data-atype', 'update');
	jQuery('.pfp-wrap .pfp-form .p-name').val(p_name);
	jQuery('.pfp-wrap .pfp-form .p-wlimit').val(p_wlimit);
	jQuery('.pfp-wrap .pfp-form .p-flimit').val(p_flimit);
	jQuery('.pfp-wrap .pfp-form .p-slimit').val(p_slimit);
	jQuery('.pfp-wrap .pfp-form .p-lang').val(p_lang);
	jQuery('.pfp-wrap .pfp-form .p-bleed').val(p_bleed);
	jQuery('.pfp-wrap .pfp-form .p-tolerance').val(p_tolerance);

	jQuery('.pfp-wrap .pfp-form .p-pcode option[value="'+p_pcode+'"]').prop('selected', true);

	if (p_fmismatch == 1) {
		jQuery('.pfp-wrap .pfp-form .p-fmismatch').prop('checked', true);
	}
	if (p_ftrimbox == 1) {
		jQuery('.pfp-wrap .pfp-form .p-ftrimbox').prop('checked', true);
	}
	if (p_def == 1) {
		jQuery('.pfp-wrap .pfp-form .p-def').prop('checked', true);
	}

	jQuery('.pfp-wrap .pfp-form').slideDown();
	return false;
}

function wp2print_pfp_save() {
	var p_error = '';
	var atype = jQuery('.pfp-wrap .pfp-form').attr('data-atype');
	var pid = jQuery('.pfp-wrap .pfp-form').attr('data-profile-id');
	var p_name = jQuery('.pfp-wrap .pfp-form .p-name').val();
	var p_wlimit = jQuery('.pfp-wrap .pfp-form .p-wlimit').val();
	var p_flimit = jQuery('.pfp-wrap .pfp-form .p-flimit').val();
	var p_slimit = jQuery('.pfp-wrap .pfp-form .p-slimit').val();
	var p_pcode = jQuery('.pfp-wrap .pfp-form .p-pcode').val();
	var p_lang = jQuery('.pfp-wrap .pfp-form .p-lang').val();
	var p_bleed = jQuery('.pfp-wrap .pfp-form .p-bleed').val();
	var p_tolerance = jQuery('.pfp-wrap .pfp-form .p-tolerance').val();

	var p_fmismatch = 0;
	var p_ftrimbox = 0;
	var p_def = 0;

	var p_fmismatch_ch = jQuery('.pfp-wrap .pfp-form .p-fmismatch').is(':checked');
	var p_ftrimbox_ch = jQuery('.pfp-wrap .pfp-form .p-ftrimbox').is(':checked');
	var p_def_ch = jQuery('.pfp-wrap .pfp-form .p-def').is(':checked');

	if (p_fmismatch_ch) { p_fmismatch = 1; }
	if (p_ftrimbox_ch) { p_ftrimbox = 1; }
	if (p_def_ch) { p_def = 1; }

	var p_pcode_name = '';
	if (p_pcode) { p_pcode_name = icc_pcodes[p_pcode]; }

	if (p_name == '' || p_wlimit == '' || p_flimit == '' || p_slimit == '' || p_pcode == '' || p_lang == '' || p_bleed == '') {
		p_error = jQuery('.pfp-wrap .pfp-form').attr('data-error');
	}
	if (p_error != '') {
		alert(p_error);
	} else {
		jQuery.post(wp2print_adminurl,
			{
				AjaxAction: 'preflight-profiles-action',
				p_atype: atype,
				p_id: pid,
				p_name: p_name,
				p_wlimit: p_wlimit,
				p_flimit: p_flimit,
				p_slimit: p_slimit,
				p_pcode: p_pcode,
				p_lang: p_lang,
				p_bleed: p_bleed,
				p_tolerance: p_tolerance,
				p_fmismatch: p_fmismatch,
				p_ftrimbox: p_ftrimbox,
				p_def: p_def
			}
		);
		var yes_label = jQuery('.pfp-wrap').attr('data-yes');
		if (atype == 'update') {
			jQuery('.pfp-'+pid+' .lp-name').html(p_name);
			jQuery('.pfp-'+pid+' .pfp-wlimit').html(p_wlimit);
			jQuery('.pfp-'+pid+' .pfp-flimit').html(p_flimit);
			jQuery('.pfp-'+pid+' .pfp-slimit').html(p_slimit);
			jQuery('.pfp-'+pid+' .pfp-pcode').html(p_pcode_name);
			jQuery('.pfp-'+pid+' .pfp-lang').html(p_lang);
			jQuery('.pfp-'+pid+' .pfp-bleed').html(p_bleed);
			jQuery('.pfp-'+pid+' .pfp-tolerance').html(p_tolerance);

			jQuery('.pfp-'+pid+' .pfp-data .p-name').html(p_name);
			jQuery('.pfp-'+pid+' .pfp-data .p-wlimit').html(p_wlimit);
			jQuery('.pfp-'+pid+' .pfp-data .p-flimit').html(p_flimit);
			jQuery('.pfp-'+pid+' .pfp-data .p-slimit').html(p_slimit);
			jQuery('.pfp-'+pid+' .pfp-data .p-pcode').html(p_pcode);
			jQuery('.pfp-'+pid+' .pfp-data .p-lang').html(p_lang);
			jQuery('.pfp-'+pid+' .pfp-data .p-bleed').html(p_bleed);
			jQuery('.pfp-'+pid+' .pfp-data .p-tolerance').html(p_tolerance);
			jQuery('.pfp-'+pid+' .pfp-data .p-fmismatch').html(p_fmismatch);
			jQuery('.pfp-'+pid+' .pfp-data .p-ftrimbox').html(p_ftrimbox);
			jQuery('.pfp-'+pid+' .pfp-data .p-def').html(p_def);

			if (p_fmismatch == 1) {
				jQuery('.pfp-'+pid+' .pfp-fmismatch').html(yes_label);
			} else {
				jQuery('.pfp-'+pid+' .pfp-fmismatch').html('-');
			}
			if (p_ftrimbox == 1) {
				jQuery('.pfp-'+pid+' .pfp-ftrimbox').html(yes_label);
			} else {
				jQuery('.pfp-'+pid+' .pfp-ftrimbox').html('-');
			}
			if (p_def == 1) {
				jQuery('.pfp-'+pid+' .pfp-def').html(yes_label);
			} else {
				jQuery('.pfp-'+pid+' .pfp-def').html('-');
			}
		} else {
			var del_label = jQuery('.pfp-wrap').attr('data-del-label');

			var p_fmismatch_label = '-'; var p_ftrimbox_label = '-'; var p_def_label = '-';
			if (p_fmismatch == 1) { p_fmismatch_label = yes_label; }
			if (p_ftrimbox == 1) { p_ftrimbox_label = yes_label; }
			if (p_def == 1) { p_def_label = yes_label; }

			var tr_html = '<tr class="pfp-row pfp-'+pid+'">';
			tr_html += '<td class="pfp-nm"><a href="#edit" onclick="return wp2print_pfp_edit('+pid+');" class="lp-name">'+p_name+'</a>';
			tr_html += '<div class="pfp-data" style="display:none;"><span class="p-name">'+p_name+'</span><span class="p-wlimit">'+p_wlimit+'</span><span class="p-flimit">'+p_flimit+'</span><span class="p-slimit">'+p_slimit+'</span><span class="p-pcode">'+p_pcode+'</span><span class="p-lang">'+p_lang+'</span><span class="p-bleed">'+p_bleed+'</span><span class="p-tolerance">'+p_tolerance+'</span><span class="p-fmismatch">'+p_fmismatch+'</span><span class="p-ftrimbox">'+p_ftrimbox+'</span><span class="p-def">'+p_def+'</span></div></td>';
			tr_html += '<td class="pfp-wlimit">'+p_wlimit+'</td>';
			tr_html += '<td class="pfp-flimit">'+p_flimit+'</td>';
			tr_html += '<td class="pfp-slimit">'+p_slimit+'</td>';
			tr_html += '<td class="pfp-pcode">'+p_pcode_name+'</td>';
			tr_html += '<td class="pfp-lang">'+p_lang+'</td>';
			tr_html += '<td class="pfp-bleed">'+p_bleed+'</td>';
			tr_html += '<td class="pfp-tolerance">'+p_tolerance+'</td>';
			tr_html += '<td class="pfp-fmismatch">'+p_fmismatch_label+'</td>';
			tr_html += '<td class="pfp-ftrimbox">'+p_ftrimbox_label+'</td>';
			tr_html += '<td class="pfp-def">'+p_def_label+'</td>';
			tr_html += '<td class="pfp-del"><a href="#delete" onclick="return wp2print_pfp_delete('+pid+');">'+del_label+'</a></td></tr>';

			if (jQuery('.pfp-wrap .pfp-table .pfp-no-records').length) {
				jQuery('.pfp-wrap .pfp-table .pfp-no-records').remove();
			}
			jQuery('.pfp-wrap .pfp-table').append(tr_html);
			jQuery('.pfp-wrap .pfp-form').attr('data-last-pid', pid);
		}
		if (p_def == 1) {
			jQuery('.pfp-table .pfp-row').each(function(){
				if (!jQuery(this).hasClass('pfp-'+pid)) {
					jQuery(this).find('.p-def').html(0);
					jQuery(this).find('.pfp-def').html('-');
				}
			});
		}
		jQuery('.pfp-wrap .pfp-form').slideUp();
		setTimeout(function(){ wp2print_pfp_clear_form(); }, 1000);
	}
}

function wp2print_pfp_delete(pid) {
	var dmessage = jQuery('.pfp-wrap').attr('data-del-error');
	var d = confirm(dmessage);
	if (d) {
		jQuery.post(wp2print_adminurl,
			{
				AjaxAction: 'preflight-profiles-action',
				p_atype: 'delete',
				p_id: pid
			}
		);
		jQuery('.pfp-wrap .pfp-table .pfp-'+pid).remove();
	}
	return false;
}

function wp2print_preflight_analize(o, fname, afile, tp, iid, oid) {
	var pid = jQuery('.p-profiles select').val();
	var perror = jQuery('.p-profiles').data('error');
	jQuery('.preflight-process').slideUp(200);
	if (pid) {
		jQuery(o).attr('disabled', 'disabled');
		jQuery(o).parent().find('.a-loading').show();
		jQuery.post(wp2print_adminurl,
			{
				preflight_process: 'true',
				atype: 'analize',
				pid: pid,
				fname: fname,
				afile: afile,
				atp: tp,
				item_id: iid,
				order_id: oid
			},
			function(data) {
				jQuery(o).removeAttr('disabled');
				jQuery(o).parent().find('.a-loading').hide();
				jQuery('.preflight-process .f-nm').html(fname);
				jQuery('.preflight-process .pp-result').html(data);
				jQuery('.preflight-process').slideDown(300);
				wp2print_preflight_thumbs();
			}
		);
	} else {
		alert(perror);
	}
	return false;
}

function wp2print_preflight_reject_init(iid, fnum) {
	jQuery('.pra-popup').data('item-id', iid);
	jQuery('.pra-popup').data('file-num', fnum);
	jQuery('.pra-popup .pra-reason').val('');
	jQuery.colorbox({inline:true, href:"#pra-popup"});
}

function wp2print_preflight_reject_artwork() {
	var item_id = jQuery('.pra-popup').data('item-id');
	var fnum = jQuery('.pra-popup').data('file-num');
	var order_id = jQuery('.preflight-files-list .preflight-item-'+item_id+'-'+fnum).data('order-id');
	var fname = jQuery('.preflight-files-list .preflight-item-'+item_id+'-'+fnum).data('file');
	var afile = jQuery('.preflight-files-list .preflight-item-'+item_id+'-'+fnum).data('fileurl');
	var freason = jQuery('.pra-popup .pra-reason').val();

	jQuery.colorbox.close();

	jQuery('.preflight-files-list .preflight-item-'+item_id+'-'+fnum+' .pr-loading').show();
	jQuery('.preflight-files-list .preflight-item-'+item_id+'-'+fnum+' .button-notify').addClass('disabled');
	jQuery.post(wp2print_adminurl,
		{
			AjaxAction: 'preflight-rejection-notify',
			order_id: order_id,
			item_id: item_id,
			fname: fname,
			afile: afile,
			freason: freason
		},
		function() {
			jQuery('.preflight-files-list .preflight-item-'+item_id+'-'+fnum+' .pr-loading').hide();
			jQuery('.preflight-files-list .preflight-item-'+item_id+'-'+fnum+' .pp-rejected').fadeIn(300);
			jQuery('.preflight-files-list .preflight-item-'+item_id+'-'+fnum+' .button-notify').removeClass('disabled');
			setTimeout(function(){
				jQuery('.preflight-files-list .preflight-item-'+item_id+'-'+fnum+' .pp-rejected').hide();
			}, 4000);
		}
	);
}

function wp2print_preflight_thumbs() {
	if (jQuery('.pf-page .spinning-icon').length) {
		var pttimer = setInterval(function() {
			jQuery('.pf-page .spinning-icon').each(function(){
				var thumb_src = jQuery(this).attr('data-thumb');
				var thisid = jQuery(this).attr('id');
				var imgspinning = new Image();
				imgspinning.src = thumb_src;
				imgspinning.onload = function() {
					jQuery('#'+thisid).attr('src', thumb_src);
					jQuery('#'+thisid).removeClass('spinning-icon');
				}
			});
			if (!jQuery('.pf-page .spinning-icon').length) {
				clearInterval(pttimer);
			}
		}, 2000);
	}
}

function wp2print_select_vendor(item_id, astatus) {
	jQuery('.oiv-popup').data('item_id', item_id);
	jQuery('.oiv-popup h2 span').hide();
	if (astatus == 'rejected') {
		jQuery('.oiv-popup h2 span.rejected-text').show();
	} else {
		jQuery('.oiv-popup h2 span.avaiting-text').show();
	}
	jQuery.colorbox({inline:true, href:"#oiv-popup"});
}

function wp2print_confirm_selection_vendor() {
	var item_id = jQuery('.oiv-popup').data('item_id');
	jQuery('.v-order-item-'+item_id+' .select-item-vendor').hide();
	jQuery('.v-order-item-'+item_id+' select.order-item-vendor').show();
	jQuery.colorbox.close();
}

function wp2print_cancel_selection_vendor() {
	jQuery.colorbox.close();
}

function wp2print_sqh_apply_tax(o) {
	var aval = jQuery(o).val();
	if (aval != '') {
		if (aval == 'products' || aval == 'both') {
			jQuery('.sqh-convert-form .sqh-convert-tax-field').show();
		} else {
			jQuery('.sqh-convert-form .sqh-convert-tax-field').hide();
		}
		if (aval == 'shipping' || aval == 'both') {
			jQuery('.sqh-convert-form .sqh-convert-shipping-tax-field').show();
		} else {
			jQuery('.sqh-convert-form .sqh-convert-shipping-tax-field').hide();
		}
	} else {
		jQuery('.sqh-convert-form .sqh-convert-tax-field').hide();
		jQuery('.sqh-convert-form .sqh-convert-shipping-tax-field').hide();
	}
}

function wp2print_replace(s, f, r) {
	return s.replace(new RegExp(f, 'gi'), r);
}

function wp2print_clear_fname(s) {
	s = s.toLowerCase();
	s = s.replace(/ /g, '');
	s = s.replace(/[&\/\\#,+()$~%'":*?<>{}^@!]/g, '');
	s = wp2print_transliterate(s);
	s = s.replace(/[^\x00-\x7F]/g, '');
	s = encodeURI(s);
	s = s.replace(/[%]/g, '');
	return s;
}

var wp2print_transliterate = function(text) {
    text = text
        .replace(/\u042A/g, '-')
        .replace(/\u0451/g, 'yo')
        .replace(/\u0439/g, 'i')
        .replace(/\u0446/g, 'ts')
        .replace(/\u0443/g, 'u')
        .replace(/\u043A/g, 'k')
        .replace(/\u0435/g, 'e')
        .replace(/\u043D/g, 'n')
        .replace(/\u0433/g, 'g')
        .replace(/\u0448/g, 'sh')
        .replace(/\u0449/g, 'sch')
        .replace(/\u0437/g, 'z')
        .replace(/\u0445/g, 'h')
        .replace(/\u044A/g, '-')
        .replace(/\u0410/g, 'a')
        .replace(/\u0444/g, 'f')
        .replace(/\u044B/g, 'i')
        .replace(/\u0432/g, 'v')
        .replace(/\u0430/g, 'a')
        .replace(/\u043F/g, 'p')
        .replace(/\u0440/g, 'r')
        .replace(/\u043E/g, 'o')
        .replace(/\u043B/g, 'l')
        .replace(/\u0434/g, 'd')
        .replace(/\u0436/g, 'zh')
        .replace(/\u044D/g, 'e')
        .replace(/\u042C/g, '-')
        .replace(/\u044F/g, 'a')
        .replace(/\u0447/g, 'ch')
        .replace(/\u0441/g, 's')
        .replace(/\u043C/g, 'm')
        .replace(/\u0438/g, 'i')
        .replace(/\u0442/g, 't')
        .replace(/\u044C/g, '-')
        .replace(/\u0431/g, 'b')
        .replace(/\u044E/g, 'u');

    return text;
};
