=== Plugin Name ===
Contributors: jw@printscience.com
Donate link: http://wp2print.printscience.com
Tags: product,customize,personalize,customization,personalization,design,online
Requires at least: 4.5
Tested up to: 6.4
Stable tag: 3.7.478
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
WC tested up to: 8.4

wp2print brings full Web-to-Print functions to Wordpress/WooCommerce

== Description ==

wp2print provides everything your printshop needs to begin selling online. Your customers will be able to get a price quote, upload their file or create a new design online and then make a payment.

wp2print can handle the complicated products that you print every day including:

* Brochures and Flyers
* Postcards
* Business cards
* Saddle-stitched catalogs
* Books with perfect binding and wire binding
* Wide-format banners

You can host wp2print on your own servers or receive managed-hosting from Print Science.



== Installation ==

There are 2 options for installing the wp2print plugin:

1. Automated installation via our 1-click installer

2. Manual installation
a. Install Wordpress
b. Install WooCommerce plugin
c. Activate WooCommerce plugin
d. Install Storefront theme
e. Activate Storefront theme
f. Install wp2print the plugin
g. Activate the wp2print plugin
h. Configure the wp2print plugin License code in the 'wp2print' tab in 'Settings' menu
i. Configure the wp2print plugin File Upload Target in the 'wp2print' tab in 'Settings' menu 

== Frequently Asked Questions ==

= Can I purchase a license for wp2print =
Yes, you can purchase a license. Once you have purchased the license, there are no recurring monthly fees to pay.

= Can I host wp2print on my own server =
Yes. You can host wp2print on your own server.

= Do I need a VPS or a dedicated server to host wp2print =

Wordpress with WooCommerce and wp2print is not very demanding of server resources. With reliable hosting providers, shared hosting is normally sufficient to guarantee fast speed for your website. 

= Does the wp2print plugin require WooCommerce  =

Yes. You must have the WooCommerce plugin installed.

= Where do files uploaded by customers arrive =

You have 2 options for the file upload target. We recommend that you choose to use Amazon Web Services S3:

1. Hosting server
Uploaded files arrive in a sub-folder inside your woocommerce folder on the same server hosting your website. Your hosting provider may limit the maximum size of files that you can upload. Since your disk space on the server may be limited, you must regularly remove these uploaded files or you risk running out of space. 

2. Amazon Web Services S3
Uploaded files arrive in a bucket you control on Amazon Web Services S3. There is no limitation to the maximum file size. There is no limit to the total amount of storage space you consume. You can keep the files on Amazon S3 as long as you would like.

= How do I receive the files uploaded by my customers during the order =

In the WooCommerce order details page, you will see a link to download the files uploaded by your customers during the order.

= Does wp2print allow customers to create a new design online =
Yes. wp2print is integrated with the Print Science Designer which allows customers to personalize products online.

= Does wp2print offer Design Online templates that my customers can use =

Yes. We offer templates for the products listed below:

* Business cards
* Flyers
* Brochures
* Presentation covers

These products are included in the wp2print license cost.

== Do I pay money each time a customer uses the Design Online system to personalize a product =
No. You do not pay anything extra for each use of the Designer.

= Can I create my own templates for Design Online =
Yes. However, if you want to create your own templates, then you must have a paid, monthly subscription for the Print Science Designer.


== Screenshots ==

1. Plugin configuration screen
2. Product configuration screen
3. Pricing configuration screen
4. Order details screen

== Changelog ==

= 1.0.4 =
Improved integration with the Print Science Designer

= 1.0.3 =
Improved weight and shipping calculations

= 1.0.2 =
Improvements for pricing calculator for mixed black and white and color books

= 1.0.1 =
Improved translation and internationalization


== Upgrade Notice ==

= 1.0.4 =
Improved integration with the Print Science Designer

= 1.0.3 =
Improved weight and shipping calculations

= 1.0.2 =
Improvements for pricing calculator for mixed black and white and color books

= 1.0.1 =
Improved translation and internationalization



== Arbitrary section ==
