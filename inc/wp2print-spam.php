<?php
add_action('admin_menu', 'print_products_spam_admin_menu');
function print_products_spam_admin_menu() {
	add_users_page(
		__('Spam Registrations', 'wp2print'),
		__('Spam Registrations', 'wp2print'),
		'create_users',
		'print-products-spam-registrations',
		'print_products_spam_admin_page'
	);
}

function print_products_spam_admin_page() {
	global $wpdb;

	$exclude_users = array();

	$spam_users = array();
	$wpusers = $wpdb->get_results(sprintf("SELECT * FROM %susers ORDER BY ID", $wpdb->prefix));
	if ($wpusers) {
		foreach ($wpusers as $wpuser) {
			$spam_users[$wpuser->ID] = $wpuser;
		}
	}

	$has_order_users = $wpdb->get_results(sprintf("SELECT meta_value FROM %spostmeta WHERE meta_key = '_customer_user'", $wpdb->prefix));
	if ($has_order_users) {
		foreach ($has_order_users as $has_order_user) {
			$uid = (int)$has_order_user->meta_value;
			if (isset($spam_users[$uid])) {
				unset($spam_users[$uid]);
			}
		}
	}

	$last_active_users = $wpdb->get_results(sprintf("SELECT user_id FROM %susermeta WHERE meta_key = 'wc_last_active'", $wpdb->prefix));
	if ($last_active_users) {
		foreach ($last_active_users as $last_active_user) {
			$uid = (int)$last_active_user->user_id;
			$adate = date('Y-m-d', (int)$last_active_user->meta_value);
			if (isset($spam_users[$uid])) {
				$udata = $spam_users[$uid];
				$rdate = date('Y-m-d', strtotime($udata->user_registered));
				if ($adate == $rdate) {
					unset($spam_users[$uid]);
				}
			}
		}
	}

	$first_name_users = $wpdb->get_results(sprintf("SELECT user_id FROM %susermeta WHERE meta_key = 'first_name'", $wpdb->prefix));
	if ($first_name_users) {
		foreach ($first_name_users as $first_name_user) {
			$uid = (int)$first_name_user->user_id;
			if (isset($spam_users[$uid])) {
				unset($spam_users[$uid]);
			}
		}
	}

	$last_name_users = $wpdb->get_results(sprintf("SELECT user_id FROM %susermeta WHERE meta_key = 'last_name'", $wpdb->prefix));
	if ($last_name_users) {
		foreach ($last_name_users as $last_name_user) {
			$uid = (int)$last_name_user->user_id;
			if (isset($spam_users[$uid])) {
				unset($spam_users[$uid]);
			}
		}
	}

	if ($spam_users) {
		foreach ($spam_users as $user_id => $spam_user) {
			$userdata = get_userdata($user_id);
			if (!in_array('customer', $userdata->roles) && !in_array('subscriber', $userdata->roles)) {
				unset($spam_users[$user_id]);
			} else {
				$spam_users[$user_id]->role = ucfirst($userdata->roles[0]);
			}
		}
	}
	?>
	<style>
	.wp2print-spam-wrap table{
		background:#FFF;
		margin-bottom:15px;
	}
	.wp2print-spam-wrap table th{
		text-align:left;
	}
	.wp2print-spam-wrap table th,
	.wp2print-spam-wrap table td{
		padding:5px 10px;
		border-bottom:1px solid #EEE;
	}
	.wp2print-spam-wrap table .ch{
		padding-right:0;
	}
	.wp2print-spam-wrap table input{
		margin:0;
	}
	.wp2print-spam-wrap .no-spam{
		background:#FFF;
		padding:10px;
	}
	</style>
	<div class="wrap wp2print-spam-wrap">
		<h2><?php _e('Spam Registrations', 'wp2print'); ?></h2>
		<?php if (isset($_GET['sprocessed']) && $_GET['sprocessed'] == '1') { ?><div id="message" class="notice updated"><p><?php _e('Spam registrations were successfully deleted.', 'wp2print'); ?></p></div><?php } ?>
		<?php if (count($spam_users)) { ?>
			<p><?php _e('Found spam registrations', 'wp2print'); ?>: <?php echo count($spam_users); ?></p>
			<form method="POST" class="wp2print-spam-form">
			<input type="hidden" name="wp2print_spam_action" value="delete">
			<table cellspacing="0" cellpadding="0">
				<tr>
					<th class="ch"><input type="checkbox" name="sel_all" value="1" class="sel-all" checked></th>
					<th><?php _e('ID', 'wp2print'); ?></th>
					<th><?php _e('Username', 'wp2print'); ?></th>
					<th><?php _e('Email', 'wp2print'); ?></th>
					<th><?php _e('Role', 'wp2print'); ?></th>
					<th><?php _e('Created', 'wp2print'); ?></th>
				</tr>
				<?php $unum = 0; foreach ($spam_users as $user_id => $spam_user) { ?>
				<tr>
					<td class="ch"><input type="checkbox" name="spam_ids[]" value="<?php echo $user_id; ?>" class="spam-uid" checked></td>
					<td><?php echo $user_id; ?></td>
					<td><?php echo $spam_user->user_login; ?></td>
					<td><?php echo $spam_user->user_email; ?></td>
					<td><?php echo $spam_user->role; ?></td>
					<td><?php echo $spam_user->user_registered; ?></td>
				</tr>
				<?php $unum++; if ($unum > 2000) { break; } ?>
				<?php } ?>
			</table>
			<input type="submit" class="button button-primary" value="<?php _e('Delete spam registrations', 'wp2print'); ?>">
			</form>
		<?php } else { ?>
			<p class="no-spam"><?php _e('No spam registrations found.', 'wp2print'); ?></p>
		<?php } ?>
	</div>
	<script>
	<!--
	jQuery('.wp2print-spam-wrap .sel-all').change(function(){
		jQuery('.wp2print-spam-wrap .spam-uid').prop('checked', jQuery(this).is(':checked'));
	});
	jQuery('form.wp2print-spam-form').submit(function(){
		return confirm('<?php _e('Are you sure?', 'wp2print'); ?>');
	});
	//--></script>
	<?php
}

add_action('wp_loaded', 'print_products_spam_admin_actions');
function print_products_spam_admin_actions() {
	if (isset($_POST['wp2print_spam_action']) && $_POST['wp2print_spam_action'] == 'delete') {
		require_once( ABSPATH.'wp-admin/includes/user.php' );
		$spam_ids = $_POST['spam_ids'];
		if ($spam_ids) {
			foreach ($spam_ids as $spam_id) {
				wp_delete_user($spam_id);
			}
		}
		wp_redirect('users.php?page=print-products-spam-registrations&sprocessed=1');
		exit;
	}
}
?>