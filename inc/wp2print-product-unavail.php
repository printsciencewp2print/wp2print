<?php
add_action('wp_loaded', 'print_products_unavailability_actions');
function print_products_unavailability_actions() {
	if (isset($_POST['print_products_unavailability_action']) && $_POST['print_products_unavailability_action'] == 'submit') {
		$product_id = $_POST['product_id'];
		update_post_meta($product_id, '_printing_attributes_unavail', $_POST['printing_attributes_unavail']);
		update_post_meta($product_id, '_finishing_attributes_unavail', $_POST['finishing_attributes_unavail']);
		wp_redirect('post.php?post='.$product_id.'&action=edit');
		exit;
	}
}

function print_products_unavailability_page() {
	global $wpdb, $attribute_names, $terms_names, $print_products_settings;
	$product_id = isset($_GET['product_id']) ? $_GET['product_id'] : 0; ?>
	<div class="wrap">
		<?php if ($product_id) {
			$product_type = print_products_get_type($product_id);
			$product_data = get_post($product_id);

			$printing_attributes_unavail = get_post_meta($product_id, '_printing_attributes_unavail', true); if (!$printing_attributes_unavail) { $printing_attributes_unavail = array(); }
			$finishing_attributes_unavail = get_post_meta($product_id, '_finishing_attributes_unavail', true); if (!$finishing_attributes_unavail) { $finishing_attributes_unavail = array(); }

			$product_type_printing_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s AND mtype = 0 ORDER BY mtype, sorder", $wpdb->prefix, $product_id));
			$product_type_finishing_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s AND mtype = 1 ORDER BY mtype, sorder", $wpdb->prefix, $product_id));

			$attributes = $wpdb->get_results(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies ORDER BY attribute_order, attribute_label", $wpdb->prefix));
			print_products_price_matrix_attr_names_init($attributes);

			$finishing_attributes = array();
			if ($product_type_finishing_matrix_types) {
				foreach ($product_type_finishing_matrix_types as $finishing_matrix_type) {
					$mtype_id = $finishing_matrix_type->mtype_id;
					$mtattributes = unserialize($finishing_matrix_type->attributes);
					$aterms = unserialize($finishing_matrix_type->aterms);

					if ($mtattributes) {
						foreach($mtattributes as $mkey => $mtattribute) {
							if ($attribute_types[$mtattribute] == 'text') {
								unset($mtattributes[$mkey]);
							}
						}
					}

					foreach($mtattributes as $mattr_id) {
						$finishing_attributes[$mattr_id] = $aterms[$mattr_id];
					}
				}
			}
			?>
			<?php if ($product_type_printing_matrix_types) { ?>
				<h3 style="font-size:24px;font-weight:500;margin-bottom:20px !important;padding:0;"><?php echo __('Printing attributes availability for', 'wp2print'); ?> "<?php echo $product_data->post_title; ?>"</h3>
				<form action="edit.php?post_type=product&page=print-products-unavailability" method="POST" class="unavailability-matrix">
				<input type="hidden" name="print_products_unavailability_action" value="submit">
				<input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
				<table style="width:auto;margin-bottom:20px;"><tr>
					<?php foreach ($product_type_printing_matrix_types as $product_type_matrix_type_data) {
						$mtype_id = $product_type_matrix_type_data->mtype_id;
						$mtattributes = unserialize($product_type_matrix_type_data->attributes);
						$aterms = unserialize($product_type_matrix_type_data->aterms);
						$num_type = $product_type_matrix_type_data->num_type;

						if ($mtattributes) {
							foreach($mtattributes as $mkey => $mtattribute) {
								if (isset($attribute_types[$mtattribute]) && $attribute_types[$mtattribute] == 'text') {
									unset($mtattributes[$mkey]);
								}
							}
						}
						$aterms = print_products_sort_attribute_terms($aterms);

						$matrix_sets = print_products_get_matrix_array($mtattributes, $aterms);
						?><td valign="top" style="padding-right:10px;">
						<table class="wp-list-table widefat striped" style="width:auto;">
							<tr>
								<?php foreach ($mtattributes as $mtattribute) { ?>
									<td style="color:#000;border-bottom:1px solid #C1C1C1;"><strong><?php echo $attribute_names[$mtattribute]; ?></strong></td>
								<?php } ?>
								<td style="color:#000;border-bottom:1px solid #C1C1C1;"><strong><?php _e('Unavailable', 'wp2print'); ?></strong></td>
							</tr>
							<?php foreach($matrix_sets as $matrix_set) { $fkeys = array(); ?>
								<tr>
									<?php foreach ($matrix_set as $mskey => $msdata) {
										$msarray = explode(':', $msdata);
										$term_id = $msarray[1];
										$fkeys[] = $msdata; ?>
										<td><?php echo $terms_names[$term_id]; ?></td>
									<?php } ?>
									<?php $fkey = implode('-', $fkeys); ?>
									<td align="center"><input type="checkbox" name="printing_attributes_unavail[<?php echo $fkey; ?>]" value="1"<?php if (isset($printing_attributes_unavail[$fkey]) && $printing_attributes_unavail[$fkey] == 1) { echo ' CHECKED'; } ?>></td>
								</tr>
							<?php } ?>
						</table></td>
					<?php } ?>
				</tr></table>

				<?php if ($finishing_attributes) { ?>
					<h3 style="font-size:24px;font-weight:500;margin-bottom:20px !important;padding:0;"><?php echo __('Finishing attributes availability for', 'wp2print'); ?> "<?php echo $product_data->post_title; ?>"</h3>
					<table style="width:auto;margin-bottom:20px;"><tr>
					<?php foreach ($product_type_printing_matrix_types as $product_type_matrix_type_data) {
						$mtype_id = $product_type_matrix_type_data->mtype_id;
						$omtattributes = unserialize($product_type_matrix_type_data->attributes);
						$oaterms = unserialize($product_type_matrix_type_data->aterms);

						if ($omtattributes) {
							foreach($omtattributes as $mkey => $mtattribute) {
								if ($attribute_types[$mtattribute] == 'text') {
									unset($omtattributes[$mkey]);
								}
							}
						}
						foreach ($finishing_attributes as $fattr_id => $faterms) {
							$mtattributes = $omtattributes;
							$mtattributes[] = $fattr_id;

							$aterms = $oaterms;
							$aterms[$fattr_id] = $faterms;
							$aterms = print_products_sort_attribute_terms($aterms);

							$matrix_sets = print_products_get_matrix_array($mtattributes, $aterms);
							?><td valign="top" style="padding-right:10px;">
							<table class="wp-list-table widefat striped" style="width:auto;">
								<tr>
									<?php foreach ($mtattributes as $mtattribute) { ?>
										<td style="color:#000;border-bottom:1px solid #C1C1C1;"><strong><?php echo $attribute_names[$mtattribute]; ?></strong></td>
									<?php } ?>
									<td style="color:#000;border-bottom:1px solid #C1C1C1;"><strong><?php _e('Unavailable', 'wp2print'); ?></strong></td>
								</tr>
								<?php foreach($matrix_sets as $matrix_set) { $fkeys = array(); ?>
									<tr>
										<?php foreach ($matrix_set as $mskey => $msdata) {
											$msarray = explode(':', $msdata);
											$term_id = $msarray[1];
											$fkeys[] = $msdata; ?>
											<td><?php echo $terms_names[$term_id]; ?></td>
										<?php } ?>
										<?php $fkey = implode('-', $fkeys); ?>
										<td align="center"><input type="checkbox" name="finishing_attributes_unavail[<?php echo $fkey; ?>]" value="1"<?php if (isset($finishing_attributes_unavail[$fkey]) && $finishing_attributes_unavail[$fkey] == 1) { echo ' CHECKED'; } ?>></td>
									</tr>
								<?php } ?>
							</table></td>
						<?php } ?>
					<?php } ?>
					</tr></table>
				<?php } ?>
				<p class="submit">
					<a href="post.php?post=<?php echo $product_id; ?>&action=edit" class="button"><?php _e('Back', 'wp2print'); ?></a>
					<input type="submit" value="<?php _e('Submit', 'wp2print'); ?>" class="button-primary">
				</p>
				</form>
			<?php } else { ?>
				<p><?php echo __('No selected attributes', 'wp2print'); ?>.</p>
			<?php } ?>
		<?php } else { ?>
			<p><?php echo __('Product ID is incorrect.', 'wp2print'); ?></p>
		<?php } ?>
	</div>
	<?php
}
?>