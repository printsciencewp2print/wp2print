<?php
add_filter('woocommerce_account_menu_items', 'print_products_orders_rejected_files_account_menu_items', 21);
function print_products_orders_rejected_files_account_menu_items($items) {
	$new_items = array();
	foreach($items as $ikey => $ival) {
		$new_items[$ikey] = $ival;
		if (isset($items['orders-missing-files'])) {
			if ($ikey == 'orders-missing-files') {
				$new_items['orders-rejected-files'] = __('Orders rejected files', 'wp2print');
			}
		} else if (isset($items['orders-missing-approval'])) {
			if ($ikey == 'orders-missing-approval') {
				$new_items['orders-rejected-files'] = __('Orders rejected files', 'wp2print');
			}
		} else if (isset($items['orders-awaiting-approval'])) {
			if ($ikey == 'orders-awaiting-approval') {
				$new_items['orders-rejected-files'] = __('Orders rejected files', 'wp2print');
			}
		} else {
			if ($ikey == 'orders') {
				$new_items['orders-rejected-files'] = __('Orders rejected files', 'wp2print');
			}
		}
	}
	return $new_items;
}

add_action('init', 'print_products_orders_rejected_files_rewrite_endpoint');
function print_products_orders_rejected_files_rewrite_endpoint() {
	if (print_products_my_account_is_front()) {
		add_rewrite_endpoint('orders-rejected-files', EP_ROOT | EP_PAGES);
	} else {
		add_rewrite_endpoint('orders-rejected-files', EP_PAGES);
	}
	flush_rewrite_rules();
}

add_filter('query_vars', 'print_products_orders_rejected_files_query_vars', 10);
function print_products_orders_rejected_files_query_vars($vars) {
	$vars[] = 'orders-rejected-files';
	return $vars;
}

add_action('parse_request', 'print_products_orders_rejected_files_parse_request', 10);
function print_products_orders_rejected_files_parse_request() {
	global $wp;
	$var = 'orders-rejected-files';
	if (isset($wp->query_vars['name']) && $wp->query_vars['name'] == $var) {
		unset($wp->query_vars['name']);
		$wp->query_vars[$var] = $var;
	}
}

add_action('pre_get_posts', 'print_products_orders_rejected_files_pre_get_posts');
function print_products_orders_rejected_files_pre_get_posts($q) {
	if ( ! $q->is_main_query() ) {
		return;
	}
	if (print_products_is_showing_page_on_front($q) && ! print_products_page_on_front_is($q->get('page_id'))) {
		$_query = wp_parse_args($q->query);
		$qv_array = array('orders-rejected-files' => 'orders-rejected-files');
		if (!empty($_query) && array_intersect( array_keys($_query), array_keys($qv_array))) {
			$q->is_page     = true;
			$q->is_home     = false;
			$q->is_singular = true;
			$q->set('page_id', (int)get_option( 'page_on_front'));
			add_filter('redirect_canonical', '__return_false');
		}
	}
}

add_action('woocommerce_account_orders-rejected-files_endpoint', 'print_products_orders_rejected_files_account_page');
function print_products_orders_rejected_files_account_page() {
	include PRINT_PRODUCTS_TEMPLATES_DIR . 'orders-rejected-files.php';
}

add_action('wp_loaded', 'print_products_orders_rejected_files_actions');
function print_products_orders_rejected_files_actions() {
	global $wpdb, $current_user;
	if (isset($_POST['orders_rejected_files_submit']) && $_POST['orders_rejected_files_submit'] == 'true') {
		$order_id = $_POST['order_id'];
		$item_id = $_POST['item_id'];
		$rfile = $_POST['rfile'];
		$afile = $_POST['afile'];
		$ufile = $_POST['ufile'];
		if ($afile) {
			$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
			if ($order_item_data) {
				$artwork_files = unserialize($order_item_data->artwork_files);
				$artwork_rejected = unserialize($order_item_data->artwork_rejected);

				if (!is_array($artwork_files)) { $artwork_files = array(); }
				if ($ufile && $ufile != $afile) {
					foreach ($artwork_files as $afkey => $artwork_file) {
						if ($artwork_file == $ufile) {
							$artwork_files[$afkey] = $afile;
						}
					}
				} else {
					$artwork_files[] = $afile;
				}

				$artwork_rejected_new = array();
				if ($artwork_rejected) {
					foreach ($artwork_rejected as $arejected) {
						if (md5($arejected) != $rfile) {
							$artwork_rejected_new[] = $arejected;
						}
					}
				}

				$wpdb->update($wpdb->prefix.'print_products_order_items', array('artwork_files' => serialize($artwork_files), 'artwork_rejected' => serialize($artwork_rejected_new)), array('item_id' => $item_id));
			}
			if (function_exists('print_products_preflight_update_rstatus')) {
				print_products_preflight_update_rstatus($item_id);
			}

			$order = wc_get_order($order_id);
			// send email to admin
			$nl = '<br>';
			$admin_email = get_option('admin_email');
			$subject = __('New Order Files', 'wp2print');
			$heading = __('New Order Files', 'wp2print');

			$message  = __('Order ID', 'wp2print').': <a href="'.site_url('/wp-admin/'.print_products_woocommerce_get_order_edit_url($order_id)).'">'.$order_id.'</a>'.$nl.$nl;
			$message .= __('Files', 'wp2print').': '.$nl;
			$message .= '<a href="'.print_products_get_amazon_file_url($afile).'">'.basename($afile).'</a>'.$nl;
			print_products_send_wc_mail($admin_email, $subject, $message, $heading);
			echo __('File was successfully saved.', 'wp2print');
		}

		exit;
	}
}

add_filter('the_title', 'print_products_orders_rejected_files_the_title', 12, 2);
function print_products_orders_rejected_files_the_title($title, $id) {
	global $wp_query;
	if (is_account_page() && is_main_query() && in_the_loop() && isset($wp_query->query_vars['orders-rejected-files']) && !is_admin()) {
		$title = __('Orders rejected files', 'wp2print');
	}
	return $title;
}
?>