<?php
add_action('wp_loaded', 'print_products_fixed_actions');
function print_products_fixed_actions() {
	global $wpdb, $attribute_names, $attribute_slugs, $attribute_types, $terms_names, $print_products_settings;
	if (is_admin() && isset($_POST['print_products_fixed_action'])) {
		print_products_price_matrix_attr_names_init();
		switch($_POST['print_products_fixed_action']) {
			case 'import':
				if (isset($_FILES['import_file'])) {
					require_once('includes/image.php');
					require_once('includes/file.php');
					require_once('includes/media.php');
					$ufile = wp_handle_upload($_FILES['import_file'], array('test_form' => false), current_time('mysql'));
					if ($ufile && !$ufile['error']) {
						$import_csv_data = array();
						$csv_file = $ufile['file'];
						$handle = fopen($csv_file, "r");
						if ($handle !== false) {
							$clnmb = 0;
							while (($data = fgetcsv($handle, 1000, ';')) !== false) {
									$import_csv_data[] = $data;
								$clnmb++;
							}
							fclose($handle);
						}
						$mattributes = array();
						foreach($attribute_names as $aid => $aname) { $mattributes[$aname] = $aid; }
						$printing_attributes = unserialize($print_products_settings['printing_attributes']);
						$finishing_attributes = unserialize($print_products_settings['finishing_attributes']);

						$csv_fields = array();
						$csv_attributes = array();
						$csv_attributes_id = array();
						$description_presented = false;
						foreach($import_csv_data[0] as $fkey => $fname) {
							switch($fname) {
								case 'Product Name':
									$csv_fields[$fkey] = 'name';
								break;
								case 'Description':
									$csv_fields[$fkey] = 'description';
									$description_presented = true;
								break;
								case 'Category':
									$csv_fields[$fkey] = 'category';
								break;
								case 'Quantity Style':
									$csv_fields[$fkey] = 'qstyle';
								break;
								case 'Quantities':
									$csv_fields[$fkey] = 'quantities';
								break;
								case 'sku':
								case 'SKU':
									$csv_fields[$fkey] = 'sku';
								break;
								case 'Product image':
									$csv_fields[$fkey] = 'image';
								break;
								default:
									$csv_fields[$fkey] = 'attribute-'.$fkey;
									$csv_attributes[$fkey] = $fname;
									$csv_attributes_id[$fname] = $mattributes[$fname];
								break;
							}
						}
						unset($import_csv_data[0]);

						$imported = 0;
						$csv_records = array();
						if (count($import_csv_data)) {
							foreach($import_csv_data as $csv_data) {
								$pdata = array();
								$attributes = array();
								foreach($csv_fields as $fkey => $fname) {
									if (substr($fname, 0, 9) == 'attribute') {
										if (strlen($csv_data[$fkey])) {
											$aname = $csv_attributes[$fkey];
											$attributes[$aname] = stripcslashes($csv_data[$fkey]);
										}
									} else {
										$pdata[$fname] = stripcslashes($csv_data[$fkey]);
									}
								}
								$pdata['attributes'] = $attributes;
								$csv_records[] = $pdata;
							}
							foreach($csv_records as $csv_record) {
								$product_name = $csv_record['name'];
								$product_description = $csv_record['description'];
								$product_category = $csv_record['category'];
								$product_qstyle = $csv_record['qstyle'];
								$product_quantities = $csv_record['quantities'];
								$product_attributes = $csv_record['attributes'];
								$product_sku = $csv_record['sku'];
								$product_image = $csv_record['image'];
								$pattributes = array();
								$fattributes = array();
								if ($product_attributes) {
									foreach($product_attributes as $aname => $avals) {
										$aid = $csv_attributes_id[$aname];
										$aslug = $attribute_slugs[$aid];
										$avals = explode('|', $avals);
										$aterms = print_products_fixed_get_aterms($avals, 'pa_'.$aslug);
										if ($aid && $aterms) {
											if (in_array($aid, $printing_attributes)) {
												$pattributes[$aid] = $aterms;
											} else {
												$fattributes[$aid] = $aterms;
											}
										}
									}
								}

								$product_id = $wpdb->get_var(sprintf("SELECT ID FROM %sposts WHERE post_type = 'product' AND post_title = '%s'", $wpdb->prefix, $product_name));
								if ($product_id) {
									if ($description_presented) {
										$wpdb->update($wpdb->prefix.'posts', array('post_content' => $product_description), array('ID' => $product_id));
									}
									$product_type_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s ORDER BY mtype, sorder", $wpdb->prefix, $product_id));
									if ($product_type_matrix_types) {
										foreach($product_type_matrix_types as $product_type_matrix_type) {
											$mtype_id = $product_type_matrix_type->mtype_id;
											$mtype = $product_type_matrix_type->mtype;
											$mattributes = unserialize($product_type_matrix_type->attributes);
											$materms = unserialize($product_type_matrix_type->aterms);
											$numbers = explode(',', $product_type_matrix_type->numbers);
											$num_style = $product_type_matrix_type->num_style;

											$nattributes = array();
											if ($mtype == 0) {
												$nattributes = $pattributes;
											} else {
												$nattributes = $fattributes;
											}
											if ($nattributes) {
												$new_mattributes = array();
												$new_materms = array();
												foreach($nattributes as $aid => $aterms) {
													$new_mattributes[] = $aid;
													$new_materms[$aid] = $aterms;
												}
												$num_style = 0; if ($product_qstyle == 'DROPDOWN') { $num_style = 1; }
												$update = array();
												$update['attributes'] = serialize($new_mattributes);
												$update['aterms'] = serialize($new_materms);
												$update['numbers'] = str_replace('|', ',', $product_quantities);
												$update['num_style'] = $num_style;
												$wpdb->update($wpdb->prefix.'print_products_matrix_types', $update, array('mtype_id' => $mtype_id));
											}
											if ($product_sku) {
												$print_products_matrix_prices = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_sku WHERE mtype_id = %s", $wpdb->prefix, $mtype_id));
												if ($print_products_matrix_prices && count($print_products_matrix_prices) == 1) {
													$wpdb->update($wpdb->prefix.'print_products_matrix_sku', array('sku' => $product_sku), array('mtype_id' => $mtype_id));
												}
											}
										}
									}
								} else {
									$insert = array();
									$insert['post_type'] = 'product';
									$insert['post_title'] = $product_name;
									$insert['post_status'] = 'publish';
									if ($description_presented) {
										$insert['post_content'] = $product_description;
									}
									$product_id = wp_insert_post($insert);
									if ($product_id) {
										$num_style = 0; if ($product_qstyle == 'DROPDOWN') { $num_style = 1; }
										wp_set_object_terms($product_id, 'fixed', 'product_type');
										if (count($pattributes)) {
											$new_mattributes = array();
											$new_materms = array();
											foreach($pattributes as $aid => $aterms) {
												$new_mattributes[] = $aid;
												$new_materms[$aid] = $aterms;
											}
											$insert = array();
											$insert['product_id'] = $product_id;
											$insert['mtype'] = 0;
											$insert['attributes'] = serialize($new_mattributes);
											$insert['aterms'] = serialize($new_materms);
											$insert['numbers'] = str_replace('|', ',', $product_quantities);
											$insert['num_style'] = $num_style;
											$insert['num_type'] = 0;
											$insert['sorder'] = 0;
											$wpdb->insert($wpdb->prefix.'print_products_matrix_types', $insert);
										}
										if (count($fattributes)) {
											$new_mattributes = array();
											$new_materms = array();
											foreach($fattributes as $aid => $aterms) {
												$new_mattributes[] = $aid;
												$new_materms[$aid] = $aterms;
											}
											$insert = array();
											$insert['product_id'] = $product_id;
											$insert['mtype'] = 1;
											$insert['attributes'] = serialize($new_mattributes);
											$insert['aterms'] = serialize($new_materms);
											$insert['numbers'] = str_replace('|', ',', $product_quantities);
											$insert['num_style'] = $num_style;
											$insert['num_type'] = 0;
											$insert['sorder'] = 1;
											$wpdb->insert($wpdb->prefix.'print_products_matrix_types', $insert);
										}
									}
								}
								if ($product_id) {
									print_products_fixed_set_category($product_id, $product_category);
									if ($product_image) {
										$image_id = media_sideload_image($product_image, $product_id, null, 'id');
										if (!is_wp_error($image_id)) {
											set_post_thumbnail($product_id, $image_id);
										}
									}
									$imported++;
								}
							}
						}
						wp_redirect('edit.php?post_type=product&page=print-products-fixed-import&fimported='.$imported);
						exit;
					}
				}
			break;
			case 'export':
				$csvsep = ';';
				$csvnl = "\r\n";
				$csv_filename = 'fixed-size-export-'.date('Y-m-d-H-i-s').'.csv';
				$csv_fields = array('name' => 'Product Name', 'description' => 'Description', 'category' => 'Category', 'qstyle' => 'Quantity Style', 'quantities' => 'Quantities', 'sku' => 'SKU');
				$csv_records = array();
				$csv_mtypes = array(array(), array());

				$export_product = $_POST['export_product'];
				if ($export_product) {
					$fixed_products = array(wc_get_product($export_product));
				} else {
					$fixed_products = wc_get_products(array('type' => 'fixed', 'posts_per_page' => -1));
				}
				if ($fixed_products) { $sep = '';
					foreach($fixed_products as $fixed_product) {
						$product_id = $fixed_product->get_id();
						$product_name = $fixed_product->get_name();
						$product_description = $fixed_product->get_description();
						$product_thumb_id = get_post_thumbnail_id($product_id);

						$product_thumb_url = '';
						if ($product_thumb_id) {
							$image_attributes = wp_get_attachment_image_src($product_thumb_id, 'full');
							if ($image_attributes) {
								$product_thumb_url = $image_attributes[0];
							}
						}

						$product_category = array();
						$pcats = get_the_terms($product_id, 'product_cat');
						if ($pcats) {
							foreach($pcats as $pcat) {
								$product_category[] = $pcat->name;
							}
						}

						$qstyle = 'TEXTFIELD';
						$qnumbers = array();
						$matrix_sku = '';
						$product_attributes = array();
						$product_type_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s ORDER BY mtype, sorder", $wpdb->prefix, $product_id));
						if ($product_type_matrix_types) {
							foreach($product_type_matrix_types as $product_type_matrix_type) {
								$mtype_id = $product_type_matrix_type->mtype_id;
								$mtype = $product_type_matrix_type->mtype;
								$mattributes = unserialize($product_type_matrix_type->attributes);
								$materms = unserialize($product_type_matrix_type->aterms);
								$numbers = explode(',', $product_type_matrix_type->numbers);
								$num_style = $product_type_matrix_type->num_style;

								if ($mtype == 0) {
									if ($num_style == 1) { $qstyle = 'DROPDOWN'; }
									$qnumbers = $numbers;
								}
								if ($mattributes) {
									$mattributes = print_products_sort_attributes($mattributes);
									foreach($mattributes as $mattribute) {
										$aterms = $materms[$mattribute];
										$aterm_names = array();
										if ($aterms) {
											$aterms = print_products_get_attribute_terms($aterms);
											foreach($aterms as $aterm_id => $aterm_name) {
												$aterm_names[] = $aterm_name;
											}
										}
										$product_attributes[$mattribute] = implode('|', $aterm_names);
										if (is_array($csv_mtypes[$mtype]) && !in_array($mattribute, $csv_mtypes[$mtype])) {
											$csv_mtypes[$mtype][] = $mattribute;
										}
									}
								}
								$print_products_matrix_prices = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_sku WHERE mtype_id = %s", $wpdb->prefix, $mtype_id));
								if ($print_products_matrix_prices && !$matrix_sku) {
									if (count($print_products_matrix_prices) == 1) {
										$matrix_sku = $print_products_matrix_prices[0]->sku;
									}
								}
							}
						}
						$csv_records[$product_id] = array(
							'name' => $product_name,
							'description' => $product_description,
							'category' => implode('|', $product_category),
							'qstyle' => $qstyle,
							'quantities' => implode('|', $qnumbers),
							'attributes' => $product_attributes,
							'sku' => $matrix_sku,
							'image' => $product_thumb_url
						);
					}
					$exfields = array();
					if (count($csv_mtypes[0])) {
						foreach($csv_mtypes[0] as $mattribute) {
							if (is_array($exfields) && !in_array($mattribute, $exfields)) {
								$csv_fields['attr-'.$mattribute] = $attribute_names[$mattribute];
								$exfields[] = $mattribute;
							}
						}
					}
					if (count($csv_mtypes[1])) {
						foreach($csv_mtypes[1] as $mattribute) {
							if (is_array($exfields) && !in_array($mattribute, $exfields)) {
								$csv_fields['attr-'.$mattribute] = $attribute_names[$mattribute];
								$exfields[] = $mattribute;
							}
						}
					}
					$csv_fields['image'] = 'Product image';
					$fnums = array();
					foreach($csv_fields as $fkey => $fname) {
						if (!isset($fnums[$fkey])) { $fnums[$fkey] = 0; }
						foreach($csv_records as $csv_record) {
							$aval = '';
							if (substr($fkey, 0, 4) == 'attr') {
								$aid = str_replace('attr-', '', $fkey);
								$attributes = $csv_record['attributes'];
								if (isset($attributes[$aid])) {
									$aval = $attributes[$aid];
								}
							} else {
								$aval = $csv_record[$fkey];
							}
							if (strlen($aval)) {
								$fnums[$fkey]++;
							}
						}
					}
					foreach($csv_fields as $fkey => $fname) {
						if (!$fnums[$fkey]) {
							unset($csv_fields[$fkey]);
						}
					}

					$csv_content = implode($csvsep, $csv_fields).$csvnl;
					foreach($csv_records as $csv_record) {
						$pfields = array();
						foreach($csv_fields as $fkey => $fname) {
							if (substr($fkey, 0, 4) == 'attr') {
								$aid = str_replace('attr-', '', $fkey);
								$attributes = $csv_record['attributes'];
								$aval = '';
								if (isset($attributes[$aid])) {
									$aval = $attributes[$aid];
								}
								$pfields[] = print_products_fixed_to_csv($aval);
							} else {
								$pfields[] = print_products_fixed_to_csv($csv_record[$fkey]);
							}
						}
						$csv_content .= implode($csvsep, $pfields).$csvnl;
					}

					header("Content-Type: application/zip");
					header("Content-Disposition: attachment; filename=".basename($csv_filename));
					header("Content-Length: ".strlen($csv_content));
					echo($csv_content);
					exit;
				}
			break;
		}
	}
}

function print_products_fixed_set_category($product_id, $product_category) {
	if (strlen($product_category)) {
		$product_categories = explode('|', $product_category);
		wp_set_object_terms($product_id, $product_categories, 'product_cat');
	}
}

function print_products_fixed_get_aterms($avals, $ataxonomy) {
	$aterms = array();
	foreach($avals as $aval) {
		$term = term_exists($aval, $ataxonomy);
		if (!$term) {
			$term = wp_insert_term($aval, $ataxonomy);
		}
		if (is_array($term) && isset($term['term_id'])) {
			$aterms[] = $term['term_id'];
		}
	}
	return $aterms;
}

function print_products_fixed_to_csv($val) {
	if (strlen($val)) {
		$val = '"'.str_replace('"', '\"', $val).'"';
	}
	return $val;
}

function print_products_fixed_import_page() {
	$fixed_products = wc_get_products(array('type' => 'fixed'));
	?>
	<div class="wrap">
		<h2><?php _e('Fixed-size products Import', 'wp2print'); ?></h2><br>
		<?php if (isset($_GET['fimported'])) { ?>
			<div id="message" class="updated fade"><p><?php _e('Import was successfully processed. Imported products', 'wp2print'); ?> <?php echo $_GET['fimported']; ?></p></div>
		<?php } ?>
		<form method="POST" enctype="multipart/form-data">
		<input type="hidden" name="print_products_fixed_action" value="import">
		<input type="file" name="import_file">
		<input type="submit" value="<?php _e('Import products', 'wp2print'); ?>" class="button button-primary">
		</form>
	</div>
	<?php
}

function print_products_fixed_export_page() {
	$fixed_products = wc_get_products(array('type' => 'fixed', 'posts_per_page' => -1));
	$fixed_products  = wc_products_array_orderby($fixed_products, 'title', 'ASC');
	?>
	<div class="wrap">
		<h2><?php _e('Fixed-size products Export', 'wp2print'); ?></h2><br>
		<?php if ($fixed_products) { ?>
			<form method="POST">
			<input type="hidden" name="print_products_fixed_action" value="export">
			<select name="export_product">
				<option value="">- <?php _e('All fixed-size products'); ?> -</option>
				<?php foreach($fixed_products as $fixed_product) { ?>
				<option value="<?php echo $fixed_product->get_id(); ?>"><?php echo $fixed_product->get_name(); ?></option>
				<?php } ?>
			</select>
			<div style="margin-top:10px;">
				<input type="submit" value="<?php _e('Export products', 'wp2print'); ?>" class="button button-primary">
			</div>
			</form>
		<?php } else { ?>
			<p><?php _e('No fixed-size products.', 'wp2print'); ?></p>
		<?php } ?>
	</div>
	<?php
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
add_action('network_admin_menu', 'print_products_fixed_import_network_admin_menu');
function print_products_fixed_import_network_admin_menu() {
	add_submenu_page('woonet-woocommerce', __('Fixed-size Import', 'wp2print'), __('Fixed-size Import', 'wp2print'), 'create_users', 'woonet-woocommerce-fixed-import', 'print_products_fixed_network_import_page');
	add_submenu_page('woonet-woocommerce', __('Fixed-size Export', 'wp2print'), __('Fixed-size Export', 'wp2print'), 'create_users', 'woonet-woocommerce-fixed-export', 'print_products_fixed_network_export_page');
}

function print_products_fixed_network_import_page() {
	$blog_id = isset($_GET['site_id']) ? $_GET['site_id'] : 1;
	$sites = get_sites();
	switch_to_blog($blog_id);
	?>
	<div class="wrap">
		<h2><?php _e('Fixed-size products Import', 'wp2print'); ?></h2><br>
		<?php if (isset($_GET['fimported'])) { ?>
			<div id="message" class="updated fade"><p><?php _e('Import was successfully processed. Imported products', 'wp2print'); ?> <?php echo $_GET['fimported']; ?></p></div>
		<?php } ?>
		<div class="msites-wrap" style="padding-bottom:20px;">
			<form method="GET" action="admin.php" class="msites-form">
			<input type="hidden" name="page" value="woonet-woocommerce-fixed-import">
			<label><?php _e('Select site', 'wp2print'); ?></label>
			<select name="site_id" onchange="jQuery('form.msites-form').submit();">
				<?php foreach($sites as $site) { ?>
				<option value="<?php echo $site->blog_id; ?>"<?php if ($site->blog_id == $blog_id) { echo ' SELECTED'; } ?>><?php echo $site->blogname; ?></option>
				<?php } ?>
			</select>
			</form>
		</div>
		<form method="POST" enctype="multipart/form-data">
		<input type="hidden" name="print_products_fixed_network_action" value="import">
		<input type="hidden" name="site_id" value="<?php echo $blog_id; ?>">
		<input type="file" name="import_file">
		<input type="submit" value="<?php _e('Import products', 'wp2print'); ?>" class="button button-primary">
		</form>
	</div>
	<?php
}

function print_products_fixed_network_export_page() {
	$blog_id = isset($_GET['site_id']) ? $_GET['site_id'] : 1;
	$sites = get_sites();
	switch_to_blog($blog_id);
	$fixed_products = wc_get_products(array('type' => 'fixed', 'posts_per_page' => -1));
	?>
	<div class="wrap">
		<h2><?php _e('Fixed-size products Export', 'wp2print'); ?></h2><br>
		<div class="msites-wrap" style="padding-bottom:20px;">
			<form method="GET" action="admin.php" class="msites-form">
			<input type="hidden" name="page" value="woonet-woocommerce-fixed-export">
			<label><?php _e('Select site', 'wp2print'); ?></label>
			<select name="site_id" onchange="jQuery('form.msites-form').submit();">
				<?php foreach($sites as $site) { ?>
				<option value="<?php echo $site->blog_id; ?>"<?php if ($site->blog_id == $blog_id) { echo ' SELECTED'; } ?>><?php echo $site->blogname; ?></option>
				<?php } ?>
			</select>
			</form>
		</div>
		<?php if ($fixed_products) { ?>
			<form method="POST">
			<input type="hidden" name="print_products_fixed_network_action" value="export">
			<input type="hidden" name="site_id" value="<?php echo $blog_id; ?>">
			<select name="export_product">
				<option value="">- <?php _e('All fixed-size products'); ?> -</option>
				<?php foreach($fixed_products as $fixed_product) { ?>
				<option value="<?php echo $fixed_product->get_id(); ?>"><?php echo $fixed_product->get_name(); ?></option>
				<?php } ?>
			</select>
			<div style="margin-top:10px;">
				<input type="submit" value="<?php _e('Export products', 'wp2print'); ?>" class="button button-primary">
			</div>
			</form>
		<?php } else { ?>
			<p><?php _e('No fixed-size products.', 'wp2print'); ?></p>
		<?php } ?>
	</div>
	<?php
}

add_action('wp_loaded', 'print_products_fixed_network_actions');
function print_products_fixed_network_actions() {
	global $wpdb, $attribute_names, $attribute_slugs, $attribute_types, $terms_names, $print_products_settings;
	if (is_admin() && isset($_POST['print_products_fixed_network_action'])) {
		$site_id = (int)$_POST['site_id'];
		switch_to_blog($site_id);
		print_products_price_matrix_attr_names_init();
		switch($_POST['print_products_fixed_network_action']) {
			case 'import':
				if (isset($_FILES['import_file'])) {
					require_once(ABSPATH . 'wp-admin/includes/file.php');
					$ufile = wp_handle_upload($_FILES['import_file'], array('test_form' => false), current_time('mysql'));
					if ($ufile && !$ufile['error']) {
						$import_csv_data = array();
						$csv_file = $ufile['file'];
						$handle = fopen($csv_file, "r");
						if ($handle !== false) {
							$clnmb = 0;
							while (($data = fgetcsv($handle, 1000, ';')) !== false) {
									$import_csv_data[] = $data;
								$clnmb++;
							}
							fclose($handle);
						}
						$mattributes = array();
						foreach($attribute_names as $aid => $aname) { $mattributes[$aname] = $aid; }
						$printing_attributes = unserialize($print_products_settings['printing_attributes']);
						$finishing_attributes = unserialize($print_products_settings['finishing_attributes']);

						$csv_fields = array();
						$csv_attributes = array();
						$csv_attributes_id = array();
						$description_presented = false;
						foreach($import_csv_data[0] as $fkey => $fname) {
							switch($fname) {
								case 'Product Name':
									$csv_fields[$fkey] = 'name';
								break;
								case 'Description':
									$csv_fields[$fkey] = 'description';
									$description_presented = true;
								break;
								case 'Category':
									$csv_fields[$fkey] = 'category';
								break;
								case 'Quantity Style':
									$csv_fields[$fkey] = 'qstyle';
								break;
								case 'Quantities':
									$csv_fields[$fkey] = 'quantities';
								break;
								case 'SKU':
									$csv_fields[$fkey] = 'sku';
								break;
								default:
									$csv_fields[$fkey] = 'attribute-'.$fkey;
									$csv_attributes[$fkey] = $fname;
									$csv_attributes_id[$fname] = $mattributes[$fname];
								break;
							}
						}
						unset($import_csv_data[0]);

						$imported = 0;
						$csv_records = array();
						if (count($import_csv_data)) {
							foreach($import_csv_data as $csv_data) {
								$pdata = array();
								$attributes = array();
								foreach($csv_fields as $fkey => $fname) {
									if (substr($fname, 0, 9) == 'attribute') {
										if (strlen($csv_data[$fkey])) {
											$aname = $csv_attributes[$fkey];
											$attributes[$aname] = stripcslashes($csv_data[$fkey]);
										}
									} else {
										$pdata[$fname] = stripcslashes($csv_data[$fkey]);
									}
								}
								$pdata['attributes'] = $attributes;
								$csv_records[] = $pdata;
							}
							foreach($csv_records as $csv_record) {
								$product_name = $csv_record['name'];
								$product_description = $csv_record['description'];
								$product_category = $csv_record['category'];
								$product_qstyle = $csv_record['qstyle'];
								$product_quantities = $csv_record['quantities'];
								$product_attributes = $csv_record['attributes'];
								$product_sku = $csv_record['sku'];
								if ($product_attributes) {
									$pattributes = array();
									$fattributes = array();
									foreach($product_attributes as $aname => $avals) {
										$aid = $csv_attributes_id[$aname];
										$aslug = $attribute_slugs[$aid];
										$avals = explode('|', $avals);
										$aterms = print_products_fixed_get_aterms($avals, 'pa_'.$aslug);
										if (in_array($aid, $printing_attributes)) {
											$pattributes[$aid] = $aterms;
										} else {
											$fattributes[$aid] = $aterms;
										}
									}
								}

								$product_id = $wpdb->get_var(sprintf("SELECT ID FROM %sposts WHERE post_type = 'product' AND post_title = '%s'", $wpdb->prefix, $product_name));
								if ($product_id) {
									if ($description_presented) {
										$wpdb->update($wpdb->prefix.'posts', array('post_content' => $product_description), array('ID' => $product_id));
									}
									$product_type_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s ORDER BY mtype, sorder", $wpdb->prefix, $product_id));
									if ($product_type_matrix_types) {
										foreach($product_type_matrix_types as $product_type_matrix_type) {
											$mtype_id = $product_type_matrix_type->mtype_id;
											$mtype = $product_type_matrix_type->mtype;
											$mattributes = unserialize($product_type_matrix_type->attributes);
											$materms = unserialize($product_type_matrix_type->aterms);
											$numbers = explode(',', $product_type_matrix_type->numbers);
											$num_style = $product_type_matrix_type->num_style;

											$nattributes = array();
											if ($mtype == 0) {
												$nattributes = $pattributes;
											} else {
												$nattributes = $fattributes;
											}
											if ($nattributes) {
												$new_mattributes = array();
												$new_materms = array();
												foreach($nattributes as $aid => $aterms) {
													$new_mattributes[] = $aid;
													$new_materms[$aid] = $aterms;
												}
												$num_style = 0; if ($product_qstyle == 'DROPDOWN') { $num_style = 1; }
												$update = array();
												$update['attributes'] = serialize($new_mattributes);
												$update['aterms'] = serialize($new_materms);
												$update['numbers'] = str_replace('|', ',', $product_quantities);
												$update['num_style'] = $num_style;
												$wpdb->update($wpdb->prefix.'print_products_matrix_types', $update, array('mtype_id' => $mtype_id));
											}
											if ($product_sku) {
												$print_products_matrix_prices = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_sku WHERE mtype_id = %s", $wpdb->prefix, $mtype_id));
												if ($print_products_matrix_prices && count($print_products_matrix_prices) == 1) {
													$wpdb->update($wpdb->prefix.'print_products_matrix_sku', array('sku' => $product_sku), array('mtype_id' => $mtype_id));
												}
											}
										}
									}
								} else {
									$insert = array();
									$insert['post_type'] = 'product';
									$insert['post_title'] = $product_name;
									$insert['post_status'] = 'publish';
									if ($description_presented) {
										$insert['post_content'] = $product_description;
									}
									$product_id = wp_insert_post($insert);
									if ($product_id) {
										$num_style = 0; if ($product_qstyle == 'DROPDOWN') { $num_style = 1; }
										wp_set_object_terms($product_id, 'fixed', 'product_type');
										if (count($pattributes)) {
											$new_mattributes = array();
											$new_materms = array();
											foreach($pattributes as $aid => $aterms) {
												$new_mattributes[] = $aid;
												$new_materms[$aid] = $aterms;
											}
											$insert = array();
											$insert['product_id'] = $product_id;
											$insert['mtype'] = 0;
											$insert['attributes'] = serialize($new_mattributes);
											$insert['aterms'] = serialize($new_materms);
											$insert['numbers'] = str_replace('|', ',', $product_quantities);
											$insert['num_style'] = $num_style;
											$insert['num_type'] = 0;
											$insert['sorder'] = 0;
											$wpdb->insert($wpdb->prefix.'print_products_matrix_types', $insert);
										}
										if (count($fattributes)) {
											$new_mattributes = array();
											$new_materms = array();
											foreach($fattributes as $aid => $aterms) {
												$new_mattributes[] = $aid;
												$new_materms[$aid] = $aterms;
											}
											$insert = array();
											$insert['product_id'] = $product_id;
											$insert['mtype'] = 1;
											$insert['attributes'] = serialize($new_mattributes);
											$insert['aterms'] = serialize($new_materms);
											$insert['numbers'] = str_replace('|', ',', $product_quantities);
											$insert['num_style'] = $num_style;
											$insert['num_type'] = 0;
											$insert['sorder'] = 1;											$wpdb->insert($wpdb->prefix.'print_products_matrix_types', $insert);
										}
									}
								}
								print_products_fixed_set_category($product_id, $product_category);
								$imported++;
							}
						}
						wp_redirect('admin.php?page=woonet-woocommerce-fixed-import&site_id='.$site_id.'&fimported='.$imported);
						exit;
					}
				}
			break;
			case 'export':
				$csvsep = ';';
				$csvnl = "\r\n";
				$csv_filename = 'fixed-size-export-site'.$site_id.'-'.date('Y-m-d-H-i-s').'.csv';
				$csv_fields = array('name' => 'Product Name', 'description' => 'Description', 'category' => 'Category', 'qstyle' => 'Quantity Style', 'quantities' => 'Quantities', 'sku' => 'SKU');
				$csv_records = array();
				$csv_mtypes = array(array(), array());

				$export_product = $_POST['export_product'];
				if ($export_product) {
					$fixed_products = array(wc_get_product($export_product));
				} else {
					$fixed_products = wc_get_products(array('type' => 'fixed', 'posts_per_page' => -1));
				}
				if ($fixed_products) { $sep = '';
					foreach($fixed_products as $fixed_product) {
						$product_id = $fixed_product->get_id();
						$product_name = $fixed_product->get_name();
						$product_description = $fixed_product->get_description();
						$product_thumb_id = get_post_thumbnail_id($product_id);

						$product_thumb_url = '';
						if ($product_thumb_id) {
							$image_attributes = wp_get_attachment_image_src($product_thumb_id, 'full');
							if ($image_attributes) {
								$product_thumb_url = $image_attributes[0];
							}
						}

						$product_category = array();
						$pcats = get_the_terms($product_id, 'product_cat');
						if ($pcats) {
							foreach($pcats as $pcat) {
								$product_category[] = $pcat->name;
							}
						}

						$qstyle = 'TEXTFIELD';
						$qnumbers = array();
						$matrix_sku = '';
						$product_attributes = array();
						$product_type_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s ORDER BY mtype, sorder", $wpdb->prefix, $product_id));
						if ($product_type_matrix_types) {
							foreach($product_type_matrix_types as $product_type_matrix_type) {
								$mtype_id = $product_type_matrix_type->mtype_id;
								$mtype = $product_type_matrix_type->mtype;
								$mattributes = unserialize($product_type_matrix_type->attributes);
								$materms = unserialize($product_type_matrix_type->aterms);
								$numbers = explode(',', $product_type_matrix_type->numbers);
								$num_style = $product_type_matrix_type->num_style;

								if ($mtype == 0) {
									if ($num_style == 1) { $qstyle = 'DROPDOWN'; }
									$qnumbers = $numbers;
								}
								if ($mattributes) {
									$mattributes = print_products_sort_attributes($mattributes);
									foreach($mattributes as $mattribute) {
										$aterms = $materms[$mattribute];
										$aterm_names = array();
										if ($aterms) {
											$aterms = print_products_get_attribute_terms($aterms);
											foreach($aterms as $aterm_id => $aterm_name) {
												$aterm_names[] = $aterm_name;
											}
										}
										$product_attributes[$mattribute] = implode('|', $aterm_names);
										if (is_array($csv_mtypes[$mtype]) && !in_array($mattribute, $csv_mtypes[$mtype])) {
											$csv_mtypes[$mtype][] = $mattribute;
										}
									}
								}
								$print_products_matrix_prices = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_sku WHERE mtype_id = %s", $wpdb->prefix, $mtype_id));
								if ($print_products_matrix_prices && !$matrix_sku) {
									if (count($print_products_matrix_prices) == 1) {
										$matrix_sku = $print_products_matrix_prices[0]->sku;
									}
								}
							}
						}
						$csv_records[$product_id] = array(
							'name' => $product_name,
							'description' => $product_description,
							'category' => implode('|', $product_category),
							'qstyle' => $qstyle,
							'quantities' => implode('|', $qnumbers),
							'attributes' => $product_attributes,
							'sku' => $matrix_sku,
							'image' => $product_thumb_url
						);
					}
					$exfields = array();
					if (count($csv_mtypes[0])) {
						foreach($csv_mtypes[0] as $mattribute) {
							if (is_array($exfields) && !in_array($mattribute, $exfields)) {
								$csv_fields['attr-'.$mattribute] = $attribute_names[$mattribute];
								$exfields[] = $mattribute;
							}
						}
					}
					if (count($csv_mtypes[1])) {
						foreach($csv_mtypes[1] as $mattribute) {
							if (is_array($exfields) && !in_array($mattribute, $exfields)) {
								$csv_fields['attr-'.$mattribute] = $attribute_names[$mattribute];
								$exfields[] = $mattribute;
							}
						}
					}
					$csv_fields['image'] = 'Product image';
					$fnums = array();
					foreach($csv_fields as $fkey => $fname) {
						if (!isset($fnums[$fkey])) { $fnums[$fkey] = 0; }
						foreach($csv_records as $csv_record) {
							$aval = '';
							if (substr($fkey, 0, 4) == 'attr') {
								$aid = str_replace('attr-', '', $fkey);
								$attributes = $csv_record['attributes'];
								if (isset($attributes[$aid])) {
									$aval = $attributes[$aid];
								}
							} else {
								$aval = $csv_record[$fkey];
							}
							if (strlen($aval)) {
								$fnums[$fkey]++;
							}
						}
					}
					foreach($csv_fields as $fkey => $fname) {
						if (!$fnums[$fkey]) {
							unset($csv_fields[$fkey]);
						}
					}

					$csv_content = implode($csvsep, $csv_fields).$csvnl;
					foreach($csv_records as $csv_record) {
						$pfields = array();
						foreach($csv_fields as $fkey => $fname) {
							if (substr($fkey, 0, 4) == 'attr') {
								$aid = str_replace('attr-', '', $fkey);
								$attributes = $csv_record['attributes'];
								$aval = '';
								if (isset($attributes[$aid])) {
									$aval = $attributes[$aid];
								}
								$pfields[] = print_products_fixed_to_csv($aval);
							} else {
								$pfields[] = print_products_fixed_to_csv($csv_record[$fkey]);
							}
						}
						$csv_content .= implode($csvsep, $pfields).$csvnl;
					}

					header("Content-Type: application/zip");
					header("Content-Disposition: attachment; filename=".basename($csv_filename));
					header("Content-Length: ".strlen($csv_content));
					echo($csv_content);
					exit;
				}
			break;
		}
	}
}
?>