<?php
add_action('wp_loaded', 'print_products_oirsdate_actions');
function print_products_oirsdate_actions() {
	if (isset($_POST['OirsdateAjaxAction'])) {
		switch ($_POST['OirsdateAjaxAction']) {
			case 'change-oirsdate':
				print_products_oirsdate_change_oirsdate();
			break;
		}
		exit;
	}
}

function print_products_oirsdate_change_oirsdate() {
	$order_id = $_POST['order_id'];
	$item_id = $_POST['item_id'];
	$item_sdate = $_POST['item_sdate'];
	$poption = (int)$_POST['poption'];
	if ($poption == 1) {
		$order = wc_get_order($order_id);
		$order_items = $order->get_items();
		foreach($order_items as $order_item) {
			$order_item_id = $order_item->get_id();
			wc_update_order_item_meta($order_item_id, '_item_rsdate', $item_sdate);
		}
	} else {
		wc_update_order_item_meta($item_id, '_item_rsdate', $item_sdate);
	}
}

function print_products_oirsdate_show_prodview_shipdate_column() {
	global $print_products_prodview_options;
	if (isset($print_products_prodview_options['display_shipdate'])) {
		return (int)$print_products_prodview_options['display_shipdate'];
	}
	return false;
}

function print_products_oirsdate_show_orders_shipdate_column() {
	global $print_products_prodview_options;
	if (isset($print_products_prodview_options['orders_display_shipdate'])) {
		return (int)$print_products_prodview_options['orders_display_shipdate'];
	}
	return false;
}

function print_products_oirsdate_popup_html() {
	$order_id = print_products_woocommerce_get_current_order_id();
	?>
	<div style="display:none;">
		<div id="oirsdate-popup" class="oirsdate-popup">
			<h2><?php _e('Modify required ship date for order #', 'wp2print'); ?><span><?php echo $order_id; ?></span></h2>
			<ul>
				<li><input type="radio" name="oirsdate_option" value="0" checked><?php _e('Apply to this item only', 'wp2print'); ?></li>
				<li><input type="radio" name="oirsdate_option" value="1"><?php _e('Apply to all items in order ', 'wp2print'); ?></li>
			</ul>
			<input type="button" value="<?php _e('Submit', 'wp2print'); ?>" class="button-primary" onclick="wp2print_oirsdate_submit()">
		</div>
	</div>
	<?php
}

// order edit page
add_action('woocommerce_admin_order_item_headers', 'print_products_oirsdate_woocommerce_admin_order_item_headers', 12);
function print_products_oirsdate_woocommerce_admin_order_item_headers($order) {
	if (print_products_oirsdate_show_orders_shipdate_column()) { ?>
		<th class="item_rsdate" data-reqtime="<?php if (print_products_oirsdate_pview_display_required_date()) { echo '1'; } else { echo '0'; } ?>" data-tformat="<?php echo print_products_oirsdate_get_time_format(); ?>"><?php esc_html_e('Required ship date', 'wp2print'); ?></th>
		<?php
	}
}

add_action('woocommerce_admin_order_item_values', 'print_products_oirsdate_woocommerce_admin_order_item_values', 12, 3);
function print_products_oirsdate_woocommerce_admin_order_item_values($product, $item, $item_id) {
	if (print_products_oirsdate_show_orders_shipdate_column()) {
		$order_id = print_products_woocommerce_get_current_order_id();
		$item_type = $item->get_type();
		if ($item_type == 'line_item') {
			$item_rsdate = wc_get_order_item_meta($item_id, '_item_rsdate', true); ?>
			<td class="item_rsdate oirsd-order-<?php echo $order_id; ?>"><input type="text" name="oirsdate[<?php echo $item_id; ?>]" value="<?php echo $item_rsdate; ?>" class="item-rsdate" data-order-id="<?php echo $order_id; ?>" data-item-id="<?php echo $item_id; ?>">
			<div class="oirsdate-success oirsdate-success-<?php echo $item_id; ?>"><?php _e('Updated.', 'wp2print'); ?></div></td>
			<?php
		}
	}
}

function print_products_oirsdate_pview_display_required_date() {
	global $print_products_prodview_options;
	if (isset($print_products_prodview_options['display_required_date'])) {
		return (int)$print_products_prodview_options['display_required_date'];
	}
	return false;
}

function print_products_oirsdate_get_time_format() {
	$tformat = 'HH:mm';
	$time_format = get_option('time_format');
	if (strpos($time_format, 'a') || strpos($time_format, 'A')) {
		$tformat = 'hh:mm TT';
	}
	return $tformat;
}

function print_products_oirsdate_get_time_colour($rsdate) {
	global $print_products_prodview_options;
	$h = 0;
	$rsdate_array = explode(' ', $rsdate);
	if (count($rsdate_array) > 1) {
		$dnow = current_time('mysql');
		$dtime1 = strtotime($dnow);
		$dtime2 = strtotime($rsdate);
		if ($dtime1 < $dtime2) {
			$datetime1 = date_create($rsdate);
			$datetime2 = date_create($dnow);
			$interval = date_diff($datetime1, $datetime2);
			$h = (int)$interval->format('%h');
			$d = $interval->format('%d');
			if ($h == 0) { $h = 1; }
			if ($d > 0) { $h = $h + ($d * 24); }
		}
	}
	if ($h > 3) { $h = 6; } else if ($h > 1) { $h = 3; }
	if (isset($print_products_prodview_options['rsdate_colour_'.$h]) && strlen($print_products_prodview_options['rsdate_colour_'.$h])) {
		return $print_products_prodview_options['rsdate_colour_'.$h];
	}
	return '';
}
?>