<?php
add_action('wp_loaded', 'print_products_orders_backup_actions');
function print_products_orders_backup_actions() {
	global $wpdb;
	if (isset($_POST['print_products_orders_backup_action'])) {
		switch ($_POST['print_products_orders_backup_action']) {
			case 'backup':
				print_products_orders_backup_process();
			break;
			case 'export':
				print_products_orders_backup_export();
			break;
		}
	}
}

function print_products_orders_backup_admin_page() {
	global $wpdb;
	$year_orders = $wpdb->get_results(sprintf("SELECT EXTRACT(year FROM post_date) AS year, COUNT(ID) AS orders_number FROM %sposts WHERE post_type = 'shop_order' AND post_status != 'trash' GROUP BY EXTRACT(year FROM post_date)", $wpdb->prefix));
	$backup_orders = $wpdb->get_results(sprintf("SELECT EXTRACT(year FROM post_date) AS year, COUNT(ID) AS orders_number FROM backup_posts GROUP BY EXTRACT(year FROM post_date)"));
	?>
	<div class="wrap wp2print-orders-backup">
		<h2 style="margin-bottom:8px;"><?php _e('Order archiving', 'wp2print'); ?></h2>
		<?php if (isset($_GET['obprocessed']) && $_GET['obprocessed'] == 'true') { ?>
			<div class="updated notice notice-success">
				<p><?php _e('Order archiving was successfully processed.', 'wp2print'); ?></p>
			</div>
		<?php } ?>
		<div class="postbox orders-backup-box">
			<?php if ($year_orders) {
				$backup_to_date = date('Y-m-d', mktime(0, 0, 0, 1, 1, date('Y'))); ?>
				<h4><?php _e('Count of orders by year', 'wp2print'); ?>:</h4>
				<ul>
					<?php foreach ($year_orders as $year_order) { ?>
						<li><?php echo $year_order->year; ?>: <?php echo $year_order->orders_number; ?></li>
					<?php } ?>
				</ul>
				<form method="POST" class="orders-backup-process-form">
					<input type="hidden" name="print_products_orders_backup_action" value="backup">
					<h4><?php _e('Archive orders older than', 'wp2print'); ?>:</h4>
					<input type="text" name="ob_date" value="<?php echo $backup_to_date; ?>" class="ob-date">
					<input type="submit" value="<?php _e('Archive', 'wp2print'); ?>" class="button button-primary">
				</form>
				<p><?php _e('Orders will be moved from live database tables to backup tables.', 'wp2print'); ?></p>
			<?php } else { ?>
				<p><?php _e('No orders.', 'wp2print'); ?></p>
			<?php } ?>
		</div>
		<div class="postbox orders-backup-box orders-backup-export">
			<h4><?php _e('Export order data from backup database table', 'wp2print'); ?></h4>
			<?php if ($backup_orders) {
				$min_year = $backup_orders[0]->year;
				$ob_from_date = date('Y-m-d', mktime(0, 0, 0, 1, 1, $min_year));
				$ob_to_date = date('Y-m-d');
				?>
				<form method="POST" class="orders-backup-export-form">
					<input type="hidden" name="print_products_orders_backup_action" value="export">
					<table cellpadding="0" cellspacing="0">
						<tr class="f-line">
							<td><?php _e('Date range', 'wp2print'); ?>:&nbsp;</td>
							<td><input type="text" name="ob_from_date" value="<?php echo $ob_from_date; ?>" class="ob-date"></td>
							<td>&nbsp;&nbsp;</td>
							<td><?php _e('to', 'wp2print'); ?>&nbsp;</td>
							<td><input type="text" name="ob_to_date" value="<?php echo $ob_to_date; ?>" class="ob-date"></td>
						</tr>
						<tr class="f-line">
							<td><?php _e('OrderID range', 'wp2print'); ?>:&nbsp;</td>
							<td><input type="text" name="ob_from_order_id"></td>
							<td>&nbsp;&nbsp;</td>
							<td><?php _e('to', 'wp2print'); ?>&nbsp;</td>
							<td><input type="text" name="ob_to_order_id"></td>
						</tr>
						<tr>
							<td colspan="5" align="right"><input type="submit" value="<?php _e('Export', 'wp2print'); ?>" class="button button-primary"></td>
						</tr>
					</table>
				</form>
			<?php } else { ?>
				<p><?php _e('No orders in backup.', 'wp2print'); ?></p>
			<?php } ?>
		</div>
	</div>
	<script>
	jQuery(document).ready(function() {
		jQuery('.ob-date').datepicker({dateFormat:'yy-mm-dd'});
	});
	</script>
	<?php
}

function print_products_orders_backup_process() {
	global $wpdb;

	$ob_date = $_POST['ob_date'];
	if (!$ob_date) { $ob_date = date('Y-m-d', mktime(0, 0, 0, 1, 1, date('Y'))); }

	$backuped = $wpdb->query(sprintf("INSERT IGNORE INTO backup_posts SELECT * FROM %sposts WHERE post_type = 'shop_order' AND post_date <= '%s 00:00:00'", $wpdb->prefix, $ob_date));
	if ($backuped) {
		$wpdb->query(sprintf("INSERT IGNORE INTO backup_postmeta SELECT * FROM %spostmeta WHERE post_id IN (SELECT ID FROM %sposts WHERE post_type = 'shop_order' AND post_date <= '%s 00:00:00')", $wpdb->prefix, $wpdb->prefix, $ob_date));
		$wpdb->query(sprintf("INSERT IGNORE INTO backup_woocommerce_order_items SELECT * FROM %swoocommerce_order_items WHERE order_id IN (SELECT ID FROM %sposts WHERE post_type = 'shop_order' AND post_date <= '%s 00:00:00')", $wpdb->prefix, $wpdb->prefix, $ob_date));
		$wpdb->query(sprintf("INSERT IGNORE INTO backup_woocommerce_order_itemmeta SELECT * FROM %swoocommerce_order_itemmeta WHERE order_item_id IN (SELECT order_item_id FROM %swoocommerce_order_items WHERE order_id IN (SELECT ID FROM %sposts WHERE post_type = 'shop_order' AND post_date <= '%s 00:00:00'))", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $ob_date));
		$wpdb->query(sprintf("INSERT IGNORE INTO backup_print_products_order_items SELECT * FROM %sprint_products_order_items WHERE item_id IN (SELECT order_item_id FROM %swoocommerce_order_items WHERE order_id IN (SELECT ID FROM %sposts WHERE post_type = 'shop_order' AND post_date <= '%s 00:00:00'))", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $ob_date));
		
		// remove records from wp tables
		$wpdb->query(sprintf("DELETE FROM %spostmeta WHERE post_id IN (SELECT ID FROM %sposts WHERE post_type = 'shop_order' AND post_date <= '%s 00:00:00')", $wpdb->prefix, $wpdb->prefix, $ob_date));
		$wpdb->query(sprintf("DELETE FROM %swoocommerce_order_items WHERE order_id IN (SELECT ID FROM %sposts WHERE post_type = 'shop_order' AND post_date <= '%s 00:00:00')", $wpdb->prefix, $wpdb->prefix, $ob_date));
		$wpdb->query(sprintf("DELETE FROM %swoocommerce_order_itemmeta WHERE order_item_id IN (SELECT order_item_id FROM %swoocommerce_order_items WHERE order_id IN (SELECT ID FROM %sposts WHERE post_type = 'shop_order' AND post_date <= '%s 00:00:00'))", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $ob_date));
		$wpdb->query(sprintf("DELETE FROM %sprint_products_order_items WHERE item_id IN (SELECT order_item_id FROM %swoocommerce_order_items WHERE order_id IN (SELECT ID FROM %sposts WHERE post_type = 'shop_order' AND post_date <= '%s 00:00:00'))", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $ob_date));
		$wpdb->query(sprintf("DELETE FROM %sposts WHERE post_type = 'shop_order' AND post_status != 'trash' AND post_date <= '%s 00:00:00'", $wpdb->prefix, $ob_date));
	}

	wp_redirect('admin.php?page=print-products-order-archiving&obprocessed=true');
	exit;
}

$ob_price_args = array();
function print_products_orders_backup_export() {
	global $wpdb, $ob_price_args;
	$csvsep = ';';
	$csvnl = "\r\n";
	$csv_filename = 'orders-backup-export-'.date('Y-m-d-H-i-s').'.csv';
	$csv_records = array();
	$ob_from_date = $_POST['ob_from_date'];
	$ob_to_date = $_POST['ob_to_date'];
	$ob_from_order_id = (int)$_POST['ob_from_order_id'];
	$ob_to_order_id = (int)$_POST['ob_to_order_id'];

	set_time_limit(3600);

	$ob_price_args = array(
		'decimal_separator'  => wc_get_price_decimal_separator(),
		'thousand_separator' => wc_get_price_thousand_separator(),
		'decimals'           => wc_get_price_decimals()
	);

	$csv_fields = array(
		'OrderID',
		'Date',
		'Customer',
		'UserID',
		'Billing Company name',
		'Billing Address1',
		'Billing Address2',
		'Billing City',
		'Billing State',
		'Billing Zip code',
		'Billing Country',
		'Shipping Company name',
		'Shipping Address1',
		'Shipping Address2',
		'Shipping City',
		'Shipping State',
		'Shipping Zip code',
		'Shipping Country',
		'Status',
		'Item',
		'Quantity',
		'Price',
		'Shipping',
		'Tax',
		'Total',
		'Files'
	);

	$where = array();
	if (strlen($ob_from_date)) {
		$where[] = sprintf("post_date >= '%s 00:00:00'", $ob_from_date);
	}
	if (strlen($ob_to_date)) {
		$where[] = sprintf("post_date <= '%s 23:59:59'", $ob_to_date);
	}
	if ($ob_from_order_id) {
		$where[] = sprintf("ID >= %s", $ob_from_order_id);
	}
	if ($ob_to_order_id) {
		$where[] = sprintf("ID <= %s", $ob_to_order_id);
	}
	$where_string = '';
	if (count($where)) {
		$where_string = 'WHERE '.implode(' AND ', $where);
	}

	$backup_orders = $wpdb->get_results(sprintf("SELECT * FROM backup_posts %s ORDER BY post_date DESC", $where_string));
	if ($backup_orders) {
		$postmetas = print_products_orders_backup_get_records('postmeta', $ob_from_date, $ob_to_date);
		$woocommerce_order_items = print_products_orders_backup_get_records('woocommerce_order_items', $ob_from_date, $ob_to_date);
		$woocommerce_order_itemmetas = print_products_orders_backup_get_records('woocommerce_order_itemmeta', $ob_from_date, $ob_to_date);
		$print_products_order_items = print_products_orders_backup_get_records('print_products_order_items', $ob_from_date, $ob_to_date);
		foreach($backup_orders as $backup_order) {
			$order_id = $backup_order->ID;
			$order_date = $backup_order->post_date;
			$order_status = str_replace('wc-', '', $backup_order->post_status);
			$order_postmetas = isset($postmetas[$order_id]) ? $postmetas[$order_id] : array();
			$order_items = isset($woocommerce_order_items[$order_id]) ? $woocommerce_order_items[$order_id] : array();
			$order_itemmetas = isset($woocommerce_order_itemmetas[$order_id]) ? $woocommerce_order_itemmetas[$order_id] : array();
			$order_pp_order_items = isset($print_products_order_items[$order_id]) ? $print_products_order_items[$order_id] : array();

			$fname = print_products_orders_backup_get_meta_value('_billing_first_name', $order_postmetas);
			$lname = print_products_orders_backup_get_meta_value('_billing_last_name', $order_postmetas);
			$customer_name = $fname.' '.$lname;
			$customer_id = print_products_orders_backup_get_meta_value('_customer_user', $order_postmetas);
			$billing_company = print_products_orders_backup_get_meta_value('_billing_company', $order_postmetas);
			$billing_address1 = print_products_orders_backup_get_meta_value('_billing_address_1', $order_postmetas);
			$billing_address2 = print_products_orders_backup_get_meta_value('_billing_address_2', $order_postmetas);
			$billing_city = print_products_orders_backup_get_meta_value('_billing_city', $order_postmetas);
			$billing_state = print_products_orders_backup_get_meta_value('_billing_state', $order_postmetas);
			$billing_zip = print_products_orders_backup_get_meta_value('_billing_postcode', $order_postmetas);
			$billing_country = print_products_orders_backup_get_meta_value('_billing_country', $order_postmetas);
			$shipping_company = print_products_orders_backup_get_meta_value('_shipping_company', $order_postmetas);
			$shipping_address1 = print_products_orders_backup_get_meta_value('_shipping_address_1', $order_postmetas);
			$shipping_address2 = print_products_orders_backup_get_meta_value('_shipping_address_2', $order_postmetas);
			$shipping_city = print_products_orders_backup_get_meta_value('_shipping_city', $order_postmetas);
			$shipping_state = print_products_orders_backup_get_meta_value('_shipping_state', $order_postmetas);
			$shipping_zip = print_products_orders_backup_get_meta_value('_shipping_postcode', $order_postmetas);
			$shipping_country = print_products_orders_backup_get_meta_value('_shipping_country', $order_postmetas);

			$shipping = print_products_orders_backup_get_meta_value('_order_shipping', $order_postmetas);
			$tax = print_products_orders_backup_get_meta_value('_order_tax', $order_postmetas);
			$total = print_products_orders_backup_get_meta_value('_order_total', $order_postmetas);

			$shipping = print_products_orders_backup_price($shipping);
			$tax = print_products_orders_backup_price($tax);
			$total = print_products_orders_backup_price($total);

			foreach($order_items as $order_item) {
				if ($order_item->order_item_type == 'line_item') {
					$item_id = $order_item->order_item_id;
					$item_qty = print_products_orders_backup_get_oitem_meta_value($item_id, '_qty', $order_itemmetas);
					$item_price = print_products_orders_backup_get_oitem_meta_value($item_id, '_line_total', $order_itemmetas);
					$pdf_link = print_products_orders_backup_get_oitem_meta_value($item_id, '_pdf_link', $order_itemmetas);
					$image_link = print_products_orders_backup_get_oitem_meta_value($item_id, '_image_link', $order_itemmetas);
					$files = print_products_orders_backup_get_pp_oitem_meta_value($item_id, 'artwork_files', $order_pp_order_items);
					$item_price = print_products_orders_backup_price($item_price);

					$item_files = '';
					if (strlen($files)) {
						$files = unserialize($files);
						if (count($files)) {
							$item_files = implode(',', $files);
						}
					}
					if (strlen($pdf_link)) {
						if (strlen($item_files)) { $item_files .= ','; }
						$item_files .= $pdf_link;
					}
					if (strlen($image_link)) {
						if (strlen($item_files)) { $item_files .= ','; }
						$item_files .= $image_link;
					}

					$csv_lines = array();
					$csv_lines[] = $order_id;
					$csv_lines[] = print_products_orders_backup_to_csv($order_date);
					$csv_lines[] = print_products_orders_backup_to_csv($customer_name);
					$csv_lines[] = print_products_orders_backup_to_csv($customer_id);
					$csv_lines[] = print_products_orders_backup_to_csv($billing_company);
					$csv_lines[] = print_products_orders_backup_to_csv($billing_address1);
					$csv_lines[] = print_products_orders_backup_to_csv($billing_address2);
					$csv_lines[] = print_products_orders_backup_to_csv($billing_city);
					$csv_lines[] = print_products_orders_backup_to_csv($billing_state);
					$csv_lines[] = print_products_orders_backup_to_csv($billing_zip);
					$csv_lines[] = print_products_orders_backup_to_csv($billing_country);
					$csv_lines[] = print_products_orders_backup_to_csv($shipping_company);
					$csv_lines[] = print_products_orders_backup_to_csv($shipping_address1);
					$csv_lines[] = print_products_orders_backup_to_csv($shipping_address2);
					$csv_lines[] = print_products_orders_backup_to_csv($shipping_city);
					$csv_lines[] = print_products_orders_backup_to_csv($shipping_state);
					$csv_lines[] = print_products_orders_backup_to_csv($shipping_zip);
					$csv_lines[] = print_products_orders_backup_to_csv($shipping_country);
					$csv_lines[] = print_products_orders_backup_to_csv($order_status);
					$csv_lines[] = print_products_orders_backup_to_csv($order_item->order_item_name);
					$csv_lines[] = print_products_orders_backup_to_csv($item_qty);
					$csv_lines[] = print_products_orders_backup_to_csv($item_price);
					$csv_lines[] = print_products_orders_backup_to_csv($shipping);
					$csv_lines[] = print_products_orders_backup_to_csv($tax);
					$csv_lines[] = print_products_orders_backup_to_csv($total);
					$csv_lines[] = print_products_orders_backup_to_csv($item_files);

					$csv_records[] = implode($csvsep, $csv_lines);
				}
			}
		}
	}

	$csv_content = implode($csvsep, $csv_fields).$csvnl;
	$csv_content .= implode($csvnl, $csv_records);

	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=".basename($csv_filename));
	header("Content-Length: ".strlen($csv_content));
	echo($csv_content);
	exit;
}

function print_products_orders_backup_get_records($table, $ob_from_date, $ob_to_date) {
	global $wpdb;
	$records = array();
	switch($table) {
		case 'postmeta':
			$backup_postmetas = $wpdb->get_results(sprintf("SELECT * FROM backup_postmeta WHERE post_id IN (SELECT ID FROM backup_posts WHERE post_date >= '%s 00:00:00' AND post_date <= '%s 23:59:59') AND meta_key IN ('_billing_first_name', '_billing_last_name', '_customer_user', '_billing_company', '_billing_address_1', '_billing_address_2', '_billing_city', '_billing_state', '_billing_postcode', '_billing_country', '_shipping_company', '_shipping_address_1', '_shipping_address_2', '_shipping_city', '_shipping_state', '_shipping_postcode', '_shipping_country', '_order_shipping', '_order_tax', '_order_total') LIMIT 0, 200000", $ob_from_date, $ob_to_date));
			if ($backup_postmetas) {
				foreach($backup_postmetas as $postmeta) {
					$records[$postmeta->post_id][$postmeta->meta_key] = $postmeta->meta_value;
				}
			}
		break;
		case 'woocommerce_order_items':
			$backup_woocommerce_order_items = $wpdb->get_results(sprintf("SELECT * FROM backup_woocommerce_order_items WHERE order_id IN (SELECT ID FROM backup_posts WHERE post_date >= '%s 00:00:00' AND post_date <= '%s 23:59:59')", $ob_from_date, $ob_to_date));
			if ($backup_woocommerce_order_items) {
				foreach($backup_woocommerce_order_items as $woitem) {
					$records[$woitem->order_id][] = $woitem;
				}
			}
		break;
		case 'woocommerce_order_itemmeta':
			$backup_woocommerce_order_itemmetas = $wpdb->get_results(sprintf("SELECT oim.*, oi.order_id FROM backup_woocommerce_order_itemmeta oim LEFT JOIN backup_woocommerce_order_items oi ON oi.order_item_id = oim.order_item_id WHERE oi.order_id IN (SELECT ID FROM backup_posts WHERE post_date >= '%s 00:00:00' AND post_date <= '%s 23:59:59')", $ob_from_date, $ob_to_date));
			if ($backup_woocommerce_order_itemmetas) {
				foreach($backup_woocommerce_order_itemmetas as $woitemmeta) {
					$records[$woitemmeta->order_id][] = $woitemmeta;
				}
			}
			
		break;
		case 'print_products_order_items':
			$backup_print_products_order_items = $wpdb->get_results(sprintf("SELECT ppoi.*, oi.order_id FROM backup_print_products_order_items ppoi LEFT JOIN backup_woocommerce_order_items oi ON oi.order_item_id = ppoi.item_id WHERE oi.order_id IN (SELECT ID FROM backup_posts WHERE post_date >= '%s 00:00:00' AND post_date <= '%s 23:59:59')", $ob_from_date, $ob_to_date));
			if ($backup_print_products_order_items) {
				foreach($backup_print_products_order_items as $pp_oitem) {
					$records[$pp_oitem->order_id][] = $pp_oitem;
				}
			}
		break;
	}
	return $records;
}

function print_products_orders_backup_get_meta_value($meta_key, $metas) {
	$meta_value = '';
	if (isset($metas[$meta_key])) {
		$meta_value = $metas[$meta_key];
	}
	return $meta_value;
}

function print_products_orders_backup_get_oitem_meta_value($item_id, $meta_key, $metas) {
	$meta_value = '';
	$oimetas = array();
	foreach($metas as $meta) {
		if ($meta->order_item_id == $item_id) {
			$oimetas[$meta->meta_key] = $meta->meta_value;
		}
	}
	if (isset($oimetas[$meta_key])) {
		$meta_value = $oimetas[$meta_key];
	}
	return $meta_value;
}

function print_products_orders_backup_get_pp_oitem_meta_value($item_id, $meta_key, $metas) {
	$meta_value = '';
	$oimetas = array();
	foreach($metas as $meta) {
		if ($meta->item_id == $item_id) {
			$meta_value = $meta->$meta_key;
		}
	}
	return $meta_value;
}

function print_products_orders_backup_price($price) {
	global $ob_price_args;
	if ($price) {
		$price = (float)$price;
		$price = apply_filters( 'formatted_woocommerce_price', number_format( $price, $ob_price_args['decimals'], $ob_price_args['decimal_separator'], $ob_price_args['thousand_separator'] ), $price, $ob_price_args['decimals'], $ob_price_args['decimal_separator'], $ob_price_args['thousand_separator'], $price );
	}
	return $price;
}

function print_products_orders_backup_to_csv($val) {
	if (strlen($val)) {
		$val = '"'.str_replace('"', '\"', $val).'"';
	}
	return $val;
}
?>