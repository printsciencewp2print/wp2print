<?php
function print_products_user_orders_admin_page() {
	global $wpdb, $current_user;
	$wc_ostatuses = wc_get_order_statuses();
	$oistatuses = print_products_oistatus_get_list();
	$approval_statuses = print_products_get_approval_statuses();

	$user_id = 0;
	$user_name = '';
	$total_pages = 0;
	if (isset($_GET['user_id'])) { $user_id = (int)$_GET['user_id']; }
	$paged = isset($_GET['paged']) ? (int)$_GET['paged'] : 1;

	$user_orders = false;
	if ($user_id) {
		$user_orders_query = wc_get_orders(array('customer_id' => $user_id, 'limit' => 20, 'paged' => $paged, 'paginate' => true));
		$user_orders = $user_orders_query->orders;
		$total_orders = $user_orders_query->total;
		$total_pages = $user_orders_query->max_num_pages;
	}
	$rapid_user_search = print_products_is_rapid_user_search();
	?>
	<div class="wrap wp2print-uaf-wrap wp2print-uo-wrap">
		<h2><?php _e('Users Orders', 'wp2print'); ?></h2>
		<div class="tablenav top">
			<form>
				<?php if (isset($_GET['post_type'])) { ?><input type="hidden" name="post_type" value="<?php echo $_GET['post_type']; ?>"><?php } ?>
				<input type="hidden" name="page" value="print-products-users-orders">
				<div class="alignleft actions bulkactions">
					<select name="user_id" class="order-customer">
						<option value="">-- <?php _e('Select user', 'wp2print'); ?> --</option>
						<?php if ($rapid_user_search == 1) { ?>
							<?php if ($user_id) {
								$userdata = get_userdata($user_id);
								if ($userdata) { $user_name = $userdata->display_name; ?>
									<option value="<?php echo $userdata->ID; ?>" selected="selected"><?php echo $userdata->display_name; ?></option>
								<?php } ?>
							<?php } ?>
						<?php } else { ?>
							<?php $wp_users = get_users(array('orderby' => 'display_name'));
							foreach($wp_users as $wp_user) { ?>
								<option value="<?php echo $wp_user->ID; ?>"<?php if ($wp_user->ID == $user_id) { echo ' SELECTED'; $user_name = $wp_user->display_name; } ?>><?php echo $wp_user->display_name; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
					<input type="submit" class="button" value="<?php _e('Filter', 'wp2print'); ?>">
					<script>
					<!--
					jQuery(document).ready(function() {
					<?php if ($rapid_user_search == 1) { ?>
						jQuery('select.order-customer').select2({ajax: {url:'<?php echo site_url('/?AjaxAction=rapid-select-users'); ?>', dataType: 'json'}});
					<?php } else { ?>
						jQuery('select.order-customer').select2();
					<?php } ?>
					});
					//--></script>
				</div>
			</form>
		</div>
		<?php if ($user_id) { ?>
			<table class="wp-list-table widefat" width="100%">
				<thead>
					<tr>
						<th><?php _e('Customer name', 'wp2print'); ?></th>
						<th><?php _e('Date', 'wp2print'); ?></th>
						<th><?php _e('OrderID', 'wp2print'); ?></th>
						<th><?php _e('Amount', 'wp2print'); ?></th>
						<th><?php _e('Order Status', 'wp2print'); ?></th>
						<th><?php _e('Order Items', 'wp2print'); ?></th>
						<th style="text-align:center;"><?php _e('Proof Status', 'wp2print'); ?></th>
						<th><?php _e('Production Status', 'wp2print'); ?></th>
						<?php if (!in_array('vendor', $current_user->roles)) { ?><th><?php _e('Reorder', 'wp2print'); ?></th><?php } ?>
					</tr>
				</thead>
				<tbody>
					<?php if ($user_orders) { ?>
						<?php foreach ($user_orders as $order) {
							$order_id = $order->get_id();
							$order_date = $order->get_date_created();
							$order_status = $order->get_status();
							$order_total = $order->get_total();
							$order_items = $order->get_items();

							$oitems = array();
							$prod_statuses = array();
							$proof_statuses = array();
							$reorder_links = array();
							foreach($order_items as $item_id => $order_item) {
								$approval_status = wc_get_order_item_meta($item_id, '_approval_status', true);
								$item_status = wc_get_order_item_meta($item_id, '_item_status', true);

								$approval_html = '&nbsp;';
								if (strlen($approval_status) && isset($approval_statuses[$approval_status])) {
									$approval_html = '<mark class="'.$approval_status.'" title="'.$approval_statuses[$approval_status].'"></mark>';
								}
								$proof_status_val = (isset($oistatuses[$item_status]) ? $oistatuses[$item_status] : '');

								$oitems[] = '<tr><td>'.$order_item->get_name().'</td></tr>';
								$proof_statuses[] = '<tr><td>'.$approval_html.'</td></tr>';
								$prod_statuses[] = '<tr><td>'.$proof_status_val.'</td></tr>';
								$reorder_links[] = '<tr><td><a href="#reorder" class="button" onclick="return user_orders_reorder('.$order_id.', '.$item_id.');">'.__('Reorder', 'wp2print').'</a></td></tr>';
							}
							?>
							<tr>
								<td><?php echo $user_name; ?></td>
								<td><?php echo wc_format_datetime($order_date, 'd-M-y'); ?></td>
								<td><a href="<?php echo print_products_woocommerce_get_order_edit_url($order_id); ?>"><?php echo $order_id; ?></a></td>
								<td><?php echo wc_price($order_total); ?></td>
								<td><?php echo $wc_ostatuses['wc-'.$order_status]; ?></td>
								<td><table cellspacing="0" cellpadding="0"><?php echo implode('', $oitems); ?></table></td>
								<td class="column-approval"><table cellspacing="0" cellpadding="0" width="100%"><?php echo implode('', $proof_statuses); ?></table></td>
								<td><table cellspacing="0" cellpadding="0"><?php echo implode('', $prod_statuses); ?></table></td>
								<?php if (!in_array('vendor', $current_user->roles)) { ?><td><table cellspacing="0" cellpadding="0"><?php echo implode('', $reorder_links); ?></table></td><?php } ?>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td colspan="9"><?php _e('Nothing found.', 'wp2print'); ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php if ($total_pages > 1) { print_products_admin_pager('admin.php?page=print-products-users-orders&user_id='.$user_id, $paged, $total_pages, $total_orders); } ?>
		<?php } ?>
	</div>
	<div style="display:none;">
		<form method="POST" class="user-orders-form">
		<input type="hidden" name="user_orders_action" value="reorder">
		<input type="hidden" name="order_id" class="uoa-order-id">
		<input type="hidden" name="item_id" class="uoa-item-id">
		</form>
	</div>
	<?php
}
?>