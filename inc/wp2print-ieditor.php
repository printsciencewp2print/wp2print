<?php
add_action('wp_loaded', 'print_products_ieditor_init');
function print_products_ieditor_init() {
	if (isset($_REQUEST['ieditor']) && $_REQUEST['ieditor'] == 'request') {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Allow-Headers: Content-Type');
		header('Content-Type: application/json');
		if (isset($_REQUEST['rtype'])) {
			switch($_REQUEST['rtype']) {
				case 'fields':
					$json_data = print_products_ieditor_get_json_data();
					if ($json_data && isset($json_data['productID']) && $json_data['productID']) {
						$api_key = $json_data['api-key'];
						if ($api_key == 'gUV2455S5W2DQzUF') {
							$product_id = (int)$json_data['productID'];
							$request_fields = print_products_ieditor_request_fields($product_id);
							if ($request_fields) {
								echo json_encode($request_fields);
							} else {
								echo json_encode(array('error' => 'Product does not exist'));
							}
						}
					}
				break;
				case 'price':
					$json_data = print_products_ieditor_get_json_data();
					if ($json_data && isset($json_data['productID']) && $json_data['productID']) {
						$session_id = $json_data['sessionID'];
						$product_id = (int)$json_data['productID'];
						$fields = $json_data['fields'];
						if (isset($_REQUEST['test']) && $_REQUEST['test'] == 'true') {
							$fields = array();
							$fields[] = array('value' => '250');
							$fields[] = array('value' => '76');
							$fields[] = array('value' => '80');
							$fields[] = array('value' => '105');
							$fields[] = array('value' => '188');
							$fields[] = array('value' => '0');
						}
						$request_price = print_products_ieditor_request_price($product_id, $fields);
						if ($request_price) {
							echo json_encode(array('sessionID' => $session_id, 'currency-symbol' => get_woocommerce_currency_symbol(), 'price-excl-tax' => $request_price[0], 'price-excl-suffix' => get_option('woocommerce_price_display_excl_suffix'), 'redirectUrl' => $request_price[1]));
						} else {
							echo json_encode(array('error' => 'Product does not exist'));
						}
					}
				break;
			}
		}
		exit;
	}
}

function print_products_ieditor_get_json_data() {
	if (isset($_REQUEST['test']) && $_REQUEST['test'] == 'true') {
		return array('productID' => '3283', 'api-key' => 'gUV2455S5W2DQzUF'); // fixed - 231, area - 50, book - 534
	} else {
		return json_decode(file_get_contents('php://input'), true);
	}
}

function print_products_ieditor_get_terms_images() {
	$terms_images = array();
	$wp2print_attribute_images = get_option('wp2print_attribute_images');
	if ($wp2print_attribute_images) {
		foreach ($wp2print_attribute_images as $term_id => $image_id) {
			if ($image_id) {
				$image_attributes = wp_get_attachment_image_src($image_id, 'large');
				if ($image_attributes) {
					$terms_images[$term_id] = $image_attributes[0];
				}
			}
		}
	}
	return $terms_images;
}

function print_products_ieditor_get_terms_colors() {
	global $wpdb;
	$terms_colors = array();
	$crecords = $wpdb->get_results(sprintf("SELECT * FROM %stermmeta WHERE meta_key = '_color'", $wpdb->prefix));
	if ($crecords) {
		foreach ($crecords as $crecord) {
			$terms_colors[$crecord->term_id] = $crecord->meta_value;
		}
	}
	return $terms_colors;
}

function print_products_ieditor_request_fields($product_id, $internal = false) {
	global $wpdb, $print_products_settings, $attribute_slugs, $attribute_names, $terms_names, $terms_slugs, $attribute_types;
	$request_fields = array();
	$fie_settings = get_option('fie_settings');
	$size_attribute = $print_products_settings['size_attribute'];
	$fotovender_attributes = isset($fie_settings['attributes']) ? $fie_settings['attributes'] : array();
	if (!is_array($fotovender_attributes)) { $fotovender_attributes = array(); }
	$aterms_images = print_products_ieditor_get_terms_images();
	$aterms_colors = print_products_ieditor_get_terms_colors();
	$product = wc_get_product($product_id);
	if ($product) { // units, rounding, minimum, maximum
		$ifields = array(); $mtypes = array();
		$attribute_labels = (array)get_post_meta($product_id, '_attribute_labels', true);
		$attribute_display = get_post_meta($product_id, '_attribute_display', true);
		$attributes = $wpdb->get_results(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies ORDER BY attribute_order, attribute_label", $wpdb->prefix));
		print_products_price_matrix_attr_names_init($attributes);
		$product_type_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s ORDER BY mtype, sorder", $wpdb->prefix, $product_id));
		$product_type = $product->get_type();
		switch($product_type) {
			case 'fixed':
				$ieditor_product_id = get_post_meta($product_id, '_ieditor_product_id', true);
				if ($product_type_matrix_types) { $qtyfill = false;
					foreach($product_type_matrix_types as $product_type_matrix_type) {
						$mtype_id = $product_type_matrix_type->mtype_id;
						$mtype = $product_type_matrix_type->mtype;
						$mattributes = unserialize($product_type_matrix_type->attributes);
						$materms = unserialize($product_type_matrix_type->aterms);
						$numbers = explode(',', $product_type_matrix_type->numbers);
						$num_style = $product_type_matrix_type->num_style;
						$num_type = $product_type_matrix_type->num_type;
						$ltext_attr = (int)$product_type_matrix_type->ltext_attr;
						$min_qmailed = (int)$product_type_matrix_type->min_qmailed;
						if ($mattributes) {
							$mattributes = print_products_sort_attributes($mattributes);
							if (!$qtyfill && $numbers) {
								if ($num_style == 1) {
									$options = array();
									foreach($numbers as $n => $number) { $options[] = array('optionID' => $n, 'label' => $number); }
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'DROPDOWN',
										'slug' => 'quantity',
										'label' => print_products_attribute_label('quantity', $attribute_labels, __('Quantity', 'wp2print')),
										'hidden' => 'false',
										'options' => $options
									);
								} else {
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'TEXTBOX',
										'slug' => 'quantity',
										'label' => print_products_attribute_label('quantity', $attribute_labels, __('Quantity', 'wp2print')),
										'hidden' => 'false',
										'minimum' => $numbers[0]
									);
								}
								$qtyfill = true;
								$ifields[] = 'qty';
							}
							$mtfields = array();
							foreach($mattributes as $mattribute) {
								$matype = $attribute_types[$mattribute];
								$aterms = $materms[$mattribute];
								if ($matype == 'text') {
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'TEXTBOX',
										'slug' => in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute',
										'label' => print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]),
										'hidden' => 'false'
									);
									$ifields[] = $mattribute;
									$mtfields[] = array('matype' => $matype, 'mattribute' => $mattribute);
								} else {
									if ($aterms) {
										$aterms = print_products_get_attribute_terms($aterms);
										$do_not_display = (isset($attribute_display[$mattribute]) ? (int)$attribute_display[$mattribute] : 0);
										if ($do_not_display) {
											$request_fields[] = array(
												'fieldID' => count($request_fields),
												'fieldType' => 'TEXTBOX',
												'slug' => in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute',
												'label' => print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]),
												'hidden' => 'true'
											);
										} else {
											$options = array();
											foreach($aterms as $aterm_id => $aterm_name) {
												if ($mattribute == $size_attribute && is_array($ieditor_product_id) && isset($ieditor_product_id[$aterm_id])) {
													$ie_pid = '';
													foreach($ieditor_product_id[$aterm_id] as $iepid) { $ie_pid = $iepid; }
													$odata = array('optionID' => $aterm_id, 'label' => $aterm_name, 'productId' => $ie_pid);
												} else {
													$odata = array('optionID' => $aterm_id, 'label' => $aterm_name);
												}
												if (in_array($mattribute, $fotovender_attributes)) {
													$odata['slug'] = $terms_slugs[$aterm_id];
												}
												if (isset($aterms_images[$aterm_id]) && $aterms_images[$aterm_id]) {
													$odata['image'] = $aterms_images[$aterm_id];
												}
												if (isset($aterms_colors[$aterm_id]) && $aterms_colors[$aterm_id]) {
													$odata['color'] = $aterms_colors[$aterm_id];
												}
												$options[] = $odata;
											}
											$aslug = in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute';
											if ($mattribute == $size_attribute) { $aslug = 'size'; }
											$request_fields[] = array(
												'fieldID' => count($request_fields),
												'fieldType' => 'DROPDOWN',
												'slug' => $aslug,
												'label' => print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]),
												'hidden' => 'false',
												'options' => $options
											);
										}
										$ifields[] = $mattribute;
										$mtfields[] = array('matype' => $matype, 'mattribute' => $mattribute, 'anum' => count($ifields) - 1);
									}
								}
							}
						}
						$mtypes[$mtype_id] = array('mtype' => $mtype, 'num_style' => $num_style, 'num_type' => $num_type, 'ltext_attr' => $ltext_attr, 'min_qmailed' => $min_qmailed, 'fields' => $mtfields);
					}
					$addon_products = get_post_meta($product_id, '_addon_products', true);
					if ($addon_products && is_array($addon_products) && count($addon_products)) {
						foreach($addon_products as $aproduct_id) {
							$aproduct = wc_get_product($aproduct_id);
							$request_fields[] = array(
								'fieldID' => count($request_fields),
								'fieldType' => 'TEXTBOX',
								'slug' => 'attribute',
								'label' => $aproduct->get_name(),
								'hidden' => 'false'
							);
							$ifields[] = 'addon';
						}
					}
					$use_production_speed = (int)get_post_meta($product_id, '_use_production_speed', true);
					$production_speed_label = get_post_meta($product_id, '_production_speed_label', true);
					$production_speed_options = get_post_meta($product_id, '_production_speed_options', true);
					if ($use_production_speed && $production_speed_options && is_array($production_speed_options) && count($production_speed_options)) {
						$options = array();
						foreach($production_speed_options as $okey => $pso) { $options[] = array('optionID' => $okey, 'label' => $pso['label']); }
						$request_fields[] = array(
							'fieldID' => count($request_fields),
							'fieldType' => 'DROPDOWN',
							'slug' => 'attribute',
							'label' => $production_speed_label,
							'hidden' => 'false',
							'options' => $options
						);
						$ifields[] = 'pspeed';
					}
				}
			break;
			// ==============================================================
			case 'area':
				$dimension_unit = print_products_get_dimension_unit();
				$area_product_min_width = (float)get_post_meta($product_id, '_area_min_width', true);
				$area_product_max_width = (float)get_post_meta($product_id, '_area_max_width', true);
				$area_product_min_height = (float)get_post_meta($product_id, '_area_min_height', true);
				$area_product_max_height = (float)get_post_meta($product_id, '_area_max_height', true);
				$area_product_min_quantity = (float)get_post_meta($product_id, '_area_min_quantity', true);
				$area_product_width_round = get_post_meta($product_id, '_area_width_round', true);
				$area_product_height_round = get_post_meta($product_id, '_area_height_round', true);
				$ieditor_product_id = get_post_meta($product_id, '_ieditor_product_id', true);
				if (!$area_product_min_quantity) { $area_product_min_quantity = 1; }
				if ($product_type_matrix_types) { $qtyfill = false; $whfill = false;
					foreach($product_type_matrix_types as $product_type_matrix_type) {
						$mtype_id = $product_type_matrix_type->mtype_id;
						$mtype = $product_type_matrix_type->mtype;
						$mattributes = unserialize($product_type_matrix_type->attributes);
						$materms = unserialize($product_type_matrix_type->aterms);
						$numbers = explode(',', $product_type_matrix_type->numbers);
						$num_style = $product_type_matrix_type->num_style;
						$num_type = $product_type_matrix_type->num_type;
						if ($mattributes) {
							$mattributes = print_products_sort_attributes($mattributes);
							if (!$qtyfill && $numbers) {
								if ($num_style == 1) {
									$options = array();
									foreach($numbers as $n => $number) { $options[] = array('optionID' => $n, 'label' => $number); }
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'DROPDOWN',
										'slug' => 'quantity',
										'label' => print_products_attribute_label('quantity', $attribute_labels, __('Quantity', 'wp2print')),
										'hidden' => 'false',
										'options' => $options
									);
								} else {
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'TEXTBOX',
										'slug' => 'quantity',
										'label' => print_products_attribute_label('quantity', $attribute_labels, __('Quantity', 'wp2print')),
										'hidden' => 'false',
										'minimum' => $area_product_min_quantity
									);
								}
								$qtyfill = true;
								$ifields[] = 'qty';
							}
							if (!$whfill) {
								$request_fields[] = array(
									'fieldID' => count($request_fields),
									'fieldType' => 'TEXTBOX',
									'slug' => 'width',
									'label' => print_products_attribute_label('width', $attribute_labels, __('Width', 'wp2print')).' ('.$dimension_unit.')',
									'hidden' => 'false',
									'minimum' => $area_product_min_width,
									'maximum' => $area_product_max_width,
									'rounding' => $area_product_width_round,
									'units' => $dimension_unit
								);
								$ifields[] = 'width';
								$request_fields[] = array(
									'fieldID' => count($request_fields),
									'fieldType' => 'TEXTBOX',
									'slug' => 'height',
									'label' => print_products_attribute_label('height', $attribute_labels, __('Height', 'wp2print')).' ('.$dimension_unit.')',
									'hidden' => 'false',
									'minimum' => $area_product_min_height,
									'maximum' => $area_product_max_height,
									'rounding' => $area_product_height_round,
									'units' => $dimension_unit
								);
								$ifields[] = 'height';
								$whfill = true;
							}
							$mtfields = array();
							foreach($mattributes as $mattribute) {
								$matype = $attribute_types[$mattribute];
								$aterms = $materms[$mattribute];
								if ($matype == 'text') {
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'TEXTBOX',
										'slug' => in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute',
										'label' => print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]),
										'hidden' => 'false'
									);
									$ifields[] = $mattribute;
									$mtfields[] = array('matype' => $matype, 'mattribute' => $mattribute);
								} else {
									if ($aterms) {
										$aterms = print_products_get_attribute_terms($aterms);
										$do_not_display = (isset($attribute_display[$mattribute]) ? (int)$attribute_display[$mattribute] : 0);
										if ($do_not_display) {
											$request_fields[] = array(
												'fieldID' => count($request_fields),
												'fieldType' => 'TEXTBOX',
												'slug' => in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute',
												'label' => print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]),
												'hidden' => 'true'
											);
										} else {
											$options = array();
											foreach($aterms as $aterm_id => $aterm_name) {
												$odata = array('optionID' => $aterm_id, 'label' => $aterm_name);
												if (in_array($mattribute, $fotovender_attributes)) {
													$odata['slug'] = $terms_slugs[$aterm_id];
												}
												if (isset($aterms_images[$aterm_id]) && $aterms_images[$aterm_id]) {
													$odata['image'] = $aterms_images[$aterm_id];
												}
												if (isset($aterms_colors[$aterm_id]) && $aterms_colors[$aterm_id]) {
													$odata['color'] = $aterms_colors[$aterm_id];
												}
												$options[] = $odata;
											}
											$request_fields[] = array(
												'fieldID' => count($request_fields),
												'fieldType' => 'DROPDOWN',
												'slug' => in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute',
												'label' => print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]),
												'hidden' => 'false',
												'options' => $options
											);
										}
										$ifields[] = $mattribute;
										$mtfields[] = array('matype' => $matype, 'mattribute' => $mattribute, 'anum' => count($ifields) - 1);
									}
								}
							}
						}
						$mtypes[$mtype_id] = array('mtype' => $mtype, 'num_style' => $num_style, 'num_type' => $num_type, 'fields' => $mtfields);
					}
					$addon_products = get_post_meta($product_id, '_addon_products', true);
					if ($addon_products && is_array($addon_products) && count($addon_products)) {
						foreach($addon_products as $aproduct_id) {
							$aproduct = wc_get_product($aproduct_id);
							$request_fields[] = array(
								'fieldID' => count($request_fields),
								'fieldType' => 'TEXTBOX',
								'slug' => 'attribute',
								'label' => $aproduct->get_name(),
								'hidden' => 'false'
							);
							$ifields[] = 'addon';
						}
					}
					$use_production_speed = (int)get_post_meta($product_id, '_use_production_speed', true);
					$production_speed_label = get_post_meta($product_id, '_production_speed_label', true);
					$production_speed_options = get_post_meta($product_id, '_production_speed_options', true);
					if ($use_production_speed && $production_speed_options && is_array($production_speed_options) && count($production_speed_options)) {
						$options = array();
						foreach($production_speed_options as $okey => $pso) { $options[] = array('optionID' => $okey, 'label' => $pso['label']); }
						$request_fields[] = array(
							'fieldID' => count($request_fields),
							'fieldType' => 'DROPDOWN',
							'slug' => 'attribute',
							'label' => $production_speed_label,
							'hidden' => 'false',
							'options' => $options
						);
						$ifields[] = 'pspeed';
					}
				}
			break;
			// ==============================================================
			case 'book':
				$product_book_type = (int)get_post_meta($product_id, '_book_type', true); 
				if ($product_type_matrix_types) { $qtyfill = false; $pqtyfill = false; $sizefill = false;
					foreach($product_type_matrix_types as $product_type_matrix_type) {
						$mtype_id = $product_type_matrix_type->mtype_id;
						$mtype = $product_type_matrix_type->mtype;
						$mattributes = unserialize($product_type_matrix_type->attributes);
						$materms = unserialize($product_type_matrix_type->aterms);
						$numbers = explode(',', $product_type_matrix_type->numbers);
						$num_style = $product_type_matrix_type->num_style;
						$num_type = $product_type_matrix_type->num_type;
						$bq_numbers = explode(',', $product_type_matrix_type->bq_numbers);
						$book_min_quantity = (int)$product_type_matrix_type->book_min_quantity;
						$pq_style = $product_type_matrix_type->pq_style;
						$pq_numbers = explode(',', $product_type_matrix_type->pq_numbers);
						$title = $product_type_matrix_type->title;
						$def_quantity = (int)$product_type_matrix_type->def_quantity;

						if (strlen($title)) { $title .= ' '; }
						if (!$book_min_quantity) { $book_min_quantity = 1; }
						$pqfkey = 'bw';
						if ($pcnmb > 0) { $pqfkey = 'cl'; }

						if ($mattributes) {
							$mattributes = print_products_sort_attributes($mattributes);
							if (!$qtyfill && $bq_numbers) {
								if ($num_style == 1 && count($bq_numbers)) {
									$options = array();
									foreach($bq_numbers as $n => $number) { $options[] = array('optionID' => $n, 'label' => $number); }
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'DROPDOWN',
										'slug' => 'book-quantity',
										'label' => print_products_attribute_label('bquantity', $attribute_labels, __('Quantity of bound books', 'wp2print')),
										'hidden' => 'false',
										'options' => $options
									);
								} else {
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'TEXTBOX',
										'slug' => 'book-quantity',
										'label' => print_products_attribute_label('bquantity', $attribute_labels, __('Quantity of bound books', 'wp2print')),
										'hidden' => 'false',
										'minimum' => $book_min_quantity
									);
								}
								$qtyfill = true;
								$ifields[] = 'qty';
							}
							if (in_array($size_attribute, $mattributes)) {
								if (!$sizefill) {
									$sizekey = array_search($size_attribute, $mattributes);
									$aterms = $materms[$size_attribute];
									$aterms = print_products_get_attribute_terms($aterms);
									$do_not_display = 0;
									if (isset($attribute_display[$size_attribute])) { $do_not_display = (int)$attribute_display[$size_attribute]; }
									if ($do_not_display) {
										$request_fields[] = array(
											'fieldID' => count($request_fields),
											'fieldType' => 'TEXTBOX',
											'slug' => 'size',
											'label' => print_products_attribute_label($size_attribute, $attribute_labels, $attribute_names[$size_attribute]),
											'hidden' => 'true'
										);
									} else {
										$options = array();
										foreach($aterms as $aterm_id => $aterm_name) { $options[] = array('optionID' => $aterm_id, 'label' => $aterm_name); }
										$request_fields[] = array(
											'fieldID' => count($request_fields),
											'fieldType' => 'DROPDOWN',
											'slug' => in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute',
											'label' => print_products_attribute_label($size_attribute, $attribute_labels, $attribute_names[$size_attribute]),
											'hidden' => 'false',
											'options' => $options
										);
									}
									$ifields[] = 'size';
								}
								unset($mattributes[$sizekey]);
								$sizefill = true;
							}
							if ($mtype == 0 && $pqfkey != $lpfkey) {
								if ($product_book_type > 0) {
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'TEXTBOX',
										'slug' => 'page-quantity',
										'label' => $title.print_products_attribute_label('pquantity', $attribute_labels, __('Pages Quantity', 'wp2print')),
										'hidden' => 'true',
										'minimum' => $def_quantity
									);
								} else {
									if ($pq_style == 1 && count($pq_numbers)) {
										$options = array();
										foreach($pq_numbers as $n => $pq_number) { $options[] = array('optionID' => $n, 'label' => $pq_number); }
										$request_fields[] = array(
											'fieldID' => count($request_fields),
											'fieldType' => 'DROPDOWN',
											'slug' => 'page-quantity',
											'label' => $title.print_products_attribute_label('pquantity', $attribute_labels, __('Pages Quantity', 'wp2print')),
											'hidden' => 'false',
											'options' => $options
										);
									} else {
										$request_fields[] = array(
											'fieldID' => count($request_fields),
											'fieldType' => 'TEXTBOX',
											'slug' => 'page-quantity',
											'label' => $title.print_products_attribute_label('pquantity', $attribute_labels, __('Pages Quantity', 'wp2print')),
											'hidden' => 'false',
											'minimum' => $def_quantity
										);
									}
								}
								$ifields[] = 'pq-'.$pqfkey;
								$pqtyfill = true;
							}
							$mtfields = array();
							foreach($mattributes as $mattribute) {
								$matype = $attribute_types[$mattribute];
								$aterms = $materms[$mattribute];
								if ($matype == 'text') {
									$request_fields[] = array(
										'fieldID' => count($request_fields),
										'fieldType' => 'TEXTBOX',
										'slug' => in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute',
										'label' => print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]),
										'hidden' => 'false'
									);
									$ifields[] = $mattribute;
									$mtfields[] = array('matype' => $matype, 'mattribute' => $mattribute);
								} else {
									if ($aterms) {
										$aterms = print_products_get_attribute_terms($aterms);
										$do_not_display = (isset($attribute_display[$mattribute]) ? (int)$attribute_display[$mattribute] : 0);
										if ($do_not_display) {
											$request_fields[] = array(
												'fieldID' => count($request_fields),
												'fieldType' => 'TEXTBOX',
												'slug' => in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute',
												'label' => print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]),
												'hidden' => 'true'
											);
										} else {
											$options = array();
											foreach($aterms as $aterm_id => $aterm_name) {
												$odata = array('optionID' => $aterm_id, 'label' => $aterm_name);
												if (in_array($mattribute, $fotovender_attributes)) {
													$odata['slug'] = $terms_slugs[$aterm_id];
												}
												if (isset($aterms_images[$aterm_id]) && $aterms_images[$aterm_id]) {
													$odata['image'] = $aterms_images[$aterm_id];
												}
												if (isset($aterms_colors[$aterm_id]) && $aterms_colors[$aterm_id]) {
													$odata['color'] = $aterms_colors[$aterm_id];
												}
												$options[] = $odata;
											}
											$request_fields[] = array(
												'fieldID' => count($request_fields),
												'fieldType' => 'DROPDOWN',
												'slug' => in_array($mattribute, $fotovender_attributes) ? $attribute_slugs[$mattribute] : 'attribute',
												'label' => print_products_attribute_label($mattribute, $attribute_labels, $attribute_names[$mattribute]),
												'hidden' => 'false',
												'options' => $options
											);
										}
										$ifields[] = $mattribute;
										$mtfields[] = array('matype' => $matype, 'mattribute' => $mattribute, 'anum' => count($ifields) - 1);
									}
								}
							}
						}
						$lmtype = $mtype;
						$lpfkey = $pfkey;
						$pcnmb++;
						$mtypes[$mtype_id] = array('mtype' => $mtype, 'num_style' => $num_style, 'num_type' => $num_type, 'fields' => $mtfields);
					}
					$addon_products = get_post_meta($product_id, '_addon_products', true);
					if ($addon_products && is_array($addon_products) && count($addon_products)) {
						foreach($addon_products as $aproduct_id) {
							$aproduct = wc_get_product($aproduct_id);
							$request_fields[] = array(
								'fieldID' => count($request_fields),
								'fieldType' => 'TEXTBOX',
								'slug' => 'attribute',
								'label' => $aproduct->get_name(),
								'hidden' => 'false'
							);
							$ifields[] = 'addon';
						}
					}
					$use_production_speed = (int)get_post_meta($product_id, '_use_production_speed', true);
					$production_speed_label = get_post_meta($product_id, '_production_speed_label', true);
					$production_speed_options = get_post_meta($product_id, '_production_speed_options', true);
					if ($use_production_speed && $production_speed_options && is_array($production_speed_options) && count($production_speed_options)) {
						$options = array();
						foreach($production_speed_options as $okey => $pso) { $options[] = array('optionID' => $okey, 'label' => $pso['label']); }
						$request_fields[] = array(
							'fieldID' => count($request_fields),
							'fieldType' => 'DROPDOWN',
							'slug' => 'attribute',
							'label' => $production_speed_label,
							'hidden' => 'false',
							'options' => $options
						);
						$ifields[] = 'pspeed';
					}
				}
			break;
		}
		if ($internal) {
			return array($ifields, $mtypes);
		} else {
			return $request_fields;
		}
	} else {
		return false;
	}
}

function print_products_ieditor_request_price($product_id, $fields) {
	global $print_products_settings;
	$request_price = 0;
	$request_fields = array();
	$postage_attribute = (int)$print_products_settings['postage_attribute'];
	$size_attribute = $print_products_settings['size_attribute'];
	$product = wc_get_product($product_id);
	if ($product) {
		$product_type = $product->get_type();
		$request_fields = print_products_ieditor_request_fields($product_id, true);
		if ($request_fields) {
			$product_fields = $request_fields[0];
			$product_mtypes = $request_fields[1];
			$request_data = array('add-to-cart' => $product_id, 'product_id' => $product_id, 'product_type' => $product_type, 'atcaction' => 'artwork');
			switch($product_type) {
				case 'fixed':
					$smparams = ''; $smsep = '';
					$fmparams = ''; $fmsep = '';
					if ($product_mtypes) {
						$nmb_val = 1;
						foreach($product_fields as $fnum => $product_field) {
							if ($product_field == 'qty') {
								$nmb_val = $fields[$fnum]['value'];
							}
						}
						foreach($product_mtypes as $mtype_id => $mtype) {
							if ($mtype['mtype'] == 0) {
								$smfields = array();
								foreach($mtype['fields'] as $mfield) {
									$smfields[] = $mfield['mattribute'].':'.$fields[$mfield['anum']]['value'];
								}
								if ($mtype['num_type'] == 5) {
									foreach($product_fields as $fnum => $product_field) {
										if ($product_field == $mtype['ltext_attr']) { $nmb_val = strlen($fields[$fnum]['value']); }
									}
								}
								if (strlen($smparams)) { $smparams .= ';'; }
								$smparams .= $mtype_id.'|'.implode('-', $smfields).'|'.$nmb_val;
							} else {
								$size_id = 0; $size_val = 0;
								if ($mtype['num_type'] == 5) {
									foreach($product_fields as $fnum => $product_field) {
										if ($product_field == $mtype['ltext_attr']) { $nmb_val = strlen($fields[$fnum]['value']); }
									}
								} else {
									foreach($product_fields as $fnum => $product_field) {
										if ($product_field == $postage_attribute) { $nmb_val = (int)$fields[$fnum]['value']; }
										if ($product_field == $size_attribute) { $size_id = $product_field; $size_val = (int)$fields[$fnum]['value']; }
									}
								}
								foreach($mtype['fields'] as $mfield) {
									$fmval = $mfield['mattribute'].':'.$fields[$mfield['anum']]['value'];
									if ($size_id && $mfield['mattribute'] != $postage_attribute) {
										$fmval = $size_id.':'.$size_val.'-'.$mfield['mattribute'].':'.$fields[$mfield['anum']]['value'];
									}
									if (strlen($fmparams)) { $fmparams .= ';'; }
									$fmparams .= $mtype_id.'|'.$fmval.'|'.$nmb_val;
								}
							}
						}
					}
					foreach($product_fields as $fnum => $product_field) {
						$fval = $fields[$fnum]['value'];
						$ftype = $product_field;
						if ($ftype == 'qty') {
							$request_data['quantity'] = $fval;
						} else if ($ftype == 'addon') {
							$request_data['addon_products'] = $fval;
						} else if ($ftype == 'pspeed') {
							$request_data['production_speed'] = $fval;
						}
					}
					$request_data['smparams'] = $smparams;
					$request_data['fmparams'] = $fmparams;
					foreach($request_data as $rkey => $rval) {
						$_REQUEST[$rkey] = $rval;
					}
					$pdata = print_products_checkout_fixed(time(), false, true);
				break;
				// ==============================================================
				case 'area':
					$dimension_unit = print_products_get_dimension_unit();
					$area_unit = (int)get_post_meta($product_id, '_area_unit', true);
					$smparams = ''; $smsep = '';
					$fmparams = ''; $fmsep = '';
					if ($product_mtypes) {
						$quantity = 1; $width = 0; $height = 0;
						foreach($product_fields as $fnum => $product_field) {
							if ($product_field == 'qty') {
								$quantity = $fields[$fnum]['value'];
							} else if ($product_field == 'width') {
								$width = $fields[$fnum]['value'];
							} else if ($product_field == 'height') {
								$height = $fields[$fnum]['value'];
							}
						}
						foreach($product_mtypes as $mtype_id => $mtype) {
							$w = $width; $h = $height;
							if ($mtype['num_type'] == 2 || $mtype['num_type'] == 3) { // area type
								$area_size = print_products_get_area_size($width, $height, $dimension_unit, $area_unit);
								$w = $area_size[0];
								$h = $area_size[1];
							}
							$nmb_val = $quantity;
							if ($mtype['num_type'] == 2) { // total area
								$nmb_val = $quantity * $w * $h;
							} else if ($mtype['num_type'] == 3) { // total perimeter
								$nmb_val = $quantity * (($w * 2) + ($h * 2));
							} else if ($mtype['num_type'] == 4) {
								$nmb_val = $quantity * ($w * 2);
							}
							if ($mtype['mtype'] == 0) {
								$smfields = array();
								foreach($mtype['fields'] as $mfield) {
									$smfields[] = $mfield['mattribute'].':'.$fields[$mfield['anum']]['value'];
								}
								if (strlen($smparams)) { $smparams .= ';'; }
								$smparams .= $mtype_id.'|'.implode('-', $smfields).'|'.$nmb_val.'|'.$mtype['num_type'];
							} else {
								foreach($mtype['fields'] as $mfield) {
									$fmval = $mfield['mattribute'].':'.$fields[$mfield['anum']]['value'];
									if (strlen($fmparams)) { $fmparams .= ';'; }
									$fmparams .= $mtype_id.'|'.$fmval.'|'.$nmb_val.'|'.$mtype['num_type'];
								}
							}
						}
					}
					foreach($product_fields as $fnum => $product_field) {
						$fval = $fields[$fnum]['value'];
						$ftype = $product_field;
						if ($ftype == 'qty') {
							$request_data['quantity'] = $fval;
						} else if ($ftype == 'width') {
							$request_data['width'] = $fval;
						} else if ($ftype == 'height') {
							$request_data['height'] = $fval;
						} else if ($ftype == 'addon') {
							$request_data['addon_products'] = $fval;
						} else if ($ftype == 'pspeed') {
							$request_data['production_speed'] = $fval;
						}
					}
					$request_data['smparams'] = $smparams;
					$request_data['fmparams'] = $fmparams;
					foreach($request_data as $rkey => $rval) {
						$_REQUEST[$rkey] = $rval;
					}
					$pdata = print_products_checkout_area(time(), false, true);
				break;
				// ==============================================================
				case 'book':
					$pcnmb = 0; $pqfkey = 'bw';
					$smparams = ''; $smsep = '';
					$fmparams = ''; $fmsep = '';
					if ($product_mtypes) {
						$books_quantity = 1;
						$total_pages = 0;
						$size_id = 0; $size_val = 0;
						foreach($product_fields as $fnum => $product_field) {
							if ($product_field == 'qty') {
								$books_quantity = $fields[$fnum]['value'];
							} else if ($product_field == 'size') {
								$size_id = $size_attribute;
								$size_val = (int)$fields[$fnum]['value'];
							}
						}
						foreach($product_mtypes as $mtype_id => $mtype) {
							if ($mtype['mtype'] == 0) {
								$quantity = 1;
								if ($pcnmb > 0) { $pqfkey = 'cl'; }
								foreach($product_fields as $fnum => $product_field) {
									if ($product_field == 'pq-'.$pqfkey) {
										$quantity = $fields[$fnum]['value'];
										$total_pages = $total_pages + $quantity;
										$request_data['page_quantity_'.$mtype_id] = $quantity;
										
									}
								}
								$nmb_val = $quantity;
								if ($mtype['num_type'] == 1) {
									$nmb_val = $quantity * $books_quantity;
								}
								$smfields = array();
								if ($size_id) {
									$smfields[] = $size_id.':'.$size_val;
								}
								foreach($mtype['fields'] as $mfield) {
									$smfields[] = $mfield['mattribute'].':'.$fields[$mfield['anum']]['value'];
								}
								if (strlen($smparams)) { $smparams .= ';'; }
								$smparams .= $mtype_id.'|'.implode('-', $smfields).'|'.$nmb_val.'|'.$mtype['num_type'];
								$pcnmb++;
							} else {
								$nmb_val = $books_quantity;
								if ($mtype['num_type'] == 1) {
									$nmb_val = $total_pages;
								}
								foreach($mtype['fields'] as $mfield) {
									$fmval = $mfield['mattribute'].':'.$fields[$mfield['anum']]['value'];
									if ($size_id) {
										$fmval = $size_id.':'.$size_val.'-'.$mfield['mattribute'].':'.$fields[$mfield['anum']]['value'];
									}
									if (strlen($fmparams)) { $fmparams .= ';'; }
									$fmparams .= $mtype_id.'|'.$fmval.'|'.$nmb_val.'|'.$mtype['num_type'];
								}
							}
						}
					}
					foreach($product_fields as $fnum => $product_field) {
						$fval = $fields[$fnum]['value'];
						$ftype = $product_field;
						if ($ftype == 'qty') {
							$request_data['quantity'] = $fval;
						} else if ($ftype == 'addon') {
							$request_data['addon_products'] = $fval;
						} else if ($ftype == 'pspeed') {
							$request_data['production_speed'] = $fval;
						}
					}
					$request_data['smparams'] = $smparams;
					$request_data['fmparams'] = $fmparams;
					foreach($request_data as $rkey => $rval) {
						$_REQUEST[$rkey] = $rval;
					}
					$pdata = print_products_checkout_book(time(), false, true);
				break;
			}
		}
		$request_data['pprice'] = $pdata['price'];
		return array($pdata['price'], print_products_ieditor_get_redirect_url($request_data));
	} else {
		return false;
	}
}

function print_products_ieditor_get_redirect_url($request_data) {
	$rparams = array();
	$rparams[] = 'fie_api_action=wp2print';
	$rparams[] = 'print_products_checkout_process_action=add-to-cart';
	foreach($request_data as $rkey => $rval) {
		$rparams[] = $rkey.'='.$rval;
	}
	return site_url('/?'.implode('&', $rparams));
}

function print_products_ieditor_api_redirect() {
	$product_id = $_REQUEST['product_id'];
	$api_product_id = $_REQUEST['ieditor_product_id'];
	$initial_params = print_products_image_editor_get_initial($product_id);

	$depth = print_products_ieditor_get_depth();

	unset($_REQUEST['ieditor_product_id']);
	if (isset($_REQUEST['sattribute'])) { unset($_REQUEST['sattribute']); }
	if (isset($_REQUEST['fattribute'])) { unset($_REQUEST['fattribute']); }
	$rparams = $_REQUEST;
	$rparams['fie_api_action'] = 'wp2print';
	$rparams['add-to-cart'] = $product_id;
	$rparams['print_products_checkout_process_action'] = 'add-to-cart';

	$_SESSION['ieartworkfiles'] = $_REQUEST['artworkfiles'];

	$success_params = array();
	foreach($rparams as $rp_key => $rp_val) {
		$success_params[] = $rp_key.'='.$rp_val;
	}

	$artwork_file = $_REQUEST['artworkfiles'];

	if (function_exists('fie_get_api_url') && $api_product_id && strlen($artwork_file)) {
		$image_path = substr($artwork_file, strpos($artwork_file, 'amazonaws.com/') + 14);
		$image_path = str_replace('/large/', '/original/', $image_path);

		$exp = substr($image_path, strrpos($image_path, '.'));
		$output_path = str_replace($exp, '-edited'.$exp, $image_path);

		$api_data = array(
			'product_id' => $product_id,
			'api_product_id' => $api_product_id,
			'initial_params' => implode(';', $initial_params),
			'cpage' => $product_id,
			'input_image' => urlencode($image_path),
			'output_image' => urlencode($output_path),
			'success_params' => $success_params
		);
		if (isset($_REQUEST['width'])) {
			$api_data['unit'] = print_products_get_dimension_unit();
			$api_data['width'] = $_REQUEST['width'];
			$api_data['height'] = $_REQUEST['height'];
		}
		if ($depth) {
			$api_data['depth'] = $depth;
		}
		$api_url = fie_get_api_url($api_data);
		header('Location: '.$api_url);
		exit;
	}
}

function print_products_ieditor_get_depth() {
	global $print_products_settings, $wpdb;
	$depth_value = '';
	if (isset($print_products_settings['depth_attribute']) && $print_products_settings['depth_attribute']) {
		$depth_attribute = $print_products_settings['depth_attribute'];
		if (isset($_REQUEST['sattribute'])) {
			if (isset($_REQUEST['sattribute'][$depth_attribute])) {
				$depth_value = $_REQUEST['sattribute'][$depth_attribute];
			}
		}
		if (isset($_REQUEST['fattribute'])) {
			if (isset($_REQUEST['fattribute'][$depth_attribute])) {
				$depth_value = $_REQUEST['fattribute'][$depth_attribute];
			}
		}
		if ($depth_value) {
			$depth_object = $wpdb->get_row(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies WHERE attribute_id = %s", $wpdb->prefix, $depth_attribute));
			if ($depth_object) {
				if ($depth_object->attribute_type == 'select') {
					$depth_term = get_term_by('term_id', $depth_value, 'pa_'.$depth_object->attribute_name);
					if ($depth_term) {
						$depth_value = $depth_term->name;
					}
				}
			}
			
		}
	}
	return $depth_value;
}

add_filter('woocommerce_add_cart_item_data', 'print_products_image_editor_add_cart_item_data', 10, 2);
function print_products_image_editor_add_cart_item_data($cart_item_data, $product_id) {
	if (isset($_REQUEST['fie_api_action']) && $_REQUEST['fie_api_action'] == 'wp2print' && isset($_REQUEST['outputHR'])) {
		$cart_item_data['ieditor'] = array('image' => $_REQUEST['outputHR'], 'thumb' => $_REQUEST['outputTH']);
		if (isset($_SESSION['ieartworkfiles']) && strlen($_SESSION['ieartworkfiles'])) {
			$_REQUEST['old_artworkfiles'] = $_SESSION['ieartworkfiles'];
			unset($_SESSION['ieartworkfiles']);
		}
		$_REQUEST['artworkfiles'] = $_REQUEST['outputHR'];
	}
	return $cart_item_data;
}

function print_products_image_editor_get_initial($product_id) {
	global $print_products_settings;
	$initial = array();
	$size_attribute = $print_products_settings['size_attribute'];

	if (isset($_REQUEST['print_products_checkout_process_action']) && $_REQUEST['print_products_checkout_process_action'] == 'image-editor') {
		$sattribute = array(); if (isset($_REQUEST['sattribute']) && $_REQUEST['sattribute']) { $sattribute = $_REQUEST['sattribute']; }
		$fattribute = array(); if (isset($_REQUEST['fattribute']) && $_REQUEST['fattribute']) { $fattribute = $_REQUEST['fattribute']; }

		$request_fields = print_products_ieditor_request_fields($product_id, true);
		if ($request_fields) {
			$product_fields = $request_fields[0];
			$product_mtypes = $request_fields[1];
			$mtypes = array();
			foreach($product_mtypes as $mtype_id => $mtype) {
				if ($mtype['mtype'] == 0) {
					$mtypes[] = $mtype_id;
				}
			}
			foreach($product_fields as $pfield) {
				switch($pfield) {
					case 'qty':
						$initial[] = $_REQUEST['quantity'];
					break;
					case 'width':
						$initial[] = $_REQUEST['width'];
					break;
					case 'height':
						$initial[] = $_REQUEST['height'];
					break;
					case 'size':
						if ($size_attribute && isset($sattribute[$size_attribute])) {
							$initial[] = $sattribute[$size_attribute];
							unset($sattribute[$size_attribute]);
						}
					break;
					case 'pq-bw':
						if (isset($_REQUEST['page_quantity_'.$mtypes[0]])) {
							$initial[] = $_REQUEST['page_quantity_'.$mtypes[0]];
						}
					break;
					case 'pq-cl':
						if (isset($_REQUEST['page_quantity_'.$mtypes[1]])) {
							$initial[] = $_REQUEST['page_quantity_'.$mtypes[1]];
						}
					break;
					case 'addon':
						if (isset($_REQUEST['addon_products']) && $_REQUEST['addon_products']) {
							foreach($_REQUEST['addon_products'] as $aval) {
								$initial[] = $aval;
							}
						}
					break;
					case 'pspeed':
						if (isset($_REQUEST['production_speed'])) {
							$initial[] = $_REQUEST['production_speed'];
						}
					break;
					default:
						if (isset($sattribute[$pfield])) {
							$initial[] = $sattribute[$pfield];
						}
						if (isset($fattribute[$pfield])) {
							$initial[] = $fattribute[$pfield];
						}
					break;
				}
			}
		}
	}
	return $initial;
}
?>