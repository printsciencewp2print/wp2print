<?php
function print_products_production_view_get_orders($order, $pvpage, $orders_per_page) {
	global $wpdb, $current_user;

	$print_products_prodview_options = get_option("print_products_prodview_options");

	$limit_start = ($pvpage - 1) * $orders_per_page;

	$order_ids = array();
	$vendor_company_id = 0;
	if (print_products_vendor_is_vendor()) {
		$vendor_company = print_products_vendor_get_user_company($current_user->ID);
		if ($vendor_company) {
			if ($vendor_company['access'] != 1) {
				$vendor_company_id = $vendor_company['id'];
			}
		}
	}
	if (isset($_GET['_vendor_company']) && $_GET['_vendor_company']) {
		$vendor_company_id = (int)$_GET['_vendor_company'];
	}
	if (in_array('vendor', $current_user->roles)) {
		$vendor_company = print_products_vendor_get_user_company($current_user->ID);
		if ($vendor_company) {
			$vendor_company_id = $vendor_company['id'];
		}
	}

	if (isset($_GET['_vendor_employee']) && $_GET['_vendor_employee']) {
		$order_ids = array(0);
		$vendor_employee = (int)$_GET['_vendor_employee'];
		$order_items = $wpdb->get_results(sprintf("SELECT oi.order_id FROM %swoocommerce_order_items oi LEFT JOIN %swoocommerce_order_itemmeta oim ON oim.order_item_id = oi.order_item_id WHERE oim.meta_key = '_item_vendor_employee' AND oim.meta_value = '%s'", $wpdb->prefix, $wpdb->prefix, $_GET['_vendor_employee']));
		if ($order_items) {
			$order_ids = array();
			foreach($order_items as $order_item) {
				if (!in_array($order_item->order_id, $order_ids)) {
					$order_ids[] = $order_item->order_id;
				}
			}
		}
	}

	$include_statuses = false;
	if (isset($print_products_prodview_options['exclude_woo']) && $print_products_prodview_options['exclude_woo'] && is_array($print_products_prodview_options['exclude_woo'])) {
		$include_statuses = array();
		$wc_order_statuses = wc_get_order_statuses();
		foreach($wc_order_statuses as $os_key => $os_name) {
			if (!in_array($os_key, $print_products_prodview_options['exclude_woo'])) {
				$include_statuses[] = $os_key;
			}
		}
		if (!count($include_statuses)) { $include_statuses[] = 'nothing'; }
	}

	if (print_products_is_hpos_enabled()) {
		$where = "";
		if ($include_statuses) {
			if (strlen($where)) { $where .= " AND "; }
			$where .= "o.status IN ('".implode("','", $include_statuses)."')";
		}
		if ($vendor_company_id) {
			$vc_orders = $wpdb->get_results(sprintf("SELECT order_id FROM %swc_orders_meta WHERE meta_key = '_order_vendor' AND meta_value = '%s'", $wpdb->prefix, $vendor_company_id));
			if ($vc_orders) {
				foreach($vc_orders as $vc_order) {
					$order_ids[] = $vc_order->order_id;
				}
			} else {
				$order_ids = array(0);
			}
		}
		if ($order_ids && count($order_ids)) {
			if (strlen($where)) { $where .= " AND "; }
			$where .= "o.id IN (".implode(',', $order_ids).")";
		}
		if (isset($_GET['s']) && strlen($_GET['s'])) {
			$s = trim($_GET['s']);
			$sfields = array();
			if (is_numeric($s)) {
				$sfields[] = "o.id = '".$s."'";
			} else {
				$s = str_replace("'", "''", $s);
				$swords = explode(' ', $s);

				$sconditions = array();
				foreach($swords as $sword) {
					$sconditions[] = "oi.order_item_name LIKE '%".$sword."%'";
				}
				$sfields[] = "(".implode(' AND ', $sconditions).")";

				if (print_products_vendor_show_prodview_vendor_column()) {
					$order_ids = print_products_vendor_get_s_orders($_GET['s']);
					if ($order_ids && count($order_ids)) {
						$sfields[] = "o.id IN (".implode(',', $order_ids).")";
					}
				}
			}
			if (strlen($where)) { $where .= " AND "; }
			$where .= '('.implode(' OR ', $sfields).')';
		}
		if (strlen($where)) { $where = " WHERE ".$where; }

		return $wpdb->get_results(sprintf("SELECT SQL_CALC_FOUND_ROWS o.*, o.id as order_id FROM %swc_orders o LEFT JOIN %swoocommerce_order_items oi ON oi.order_id = o.id %s GROUP BY o.id ORDER BY o.id %s LIMIT %s, %s", $wpdb->prefix, $wpdb->prefix, $where, $order, $limit_start, $orders_per_page));
	} else {
		$where = "p.post_type = 'shop_order'";
		if ($include_statuses) {
			$where .= " AND p.post_status IN ('".implode("','", $include_statuses)."')";
		}
		if ($vendor_company_id) {
			$vc_orders = $wpdb->get_results(sprintf("SELECT post_id FROM %spostmeta WHERE meta_key = '_order_vendor' AND meta_value = '%s'", $wpdb->prefix, $vendor_company_id));
			if ($vc_orders) {
				foreach($vc_orders as $vc_order) {
					$order_ids[] = $vc_order->post_id;
				}
			} else {
				$order_ids = array(0);
			}
		}
		if ($order_ids && count($order_ids)) {
			$where .= " AND p.ID IN (".implode(',', $order_ids).")";
		}

		if (isset($_GET['s']) && strlen($_GET['s'])) {
			$s = trim($_GET['s']);
			$sfields = array();
			if (is_numeric($s)) {
				$sfields[] = "p.ID = '".$s."'";
			} else {
				$s = str_replace("'", "''", $s);
				$swords = explode(' ', $s);

				$sconditions = array();
				foreach($swords as $sword) {
					$sconditions[] = "oi.order_item_name LIKE '%".$sword."%'";
				}
				$sfields[] = "(".implode(' AND ', $sconditions).")";

				if (print_products_vendor_show_prodview_vendor_column()) {
					$order_ids = print_products_vendor_get_s_orders($_GET['s']);
					if ($order_ids && count($order_ids)) {
						$sfields[] = "p.ID IN (".implode(',', $order_ids).")";
					}
				}
			}
			$where .= ' AND ('.implode(' OR ', $sfields).')';
		}

		return $wpdb->get_results(sprintf("SELECT SQL_CALC_FOUND_ROWS p.*, p.ID as order_id FROM %sposts p LEFT JOIN %swoocommerce_order_items oi ON oi.order_id = p.ID WHERE %s GROUP BY p.ID ORDER BY p.ID %s LIMIT %s, %s", $wpdb->prefix, $wpdb->prefix, $where, $order, $limit_start, $orders_per_page));
	}
}

function print_products_production_view_network_get_orders($blog_id, $print_products_prodview_options) {
	global $wpdb, $current_user;
	$include_statuses = false;
	if ($print_products_prodview_options && isset($print_products_prodview_options['exclude_woo']) && is_array($print_products_prodview_options['exclude_woo'])) {
		$include_statuses = array();
		$wc_order_statuses = wc_get_order_statuses();
		foreach($wc_order_statuses as $os_key => $os_name) {
			if (!in_array($os_key, $print_products_prodview_options['exclude_woo'])) {
				$include_statuses[] = $os_key;
			}
		}
		if (!count($include_statuses)) { $include_statuses[] = 'nothing'; }
	}
	$order_ids = array();
	$vendor_company_id = 0;
	if (isset($_GET['_vendor_company']) && $_GET['_vendor_company']) {
		$vendor_company_id = (int)$_GET['_vendor_company'];
	}
	if ($vendor_company_id) {
		if (print_products_is_hpos_enabled()) {
			$vc_orders = $wpdb->get_results(sprintf("SELECT order_id FROM %swc_orders_meta WHERE meta_key = '_order_vendor' AND meta_value = '%s'", $wpdb->prefix, $vendor_company_id));
			if ($vc_orders) {
				foreach($vc_orders as $vc_order) {
					$order_ids[] = $vc_order->order_id;
				}
			} else {
				$order_ids = array(0);
			}
		} else {
			$vc_orders = $wpdb->get_results(sprintf("SELECT post_id FROM %spostmeta WHERE meta_key = '_order_vendor' AND meta_value = '%s'", $wpdb->prefix, $vendor_company_id));
			if ($vc_orders) {
				foreach($vc_orders as $vc_order) {
					$order_ids[] = $vc_order->post_id;
				}
			} else {
				$order_ids = array(0);
			}
		}
	}
	if (isset($_GET['_vendor_employee']) && $_GET['_vendor_employee']) {
		$order_ids = array(0);
		$vendor_employee = (int)$_GET['_vendor_employee'];
		$order_items = $wpdb->get_results(sprintf("SELECT oi.order_id FROM %swoocommerce_order_items oi LEFT JOIN %swoocommerce_order_itemmeta oim ON oim.order_item_id = oi.order_item_id WHERE oim.meta_key = '_item_vendor_employee' AND oim.meta_value = '%s'", $wpdb->prefix, $wpdb->prefix, $_GET['_vendor_employee']));
		if ($order_items) {
			$order_ids = array();
			foreach($order_items as $order_item) {
				if (!in_array($order_item->order_id, $order_ids)) {
					$order_ids[] = $order_item->order_id;
				}
			}
		}
	}
	if (print_products_is_hpos_enabled()) {
		$where = "";
		if ($include_statuses) {
			if (strlen($where)) { $where .= " AND "; }
			$where .= "o.status IN ('".implode("','", $include_statuses)."')";
		}
		if ($order_ids && count($order_ids)) {
			if (strlen($where)) { $where .= " AND "; }
			$where .= "o.id IN (".implode(',', $order_ids).")";
		}
		if (isset($_GET['s']) && strlen($_GET['s'])) {
			$s = trim($_GET['s']);
			$sfields = array();
			if (is_numeric($s)) {
				$sfields[] = "o.id = '".$s."'";
			} else {
				$s = str_replace("'", "''", $s);
				$swords = explode(' ', $s);

				$sconditions = array();
				foreach($swords as $sword) {
					$sconditions[] = "oi.order_item_name LIKE '%".$sword."%'";
				}
				$sfields[] = "(".implode(' AND ', $sconditions).")";

				if (print_products_vendor_show_prodview_vendor_column()) {
					$order_ids = print_products_vendor_get_s_orders($_GET['s']);
					if ($order_ids && count($order_ids)) {
						$sfields[] = "o.id IN (".implode(',', $order_ids).")";
					}
				}
			}
			if (strlen($where)) { $where .= " AND "; }
			$where .= '('.implode(' OR ', $sfields).')';
		}
		if (strlen($where)) { $where = " WHERE ".$where; }

		return sprintf("SELECT %s AS blog_id, o.*, o.id as order_id FROM %swc_orders o LEFT JOIN %swoocommerce_order_items oi ON oi.order_id = o.id %s GROUP BY o.id ORDER BY o.id DESC", $blog_id, $wpdb->get_blog_prefix($blog_id), $wpdb->get_blog_prefix($blog_id), $where);
	} else {
		$where = "p.post_type = 'shop_order'";
		if ($include_statuses) {
			$where .= " AND p.post_status IN ('".implode("','", $include_statuses)."')";
		}
		if ($order_ids && count($order_ids)) {
			$where .= " AND p.ID IN (".implode(',', $order_ids).")";
		}
		if (isset($_GET['s']) && strlen($_GET['s'])) {
			$s = trim($_GET['s']);
			$sfields = array();
			if (is_numeric($s)) {
				$sfields[] = "p.ID = '".$s."'";
			} else {
				$s = str_replace("'", "''", $s);
				$swords = explode(' ', $s);

				$sconditions = array();
				foreach($swords as $sword) {
					$sconditions[] = "oi.order_item_name LIKE '%".$sword."%'";
				}
				$sfields[] = "(".implode(' AND ', $sconditions).")";

				if (print_products_vendor_show_prodview_vendor_column()) {
					$order_ids = print_products_vendor_get_s_orders($_GET['s']);
					if ($order_ids && count($order_ids)) {
						$sfields[] = "p.ID IN (".implode(',', $order_ids).")";
					}
				}
			}
			$where .= ' AND ('.implode(' OR ', $sfields).')';
		}
		return sprintf("SELECT %s AS blog_id, p.*, p.ID as order_id FROM %sposts p LEFT JOIN %swoocommerce_order_items oi ON oi.order_id = p.ID WHERE %s GROUP BY p.ID ORDER BY p.post_date DESC", $blog_id, $wpdb->get_blog_prefix($blog_id), $wpdb->get_blog_prefix($blog_id), $where);
	}
}
?>