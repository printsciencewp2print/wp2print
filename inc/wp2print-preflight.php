<?php
add_action('wp_loaded', 'print_products_preflight_actions');
function print_products_preflight_actions() {
	if (isset($_REQUEST['preflight_process']) && $_REQUEST['preflight_process'] == 'true') {
		switch($_REQUEST['atype']) {
			case 'analize':
				print_products_preflight_analize();
			break;
			case 'customer-analize':
				$customer_analize = print_products_preflight_customer_analize();
				if ($customer_analize) {
					echo json_encode($customer_analize);
				}
			break;
		}
		exit;
	}
	if (isset($_POST['AjaxAction'])) {
		switch($_POST['AjaxAction']) {
			case 'preflight-profiles-action':
				$p_id = (int)$_POST['p_id'];
				$print_products_preflight_profiles = get_option('print_products_preflight_profiles');
				if (!is_array($print_products_preflight_profiles)) { $print_products_preflight_profiles = array(); }
				if ($p_id) {
					switch($_POST['p_atype']) {
						case 'add':
						case 'update':
							$p_fmismatch = (int)$_POST['p_fmismatch'];
							$p_ftrimbox = (int)$_POST['p_ftrimbox'];
							$p_def = (int)$_POST['p_def'];
							$print_products_preflight_profiles[$p_id] = array(
								'name' => trim($_POST['p_name']),
								'wlimit' => trim($_POST['p_wlimit']),
								'flimit' => trim($_POST['p_flimit']),
								'slimit' => trim($_POST['p_slimit']),
								'pcode' => trim($_POST['p_pcode']),
								'lang' => trim($_POST['p_lang']),
								'bleed' => trim($_POST['p_bleed']),
								'tolerance' => trim($_POST['p_tolerance']),
								'fmismatch' => $p_fmismatch,
								'ftrimbox' => $p_ftrimbox,
								'def' => $p_def
							);
							if ($p_def == 1) {
								foreach($print_products_preflight_profiles as $pp => $ppdata) {
									if ($pp != $p_id && $ppdata['def'] == 1) {
										$print_products_preflight_profiles[$pp]['def'] = 0;
									}
								}
							}
						break;
						case 'delete':
							unset($print_products_preflight_profiles[$p_id]);
						break;
					}
					update_option('print_products_preflight_profiles', $print_products_preflight_profiles);
				}
				exit;
			break;
			case 'preflight-rejection-notify':
				$order_id = (int)$_POST['order_id'];
				$item_id = (int)$_POST['item_id'];
				$fname = $_POST['fname'];
				$afile = $_POST['afile'];
				$freason = $_POST['freason'];
				$print_products_preflight_options = get_option('print_products_preflight_options');
				if ($print_products_preflight_options && isset($print_products_preflight_options['reject_email_subject']) && isset($print_products_preflight_options['reject_email_message']) && $print_products_preflight_options['reject_email_subject'] && $print_products_preflight_options['reject_email_message']) {
					$subject = $print_products_preflight_options['reject_email_subject'];
					$message = chr(10).chr(13).$print_products_preflight_options['reject_email_message'].chr(10).chr(13);

					$order = wc_get_order($order_id);
					$user_email = $order->get_billing_email();
					$upage_url = get_permalink(wc_get_page_id('myaccount')).'orders-missing-files/?view='.$order_id;

					$subject = str_replace('[ORDER_ID]', $order_id, $subject);
					$message = str_replace('[ORDER_ID]', $order_id, $message);
					$message = str_replace('[REJECTED_FILE_NAME]', $fname, $message);
					$message = str_replace('[UPLOAD_FILE_PAGE_URL]', '<a href="'.$upage_url.'">'.$upage_url.'</a>', $message);
					$message = str_replace('[FAILURE-REASON]', $freason, $message);

					print_products_send_wc_mail($user_email, $subject, $message);

					print_products_preflight_rejected_file($order_id, $item_id, $afile);
				}
				exit;
			break;
		}
	}
}

function print_products_preflight_rejected_file($order_id, $item_id, $afile) {
	global $wpdb;
	wc_update_order_item_meta($item_id, '_preflight_rejected', '1');
	$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
	if ($order_item_data) {
		$artwork_files = unserialize($order_item_data->artwork_files);
		$artwork_files_new = array();
		$artwork_rejected_files = array();
		$artwork_rejected = array();
		if ($order_item_data->artwork_rejected_files) {
			$artwork_rejected_files = unserialize($order_item_data->artwork_rejected_files);
		}
		if ($order_item_data->artwork_rejected) {
			$artwork_rejected = unserialize($order_item_data->artwork_rejected);
		}
		foreach ($artwork_files as $afkey => $artwork_file) {
			if ($artwork_file == $afile) {
				$artwork_rejected_files[] = $artwork_file;
				$artwork_rejected[] = $artwork_file;
			} else {
				$artwork_files_new[] = $artwork_file;
			}
		}
		$update = array();
		$update['artwork_files'] = serialize($artwork_files_new);
		$update['artwork_rejected_files'] = serialize($artwork_rejected_files);
		$update['artwork_rejected'] = serialize($artwork_rejected);
		$wpdb->update($wpdb->prefix.'print_products_order_items', $update, array('item_id' => $item_id));
	}
}

function print_products_preflight_update_rstatus($item_id) {
	global $wpdb;
	$_preflight_rejected = 0;

	$preflight_rejected = wc_get_order_item_meta($item_id, '_preflight_rejected', true);
	if ($preflight_rejected == '1') {
		$_preflight_rejected = 2;
	}

	$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
	if ($order_item_data) {
		if ($order_item_data->artwork_rejected) {
			$artwork_rejected = unserialize($order_item_data->artwork_rejected);
			if (count($artwork_rejected)) {
				$_preflight_rejected = 1;
			}
		}
	}
	wc_update_order_item_meta($item_id, '_preflight_rejected', $_preflight_rejected);
}

function print_products_preflight_customer_analize() {
	$product_id = $_REQUEST['product_id'];
	$afile = $_REQUEST['afile'];
	$size_term_id = $_REQUEST['size_term_id'];

	$dimension_unit = print_products_get_dimension_unit();
	$afile_url = print_products_get_amazon_file_url($afile);
	$product_type = print_products_get_type($product_id);
	$icc_profile_codes = print_products_preflight_get_profile_codes();
	$preflight_profiles = get_option('print_products_preflight_profiles');
	$preflight_options = get_post_meta($product_id, '_preflight_options', true);
	$small_font = 0; $missing_font = 1; $low_resolution = 0; $dimensions_incorrect = 0;
	$summary_fail_status = false;
	if ($preflight_profiles) {
		$pid = isset($preflight_options['profile']) ? $preflight_options['profile'] : 0;
		$preflight_profile = false;
		foreach ($preflight_profiles as $pfpid => $pprofile) {
			if (!$pid && $pprofile['def'] == 1) { $pid = $pfpid; }
			if ($pfpid == $pid) {
				$preflight_profile = $pprofile;
			}
		}
		if (!$preflight_profile) { $preflight_profile = current($preflight_profiles); }
		$size_mismatch_tolerance = isset($preflight_profile['tolerance']) ? (float)$preflight_profile['tolerance'] : 0;
		$fail_size_mismatch = isset($preflight_profile['fmismatch']) ? (int)$preflight_profile['fmismatch'] : 0;
		$fail_missing_trimbox = isset($preflight_profile['ftrimbox']) ? (int)$preflight_profile['ftrimbox'] : 0;
		$size_dimension = 0; $tribox_dimension = 0; $required_size_width = ''; $required_size_height = ''; $tb_width = ''; $tb_height = '';
		if ($size_term_id) {
			$dimensions = get_term_meta($size_term_id, '_dimensions', true);
			$dimensions_pts = get_term_meta($size_term_id, '_dimensions_pts', true);
			if ($dimensions) {
				$d_width = (isset($dimensions['width']) && $dimensions['width']) ? $dimensions['width'] : 0;
				$d_height = (isset($dimensions['height']) && $dimensions['height']) ? $dimensions['height'] : 0;
				if ($d_width > $d_height) {
					$required_size_width = $d_height;
					$required_size_height = $d_width;
				} else {
					$required_size_width = $d_width;
					$required_size_height = $d_height;
				}
			}
			if ($dimensions_pts) {
				$dw = (isset($dimensions_pts['width']) && $dimensions_pts['width']) ? $dimensions_pts['width'] : 0;
				$dh = (isset($dimensions_pts['height']) && $dimensions_pts['height']) ? $dimensions_pts['height'] : 0;
				if ($dw && $dh) {
					$size_dimension = $dw * $dh;
				}
			}
		}
		if ($product_type == 'area') {
			$awidth = $_REQUEST['width'];
			$aheight = $_REQUEST['height'];
			if ($awidth && $aheight) {
				$required_size_width = $awidth;
				$required_size_height = $aheight;
			}
		}

		$pfobject = new Preflight();
		$json_data = $pfobject->analize($afile_url, $pid);
		if ($json_data && is_array($json_data)) {
			//echo '<pre>'; var_dump($json_data); echo '</pre>'; exit;
			$analyze = $json_data['pages'];
			$p_width = ''; $p_height = '';
			$pages_bleedbox = 'TRUE'; $pages_bleedbox_nums = '';
			$pages_trimbox = 'TRUE'; $pages_trimbox_nums = ''; $first_trimbox_nums = '';
			$trimbox_defined = true;

			if ($json_data['status'] == 'FAIL') {
				$summary_fail_status = true;
			}

			foreach($analyze as $data) {
				if ($data['status'] == 'FAIL') { $summary_fail_status = true; }
				if (!$data['correct_trimbox']) { $pages_correctly = 'FALSE'; }
				if (isset($data['preflight_info']['bleedbox'])) {
					if ($pages_bleedbox_nums == '') {
						$pages_bleedbox_nums = $data['preflight_info']['bleedbox'];
					} else {
						if ($pages_bleedbox_nums != $data['preflight_info']['bleedbox']) {
							$pages_bleedbox = 'FALSE';
						}
					}
				}
				if (isset($data['preflight_info']['trimbox']) && $data['preflight_info']['trimbox']) {
					if ($pages_trimbox_nums == '') {
						$pages_trimbox_nums = $data['preflight_info']['trimbox'];
						$first_trimbox_nums = $data['preflight_info']['trimbox'];
					} else {
						if ($pages_trimbox_nums != $data['preflight_info']['trimbox']) {
							$pages_trimbox = 'FALSE';
						}
					}
					if ($fail_size_mismatch == 1 && $required_size_width && $required_size_height) {
						$tb_array = explode(' x ', $data['preflight_info']['trimbox']);
						$tb_width = (float)$tb_array[0];
						$tb_height = (float)$tb_array[1];
						$size_mismatch_x = $required_size_width - $tb_width;
						$size_mismatch_y = $required_size_height - $tb_height;

						if ($size_mismatch_tolerance) {
							if ($size_mismatch_x > $size_mismatch_tolerance || $size_mismatch_y > $size_mismatch_tolerance) {
								$summary_fail_status = true;
								$dimensions_incorrect = 1;
							}
						}
					}
				} else {
					$trimbox_defined = false;
				}
				if (isset($data['SmallFontDetected']) && $data['SmallFontDetected']) {
					$small_font = 1;
				}
				if (isset($data['embeddedfonts']) && is_array($data['embeddedfonts']) && count($data['embeddedfonts'])) {
					$missing_font = 0;
				}
				if (isset($data['preview']) && $data['preview'] && is_array($data['preview']) && count($data['preview'])) {
					$low_resolution = 1;
				}
			}
			if ($pages_trimbox == 'TRUE' && strlen($first_trimbox_nums)) {
				$tb_array = explode(' x ', $first_trimbox_nums);
				$p_width = $tb_array[0];
				$p_height = $tb_array[1];
				$tribox_dimension = (float)$p_width * (float)$p_height;
			}
			if ($tribox_dimension && $size_dimension && $size_mismatch_tolerance && $fail_size_mismatch == 1) {
				if ($tribox_dimension > $size_dimension) {
					if (abs($tribox_dimension - $size_dimension) > $size_mismatch_tolerance) {
						$summary_fail_status = true;
						$dimensions_incorrect = 1;
					}
				}
			}

			if ($fail_missing_trimbox == 1 && !$trimbox_defined) {
				$summary_fail_status = true;
			}

			$required_dimensions = '';
			$uploaded_dimensions = '';
			if ($required_size_width && $required_size_height && $tb_width && $tb_height) {
				$required_dimensions = $required_size_width.$dimension_unit.' x '.$required_size_height.$dimension_unit;
				$uploaded_dimensions = $tb_width.$dimension_unit.' x '.$tb_height.$dimension_unit;
				if ($tb_width != $required_size_width || $tb_height != $required_size_height) {
					$dimensions_incorrect = 1;
					$summary_fail_status = true;
				}
			}
			$status = 'PASS';
			if ($summary_fail_status) {
				$status = 'FAIL';
			}
			return array('status' => $status, 'small_font' => $small_font, 'missing_font' => $missing_font, 'low_resolution' => $low_resolution, 'required_dimensions' => $required_dimensions, 'uploaded_dimensions' => $uploaded_dimensions, 'dimensions_incorrect' => $dimensions_incorrect);
		}
	}
	return false;
}

function print_products_preflight_analize() {
	global $wpdb, $print_products_settings;
	$dimension_unit = print_products_get_dimension_unit();
	$icc_profile_codes = print_products_preflight_get_profile_codes();
	$preflight_profiles = get_option('print_products_preflight_profiles');
	$wait_icon = PRINT_PRODUCTS_PLUGIN_URL.'images/icons/wait.gif';
	$pid = (int)$_REQUEST['pid'];
	$fname = $_REQUEST['fname'];
	$afile = $_REQUEST['afile'];
	$atp = $_REQUEST['atp'];
	$item_id = $_REQUEST['item_id'];
	$order_id = $_REQUEST['order_id'];
	if ($pid && strlen($afile)) {
		$preflight_profile = $preflight_profiles[$pid];
		$size_mismatch_tolerance = isset($preflight_profile['tolerance']) ? (float)$preflight_profile['tolerance'] : 0;
		$fail_size_mismatch = isset($preflight_profile['fmismatch']) ? (int)$preflight_profile['fmismatch'] : 0;
		$fail_missing_trimbox = isset($preflight_profile['ftrimbox']) ? (int)$preflight_profile['ftrimbox'] : 0;
		$size_dimension = 0; $tribox_dimension = 0; $required_size = ''; $required_size_width = ''; $required_size_height = ''; $tb_width = ''; $tb_height = '';

		if (isset($print_products_settings['size_attribute']) && $print_products_settings['size_attribute']) {
			$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
			if ($order_item_data) {
				$product_type = $order_item_data->product_type;
				$additional = unserialize($order_item_data->additional);
				$size_attribute = $print_products_settings['size_attribute'];
				$product_attributes = unserialize($order_item_data->product_attributes);
				if ($product_attributes) {
					foreach($product_attributes as $product_attribute) {
						$aarray = explode(':', $product_attribute);
						if ($aarray[0] == $size_attribute && !$size_term_id) {
							$size_term_id = $aarray[1];
							$dimensions = get_term_meta($size_term_id, '_dimensions', true);
							$dimensions_pts = get_term_meta($size_term_id, '_dimensions_pts', true);
							if ($dimensions) {
								$d_width = (isset($dimensions['width']) && $dimensions['width']) ? $dimensions['width'] : 0;
								$d_height = (isset($dimensions['height']) && $dimensions['height']) ? $dimensions['height'] : 0;
								if ($d_width > $d_height) {
									$required_size_width = $d_height;
									$required_size_height = $d_width;
								} else {
									$required_size_width = $d_width;
									$required_size_height = $d_height;
								}
							}
							if ($dimensions_pts) {
								$dw = (isset($dimensions_pts['width']) && $dimensions_pts['width']) ? $dimensions_pts['width'] : 0;
								$dh = (isset($dimensions_pts['height']) && $dimensions_pts['height']) ? $dimensions_pts['height'] : 0;
								if ($dw && $dh) {
									$size_dimension = $dw * $dh;
								}
							}
						}
					}
				}
				if ($product_type == 'area') {
					if (isset($additional['width']) && $additional['width'] && isset($additional['height']) && $additional['height']) {
						$required_size_width = $additional['height'];
						$required_size_height = $additional['width'];
					}
				}
			}
		}

		if ($required_size_width && $required_size_height) {
			$required_size = $required_size_width.' x '.$required_size_height.' '.$dimension_unit;
		}

		if ($atp == 2) {
			$afile_url = $afile;
		} else {
			$afile_url = print_products_get_amazon_file_url($afile);
		}

		$pfobject = new Preflight();
		$json_data = $pfobject->analize($afile_url, $pid);
		if ($json_data && is_array($json_data)) {
			//echo '<pre>'; var_dump($json_data); echo '</pre>';
			$analyze = $json_data['pages'];
			$p_width = ''; $p_height = ''; $tbox = 'FALSE';
			$is_warning = false; $is_all_warning = true;
			$pages_bleedbox = 'TRUE'; $pages_bleedbox_nums = '';
			$pages_trimbox = 'TRUE'; $pages_trimbox_nums = ''; $first_trimbox_nums = '';
			$trimbox_defined = true;
			$pages_correctly = 'TRUE';
			$summary = array('pagecount' => $json_data['pagescount'], 'status' => 'PASS', 'statusimg' => 'icon-yes.svg', 'preflight_info' => '', 'colorspaces' => '');

			$summary_fail_status = false;
			if ($json_data['status'] == 'FAIL') {
				$summary_fail_status = true;
			}

			foreach($analyze as $data) {
				if (isset($data['preflight_info']) && $data['preflight_info'] && !$summary['preflight_info']) { $summary['preflight_info'] = $data['preflight_info']; $summary['colorspaces'] = $data['colorspaces']; }
				if ($data['status'] == 'FAIL') { $summary_fail_status = true; }
				if (isset($data['warnings']) && $data['warnings'] && is_array($data['warnings']) && count($data['warnings'])) { $is_warning = true; } else { $is_all_warning = false; }
				if (!$data['correct_trimbox']) { $pages_correctly = 'FALSE'; }
				if (isset($data['preflight_info']['bleedbox'])) {
					if ($pages_bleedbox_nums == '') {
						$pages_bleedbox_nums = $data['preflight_info']['bleedbox'];
					} else {
						if ($pages_bleedbox_nums != $data['preflight_info']['bleedbox']) {
							$pages_bleedbox = 'FALSE';
						}
					}
				}
				if (isset($data['preflight_info']['trimbox']) && $data['preflight_info']['trimbox']) {
					if ($pages_trimbox_nums == '') {
						$pages_trimbox_nums = $data['preflight_info']['trimbox'];
						$first_trimbox_nums = $data['preflight_info']['trimbox'];
					} else {
						if ($pages_trimbox_nums != $data['preflight_info']['trimbox']) {
							$pages_trimbox = 'FALSE';
						}
					}
					if ($fail_size_mismatch == 1 && $required_size_width && $required_size_height) {
						$tb_array = explode(' x ', $data['preflight_info']['trimbox']);
						$tb_width = (float)$tb_array[0];
						$tb_height = (float)$tb_array[1];
						$size_mismatch_x = $required_size_width - $tb_width;
						$size_mismatch_y = $required_size_height - $tb_height;

						if ($size_mismatch_tolerance) {
							if ($size_mismatch_x > $size_mismatch_tolerance || $size_mismatch_y > $size_mismatch_tolerance) {
								$summary_fail_status = true;
							}
						}
					}
				} else {
					$trimbox_defined = false;
				}
			}
			if (!$is_warning) { $is_all_warning = false; }
			if ($pages_trimbox == 'TRUE' && $pages_correctly == 'TRUE') { $tbox = 'TRUE'; }
			if ($pages_trimbox == 'TRUE' && strlen($first_trimbox_nums)) {
				$tb_array = explode(' x ', $first_trimbox_nums);
				$p_width = $tb_array[0];
				$p_height = $tb_array[1];
				$tribox_dimension = (float)$p_width * (float)$p_height;
			}
			if ($tribox_dimension && $size_dimension && $size_mismatch_tolerance && $fail_size_mismatch == 1) {
				if ($tribox_dimension > $size_dimension) {
					if (abs($tribox_dimension - $size_dimension) > $size_mismatch_tolerance) {
						$summary_fail_status = true;
					}
				}
			}

			if ($fail_missing_trimbox == 1 && !$trimbox_defined) {
				$summary_fail_status = true;
			}

			if ($required_size_width && $required_size_height && $tb_width && $tb_height) {
				if ($tb_width != $required_size_width || $tb_height != $required_size_height) {
					$summary_fail_status = true;
				}
			}

			if ($summary_fail_status) {
				$summary['status'] = 'FAIL';
				$summary['statusimg'] = 'icon-no.svg';
			}

			$summary['preflight_info']['pages_bleedbox'] = $pages_bleedbox;
			$summary['preflight_info']['pages_trimbox'] = $pages_trimbox;
			$summary['preflight_info']['pages_correctly'] = $pages_correctly;
			$preflight_profiles = get_option('print_products_preflight_profiles');
			?>
			<div class="pf-results">
				<table class="pf-rtop">
					<tr>
						<td><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL . 'images/' . $summary['statusimg']; ?>" alt="" style="width:22px;margin-bottom:-4px;"></td>
						<td><?php echo $summary['status']; ?></td>
						<td> - </td>
						<td><?php echo $fname; ?></td>
					</tr>
				</table>
				<h3><?php _e('Summary preflight result', 'wp2print'); ?></h3>
				<p><?php _e('Analysis result', 'wp2print'); ?>: <span><?php echo $summary['status']; ?></span></p>
				<?php if (isset($summary['preflight_info']) && $summary['preflight_info']) { ?>
					<?php if (isset($summary['colorspaces']) && $summary['colorspaces'] && is_array($summary['colorspaces'])) { ?><p><?php _e('Colorspaces', 'wp2print'); ?>: <span><?php echo implode(', ', $summary['colorspaces']); ?></span></p><?php } ?>
					<?php if (isset($summary['preflight_info']['maxcmykdensity']) && $summary['preflight_info']['maxcmykdensity']) { ?><p><?php _e('MaxCMYK density', 'wp2print'); ?>: <span><?php echo $summary['preflight_info']['maxcmykdensity']; ?></span></p><?php } ?>
					<p><?php _e('Dimensions of bleed box same for all pages', 'wp2print'); ?>: <span><?php echo $summary['preflight_info']['pages_bleedbox']; ?></span></p>
					<p><?php _e('Dimensions of trim box same for all pages', 'wp2print'); ?>: <span><?php echo $summary['preflight_info']['pages_trimbox']; ?></span></p>
					<p><?php _e('All pages contain correctly defined trim box', 'wp2print'); ?>: <span><?php echo $summary['preflight_info']['pages_correctly']; ?></span></p>
					<?php if (isset($summary['preflight_info']['font']) && $summary['preflight_info']['font']) { ?>
						<p><?php _e('Font list', 'wp2print'); ?>:</p>
						<?php foreach ($summary['preflight_info']['font'] as $font) { ?>
							<p><span><?php echo $font; ?></span></p>
						<?php } ?>
					<?php } ?>
				<?php } ?>

				<h3><?php _e('Detailed preflight result', 'wp2print'); ?></h3>
				<?php foreach($analyze as $pnum => $data) {
					$status = $data['status']; $status_img = 'icon-yes.svg'; $dimensions_incorrect = 0;
					if ($status == 'FAIL') {
						$status_img = 'icon-no.svg';
					} else if ($data['warnings'] && is_array($data['warnings']) && count($data['warnings'])) {
						$status = 'WARNING'; $status_img = 'icon-warn.svg';
					}
					if (isset($data['preview']) && $data['preview'] && is_array($data['preview']) && $data['errors'] && is_array($data['errors'])) {
						$status = 'FAIL'; $status_img = 'icon-no.svg';
					}
					$size_mismatch = array();
					if ($fail_size_mismatch == 1 && $required_size_width && $required_size_height) {
						if (isset($data['preflight_info']['trimbox']) && $data['preflight_info']['trimbox']) {
							$tb_array = explode(' x ', $data['preflight_info']['trimbox']);
							$tb_width = (float)$tb_array[0];
							$tb_height = (float)$tb_array[1];
							$size_mismatch_x = $required_size_width - $tb_width;
							$size_mismatch_y = $required_size_height - $tb_height;
							$tb_dimension = $tb_width * $tb_height;

							if ($size_mismatch_tolerance) {
								if ($size_mismatch_x > $size_mismatch_tolerance) {
									$size_mismatch['x'] = $size_mismatch_x;
									$dimensions_incorrect = 1;
								}
								if ($size_mismatch_y > $size_mismatch_tolerance) {
									$size_mismatch['y'] = $size_mismatch_y;
									$dimensions_incorrect = 1;
								}
								if ($tb_dimension && $size_dimension) {
									if ($tb_dimension > $size_dimension) {
										if (abs($tb_dimension - $size_dimension) > $size_mismatch_tolerance) {
											$dimensions_incorrect = 1;
										}
									}
								}
							} else {
								$size_mismatch['x'] = $size_mismatch_x;
								$size_mismatch['y'] = $size_mismatch_y;
							}
						}
					}
					if ($dimensions_incorrect == 1) {
						$status = 'FAIL'; $status_img = 'icon-no.svg';
					}
					?>
					<div class="pf-page">
						<p><?php _e('Page', 'wp2print'); ?>: <span><?php echo $pnum + 1; ?></span></p>
						<p><?php _e('Page-level analysis result', 'wp2print'); ?>: <img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/<?php echo $status_img; ?>" alt="" style="width:18px;margin-bottom:-4px;"> <span><?php echo $status; ?></span></p>
						<?php if (isset($data['preflight_info']) && $data['preflight_info']) { ?>
							<?php if (isset($data['preflight_info']['bleedbox']) && $data['preflight_info']['bleedbox']) { ?><p><?php _e('Dimensions bleed box', 'wp2print'); ?>: <span><?php echo $data['preflight_info']['bleedbox']; ?> <?php echo $dimension_unit; ?></span></p><?php } ?>
							<?php if (isset($data['preflight_info']['mediabox']) && $data['preflight_info']['mediabox']) { ?><p><?php _e('Dimensions media box', 'wp2print'); ?>: <span><?php echo $data['preflight_info']['mediabox']; ?> <?php echo $dimension_unit; ?></span></p><?php } ?>
							<?php if (isset($data['preflight_info']['trimbox']) && $data['preflight_info']['trimbox']) { ?><p><?php _e('Dimensions trim box', 'wp2print'); ?>: <span><?php echo $data['preflight_info']['trimbox']; ?> <?php echo $dimension_unit; ?></span></p><?php } ?>
						<?php } ?>
						<?php if ($fail_size_mismatch == 1 && $required_size && $size_mismatch) { ?>
							<p><?php _e('Required size', 'wp2print'); ?>: <span><?php echo $required_size; ?></span></p>
							<?php if ($size_mismatch_tolerance) { ?><p><?php _e('Size mismatch tolerance', 'wp2print'); ?>: <span><?php echo $size_mismatch_tolerance.' '.$dimension_unit; ?></span></p><?php } ?>
							<?php if (isset($size_mismatch['x']) && $size_mismatch['x']) { ?><p><?php _e('Size mismatch X', 'wp2print'); ?>: <span><?php echo $size_mismatch['x'].' '.$dimension_unit; ?></span></p><?php } ?>
							<?php if (isset($size_mismatch['y']) && $size_mismatch['y']) { ?><p><?php _e('Size mismatch Y', 'wp2print'); ?>: <span><?php echo $size_mismatch['y'].' '.$dimension_unit; ?></span></p><?php } ?>
							<p><?php _e('Dimension match', 'wp2print'); ?>: <span><?php if ($dimensions_incorrect == 1) { echo 'FALSE'; } else { echo 'TRUE'; } ?></span></p>
						<?php } ?>
						<p><?php _e('Page contains trim box', 'wp2print'); ?>: <span><?php if (isset($data['correct_trimbox']) && $data['correct_trimbox']) { echo 'TRUE'; } else { echo 'FALSE'; } ?></span></p>
						<?php if (isset($data['notembeddedfonts']) && $data['notembeddedfonts'] && is_array($data['notembeddedfonts']) && count($data['notembeddedfonts'])) { ?>
							<p><?php _e('Not Embedded Fonts', 'wp2print'); ?>:<br><span><?php echo implode('</span><br><span>', $data['notembeddedfonts']); ?></span></p>
						<?php } ?>
						<?php if (isset($data['SmallFontDetected']) && $data['SmallFontDetected']) { ?>
							<p><?php _e('Small Font', 'wp2print'); ?>: <span><?php echo sprintf(__('Detected font smaller than %spt', 'wp2print'), $preflight_profile['slimit']); ?></span></p>
						<?php } ?>
						<?php if (isset($data['preview']) && $data['preview'] && is_array($data['preview']) && count($data['preview'])) { ?>
							<p><?php _e('Low-resolution images', 'wp2print'); ?>:</p>
							<?php foreach ($data['preview'] as $inm => $lrimage) { ?>
								<p><?php _e('Image', 'wp2print'); ?><?php echo $inm + 1; ?> : (hdpi: <?php echo $lrimage['hdpi']; ?>, vdpi: <?php echo $lrimage['vdpi']; ?>)</p>
							<?php } ?>
						<?php } ?>
						<?php if (isset($data['warnings']) && is_array($data['warnings']) && count($data['warnings'])) { ?>
							<p><?php _e('Warnings', 'wp2print'); ?>: <span><?php echo implode(', ', $data['warnings']); ?></span></p>
						<?php } ?>
						<?php if (isset($data['pagethumb']) && strlen($data['pagethumb'])) { ?>
							<p><img src="<?php echo $wait_icon; ?>" alt="" style="max-width:300px;" data-thumb="<?php echo $data['pagethumb']; ?>" id="<?php echo md5($data['pagethumb']); ?>" class="spinning-icon"></p>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<?php
		} else {
			if ($json_data) {
				echo '<p>'.$json_data.'</p>';
			} else {
				echo '<p>'.__('Preflight analizing error.', 'wp2print').'</p>';
			}
		}
	} else {
		echo '<p>'.__('Incorrect data.', 'wp2print').'</p>';
	}
}

// order edit page
add_action('add_meta_boxes', 'print_products_preflight_add_meta_boxes', 11);
function print_products_preflight_add_meta_boxes() {
	add_meta_box('order-preflight-box', __('Preflight Analysis', 'wp2print'), 'print_products_preflight_meta_box', print_products_woocommerce_get_orders_screen(), 'normal');
}

function print_products_preflight_meta_box($post) {
	global $current_user, $wpdb;
	$order_id = print_products_woocommerce_get_order_id($post);
	$order_data = wc_get_order($order_id);
	$order_items = $order_data->get_items('line_item');
	if ($order_items) {
		$preflight_items = array();
		foreach($order_items as $item_id => $item) {
			$item_name = wp_kses_post($item->get_name());
			$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
			if ($order_item_data) {
				$artwork_files = unserialize($order_item_data->artwork_files);
				if ($artwork_files) {
					foreach($artwork_files as $af_key => $artwork_file) {
						if (substr(basename($artwork_file), -3) == 'pdf') {
							$preflight_items[] = array('item_id' => $item_id, 'type' => '1', 'name' => $item_name, 'filetype' => 'pdf', 'file' => basename($artwork_file), 'fileurl' => $artwork_file);
						} else {
							$preflight_items[] = array('item_id' => $item_id, 'type' => '1', 'name' => $item_name, 'filetype' => 'image', 'file' => basename($artwork_file), 'fileurl' => $artwork_file);
						}
					}
				}
			}
			$pdf_link = wc_get_order_item_meta($item_id, '_pdf_link', true);
			if (strlen($pdf_link)) {
				$pdf_links = explode(',', $pdf_link);
				foreach($pdf_links as $pdf_link) {
					$preflight_items[] = array('item_id' => $item_id, 'type' => '2', 'name' => $item_name, 'filetype' => 'pdf', 'file' => basename($pdf_link), 'fileurl' => $pdf_link);
				}
			}
			if (function_exists('printess_get_item_pdf_files')) {
				$pdf_links = printess_get_item_pdf_files($item);
				if ($pdf_links) {
					foreach($pdf_links as $pdf_link) {
						$preflight_items[] = array('item_id' => $item_id, 'type' => '2', 'name' => $item_name, 'filetype' => 'pdf', 'file' => basename($pdf_link), 'fileurl' => $pdf_link);
					}
				}
			}
		}
		if (count($preflight_items)) {
			$print_products_preflight_profiles = get_option("print_products_preflight_profiles");
			?>
			<div class="ppv-area preflight-wrap">
				<p style="margin:0 0 8px; font-weight:700;"><?php _e('Please select preflight profile', 'wp2print'); ?>:</p>
				<div class="p-profiles" style="margin-bottom:15px;" data-error="<?php _e('Please select preflight profile', 'wp2print'); ?>.">
					<select name="p_profile" style="width:auto; max-width:100%;">
						<option value=""><?php _e('Select profile', 'wp2print'); ?></option>
						<?php if ($print_products_preflight_profiles) { ?>
							<?php foreach ($print_products_preflight_profiles as $pid => $pprofile) { ?>
								<option value="<?php echo $pid; ?>"<?php if ($pprofile['def'] == 1) { echo ' SELECTED'; } ?>><?php echo $pprofile['name']; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
				<table class="woocommerce_order_items preflight-files-list">
					<thead>
						<tr>
							<th><?php _e('Item', 'wp2print'); ?></th>
							<th><?php _e('File', 'wp2print'); ?></th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($preflight_items as $fnum => $preflight_item) { ?>
							<tr class="preflight-item-<?php echo $preflight_item['item_id']; ?>-<?php echo $fnum; ?>" data-order-id="<?php echo $order_id; ?>" data-file="<?php echo $preflight_item['file']; ?>" data-fileurl="<?php echo $preflight_item['fileurl']; ?>">
								<td><?php echo $preflight_item['name']; ?></td>
								<td><a href="<?php if ($preflight_item['type'] == '1') { echo print_products_get_amazon_file_url($preflight_item['fileurl']); } else { echo $preflight_item['fileurl']; } ?>" target="_blank"><?php echo $preflight_item['file']; ?></a></td>
								<?php if ($preflight_item['filetype'] == 'pdf') { ?>
									<td><input type="button" class="button button-primary" value="<?php _e('Analyze', 'wp2print'); ?>" onclick="wp2print_preflight_analize(this, '<?php echo $preflight_item['file']; ?>', '<?php echo $preflight_item['fileurl']; ?>', '<?php echo $preflight_item['type']; ?>', <?php echo $preflight_item['item_id']; ?>, <?php echo $order_id; ?>);"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="a-loading" style="margin:5px; display:none;"></td>
								<?php } else { ?>
									<td></td>
								<?php } ?>
								<td style="padding-right:5px;"><input type="button" class="button button-primary button-notify" value="<?php _e('Reject artwork', 'wp2print'); ?>" onclick="wp2print_preflight_reject_init(<?php echo $preflight_item['item_id']; ?>, <?php echo $fnum; ?>);"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="pr-loading" style="margin:5px; display:none;"></td>
								<td><span class="pp-rejected" style="color:#009900; display:none;"><?php _e('Rejection notification was successfully sent.', 'wp2print'); ?></span></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<div class="preflight-process">
					<div class="pp-result"></div>
				</div>
			</div>
			<?php
		} else {
			echo '<p>'.__('No uploaded files.', 'wp2print').'</p>';
		}
	}
}

// admin page
function print_products_preflight_admin_page() {
	include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-preflight.php';
}

function print_products_preflight_popup_html() {
	?>
	<div class="pra-popup-hidden" style="display:none;">
		<div id="pra-popup" class="pra-popup" data-item-id="" data-file-num="">
			<h2><?php _e('Reason for failure', 'wp2print'); ?></h2>
			<textarea name="pra_reason" class="pra-reason" style="width:600px;height:150px;"></textarea>
			<div style="margin-top:10px;"><input type="button" value="<?php _e('Submit', 'wp2print'); ?>" class="button-primary" onclick="wp2print_preflight_reject_artwork()"></div>
		</div>
	</div>
	<?php
}

class Preflight {
	public $profile;
	public $profiles;
	public $dimunit;
	public $session_id;
	public $fields;
	public $api_url;
	public $error;

    public function __construct() {
		$this->profiles = get_option('print_products_preflight_profiles');
        $this->dimunit = get_option('woocommerce_dimension_unit');
		$this->session_id = MD5(time());
		$this->api_url = 'https://xcmg073av7.execute-api.us-east-2.amazonaws.com/v3/pdfinfo';
    }

	public function analize($afile_url, $pid) {
		$this->profile = $this->profiles[$pid];
		$this->fields = array(
			'sessionId' => $this->session_id,
			'pdf_url' => $afile_url,
			'min_dpi' => $this->profile['flimit'],
			'units' => $this->dimunit,
			'outputbucket' => 'pdfpreflight',
			'MinimumFontSize' => $this->profile['slimit']
		);
		$this->request();
		return $this->results();
	}

    public function request() {
		$data_string = json_encode($this->fields);
		set_time_limit (3600);
		$ch = curl_init($this->api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string)));
		$result = curl_exec($ch);
		curl_close($ch);
		if ($result) {
			$result = json_decode($result, true);
			if (is_array($result)) {
				if (isset($result['errorMessage']) && $result['errorMessage']) {
					$this->error = $result['errorMessage'];
				} else if (isset($result['message']) && $result['message']) {
					$this->error = $result['message'];
				}
			}
		}
	}

	public function get_json_data() {
		$upload_dir = wp_upload_dir();
		$json_url = 'https://pdfpreflight.s3.amazonaws.com/result/'.$this->session_id.'.json';

		$ch = curl_init($json_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$json_string = curl_exec($ch);
		curl_close($ch);

		if ($json_string && strlen($json_string)) {
			return json_decode($json_string);
		}
		return false;
	}

	public function results() {
		$results = array();
		$json_data = $this->get_json_data();
		if ($json_data) {
			if (isset($json_data->Error)) {
				return $json_data->Error;
			} else {
				$results['pagescount'] = $json_data->PageCount;
				$results['status'] = $this->summary_status($json_data);
				$results['pages'] = array();
				foreach($json_data->Pages as $pdata) {
					$results['pages'][] = array(
						'status' => $this->status($pdata),
						'pagethumb' => $pdata->PageThumbnailURL,
						'preflight_info' => $this->preflight_info($pdata->PreflightInfo),
						'embeddedfonts' => $pdata->EmbeddedFonts,
						'notembeddedfonts' => $pdata->NotEmbeddedFonts,
						'warnings' => $pdata->PreflightWarnings,
						'errors' => $pdata->PreflightErrors,
						'colorspaces' => $pdata->Colorspaces,
						'preview' => $this->images($pdata->LowResImagesList),
						'correct_trimbox' => $pdata->CorrectTrimBox,
						'ColorspaceOnlyCMYK' => $pdata->ColorspaceOnlyCMYK,
						'ContainsTransparency' => $pdata->ContainsTransparency,
						'AllFontsEmbedded' => $pdata->AllFontsEmbedded,
						'ContainsLowResImages' => $pdata->ContainsLowResImages,
						'SmallFontDetected' => $pdata->SmallFontDetected
					);
				}
			}
		} else {
			$results = $this->error;
		}
		return $results;
	}

	public function preflight_info($iarray) {
		$info = false;
		if (is_array($iarray)) {
			$info = array();
			foreach($iarray as $aval) {
				$avarray = explode(':', $aval);
				$nums = trim($avarray[1]);
				$anums = explode(' x ', $nums);
				$info[$avarray[0]] = $this->convert($anums[0]).' x '.$this->convert($anums[1]);
			}
		}
		return $info;
	}

	public function images($iarray) {
		$images = false;
		if (is_array($iarray)) {
			$images = array();
			foreach($iarray as $aobj) {
				if (isset($aobj->url) && strlen($aobj->url)) {
					$images[] = array('url' => $aobj->url, 'hdpi' => $aobj->hdpi, 'vdpi' => $aobj->vdpi);
				}
			}
		}
		return $images;
	}

	public function summary_status($pdata) {
		$status = 'FAIL';
		if (!$pdata->DocumentPreflightErrors) {
			$status = 'PASS';
		}
		return $status;
	}

	public function status($pdata) {
		$status = 'FAIL';
		if (!$pdata->PreflightErrors) {
			$status = 'PASS';
		}
		return $status;
	}

	public function convert($pnum) {
		$rdec = 2;
		$pnum = (float)$pnum;
		switch ($this->dimunit) {
			case 'in':
				$pnum = $pnum * 0.0138889;
			break;
			case 'ft':
				$pnum = $pnum * 0.00115741;
			break;
			case 'yd':
				$pnum = $pnum * 0.000385802;
			break;
			case 'm':
				$pnum = $pnum * 0.000352778;
			break;
			case 'cm':
				$pnum = $pnum * 0.0352778;
			break;
			case 'mm':
				$pnum = $pnum * 0.352778;
				$rdec = 1;
			break;
		}
		return round($pnum, $rdec);
	}

	public function convert_to_pt($pnum) {
		$pnum = (float)$pnum;
		switch ($this->dimunit) {
			case 'in':
				$pnum = $pnum / 0.0138889;
			break;
			case 'ft':
				$pnum = $pnum / 0.00115741;
			break;
			case 'yd':
				$pnum = $pnum / 0.000385802;
			break;
			case 'm':
				$pnum = $pnum / 0.000352778;
			break;
			case 'cm':
				$pnum = $pnum / 0.0352778;
			break;
			case 'mm':
				$pnum = $pnum / 0.352778;
			break;
		}
		return round($pnum, 1);
	}
}

function print_products_preflight_get_profile_codes() {
	return array(
		18 => 'PDF/X3 - CoatedFOGRA27',
		19 => 'PDF/X3 - CoatedFOGRA39',
		20 => 'PDF/X3 - CoatedGRACoL2006',
		21 => 'PDF/X3 - JapanColor2001Coated',
		22 => 'PDF/X3 - JapanColor2001Uncoated',
		23 => 'PDF/X3 - JapanColor2002Newspaper',
		24 => 'PDF/X3 - JapanColor2003WebCoated',
		25 => 'PDF/X3 - JapanWebCoated',
		26 => 'PDF/X3 - UncoatedFOGRA29',
		27 => 'PDF/X3 - USWebCoatedSWOP',
		28 => 'PDF/X3 - USWebUncoated',
		29 => 'PDF/X3 - WebCoatedFOGRA28',
		30 => 'PDF/X3 - WebCoatedSWOP2006Grade3',
		31 => 'PDF/X3 - WebCoatedSWOP2006Grade5',
		32 => 'PDF/X1 - CoatedFOGRA27',
		33 => 'PDF/X1 - CoatedFOGRA39',
		34 => 'PDF/X1 - CoatedGRACoL2006',
		35 => 'PDF/X1 - JapanColor2001Coated',
		36 => 'PDF/X1 - JapanColor2001Uncoated',
		37 => 'PDF/X1 - JapanColor2002Newspaper',
		38 => 'PDF/X1 - JapanColor2003WebCoated',
		39 => 'PDF/X1 - JapanWebCoated',
		40 => 'PDF/X1 - UncoatedFOGRA29',
		41 => 'PDF/X1 - USWebCoatedSWOP',
		42 => 'PDF/X1 - USWebUncoated',
		43 => 'PDF/X1 - WebCoatedFOGRA28',
		44 => 'PDF/X1 - WebCoatedSWOP2006Grade3',
		45 => 'PDF/X1 - WebCoatedSWOP2006Grade5'
	);
}
?>