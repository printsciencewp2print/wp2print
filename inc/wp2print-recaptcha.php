<?php
define('GOOGLE_RECAPTCHA_SITE_KEY', '6Lf2jMwqAAAAAFGMqYSYL0JRXmax2R--baIHiu24');
define('GOOGLE_RECAPTCHA_PROJECT_ID', 'woven-sequence-449319-q3');

$wp2print_recaptcha_options = array();
add_action('wp_loaded', 'print_products_recaptcha_init', 9);
function print_products_recaptcha_init() {
	global $pagenow, $wp2print_recaptcha_options;
	$wp2print_recaptcha_options = get_option('print_products_recaptcha_options');
	if ($pagenow == 'wp-login.php' && print_products_recaptcha_is_active()) {
		print_products_recaptcha_wp_enqueue_scripts();
		wp_register_script('wp2print', PRINT_PRODUCTS_PLUGIN_URL . 'js/wp2print.js', array('jquery'), '1.0.1', true);
		wp_enqueue_script('wp2print');
	}
	if (isset($_POST['recaptcha_shortcode']) && $_POST['recaptcha_shortcode'] == 'true') {
		if (!isset($_POST['g-recaptcha-response']) || empty($_POST['g-recaptcha-response'])) {
			wp_die(__('reCAPTCHA is incorrect.', 'wp2print'), 'External Login Error', array('response' => 400));
		}
	}
}

function print_products_recaptcha_is_active() {
	global $wp2print_recaptcha_options;
	if ($wp2print_recaptcha_options && isset($wp2print_recaptcha_options['use']) && $wp2print_recaptcha_options['use']) {
		return true;
	}
	return false;
}

function print_products_recaptcha_is_v2() {
	return true;
}

function print_products_recaptcha_is_v3() {
	return false;
}

function print_products_recaptcha_verify($gresponse) {
	global $wp2print_recaptcha_options;
	if (print_products_recaptcha_is_active()) {
		$ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('secret' => $wp2print_recaptcha_options['secret_key'], 'response' => $gresponse, 'remoteip' => $_SERVER['REMOTE_ADDR']));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);

		$result = json_decode($response);
		if (is_object($result)) {
			if (print_products_recaptcha_is_v3()) {
				$score = (float)$wp2print_recaptcha_options['score'];
				if (!$score) { $score = 0.5; }
				if ($result->score >= $score) {
					return $result->success;
				} else {
					return false;
				}
			} else {
				return $result->success;
			}
		}
		return false;
	}
	return true;
}

// V3 version
add_action('wp_footer', 'print_products_recaptcha_wp_footer');
function print_products_recaptcha_wp_footer() {
	global $wp2print_recaptcha_options;
	if (print_products_recaptcha_is_active()) { ?>
		<?php if (print_products_recaptcha_is_v3()) { ?>
			<script src="https://www.google.com/recaptcha/enterprise.js?render=<?php echo GOOGLE_RECAPTCHA_SITE_KEY; ?>"></script>
			<script>
			<!--
			grecaptcha.enterprise.ready(function() {
				grecaptcha.enterprise.execute('<?php echo GOOGLE_RECAPTCHA_SITE_KEY; ?>', {action: 'LOGIN'}).then(function(token) {
					if (jQuery('input.g-recaptcha-response').length) {
						jQuery('input.g-recaptcha-response').val(token);
					}
				});
			});
		//--></script>
		<?php } ?>
		<?php if (print_products_recaptcha_is_v2()) { ?>
			<script>
			<!--
			jQuery(document).ready(function() {
				jQuery('form').submit(function(){
					if (jQuery(this).find('.g-recaptcha-response').length) {
						if (jQuery(this).find('.g-recaptcha-response').val() == '') {
							alert('<?php _e('reCAPTCHA is incorrect.', 'wp2print'); ?>');
							return false;
						}
					}
				});
			});
			//--></script>
			<?php
		}
	}
}

add_action('wp_enqueue_scripts', 'print_products_recaptcha_wp_enqueue_scripts');
function print_products_recaptcha_wp_enqueue_scripts() {
	if (print_products_recaptcha_is_active() && print_products_recaptcha_is_v2()) {
		wp_register_script('wp2print-google-recaptcha', 'https://www.google.com/recaptcha/enterprise.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('wp2print-google-recaptcha');
	}
}

add_action('login_footer', 'print_products_recaptcha_login_footer');
function print_products_recaptcha_login_footer() {
	if (print_products_recaptcha_is_active()) {
		if (print_products_recaptcha_is_v2()) { ?>
			<style>.g-recaptcha{transform:scale(0.90); transform-origin:0 0; margin-bottom:10px;}</style>
			<?php
		} else if (print_products_recaptcha_is_v3()) {
			print_products_recaptcha_wp_footer();
		}
	}
}

add_action('login_form', 'print_products_recaptcha_wp_auth_form');
add_action('register_form', 'print_products_recaptcha_wp_auth_form');
function print_products_recaptcha_wp_auth_form() {
	if (print_products_recaptcha_is_active() && print_products_recaptcha_is_v2()) {
		wp_enqueue_script('wp2print-google-recaptcha');
		print_products_recaptcha_field();
	}
}

add_action('woocommerce_login_form', 'print_products_recaptcha_woocommerce_auth_form');
add_action('woocommerce_register_form', 'print_products_recaptcha_woocommerce_auth_form');
add_action('wp2print_simple_submit_form', 'print_products_recaptcha_woocommerce_auth_form');
function print_products_recaptcha_woocommerce_auth_form() {
	if (print_products_recaptcha_is_active() && print_products_recaptcha_is_v2()) {
		print_products_recaptcha_field();
	}
}

add_shortcode('wp2print_recaptcha', 'print_products_recaptcha_shortcode');
function print_products_recaptcha_shortcode() {
	if (print_products_recaptcha_is_active() && print_products_recaptcha_is_v2()) {
		print_products_recaptcha_field(); ?>
		<input type="hidden" name="recaptcha_shortcode" value="true">
		<?php
	}
}

function print_products_recaptcha_field() {
	global $wp2print_recaptcha_options; ?>
	<div class="g-recaptcha" data-callback="wp2print_recaptcha" data-sitekey="<?php echo GOOGLE_RECAPTCHA_SITE_KEY; ?>"></div>
	<?php
}

add_action('wp2print_simple_submit_form', 'print_products_recaptcha_hidden_field');
function print_products_recaptcha_hidden_field() {
	if (print_products_recaptcha_is_active() && print_products_recaptcha_is_v3()) {
		echo '<input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response">';
	}
}

add_filter('wp_authenticate_user', 'print_products_recaptcha_wp_authenticate_user', 10, 2);
function print_products_recaptcha_wp_authenticate_user($user, $password) {
	if (print_products_recaptcha_is_active() && print_products_recaptcha_is_v2()) {
		if (!isset($_POST['g-recaptcha-response']) || empty($_POST['g-recaptcha-response'])) {
			return new WP_Error('invalid_captacha', __('Invalid Captcha.', 'wp2print'));
		}
	}
	return $user;
}

add_filter('wp2print_simple_submit_valid', 'print_products_recaptcha_wp2print_simple_submit_valid');
function print_products_recaptcha_wp2print_simple_submit_valid($valid) {
	if (print_products_recaptcha_is_active() && print_products_recaptcha_is_v2()) {
		if (!isset($_POST['g-recaptcha-response']) || empty($_POST['g-recaptcha-response'])) {
			$valid = false;
		}
	}
	return $valid;
}
?>