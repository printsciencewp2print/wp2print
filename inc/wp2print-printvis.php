<?php
$printvis_options = get_option("print_products_printvis_options");

//add_action('woocommerce_checkout_update_order_meta', 'print_products_printvis_created_order');
function print_products_printvis_created_order($order_id) {
	global $printvis_options;
	if ($printvis_options['enable'] == 1 && strlen($printvis_options['url'])) {
		$xmldata = print_products_printvis_create_xml($order_id);
		if ($xmldata) {
			print_products_printvis_send_xml($xmldata[1]);
		}
	}
}

function print_products_printvis_get_xml_filename($order_id) {
	return 'printvis-'.$order_id.'.xml';
}

function print_products_printvis_create_xml($order_id) {
	global $printvis_options;
	$upload_dir = wp_get_upload_dir();
	$xmlfilename = print_products_printvis_get_xml_filename($order_id);
	$xmlfilepath = $upload_dir['path'].'/'.$xmlfilename;
	$xmlfileurl = $upload_dir['url'].'/'.$xmlfilename;
	if (class_exists('SimpleXMLElement')) {
		$order = wc_get_order($order_id);
		$order_customer_id = $order->get_customer_id();
		$printvis_contact_id = print_products_printvis_get_contact_id($order_customer_id);

		$xml = new DOMDocument();
		$xml->encoding = 'utf-8';
		$xml->xmlVersion = '1.0';
		$xml->formatOutput = true;

		// PlaceOrder
		$root = $xml->createElement('PlaceOrder');
		$root->setAttributeNode(new DOMAttr('xmlns', 'urn:web2pv:gateway:placeorder:1'));
		$root->setAttributeNode(new DOMAttr('xmlns:core', 'urn:web2pv:core:1'));

		$FrontendID = $xml->createElement('FrontendID', 'WEBSHOP');
		$root->appendChild($FrontendID);

		$CustomerID = $xml->createElement('CustomerID', $order_customer_id);
		$root->appendChild($CustomerID);

		$ContactID = $xml->createElement('ContactID', $printvis_contact_id);
		$root->appendChild($ContactID);
		
		$RequestNo = $xml->createElement('RequestNo', 'WEBREQ'.$order_id);
		$root->appendChild($RequestNo);

		$Shipment = print_products_printvis_get_shipment($xml, $order);
		$root->appendChild($Shipment);

		$Payment = $xml->createElement('Payment');
		$Payment->setAttributeNode(new DOMAttr('Status', 'Paid'));
		$Payment->setAttributeNode(new DOMAttr('ReferenceNo', $order->get_transaction_id()));
		$root->appendChild($Payment);

		$Notes = print_products_printvis_get_notes($xml, $order_id);
		if ($Notes) {
			$root->appendChild($Notes);
		}

		$OrderLines = print_products_printvis_get_order_lines($xml, $order);
		$root->appendChild($OrderLines);

		// ------------------------------------------

		$xml->appendChild($root);

		$xml->save($xmlfilepath);

		return array($xmlfileurl, $xmlfilepath);
	}
}

function print_products_printvis_get_shipment($xml, $order) {
	if ($order->get_shipping_state()) {
		$address = array(
			'name' => $order->get_shipping_first_name().' '.$order->get_shipping_last_name(),
			'company' => $order->get_shipping_company(),
			'address_1' => $order->get_shipping_address_1(),
			'address_2' => $order->get_shipping_address_2(),
			'city' => $order->get_shipping_city(),
			'state' => $order->get_shipping_state(),
			'postcode' => $order->get_shipping_postcode(),
			'country' => $order->get_shipping_country()
		);
	} else {
		$address = array(
			'name' => $order->get_billing_first_name().' '.$order->get_billing_last_name(),
			'company' => $order->get_billing_company(),
			'address_1' => $order->get_billing_address_1(),
			'address_2' => $order->get_billing_address_2(),
			'city' => $order->get_billing_city(),
			'state' => $order->get_billing_state(),
			'postcode' => $order->get_billing_postcode(),
			'country' => $order->get_billing_country()
		);
	}
	$Shipment = $xml->createElement('Shipment');
	$AddressCode = $xml->createElement('AddressCode', '');
	$CompanyName = $xml->createElement('Company', $address['company']);
	$Address = $xml->createElement('Address', $address['address_1'].' '.$address['address_2']);
	$PostCode = $xml->createElement('PostCode', $address['postcode']);
	$City = $xml->createElement('City', $address['city']);
	$Country = $xml->createElement('Country', $address['country']);
	$County = $xml->createElement('County', $address['state']);
	$ContactName = $xml->createElement('ContactName', $address['name']);
	$ContactPhone = $xml->createElement('ContactPhone', $order->get_billing_phone());
	$ContactEmail = $xml->createElement('ContactEmail', $order->get_billing_email());

	$Shipment->appendChild($AddressCode);
	$Shipment->appendChild($CompanyName);
	$Shipment->appendChild($Address);
	$Shipment->appendChild($PostCode);
	$Shipment->appendChild($City);
	$Shipment->appendChild($Country);
	$Shipment->appendChild($County);
	$Shipment->appendChild($ContactName);
	$Shipment->appendChild($ContactPhone);
	$Shipment->appendChild($ContactEmail);

	return $Shipment;
}

function print_products_printvis_get_notes($xml, $order_id) {
	$Notes = false;
	remove_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );
	$comments = get_comments(array('post_id' => $order_id));
	if ($comments) {
		$Notes = $xml->createElement('Notes');
		foreach ($comments as $comment) {
			$Note = $xml->createElement('Note', trim($comment->comment_content));
			$Notes->appendChild($Note);
		}
	}
	return $Notes;
}

function print_products_printvis_get_order_lines($xml, $order) {
	global $printvis_options, $wpdb;

	$order_items = $order->get_items('line_item');

	$OrderLines = $xml->createElement('OrderLines');
	$lnum = 1;
	foreach($order_items as $order_item) {
		$item_id = $order_item->get_id();
		$quantity = $order_item->get_quantity();
		$item_subtotal = $order_item->get_subtotal();
		$product = $order_item->get_product();
		$product_sku = $product->get_sku();
		if (!$product_sku) { $product_sku = $product->get_id(); }

		$OrderLine = $xml->createElement('OrderLine');
		$OrderLine->setAttributeNode(new DOMAttr('LineNo', $lnum));

		$ItemNo = $xml->createElement('ItemNo', $product_sku);
		$Quantity = $xml->createElement('Quantity', $order_item->get_quantity());
		$CaseID = $xml->createElement('CaseID', '');
		$JobID = $xml->createElement('JobID', '');
		$UnitPrice = $xml->createElement('UnitPrice', number_format(print_products_printvis_get_item_price($item_id, $product), 2));
		$LineAmount = $xml->createElement('LineAmount', number_format($item_subtotal, 2));
		$DeliveryDate = $xml->createElement('DeliveryDate', '');
		
		$OrderLine->appendChild($ItemNo);
		$OrderLine->appendChild($Quantity);
		$OrderLine->appendChild($CaseID);
		$OrderLine->appendChild($JobID);
		$OrderLine->appendChild($UnitPrice);
		$OrderLine->appendChild($LineAmount);
		$OrderLine->appendChild($DeliveryDate);

		$OrderLines->appendChild($OrderLine);

		$lnum++;
	}

	return $OrderLines;
}

function print_products_printvis_get_contact_id($user_id) {
	$user_group = print_products_users_groups_get_user_group($user_id);
	if ($user_group) {
		$options = unserialize($user_group['options']);
		if (isset($options['printvis_customer_id'])) {
			return $options['printvis_customer_id'];
		}
	} else {
		return get_user_meta($user_id, '_user_printvis_id', true);
	}
}

function print_products_printvis_get_item_price($item_id, $product) {
	global $wpdb;
	$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = %s", $wpdb->prefix, $item_id));
	if ($order_item_data) {
		return $order_item_data->price;
	}
	return $product->get_price();
}

function print_products_printvis_admin_page() {
	global $printvis_options;
	$order_id = isset($_GET['order_id']) ? $_GET['order_id'] : '';
	?>
	<div class="wrap wp2print-printers-plan-wrap">
		<h2><?php _e('PrintVIS', 'wp2print'); ?></h2>
		<?php if (isset($_GET['ppsent']) && $_GET['ppsent']) { ?>
			<div class="notice notice-success">
				<p><?php _e('XML File was successfully submitted.', 'wp2print'); ?></p>
			</div>
		<?php } ?>
		<form class="pp-search-form">
			<input type="hidden" name="page" value="print-products-printvis">
			<p>
				<input id="post-search-input" type="text" name="order_id" value="<?php echo $order_id; ?>" placeholder="Order ID" style="width:120px;">
				<input id="search-submit" type="submit" class="button" value="<?php _e('Search', 'wp2print'); ?>">
			</p>
		</form>
		<?php if ($order_id) {
			$xmldata = print_products_printvis_create_xml($order_id);
			if ($xmldata) { ?>
				<p class="pp-result">
					<?php _e('XML File', 'wp2print'); ?>: <a href="<?php echo $xmldata[0]; ?>" target="_blank"><?php echo basename($xmldata[0]); ?></a>
				</p>
				<form method="POST">
				<input type="hidden" name="printvis_action" value="send-xml">
				<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
				<input type="hidden" name="xml_file" value="<?php echo $xmldata[1]; ?>">
				<input type="submit" value="<?php _e('POST', 'wp2print'); ?>" class="button button-primary">
				</form>
			<?php } ?>
		<?php } ?>
	</div>
	<?php
}

function print_products_printvis_send_xml($xmlfile) {
	global $printvis_options;
	$url = $printvis_options['api_url'];
	if ($url) {
		$xml = file_get_contents($xmlfile);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		var_dump($result); exit;
	}
	return $result;
}

function print_products_printvis_get_token() {
	$client_id = '7fb99bd6-42f2-4a6a-ae40-7697103bdec7';
	$client_secret = 'fdW8Q~5DqI2aqsZtEZT_oV-pRojqOlm74aesLdxi';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://login.microsoft.com/bb491840-ec3f-40eb-b933-ccbfb7e34603/oauth2/v2.0/token");
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, array(
		'scope' => 'https://api.businesscentral.dynamics.com/.default',
		'client_id' => $client_id,
		'client_secret' => $client_secret,
		'grant_type' => 'client_credentials'
	));
	$response = curl_exec($ch);
	curl_close($ch);

	$data = json_decode($response);
	if (isset($data->access_token) && $data->access_token) {
		return $data->access_token;
	}
	return false;
}

add_action('wp_loaded', 'print_products_printvis_admin_actions');
function print_products_printvis_admin_actions() {
	global $printvis_options;
	if (isset($_POST['printvis_action']) && $_POST['printvis_action'] == 'send-xml') {
		$xml_file = $_POST['xml_file'];
		$order_id = $_POST['order_id'];
		if (file_exists($xml_file)) {
			print_products_printvis_send_xml($xml_file);
		}
		wp_redirect('admin.php?page=print-products-printvis&order_id='.$order_id.'&ppsent=1');
		exit;
	}

	if (isset($_GET['printvis_test'])) {
		$client_id = '7fb99bd6-42f2-4a6a-ae40-7697103bdec7';
		$client_secret = 'fdW8Q~5DqI2aqsZtEZT_oV-pRojqOlm74aesLdxi';
		$redirect_uri = 'https://wp2print.net/about-us/';
		$code = 'HELLO';


		$resource = 'https://api.businesscentral.dynamics.com/v2.0/bb491840-ec3f-40eb-b933-ccbfb7e34603/Calagaz_Printing/WS/Calagaz%20Printing/Codeunit/Web2PV';

		$access_token = print_products_printvis_get_token();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.businesscentral.dynamics.com/v2.0/bb491840-ec3f-40eb-b933-ccbfb7e34603/Calagaz_Printing/WS/Calagaz%20Printing/Codeunit/Web2PV');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$access_token,
			'Token Name: Calagaz',
			'Client ID: 7fb99bd6-42f2-4a6a-ae40-7697103bdec7',
			'Client Secret: fdW8Q~5DqI2aqsZtEZT_oV-pRojqOlm74aesLdxi'
		));
		/*curl_setopt($ch, CURLOPT_POSTFIELDS, array(
			'type' => 'OAuth 2.0',
			'client_id' => $client_id,
			'client_secret' => $client_secret,
			'redirect_uri' => $redirect_uri,
			'grant_type' => 'Client Credentials',
			'token_name' => 'Calagaz'
		));*/
		$data = curl_exec($ch);
		curl_close($ch);

		var_dump($data);
		exit;
	}
}

add_action('show_user_profile', 'print_products_printvis_profile_field');
add_action('edit_user_profile', 'print_products_printvis_profile_field');
function print_products_printvis_profile_field($profileuser) {
	$user_printvis_id = get_user_meta($profileuser->ID, '_user_printvis_id', true); ?>
	<h3><?php _e('PrintVIS', 'wp2print'); ?></h3>
	<table class="form-table">
		<tr>
			<th><label><?php _e('PrintVIS Customer ID', 'wp2print'); ?></label></th>
			<td>
				<input type="text" name="user_printvis_id" value="<?php echo $user_printvis_id; ?>">
			</td>
		</tr>
	</table>
	<?php
}

add_action('personal_options_update', 'print_products_printvis_save_profile_field');
add_action('edit_user_profile_update', 'print_products_printvis_save_profile_field');
function print_products_printvis_save_profile_field($user_id) {
	update_usermeta($user_id, '_user_printvis_id', $_POST['user_printvis_id']);
}
?>