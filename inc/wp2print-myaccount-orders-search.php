<?php
add_action('woocommerce_before_account_orders', 'print_products_myaccount_orders_search_form', 11);
function print_products_myaccount_orders_search_form($has_orders) {
	?>
	<div class="woocommerce-myaccount-orders-search">
		<form action="<?php echo wc_get_endpoint_url('orders'); ?>" class="orders-search-form">
			<?php if (isset($_REQUEST['allgroup']) && $_REQUEST['allgroup'] == 'true') { ?><input type="hidden" name="allgroup" value="true"><?php } ?>
			<input type="text" name="orders_sterm" value="<?php if (isset($_REQUEST['orders_sterm'])) { echo $_REQUEST['orders_sterm']; } ?>" class="orders-sterm" style="width:50%">
			<input type="submit" value="<?php _e('Search', 'wp2print'); ?>" class="button alt orders-search-submit">
		</form>
	</div>
	<?php
}

add_filter('woocommerce_my_account_my_orders_query', 'print_products_myaccount_orders_search_my_orders_query', 11);
function print_products_myaccount_orders_search_my_orders_query($args) {
	global $current_user;
	if (isset($_REQUEST['orders_sterm']) && trim($_REQUEST['orders_sterm'])) {
		$customer_id = (is_array($args['customer'])) ? $args['customer'] : array($args['customer']);
		$is_superuser = print_products_users_groups_is_superuser($current_user->ID);
		if ($is_superuser && isset($_REQUEST['allgroup']) && $_REQUEST['allgroup'] == 'true' && function_exists('print_products_users_groups_get_group_users')) {
			$customer_id = print_products_users_groups_get_group_users($is_superuser);
		}
		$order_ids = print_products_myaccount_orders_search_get_orders(trim($_REQUEST['orders_sterm']), $customer_id);
		if ($order_ids) {
			$args['post__in'] = $order_ids;
		} else {
			$args['post__in'] = array(0);
		}
	}
	return $args;
}

function print_products_myaccount_orders_search_get_orders($orders_sterm, $customer_id) {
	global $wpdb;
	$orders_ids = array();
	$where = array();
	$where2 = array();
	$where3 = array();
	$sterms = explode(' ', $orders_sterm);
	foreach($sterms as $sterm) {
		$where[] = sprintf("om.meta_value LIKE '%s'", '%'.str_replace("'", "''", $sterm).'%');
		$where2[] = sprintf("oi.order_item_name LIKE '%s'", '%'.str_replace("'", "''", $sterm).'%');
		$where3[] = sprintf("ppoi.additional LIKE '%s'", '%'.str_replace("'", "''", $sterm).'%');
	}

	// search order id
	if (print_products_is_hpos_enabled()) {
		$oid_data = $wpdb->get_row(sprintf("SELECT o.id as order_id FROM %swc_orders o WHERE o.customer_id IN (%s) AND o.id = '%s'", $wpdb->prefix, implode(',', $customer_id), $orders_sterm));
	} else {
		$oid_data = $wpdb->get_row(sprintf("SELECT o.ID as order_id FROM %sposts o LEFT JOIN %spostmeta omc ON omc.post_id = o.ID WHERE omc.meta_key = '_customer_user' AND omc.meta_value IN ('%s') AND o.ID = '%s'", $wpdb->prefix, $wpdb->prefix, implode("','", $customer_id), $orders_sterm));
	}
	if ($oid_data) {
		$orders_ids[] = $oid_data->order_id;
	}

	// search address
	if (print_products_is_hpos_enabled()) {
		$oids = $wpdb->get_results(sprintf("SELECT o.id as order_id FROM %swc_orders o LEFT JOIN %swc_orders_meta om ON om.order_id = o.id WHERE o.customer_id IN (%s) AND (om.meta_key = '_billing_address_index' OR om.meta_key = '_shipping_address_index' OR om.meta_key = '_order_notes' OR om.meta_key = '_order_items_tracking') AND %s", $wpdb->prefix, $wpdb->prefix, implode(',', $customer_id), implode(' AND ', $where)));
	} else {
		$oids = $wpdb->get_results(sprintf("SELECT o.ID as order_id FROM %sposts o LEFT JOIN %spostmeta omc ON omc.post_id = o.ID LEFT JOIN %spostmeta om ON om.post_id = o.ID WHERE omc.meta_key = '_customer_user' AND omc.meta_value IN ('%s') AND (om.meta_key = '_billing_address_index' OR om.meta_key = '_shipping_address_index' OR om.meta_key = '_order_notes') AND %s", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, implode("','", $customer_id), implode(' AND ', $where)));
	}
	if ($oids) {
		foreach($oids as $oid) {
			if (!in_array($oid->order_id, $orders_ids)) {
				$orders_ids[] = $oid->order_id;
			}
		}
	}

	// search order items
	if (print_products_is_hpos_enabled()) {
		$oids = $wpdb->get_results(sprintf("SELECT o.id as order_id FROM %swc_orders o LEFT JOIN %swoocommerce_order_items oi ON oi.order_id = o.id WHERE o.customer_id IN (%s) AND oi.order_item_type = 'line_item' AND %s", $wpdb->prefix, $wpdb->prefix, implode(',', $customer_id), implode(' AND ', $where2)));
	} else {
		$oids = $wpdb->get_results(sprintf("SELECT o.ID as order_id FROM %sposts o LEFT JOIN %spostmeta omc ON omc.post_id = o.ID LEFT JOIN %swoocommerce_order_items oi ON oi.order_id = o.ID WHERE omc.meta_key = '_customer_user' AND omc.meta_value IN ('%s') AND oi.order_item_type = 'line_item' AND %s", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, implode("','", $customer_id), implode(' AND ', $where2)));
	}
	if ($oids) {
		if (!is_array($orders_ids)) { $orders_ids = array(); }
		foreach($oids as $oid) {
			if (!in_array($oid->order_id, $orders_ids)) {
				$orders_ids[] = $oid->order_id;
			}
		}
	}

	// search order items project name
	if (print_products_is_hpos_enabled()) {
		$oids = $wpdb->get_results(sprintf("SELECT o.id as order_id FROM %swc_orders o LEFT JOIN %swoocommerce_order_items oi ON oi.order_id = o.id LEFT JOIN %sprint_products_order_items ppoi ON ppoi.item_id = oi.order_item_id WHERE o.customer_id IN (%s) AND oi.order_item_type = 'line_item' AND %s", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, implode(',', $customer_id), implode(' AND ', $where3)));
	} else {
		$oids = $wpdb->get_results(sprintf("SELECT o.ID as order_id FROM %sposts o LEFT JOIN %spostmeta omc ON omc.post_id = o.ID LEFT JOIN %swoocommerce_order_items oi ON oi.order_id = o.ID LEFT JOIN %sprint_products_order_items ppoi ON ppoi.item_id = oi.order_item_id WHERE omc.meta_key = '_customer_user' AND omc.meta_value IN ('%s') AND oi.order_item_type = 'line_item' AND %s", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, implode("','", $customer_id), implode(' AND ', $where3)));
	}
	if ($oids) {
		if (!is_array($orders_ids)) { $orders_ids = array(); }
		foreach($oids as $oid) {
			if (!in_array($oid->order_id, $orders_ids)) {
				$orders_ids[] = $oid->order_id;
			}
		}
	}

	return $orders_ids;
}

add_filter('woocommerce_get_endpoint_url', 'print_products_myaccount_orders_search_endpoint_url', 11);
function print_products_myaccount_orders_search_endpoint_url($url) {
	$opos = strpos($url, '/orders/');
	if ($opos && isset($_REQUEST['orders_sterm'])) {
		$opart = substr($url, $opos + 1);
		$oarray = explode('/', $opart);
		$npart = (int)$oarray[1];
		if ($npart) {
			if (strpos($url, '?')) {
				$url = $url . '&orders_sterm=' . $_REQUEST['orders_sterm'];
			} else {
				$url = $url . '?orders_sterm=' . $_REQUEST['orders_sterm'];
			}
		}
	}
	return $url;
}
?>