<?php
function print_products_send_quote_admin_page() {
	include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-send-quote.php';
}

function print_products_send_quote_history_admin_page() {
	include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-send-quote-history.php';
}

add_action('wp_loaded', 'print_products_send_quote_actions');
function print_products_send_quote_actions() {
	global $wpdb;
	if (isset($_POST['print_products_send_quote_action']) && $_POST['print_products_send_quote_action'] == 'process') {
		$send_quote_data = print_products_send_quote_get_order_data();
		switch ($_POST['process_step']) {
			case '1':
				print_products_send_quote_set_customer();
			break;
			case '2':
				if (isset($_POST['product_action']) && strlen($_POST['product_action'])) {
					$redirect = 'admin.php?page=print-products-send-quote&step=2';
					if ($_POST['product_action'] == 'add') {
						$product_key = print_products_send_quote_add_product();
						$redirect .= '&product_key='.$product_key;
					} else if ($_POST['product_action'] == 'attributes') {
						print_products_send_quote_set_product_data();
					} else if ($_POST['product_action'] == 'duplicate') {
						print_products_send_quote_duplicate_product();
					} else if ($_POST['product_action'] == 'delete') {
						print_products_send_quote_delete_product();
					}
					wp_redirect($redirect);
					exit;
				}
			break;
			case 'send':
				print_products_send_quote_save_order();
			break;
		}
	}
	if (isset($_POST['AjaxAction']) && $_POST['AjaxAction'] == 'send-quote-add-user') {
		$user_login = sanitize_user($_POST['u_username']);
		$user_email = sanitize_text_field($_POST['u_email']);
		$user_fname = sanitize_text_field($_POST['u_fname']);
		$user_lname = sanitize_text_field($_POST['u_lname']);
		$user_pass = sanitize_text_field($_POST['u_pass']);
		$print_products_send_quote_options = get_option("print_products_send_quote_options");

		$error = '';
		if (!validate_username($user_login)) {
			$error = __('<strong>ERROR</strong>: This username is invalid because it uses illegal characters. Please enter a valid username.');
		} else if (username_exists($user_login)) {
			$error = __('<strong>ERROR</strong>: This username is already registered. Please choose another one.');
		} elseif (!is_email($user_email)) {
			$error = __('<strong>ERROR</strong>: The email address isn&#8217;t correct.');
		} elseif (email_exists($user_email)) {
			$error = __('<strong>ERROR</strong>: This email is already registered, please choose another one.');
		}

		if (!strlen($error)) {
			$new_user = new stdClass;
			$new_user->user_login = $user_login;
			$new_user->user_email = $user_email;
			$new_user->user_pass = $user_pass;
			$new_user->first_name = $user_fname;
			$new_user->last_name = $user_lname;
			$new_user->role = get_option('default_role');
			$new_user->display_name = $user_login;
			if (strlen($user_fname)) {
				$new_user->display_name = $user_fname.' '.$user_lname;
			}
			$user_id = wp_insert_user($new_user);

			// send email to customer
			$admin_email = get_option('admin_email');
			$site_name = get_bloginfo('name');
			$subject = $print_products_send_quote_options['cnu_email_subject'];
			$message = $print_products_send_quote_options['cnu_email_message'];
			$message = str_replace('{USERNAME}', $user_login, $message);
			$message = str_replace('{EMAIL}', $user_email, $message);
			$message = str_replace('{PASSWORD}', $user_pass, $message);

			print_products_send_wc_mail($user_email, $subject, $message);

			$user_line = $new_user->display_name.' ('.__('Email', 'wp2print').': '.$new_user->user_email.')';
			$resp = 'success;'.$user_id.';'.$user_line;
		} else {
			$resp = 'error;'.$error;
		}
		echo $resp;
		exit;
	}
	if (isset($_POST['sqh_action'])) {
		switch ($_POST['sqh_action']) {
			case 'convert':
				$order_id = (int)$_POST['order_id'];
				if ($order_id) {
					$wc_order_id = print_products_send_quote_convert_to_order($order_id);
					$redirect = 'admin.php?page=print-products-send-quote-history';
					if (isset($_GET['s'])) { $redirect .= '&s='.$_GET['s']; }
					if (isset($_GET['qopage'])) { $redirect .= '&qopage='.$_GET['qopage']; }
					$redirect .= '&converted=true&wcoid='.$wc_order_id;
					wp_redirect($redirect);
					exit;
				}
			break;
			case 'resend':
				$print_products_send_quote_options = get_option("print_products_send_quote_options");
				$order_id = (int)$_POST['order_id'];
				if ($order_id) {
					$email_subject = $print_products_send_quote_options['email_subject'];
					$email_message = $print_products_send_quote_options['email_message'];
					print_products_send_quote_order_email($order_id, $email_subject, $email_message);
					$redirect = 'admin.php?page=print-products-send-quote-history';
					if (isset($_GET['s'])) { $redirect .= '&s='.$_GET['s']; }
					if (isset($_GET['qopage'])) { $redirect .= '&qopage='.$_GET['qopage']; }
					$redirect .= '&resent=true';
					wp_redirect($redirect);
					exit;
				}
			break;
			case 'duplicate':
				$order_id = (int)$_POST['order_id'];
				if ($order_id) {
					print_products_send_quote_duplicate_order($order_id);
					wp_redirect('admin.php?page=print-products-send-quote');
					exit;
				}
			break;
			case 'edit':
				$order_id = (int)$_POST['order_id'];
				if ($order_id) {
					print_products_send_quote_edit_order($order_id);
					wp_redirect('admin.php?page=print-products-send-quote');
					exit;
				}
			break;
		}
	}
	if (isset($_GET['qorder']) && $_GET['qorder'] == 'true') {
		print_products_send_quote_order_process();
	}
	if (isset($_REQUEST['AjaxAction']) && $_REQUEST['AjaxAction'] == 'send-quote-select-user') {
		$users_data = array();
		if (isset($_REQUEST['q']) && strlen($_REQUEST['q'])) {
			$users = $wpdb->get_results(sprintf("SELECT u.*, um.meta_value FROM %susers u LEFT JOIN %susermeta um ON um.user_id = u.ID AND um.meta_key = 'billing_email' WHERE u.display_name LIKE '%s' OR u.user_login LIKE '%s' OR u.user_email LIKE '%s' OR um.meta_value LIKE '%s' ORDER BY u.display_name LIMIT 0, 50", $wpdb->prefix, $wpdb->prefix, '%'.$_REQUEST['q'].'%', '%'.$_REQUEST['q'].'%', '%'.$_REQUEST['q'].'%', '%'.$_REQUEST['q'].'%'));
			if ($users) {
				foreach($users as $user) {
					$user_email = $user->user_email;
					if (strlen($user->meta_value)) { $user_email = $user->meta_value; }
					$users_data[] = array('id' => $user->ID, 'text' => $user->display_name.' ('.__('Email', 'wp2print').': '.$user_email.')');
				}
			}
		}
		echo json_encode(array('results' => $users_data));
		exit;
	}
}

function print_products_send_quote_get_order_data() {
	$send_quote_data = array();
	if (isset($_SESSION['send_quote_data'])) { $send_quote_data = $_SESSION['send_quote_data']; }
	return $send_quote_data;
}

function print_products_send_quote_set_order_data($send_quote_data) {
	$_SESSION['send_quote_data'] = $send_quote_data;
}

function print_products_send_quote_get_order($order_id) {
	global $wpdb;
	return $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_quotes WHERE order_id = %s", $wpdb->prefix, $order_id));
}

function print_products_send_quote_get_order_items($order_id) {
	global $wpdb;
	return $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_quotes_items WHERE order_id = %s", $wpdb->prefix, $order_id));
}

function print_products_send_quote_set_customer() {
	$send_quote_data = print_products_send_quote_get_order_data();
	$send_quote_data['sender'] = $_POST['order_sender'];
	$send_quote_data['customer'] = $_POST['order_customer'];
	print_products_send_quote_set_order_data($send_quote_data);
}

function print_products_send_quote_add_product() {
	$product_id = (int)$_POST['order_product'];
	$product_key = md5(time());

	$product_name = get_the_title($product_id);

	$products = array();
	$send_quote_data = print_products_send_quote_get_order_data();
	if (isset($send_quote_data['products'])) {
		$products = $send_quote_data['products'];
	}
	$cptype = isset($_POST['cptype']) ? stripslashes($_POST['cptype']) : '';
	$products[$product_key] = array('product_id' => $product_id, 'name' => $product_name, 'cptype' => print_products_pserialize($cptype));

	$send_quote_data['products'] = $products;
	print_products_send_quote_set_order_data($send_quote_data);
	return $product_key;
}

function print_products_send_quote_set_product_data() {
	$product_key = $_POST['product_key'];
	$product_type = $_POST['product_type'];
	$quantity = (int)$_POST['quantity'];
	$price = (float)$_POST['price'];

	$smparams = '';
	$fmparams = '';
	$artworkfiles = '';
	if (isset($_POST['smparams'])) {
		$smparams = $_POST['smparams'];
		$fmparams = $_POST['fmparams'];
	}
	if (isset($_POST['artworkfiles'])) {
		$artworkfiles = $_POST['artworkfiles'];
	}

	$send_quote_data = print_products_send_quote_get_order_data();

	$product_data = $send_quote_data['products'][$product_key];
	$product_data['product_type'] = $product_type;
	$product_data['quantity'] = $quantity;
	$product_data['price'] = $price;
	$product_data['smparams'] = $smparams;
	$product_data['fmparams'] = $fmparams;
	$product_data['artworkfiles'] = $artworkfiles;

	if ($product_type == 'custom') {
		$product_data['product_attributes'] = stripslashes($_POST['attributes']);
		$product_data['shipping_specify'] = $_POST['shipping_specify'];
		$product_data['weight'] = $_POST['weight'];
		$product_data['sboxes'] = $_POST['sboxes'];
		$product_data['shipping_cost'] = $_POST['shipping_cost'];
	}
	$_REQUEST['add-to-cart'] = $product_data['product_id'];

	$checkout_data = false;
	switch ($product_type) {
		case 'fixed':
			$checkout_data = print_products_checkout_fixed($product_key, false, true);
		break;
		case 'book':
			$checkout_data = print_products_checkout_book($product_key, false, true);
		break;
		case 'area':
			$checkout_data = print_products_checkout_area($product_key, false, true);
		break;
		case 'sticker':
			$checkout_data = print_products_checkout_sticker($product_key, false, true);
		break;
		case 'box':
			$checkout_data = print_products_checkout_box($product_key, false, true);
		break;
		case 'custom':
			$custom_data = array('cptype' => $product_data['cptype'], 'shipping_specify' => $product_data['shipping_specify'], 'weight' => $product_data['weight'], 'sboxes' => $product_data['sboxes'], 'shipping_cost' => $product_data['shipping_cost']);
			$checkout_data = array(
				'product_attributes' => $product_data['product_attributes'],
				'additional' => serialize($custom_data)
			);
		break;
	}

	if ($checkout_data) {
		if ($checkout_data['product_attributes']) {
			$product_data['product_attributes'] = $checkout_data['product_attributes'];
		}
		$product_data['additional'] = $checkout_data['additional'];
	}

	switch ($product_type) {
		case 'area':
			$product_data['width'] = $_POST['width'];
			$product_data['height'] = $_POST['height'];
		break;
		case 'sticker':
			$product_data['width'] = $_POST['width'];
			$product_data['height'] = $_POST['height'];
		break;
		case 'box':
			$product_data['width'] = $_POST['width'];
			$product_data['height'] = $_POST['height'];
			$product_data['length'] = $_POST['length'];
		break;
		case 'variable':
			$product_data['attributes'] = $_POST['attributes'];
			$product_data['variation_id'] = (int)$_POST['variation_id'];
		break;
	}

	$send_quote_data['products'][$product_key] = $product_data;
	print_products_send_quote_set_order_data($send_quote_data);
}

function print_products_send_quote_duplicate_product() {
	$product_key = $_POST['product_key'];
	if ($product_key) {
		$new_product_key = md5(time());
		$send_quote_data = print_products_send_quote_get_order_data();
		$products = $send_quote_data['products'];
		$products[$new_product_key] = $products[$product_key];
		$send_quote_data['products'] = $products;
		print_products_send_quote_set_order_data($send_quote_data);
	}
}

function print_products_send_quote_delete_product() {
	$product_key = $_POST['product_key'];
	if ($product_key) {
		$send_quote_data = print_products_send_quote_get_order_data();
		$products = $send_quote_data['products'];
		unset($products[$product_key]);
		$send_quote_data['products'] = $products;
		print_products_send_quote_set_order_data($send_quote_data);
	}
}

function print_products_send_quote_save_order() {
	global $wpdb;
	$print_products_send_quote_options = get_option("print_products_send_quote_options");
	$send_quote_data = print_products_send_quote_get_order_data();
	$sender = $send_quote_data['sender'];
	$customer_id = (int)$send_quote_data['customer'];
	$products = $send_quote_data['products'];
	$order_id = (isset($send_quote_data['edit_order_id'])) ? (int)$send_quote_data['edit_order_id'] : 0;

	$discount = (float)$_POST['discount'];
	$expire_date = $_POST['expire_date'];
	$reference = $_POST['reference'];
	$email_subject = trim($_POST['email_subject']);
	$email_message = stripslashes($_POST['email_message']);
	$process_action = $_POST['process_action'];

	$saved = ($process_action == 'save') ? 1 : 0;

	if ($order_id) {
		$update = array();
		$update['user_id'] = $customer_id;
		$update['sender'] = $sender;
		$update['discount'] = $discount;
		$update['expire_date'] = $expire_date;
		$update['reference'] = $reference;
		$update['saved'] = $saved;
		$update['created'] = current_time('mysql');
		$wpdb->update($wpdb->prefix."print_products_quotes", $update, array('order_id' => $order_id));
		$wpdb->query(sprintf("DELETE FROM %sprint_products_quotes_items WHERE order_id = %s", $wpdb->prefix, $order_id));
	} else {
		// add record to db
		$insert = array();
		$insert['user_id'] = $customer_id;
		$insert['sender'] = $sender;
		$insert['discount'] = $discount;
		$insert['expire_date'] = $expire_date;
		$insert['reference'] = $reference;
		$insert['saved'] = $saved;
		$insert['created'] = current_time('mysql');
		$wpdb->insert($wpdb->prefix."print_products_quotes", $insert);
		$order_id = $wpdb->insert_id;
	}
	if ($products) {
		foreach($products as $product_data) {
			$insert = array();
			$insert['order_id'] = $order_id;
			$insert['product_id'] = $product_data['product_id'];
			$insert['product_type'] = $product_data['product_type'];
			$insert['quantity'] = $product_data['quantity'];
			$insert['price'] = $product_data['price'];
			$insert['smparams'] = $product_data['smparams'];
			$insert['fmparams'] = $product_data['fmparams'];
			$insert['product_attributes'] = $product_data['product_attributes'];
			$insert['additional'] = $product_data['additional'];
			$insert['artworkfiles'] = $product_data['artworkfiles'];

			$product_type = $product_data['product_type'];
			switch ($product_type) {
				case 'area':
					$insert['width'] = $product_data['width'];
					$insert['height'] = $product_data['height'];
				break;
				case 'sticker':
					$insert['width'] = $product_data['width'];
					$insert['height'] = $product_data['height'];
				break;
				case 'box':
					$insert['width'] = $product_data['width'];
					$insert['height'] = $product_data['height'];
					$insert['length'] = $product_data['length'];
				break;
				case 'variable':
					$insert['variation_id'] = $product_data['variation_id'];
					$insert['attributes'] = serialize($product_data['attributes']);
				break;
			}
			$wpdb->insert($wpdb->prefix."print_products_quotes_items", $insert);
		}
	}

	// send email
	if ($process_action == 'send') {
		print_products_send_quote_order_email($order_id, $email_subject, $email_message);
	}

	unset($_SESSION['send_quote_data']);

	wp_redirect('admin.php?page=print-products-send-quote&step=completed&order='.$order_id.'&paction='.$process_action);
	exit;
}

function print_products_send_quote_convert_to_order($order_id) {
	global $wpdb;
	$shipping = (float)$_POST['shipping'];
	$apply_tax = $_POST['apply_tax'];
	$tax = (float)$_POST['tax'];
	$shipping_tax = (float)$_POST['shipping_tax'];
	$order_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_quotes WHERE order_id = '%s'", $wpdb->prefix, $order_id));
	$order_items = print_products_send_quote_get_order_items($order_id);
	if ($order_data) {
		$customer_id = $order_data->user_id;
		$discount = (float)$order_data->discount;

		$order = wc_create_order(array('customer_id' => $customer_id));

		$billing_address = print_products_send_quote_get_customer_address($customer_id, 'billing');
		$order->set_address($billing_address, 'billing');

		$shipping_address = print_products_send_quote_get_customer_address($customer_id, 'shipping');
		$order->set_address($shipping_address, 'shipping');

		$total = 0;
		$products_tax = 0;
		$products_items = array();
		if ($order_items) {
			foreach($order_items as $oikey => $order_item) {
				$product_id = $order_item->product_id;
				$product_type = $order_item->product_type;
				$variation_id = $order_item->variation_id;
				$quantity = (int)$order_item->quantity;
				$price = (float)$order_item->price;
				$attributes = $order_item->attributes;
				$smparams = $order_item->smparams;
				$fmparams = $order_item->fmparams;
				$width = $order_item->width;
				$height = $order_item->height;
				$length = $order_item->length;
				$product_attributes = $order_item->product_attributes;
				$additional = $order_item->additional;
				$artworkfiles = $order_item->artworkfiles;
				if ($artworkfiles) { $artworkfiles = serialize(explode(';', $artworkfiles)); }

				$args = array('variation_id' => $variation_id);
				if ($tax && ($apply_tax == 'products' || $apply_tax == 'both')) {
					$products_tax = $products_tax + $tax;
					$args['totals'] = array('tax' => $tax);
					$args['total_tax'] = $tax;
				}
				$order_item_id = $order->add_product(get_product($product_id), $quantity, $args);

				$products_items[] = $order_item_id;

				wc_update_order_item_meta($order_item_id, '_line_subtotal', $price);
				wc_update_order_item_meta($order_item_id, '_line_total', $price);

				$ppdata = array();
				$ppdata['item_id'] = $order_item_id;
				$ppdata['product_id'] = $product_id;
				$ppdata['product_type'] = $product_type;
				$ppdata['quantity'] = $quantity;
				$ppdata['price'] = $price;
				$ppdata['product_attributes'] = $product_attributes;
				$ppdata['additional'] = $additional;
				$ppdata['artwork_files'] = $artworkfiles;
				$ppdata['atcaction'] = 'artwork';
				$wpdb->insert($wpdb->prefix."print_products_order_items", $ppdata);

				$total = $total + $price;
			}
		}

		if ($shipping) {
			$shipping_item = new WC_Order_Item_Shipping();
			$shipping_item->set_method_title('Shipping');
			$shipping_item->set_method_id('flat_rate:1');
			if ($shipping_tax && ($apply_tax == 'shipping' || $apply_tax == 'both')) {
				$shipping_item->set_taxes(array('total' => array(1 => $shipping_tax)));
			}
			$shipping_item->set_total($shipping);
			$order->add_item($shipping_item);
			$total = $total + $shipping;
		}

		if ($tax && $apply_tax != '') {
			if ($apply_tax == 'shipping') {
				$order_tax = $shipping_tax;
			} else if ($apply_tax == 'products') {
				$order_tax = $products_tax;
			} else {
				$order_tax = $shipping_tax + $products_tax;
			}
			$tax_item = new WC_Order_Item_Tax();
			$tax_item->set_name('Tax');
			$tax_item->set_label('Tax');
			$tax_item->set_rate_id(1);
			$tax_item->set_tax_total($order_tax);
			$order->add_item($tax_item);
			$total = $total + $order_tax;
		}

		$order->calculate_totals(false);
		$order->set_total($total);
		$order->set_status('pending');

		if ($products_items && $tax && ($apply_tax == 'products' || $apply_tax == 'both')) {
			foreach ($products_items as $order_item_id) {
				wc_update_order_item_meta($order_item_id, '_line_tax', $tax);
				wc_update_order_item_meta($order_item_id, '_line_subtotal_tax', $tax);
				wc_update_order_item_meta($order_item_id, '_line_tax_data', array('total' => array(1 => $tax), 'subtotal' => array(1 => $tax)));
			}
		}

		if ($tax && ($apply_tax == 'shipping' || $apply_tax == 'both')) {
			$shipping_items = $order->get_items('shipping');
			if ($shipping_items) {
				foreach ($shipping_items as $shipping_item) {
					$shipping_item->set_taxes(array('total' => array(1 => $shipping_tax)));
				}
			}
		}

		$wc_order_id = $order->save();

		if ($wc_order_id) {
			$wpdb->update($wpdb->prefix.'print_products_quotes', array('status' => '1', 'wc_order_id' => $wc_order_id), array('order_id' => $order_id));
		}
		return $wc_order_id;
	}
}

function print_products_send_quote_get_customer_address($user_id, $atype) {
	$address = false;
	if ($atype == 'billing') {
		$user_email = get_user_meta($user_id, 'billing_email', true);
		if (!strlen($user_email)) {
			$user_data = get_userdata($user_id);
			$user_email = $user_data->user_email;
		}
		$address = array(
			'country' => get_user_meta($user_id, 'billing_country', true),
			'address_1' => get_user_meta($user_id, 'billing_address_1', true),
			'address_2' => get_user_meta($user_id, 'billing_address_2', true),
			'city' => get_user_meta($user_id, 'billing_city', true),
			'state' => get_user_meta($user_id, 'billing_state', true),
			'postcode' => get_user_meta($user_id, 'billing_postcode', true),
			'company' => get_user_meta($user_id, 'billing_company', true),
			'email' => $user_email,
			'phone' => get_user_meta($user_id, 'billing_phone', true)
		);
	} else {
		$address = array(
			'country' => get_user_meta($user_id, 'shipping_country', true),
			'address_1' => get_user_meta($user_id, 'shipping_address_1', true),
			'address_2' => get_user_meta($user_id, 'shipping_address_2', true),
			'city' => get_user_meta($user_id, 'shipping_city', true),
			'state' => get_user_meta($user_id, 'shipping_state', true),
			'postcode' => get_user_meta($user_id, 'shipping_postcode', true),
			'company' => get_user_meta($user_id, 'shipping_company', true)
		);
	}
	return $address;
}

function print_products_send_quote_edit_order($order_id) {
	print_products_send_quote_init_order($order_id);
	$send_quote_data = $_SESSION['send_quote_data'];
	$send_quote_data['edit_order_id'] = $order_id;
	print_products_send_quote_set_order_data($send_quote_data);
}

function print_products_send_quote_duplicate_order($order_id) {
	print_products_send_quote_init_order($order_id);
}

function print_products_send_quote_init_order($order_id) {
	global $wpdb;
	$order_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_quotes WHERE order_id = '%s'", $wpdb->prefix, $order_id));
	if ($order_data) {
		$send_quote_data = array();
		$send_quote_data['sender'] = $order_data->sender;
		$send_quote_data['customer'] = $order_data->user_id;
		$send_quote_data['expire_date'] = $order_data->expire_date;
		$send_quote_data['reference'] = $order_data->reference;

		$products = array();
		$order_items = print_products_send_quote_get_order_items($order_id);
		if ($order_items) {
			foreach($order_items as $oikey => $order_item) {
				$product_key = md5(time().$oikey);
				$product_id = $order_item->product_id;
				$product_data = array(
					'product_id' => $product_id,
					'name' => get_the_title($product_id),
					'product_type' => $order_item->product_type,
					'quantity' => $order_item->quantity,
					'price' => $order_item->price,
					'smparams' => $order_item->smparams,
					'fmparams' => $order_item->fmparams,
					'product_attributes' => $order_item->product_attributes,
					'artworkfiles' => $order_item->artworkfiles,
					'width' => $order_item->width,
					'height' => $order_item->height,
					'length' => $order_item->length,
					'variation_id' => $order_item->variation_id,
					'attributes' => $order_item->attributes,
					'additional' => $order_item->additional
				);
				if ($order_item->product_type == 'custom') {
					$additional = unserialize($order_item->additional);
					$product_data['cptype'] = $additional['cptype'];
					$product_data['weight'] = $additional['weight'];
					$product_data['sboxes'] = $additional['sboxes'];
					$product_data['shipping_cost'] = $additional['shipping_cost'];
				}
				$products[$product_key] = $product_data;
			}
		}
		$send_quote_data['products'] = $products;

		print_products_send_quote_set_order_data($send_quote_data);
	}
}

function print_products_send_quote_order_email($order_id, $email_subject, $email_message) {
	global $wpdb;

	$send_quote_data = print_products_send_quote_get_order_data();
	$print_products_send_quote_options = get_option("print_products_send_quote_options");

	$order_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_quotes WHERE order_id = '%s'", $wpdb->prefix, $order_id));
	if ($order_data) {
		$customer_id = $order_data->user_id;
		$customer_data = get_userdata($customer_id);

		$pay_now_text = __('Pay now', 'wp2print');
		if (strlen($print_products_send_quote_options['pay_now_text'])) {
			$pay_now_text = $print_products_send_quote_options['pay_now_text'];
		}

		$pay_now_link = '<a href="'.site_url('?qorder=true&uid='.md5($customer_id).'&oid='.md5($order_id)).'" style="background:#0085ba; border:1px solid #006799; border-color:#0073aa #006799 #006799; border-radius:3px; color:#fff; font-size:13px; line-height:26px; height:26px; text-decoration:none;font-family:Arial; display:inline-block; padding:0 10px 1px;">'.$pay_now_text.'</a>';

		$quote_detail = print_products_send_quote_quote_detail($order_id, $order_data);

		$email_subject = str_ireplace('{QUOTEID}', $order_id, $email_subject);

		$email_message = '<p>&nbsp;</p><p style="font-size:13px;">'.__('Customer', 'wp2print').': <strong>'.$customer_data->first_name.' '.$customer_data->last_name.' ('.$customer_data->user_email.')</strong></p>'.$email_message;
		$email_message = str_ireplace('{QUOTE-DETAIL}', $quote_detail, $email_message);
		$email_message = str_ireplace('{PAY-NOW-LINK}', $pay_now_link, $email_message);
		$email_message = str_ireplace('{QUOTEID}', $order_id, $email_message);
		$email_message = str_ireplace('{REFERENCE}', $order_data->reference, $email_message);

		$email_message = print_products_send_quote_billing_info($customer_id, $email_message);

		if (isset($send_quote_data['sender']) && strlen($send_quote_data['sender'])) {
			add_filter('wp_mail_from', function($email_from){
				$send_quote_data = print_products_send_quote_get_order_data();
				return $send_quote_data['sender'];
			}, 11);
		}

		$user_email = get_user_meta($customer_id, 'billing_email', true);
		if (!strlen($user_email)) { $user_email = $customer_data->user_email; }

		print_products_send_wc_mail($user_email, $email_subject, $email_message);
		if (strlen($print_products_send_quote_options['bcc_email'])) {
			print_products_send_wc_mail($print_products_send_quote_options['bcc_email'], $email_subject, $email_message);
		}
	}
}

function print_products_send_quote_billing_info($customer_id, $email_message) {
	$billing_infos = array(
		'{billing-first-name}' => get_user_meta($customer_id, 'billing_first_name', true),
		'{billing-last-name}'  => get_user_meta($customer_id, 'billing_last_name', true),
		'{billing-company}'    => get_user_meta($customer_id, 'billing_company', true),
		'{billing-address1}'   => get_user_meta($customer_id, 'billing_address_1', true),
		'{billing-address2}'   => get_user_meta($customer_id, 'billing_address_2', true),
		'{billing-city}'       => get_user_meta($customer_id, 'billing_city', true),
		'{billing-state}'      => get_user_meta($customer_id, 'billing_state', true),
		'{billing-zip-code}'   => get_user_meta($customer_id, 'billing_postcode', true)
	);

	foreach($billing_infos as $bi_key => $bi_val) {
		if (strlen($bi_val)) {
			$email_message = str_replace($bi_key, $bi_val, $email_message);
		} else {
			$email_message = str_replace($bi_key.'<br />'."\r\n", '', $email_message);
			$email_message = str_replace($bi_key.' ', '', $email_message);
			$email_message = str_replace($bi_key, '', $email_message);
		}
	}

	return $email_message;
}

function print_products_send_quote_order_process() {
	global $wpdb;

	$uid = $_GET['uid'];
	$oid = $_GET['oid'];

	if (strlen($uid) && strlen($oid)) {
		$user_data = $wpdb->get_row(sprintf("SELECT * FROM %s WHERE MD5(ID) = '%s'", $wpdb->users, $uid));
		$order_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_quotes WHERE MD5(order_id) = '%s'", $wpdb->prefix, $oid));
		if ($user_data && $order_data) {
			$order_id = $order_data->order_id;
			if (strlen($order_data->expire_date)) {
				$expire_date = strtotime($order_data->expire_date);
				if ($expire_date < time()) {
					$print_products_send_quote_options = get_option("print_products_send_quote_options");
					wp_die(nl2br($print_products_send_quote_options['expired_message']));
				}
			}
			// login user
			$user_id = $order_data->user_id;
			$user_data = get_userdata($user_id);
			$user_login = $user_data->user_login;
			wp_set_current_user($user_id, $user_login);
			wp_set_auth_cookie($user_id);
			do_action('wp_login', $user_login, $user);

			// add product to cart
			WC()->cart->empty_cart();
			$order_items = print_products_send_quote_get_order_items($order_id);
			if ($order_items) {
				foreach($order_items as $oikey => $order_item) {
					$product_id = $order_item->product_id;
					$product_type = $order_item->product_type;
					$quantity = $order_item->quantity;
					$additional = unserialize($order_item->additional);

					if ($product_type == 'variable') { $product_id = $order_item->variation_id; }

					$_REQUEST['print_products_checkout_process_action'] = 'add-to-cart';
					$_REQUEST['product_type'] = $product_type;
					$_REQUEST['product_id'] = $product_id;
					$_REQUEST['add-to-cart'] = $product_id;
					$_REQUEST['quantity'] = $quantity;
					$_REQUEST['smparams'] = $order_item->smparams;
					$_REQUEST['fmparams'] = $order_item->fmparams;
					$_REQUEST['atcaction'] = 'artwork';
					$_REQUEST['artworkfiles'] = $order_item->artworkfiles;
					$_REQUEST['price'] = $order_item->price;
					$_REQUEST['pprice'] = $order_item->price;

					if (isset($additional['pspeed'])) {
						$_REQUEST['production_speed'] = $additional['pspeed'];
					}

					switch ($product_type) {
						case 'book':
							if ($additional['page_quantity'] && is_array($additional['page_quantity'])) {
								foreach($additional['page_quantity'] as $mtype_id => $pq) {
									$_REQUEST['page_quantity_'.$mtype_id] = $pq;
								}
							}
						break;
						case 'area':
							$_REQUEST['width'] = $order_item->width;
							$_REQUEST['height'] = $order_item->height;
						break;
						case 'sticker':
							$_REQUEST['width'] = $order_item->width;
							$_REQUEST['height'] = $order_item->height;
						break;
						case 'box':
							$_REQUEST['width'] = $order_item->width;
							$_REQUEST['height'] = $order_item->height;
							$_REQUEST['length'] = $order_item->length;
						break;
						case 'custom':
							$_REQUEST['product_attributes'] = $order_item->product_attributes;
							$_REQUEST['shipping_specify'] = $additional['shipping_specify'];
							$_REQUEST['weight'] = $additional['weight'];
							$_REQUEST['sboxes'] = $additional['sboxes'];
							$_REQUEST['shipping_cost'] = $additional['shipping_cost'];
							$_REQUEST['cptype'] = $additional['cptype'];
						break;
						case 'variable':
							$_REQUEST['variation_id'] = $order_item->variation_id;
						break;
					}

					$cart_item_data = array();
					$cart_item_data['unique_key'] = md5(microtime() . rand() . md5($product_id));
					$cart_item_data['sq_order_id'] = $order_id;
					if ($order_data->discount && $oikey == 0) {
						$cart_item_data['sqdiscount'] = $order_data->discount;
					}
					WC()->cart->add_to_cart($product_id, $quantity, 0, array(), $cart_item_data);
				}
			}

			// redirect to checkout page
			$checkout_url = WC()->cart->get_checkout_url();
			wp_redirect($checkout_url);
			exit;
		}
	}
}

add_action('woocommerce_checkout_order_processed', 'print_products_send_quote_woocommerce_checkout_order_processed');
function print_products_send_quote_woocommerce_checkout_order_processed($order_id) {
	global $wpdb;
	$cart_contents = WC()->cart->cart_contents;
	if ($cart_contents && count($cart_contents) == 1) {
		foreach ($cart_contents as $cart_item_key => $cart_item) {
			if (isset($cart_item['sq_order_id']) && $cart_item['sq_order_id']) {
				// update quote status
				$wpdb->update($wpdb->prefix.'print_products_quotes', array('status' => '1', 'wc_order_id' => $order_id), array('order_id' => $cart_item['sq_order_id']));
			}
		}
	}
}

function print_products_send_quote_object_to_array($product_data) {
	return array(
		'product_id' => $product_data->product_id,
		'product_type' => $product_data->product_type,
		'variation_id' => $product_data->variation_id,
		'attributes' => $product_data->attributes,
		'quantity' => $product_data->quantity,
		'price' => $product_data->price,
		'smparams' => $product_data->smparams,
		'fmparams' => $product_data->fmparams,
		'width' => $product_data->width,
		'height' => $product_data->height,
		'length' => $product_data->length,
		'product_attributes' => $product_data->product_attributes,
		'additional' => $product_data->additional,
		'artworkfiles' => $product_data->artworkfiles
	);
}

function print_products_send_quote_product_data_html($product_data) {
	global $wpdb;
	if (is_object($product_data)) { $product_data = print_products_send_quote_object_to_array($product_data); }
	$product_id = $product_data['product_id'];
	$product_type = $product_data['product_type'];
	$dimension_unit = print_products_get_dimension_unit();
	$attribute_labels = (array)get_post_meta($product_id, '_attribute_labels', true);
	$attribute_display = (array)get_post_meta($product_id, '_attribute_display', true);
	$product_attributes = unserialize($product_data['product_attributes']);
	$additional = unserialize($product_data['additional']); ?>
	<ul style="margin-bottom:0px;">
		<?php if ($product_type == 'area' || $product_type == 'sticker') {
			echo '<li>'.print_products_attribute_label('width', $attribute_labels, __('Width', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['width'].'</strong></li>';
			echo '<li>'.print_products_attribute_label('height', $attribute_labels, __('Height', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['height'].'</strong></li>';
		} else if ($product_type == 'box') {
			echo '<li>'.print_products_attribute_label('width', $attribute_labels, __('Width', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['width'].'</strong></li>';
			echo '<li>'.print_products_attribute_label('height', $attribute_labels, __('Height', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['height'].'</strong></li>';
			echo '<li>'.print_products_attribute_label('length', $attribute_labels, __('Length', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['length'].'</strong></li>';
		}
		if ($product_attributes) {
			$product_attributes = print_products_quantity_mailed_product_attributes($product_attributes, $additional);
			$attr_terms = print_products_get_attributes_vals($product_attributes, $product_type, $attribute_labels, $attribute_display);
			echo '<li>'.implode('</li><li>', $attr_terms).'</li>';
		}
		if ($product_type == 'custom') {
			if ($product_data['product_attributes']) { echo '<li>'.nl2br($product_data['product_attributes']).'</li>'; }
			if ($product_data['additional']) {
				$additional = unserialize($product_data['additional']);
				if ($additional['weight']) { echo '<li>'.__('Weight', 'wp2print').' ('.print_products_get_weight_unit().'): <strong>'.$additional['weight'].'</strong></li>'; }
				if ($additional['sboxes']) { echo '<li>'.__('Shipping box count', 'wp2print').': <strong>'.$additional['sboxes'].'</strong></li>'; }
				if ($additional['shipping_cost']) { echo '<li>'.__('Shipping cost', 'wp2print').': <strong>'.wc_price($additional['shipping_cost']).'</strong></li>'; }
			}
		}
		if ($product_type == 'variable') {
			$attribute_names = array();
			$wc_attributes = $wpdb->get_results(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies ORDER BY attribute_order, attribute_label", $wpdb->prefix));
			if ($wc_attributes) {
				foreach($wc_attributes as $wc_attribute) {
					$attribute_names[$wc_attribute->attribute_name] = $wc_attribute->attribute_label;
				}
			}

			if (isset($product_data['variation_id']) && $product_data['variation_id']) {
				$variation_id = $product_data['variation_id'];
				$product = wc_get_product($variation_id);
				if ($product) {
					$attributes = $product->get_attributes();
					if ($attributes) {
						foreach($attributes as $akey => $aval) {
							$aname = str_replace('pa_', '', $akey);
							echo '<li>'.$attribute_names[$aname].': <strong>'.$product->get_attribute($akey).'</strong></li>';
						}
					}
				}
			}
		}
		if (isset($additional['pspeed']) && strlen($additional['pspeed']) && strlen($additional['pspeed_label'])) {
			$pspeed_label = explode(';', $additional['pspeed_label']);
			$pspeed_value = explode(' - ', $pspeed_label[1]);
			echo '<li>'.$pspeed_label[0].': <strong>'.$pspeed_value[0].'</strong></li>';
		}
		if (isset($product_data['artworkfiles']) && strlen($product_data['artworkfiles'])) {
			$afiles = array();
			$artworkfiles = explode(';', $product_data['artworkfiles']);
			foreach($artworkfiles as $afile) {
				$afiles[] = basename($afile);
			}
			echo '<li>'.__('Artwork Files', 'wp2print').': <strong>'.implode(', ', $afiles).'</strong></li>';
		}
		?>
	</ul>
	<?php
}

function print_products_send_quote_quote_detail($order_id, $order_data) {
	global $wpdb;
	$quote_detail = '<div style="font-size:13px;">';
	$quote_detail .= '-------------------------------------------------------------------------<br>';

	$products = print_products_send_quote_get_order_items($order_id);
	if ($products) {
		foreach($products as $product) {
			$product_id = $product->product_id;
			$product_data = array(
				'product_type' => $product->product_type,
				'quantity' => $product->quantity,
				'price' => $product->price,
				'smparams' => $product->smparams,
				'fmparams' => $product->fmparams,
				'width' => $product->width,
				'height' => $product->height,
				'length' => $product->length,
				'product_attributes' => $product->product_attributes,
				'additional' => $product->additional,
				'attributes' => $product->attributes,
				'artworkfiles' => $product->artworkfiles,
				'variation_id' => $product->variation_id
			);

			$product_type = $product_data['product_type'];
			$dimension_unit = print_products_get_dimension_unit();
			$attribute_labels = (array)get_post_meta($product_id, '_attribute_labels', true);
			$attribute_display = (array)get_post_meta($product_id, '_attribute_display', true);
			$product_attributes = unserialize($product_data['product_attributes']);
			$additional = unserialize($product_data['additional']);
			$product_name = get_the_title($product_id);
			if (print_products_is_custom_product($product_id) && $additional['cptype'] && strlen($additional['cptype'])) {
				$product_name = $additional['cptype'];
			}

			$quote_detail .= __('Product', 'wp2print').': <strong>'.$product_name.'</strong><br>';
			if ($product_type == 'custom') {
				if ($product_data['product_attributes']) { $quote_detail .= nl2br($product_data['product_attributes']).'<br>'; }
				$quote_detail .= __('Quantity', 'wp2print').': <strong>'.$product_data['quantity'].'</strong><br>';
				if ($additional['weight']) { $quote_detail .= __('Weight', 'wp2print').' ('.print_products_get_weight_unit().'): <strong>'.$additional['weight'].'</strong><br>'; }
				if ($additional['sboxes']) { $quote_detail .= __('Shipping box count', 'wp2print').': <strong>'.$additional['sboxes'].'</strong><br>'; }
			} else {
				$quote_detail .= __('Quantity', 'wp2print').': <strong>'.$product_data['quantity'].'</strong><br>';
			}
			if ($product_type == 'area' || $product_type == 'sticker') {
				$quote_detail .= print_products_attribute_label('width', $attribute_labels, __('Width', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['width'].'</strong><br>';
				$quote_detail .= print_products_attribute_label('height', $attribute_labels, __('Height', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['height'].'</strong><br>';
			}
			if ($product_type == 'box') {
				$quote_detail .= print_products_attribute_label('width', $attribute_labels, __('Width', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['width'].'</strong><br>';
				$quote_detail .= print_products_attribute_label('height', $attribute_labels, __('Height', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['height'].'</strong><br>';
				$quote_detail .= print_products_attribute_label('length', $attribute_labels, __('Length', 'wp2print')).' ('.$dimension_unit.'): <strong>'.$product_data['length'].'</strong><br>';
			}
			if ($product_attributes) {
				$attr_terms = print_products_get_attributes_vals($product_attributes, $product_type, $attribute_labels, $attribute_display);
				$quote_detail .= implode('<br>', $attr_terms).'<br>';
			}
			if (isset($additional['pspeed']) && strlen($additional['pspeed']) && strlen($additional['pspeed_label'])) {
				$pspeed_label = explode(';', $additional['pspeed_label']);
				$pspeed_value = explode(' - ', $pspeed_label[1]);
				$quote_detail .= $pspeed_label[0].': <strong>'.$pspeed_value[0].'</strong><br>';
			}
			if ($product_type == 'variable') {
				$attribute_names = array();
				$wc_attributes = $wpdb->get_results(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies ORDER BY attribute_order, attribute_label", $wpdb->prefix));
				if ($wc_attributes) {
					foreach($wc_attributes as $wc_attribute) {
						$attribute_names[$wc_attribute->attribute_name] = $wc_attribute->attribute_label;
					}
				}

				$variation_id = $product_data['variation_id'];
				$product = wc_get_product($variation_id);
				$attributes = $product->get_attributes();
				if ($attributes) {
					foreach($attributes as $akey => $aval) {
						$aname = str_replace('pa_', '', $akey);
						$quote_detail .= $attribute_names[$aname].': <strong>'.$product->get_attribute($akey).'</strong><br>';
					}
				}
			}
			if (strlen($product_data['artworkfiles'])) {
				$afiles = array();
				$artworkfiles = explode(';', $product_data['artworkfiles']);
				foreach($artworkfiles as $afile) {
					$afiles[] = basename($afile);
				}
				$quote_detail .= __('Artwork Files', 'wp2print').': <strong>'.implode(', ', $afiles).'</strong><br>';
			}
			$quote_detail .= __('Subtotal', 'wp2print').': <strong>'.wc_price($product_data['price']).'</strong><br>';
			if (isset($additional['shipping_cost']) && $additional['shipping_cost']) { $quote_detail .= __('Shipping cost', 'wp2print').': <strong>'.wc_price($additional['shipping_cost']).'</strong><br>'; }
			$quote_detail .= '-------------------------------------------------------------------------<br>';
		}
	}
	if ($order_data->discount) {
		$quote_detail .= __('Discount', 'wp2print').': <strong>'.wc_price($order_data->discount).'</strong><br>';
	}
	$quote_detail .= '</div>';

	return $quote_detail;
}

add_action('init', 'print_products_send_quote_cron_job');
function print_products_send_quote_cron_job() {
	if (!wp_next_scheduled('print_products_send_quote_cron')) {
		wp_schedule_event(mktime(6, 0, 0, date("m"), date("d"), date("Y")), 'daily', 'print_products_send_quote_cron');
	}
}

add_action('print_products_send_quote_cron', 'print_products_send_quote_cron_actions');
function print_products_send_quote_cron_actions() {
	global $wpdb;
	$cdate = date('Y-m-d');
	$ppsqc_date = get_option('ppsqc_date');
	$print_products_send_quote_options = get_option("print_products_send_quote_options");
	if ($ppsqc_date != $cdate && ((int)$print_products_send_quote_options['send_email2'] || (int)$print_products_send_quote_options['send_email3'])) {
		$send_email2_days = (int)$print_products_send_quote_options['send_email2_days'];
		if ((int)$print_products_send_quote_options['send_email2'] && $send_email2_days) {
			$email_subject2 = $print_products_send_quote_options['email_subject2'];
			$email_message2 = $print_products_send_quote_options['email_message2'];
			$adate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - $send_email2_days, date('Y')));
			$quote_orders = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_quotes WHERE status = 0 AND created >= '%s' AND created <= '%s' ORDER BY order_id", $wpdb->prefix, $adate.' 00:00:00', $adate.' 23:59:59'));
			if ($quote_orders) {
				foreach($quote_orders as $quote_order) { if (!$quote_order->wc_order_id) {
					print_products_send_quote_order_email($quote_order->order_id, $email_subject2, $email_message2);
				}}
			}
		}

		$send_email3_days = (int)$print_products_send_quote_options['send_email3_days'];
		if ((int)$print_products_send_quote_options['send_email3'] && $send_email3_days) {
			$email_subject3 = $print_products_send_quote_options['email_subject3'];
			$email_message3 = $print_products_send_quote_options['email_message3'];
			$adate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - $send_email3_days, date('Y')));
			$quote_orders = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_quotes WHERE status = 0 AND created >= '%s' AND created <= '%s' ORDER BY order_id", $wpdb->prefix, $adate.' 00:00:00', $adate.' 23:59:59'));
			if ($quote_orders) {
				foreach($quote_orders as $quote_order) { if (!$quote_order->wc_order_id) {
					print_products_send_quote_order_email($quote_order->order_id, $email_subject3, $email_message3);
				}}
			}
		}
		update_option('ppsqc_date', $cdate);
	}
}

add_action('woocommerce_admin_order_totals_after_discount', 'print_products_send_quote_woocommerce_admin_order_totals_after_discount');
function print_products_send_quote_woocommerce_admin_order_totals_after_discount($order_id) {
	$order = wc_get_order($order_id);
	$fees = $order->get_fees();
	if ($fees) {
		foreach($fees as $fee) {
			$fee_name = $fee->get_name();
			$fee_amount = $fee->get_amount();
			if ($fee_name == __('Discount', 'wp2print')) {
				?>
				<tr>
					<td class="label"><?php echo $fee_name; ?>:</td>
					<td width="1%"></td>
					<td class="total"><?php echo wc_price($fee_amount); ?></td>
				</tr>
				<?php
			}
		}
	}
}

function print_products_send_quote_get_tax_rate($product_id, $user_id) {
	$tax_rate = 0;
	$tax_class = get_post_meta($product_id, '_tax_class', true);
	$tax_rates = WC_Tax::get_rates($tax_class);
	if (!$tax_rates) {
		$customer_shipping_address = print_products_send_quote_get_customer_address($user_id, 'shipping');
		if (strlen($customer_shipping_address['country']) && strlen($customer_shipping_address['state'])) {
			$args = array(
				'country' => $customer_shipping_address['country'],
				'state' => $customer_shipping_address['state'],
				'city' => $customer_shipping_address['city'],
				'postcode' => $customer_shipping_address['postcode']
			);
			$tax_rates = WC_Tax::find_rates($args);
		}
	}
	if ($tax_rates) {
		$defrate = current($tax_rates);
		$tax_rate = (float)$defrate['rate'];
	}
	return $tax_rate;
}
?>