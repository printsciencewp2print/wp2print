<?php
$is_paybill = false;
$is_paybill_processed = false;
function print_products_paybill_is_paybill() {
	global $is_paybill, $is_paybill_processed;
	if (!$is_paybill_processed) {
		$is_paybill_processed = true;
		if (is_object($cart_contents = WC()->cart)) {
			$cart_contents = WC()->cart->cart_contents;
			if ($cart_contents && count($cart_contents) == 1) {
				foreach ($cart_contents as $cart_item_key => $cart_item) {
					$product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
					if ($product->get_type() == 'paybill') {
						$is_paybill = true;
					}
				}
			}
		}
	}
	return $is_paybill;
}

add_filter('woocommerce_add_to_cart_redirect', 'print_products_paybill_woocommerce_add_to_cart_redirect', 11, 2);
function print_products_paybill_woocommerce_add_to_cart_redirect($redirect, $product) {
	if ($product) {
		if ($product->get_type() == 'paybill') {
			$redirect = wc_get_checkout_url();
		}
	}
	return $redirect;
}

add_filter('woocommerce_cart_item_name', 'print_products_paybill_woocommerce_checkout_cart_item_name', 11, 3);
function print_products_paybill_woocommerce_checkout_cart_item_name($item_name, $cart_item, $cart_item_key) {
	global $wpdb;
	$product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
	if ($product->get_type() == 'paybill') {
		$item_name = '';
		$prod_cart_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_cart_data WHERE cart_item_key = '%s'", $wpdb->prefix, $cart_item_key));
		if ($prod_cart_data) {
			if (strlen($prod_cart_data->additional)) {
				$additional = unserialize($prod_cart_data->additional);
				if (isset($additional['invoice_number']) && strlen($additional['invoice_number'])) {
					$item_name = $additional['invoice_number_label'].': <strong>'.$additional['invoice_number'].'</strong>';
				}
			}
		}
	}
	return $item_name;
}

add_filter('woocommerce_checkout_cart_item_quantity', 'print_products_paybill_woocommerce_checkout_cart_item_quantity', 11, 3);
function print_products_paybill_woocommerce_checkout_cart_item_quantity($value, $cart_item, $cart_item_key) {
	$product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
	if ($product->get_type() == 'paybill') {
		$value = '';
	}
	return $value;
}

add_filter('woocommerce_order_button_text', 'print_products_paybill_woocommerce_order_button_text');
function print_products_paybill_woocommerce_order_button_text($button_text) {
	if (print_products_paybill_is_paybill()) {
		$button_text = __('Complete payment', 'wp2print');
	}
	return $button_text;
}

add_action('woocommerce_after_checkout_form', 'print_products_paybill_woocommerce_after_checkout_form');
function print_products_paybill_woocommerce_after_checkout_form() {
	if (print_products_paybill_is_paybill()) { ?>
		<script>jQuery('#order_review_heading').html('<?php _e('Your payment', 'wp2print'); ?>');</script>
		<?php
	}
}

// Payment only order status
add_action('init', 'print_products_paybill_register_payment_only_order_status');
function print_products_paybill_register_payment_only_order_status() {
	register_post_status('wc-payment-only', array(
		'label'                     => __('Payment only', 'wp2print'),
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop(__('Payment only (%s)', 'wp2print'), __('Payment only (%s)', 'wp2print'))
	) );
}

add_filter('wc_order_statuses', 'print_products_paybill_wc_order_statuses', 11);
function print_products_paybill_wc_order_statuses($statuses) {
	$statuses['wc-payment-only'] = __('Payment only', 'wp2print');
	return $statuses;
}

function print_products_paybill_is_paybill_order($order) {
	$is_paybill_order = false;
	if ($order) {
		$order_items = $order->get_items('line_item');
		if ($order_items && count($order_items) == 1) {
			foreach($order_items as $item_id => $order_item) {
				$product = $order_item->get_product();
				$rp_item = (int)wc_get_order_item_meta($item_id, '_rp_item', true);
				if ($product->get_type() == 'paybill' || $rp_item == '1') {
					$is_paybill_order = true;
				}
			}
		}
	}
	return $is_paybill_order;
}

add_action('woocommerce_thankyou', 'print_products_paybill_woocommerce_checkout_order_processed');
function print_products_paybill_woocommerce_checkout_order_processed($order_id) {
	global $wpdb;
	$order = wc_get_order($order_id);
	if (print_products_paybill_is_paybill_order($order)) {
		$wpdb->update($wpdb->prefix.'posts', array('post_status' => 'wc-payment-only'), array('ID' => $order_id));
		?>
		<style>.order_item .print-products-area .product-attributes-list{margin:0;}</style>
		<script>
		var html = jQuery('.woocommerce-order-overview__order').html();
		html = html.replace('<?php _e('Order number:', 'woocommerce'); ?>', '<?php _e('Payment number:', 'wp2print'); ?>');
		jQuery('.woocommerce-order-overview__order').html(html);
		jQuery('.woocommerce-order-details__title').html('<?php _e('Payment details', 'wp2print'); ?>');
		
		</script>
		<?php
	}
}

// replace Order word on thank-you page
add_filter('woocommerce_thankyou_order_received_text', 'print_products_paybill_woocommerce_thankyou_order_received_text', 11, 2);
function print_products_paybill_woocommerce_thankyou_order_received_text($order_received_text, $order) {
	if (print_products_paybill_is_paybill_order($order)) {
		$order_received_text = str_replace(__(' order ', 'woocommerce'), __(' payment ', 'wp2print'), $order_received_text);
	}
	return $order_received_text;
}

add_action('woocommerce_endpoint_order-received_title', 'print_products_paybill_woocommerce_endpoint_order_title');
function print_products_paybill_woocommerce_endpoint_order_title($title) {
	global $wp_query;
	if (isset($wp_query->query['order-received'])) {
		$order_id = $wp_query->query['order-received'];
		$order = wc_get_order($order_id);
		if (print_products_paybill_is_paybill_order($order)) {
			$title = __('Payment received', 'wp2print');
		}
	}
	return $title;
}


add_filter('woocommerce_order_item_name', 'print_products_paybill_woocommerce_order_item_name', 11, 2);
add_filter('woocommerce_order_item_quantity_html', 'print_products_paybill_woocommerce_order_item_name', 11, 2);
function print_products_paybill_woocommerce_order_item_name($value, $order_item) {
	$product = $order_item->get_product();
	if ($product && $product->get_type() == 'paybill') {
		$value = '';
	}
	return $value;
}

// replace Order word with Payment
add_filter('woocommerce_email_subject_new_order', 'print_products_paybill_woocommerce_email_subject', 11, 2);
add_filter('woocommerce_email_subject_customer_processing_order', 'print_products_paybill_woocommerce_email_subject', 11, 2);
add_filter('woocommerce_email_subject_customer_on_hold_order', 'print_products_paybill_woocommerce_email_subject', 11, 2);
add_filter('woocommerce_email_heading_customer_processing_order', 'print_products_paybill_woocommerce_email_subject', 11, 2);
add_filter('woocommerce_email_heading_customer_on_hold_order', 'print_products_paybill_woocommerce_email_subject', 11, 2);
function print_products_paybill_woocommerce_email_subject($value, $order) {
	if (print_products_paybill_is_paybill_order($order)) {
		$value = str_ireplace(__('Order', 'woocommerce'), __('Payment', 'wp2print'), $value);
	}
	return $value;
}

$is_paybill_email = false;
add_action('woocommerce_before_template_part', 'print_products_paybill_woocommerce_before_template_part', 11, 4);
function print_products_paybill_woocommerce_before_template_part($template_name, $template_path, $located, $args) {
	global $is_paybill_email;
	$allowed = array('emails/customer-processing-order.php', 'emails/customer-on-hold-order.php', 'emails/admin-new-order.php');
	if (in_array($template_name, $allowed)) {
		$order = $args['order'];
		if (print_products_paybill_is_paybill_order($order)) {
			$is_paybill_email = true;
		}
	}
}

add_filter('woocommerce_mail_content', 'print_products_paybill_woocommerce_mail_content');
function print_products_paybill_woocommerce_mail_content($mail_content) {
	global $is_paybill_email;
	if ($is_paybill_email) {
		$mail_content = str_ireplace(__('Order', 'woocommerce'), __('Payment', 'wp2print'), $mail_content);
	}
	return $mail_content;
}
?>