<?php
add_action('wp_loaded', 'print_products_admin_actions_init');
function print_products_admin_actions_init() {
	global $wpdb;
	if (isset($_POST['eddm_update_item']) && $_POST['eddm_update_item'] == 'true') {
		$item_id = (int)$_POST['item_id'];
		if ($item_id) {
			$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
			if ($order_item_data) {
				$additional = unserialize($order_item_data->additional);
				$additional['eddm_finished'] = true;
				$wpdb->update($wpdb->prefix.'print_products_order_items', array('additional' => serialize($additional)), array('item_id' => $item_id));
			}
		}
		exit;
	}
	if (isset($_GET['order_download_zip']) && $_GET['order_download_zip'] == 'true' && isset($_GET['item_id']) && $_GET['item_id']) {
		$order_id = $_GET['order_id'];
		$item_id = $_GET['item_id'];
		$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
		if ($order_item_data) {
			$artwork_files = unserialize($order_item_data->artwork_files);
			if ($artwork_files) {
				$zip_filename = 'artwork-order-'.$order_id.'-item-'.$item_id.'.zip';
				$amazon_s3_settings = get_option('print_products_amazon_s3_settings');
				$download_zip_api_url = get_option('print_products_download_zip_api_url');
				$amazon_url = 'https://'.$amazon_s3_settings['s3_bucketname'].'.s3.amazonaws.com/';
				if (strlen($amazon_s3_settings['s3_region'])) {
					$amazon_url = 'https://'.$amazon_s3_settings['s3_bucketname'].'.s3-'.$amazon_s3_settings['s3_region'].'.amazonaws.com/';
				}
				$zip_url = print_products_get_amazon_file_url($amazon_url . $zip_filename);
				foreach($artwork_files as $fkey => $fval) { $artwork_files[$fkey] = substr($fval, strpos($fval, 'amazonaws.com/') + 14); }
				$api_data = array(
					'files' => $artwork_files,
					'bucketname' => $amazon_s3_settings['s3_bucketname'],
					'region' => $amazon_s3_settings['s3_region'],
					'access_key' => $amazon_s3_settings['s3_access_key'],
					'secret_key' => $amazon_s3_settings['s3_secret_key'],
					'output_filename' => $zip_filename
				);
				if ($download_zip_api_url) {
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($api_data));
					curl_setopt($ch, CURLOPT_URL, $download_zip_api_url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$results = json_decode(curl_exec($ch));
					curl_close($ch);

					if (is_object($results) && isset($results->statusCode) && $results->statusCode == 200) {
						header('Location: '.$zip_url);
						exit;
					}
				}
			}
		}
	}
}

add_filter('product_type_selector', 'print_products_selector_matrix');
function print_products_selector_matrix($sarray) {
	$product_types = print_products_get_product_types();
	foreach($product_types as $tpkey => $tpname) {
		$sarray[$tpkey] = $tpname;
	}
	return $sarray;
}

add_action('woocommerce_delete_order_item', 'print_products_delete_order_item', 10, 1);
function print_products_delete_order_item($item_id) {
	global $wpdb;
	$wpdb->delete($wpdb->prefix."print_products_order_items", array('item_id' => $item_id));
}

$woocommerce_disable_product_list_price = get_option('woocommerce_disable_product_list_price');
add_filter('wc_get_template', 'print_products_woo_get_template', 10, 2);
function print_products_woo_get_template($located, $template_name) {
	global $product, $woocommerce_disable_product_list_price;
	$print_type = false;
	$product_type = '';
	$artwork_source = false;
	if (is_single() && $product && is_object($product)) {
		$product_type = print_products_get_type($product->get_id());
		$artwork_source = get_post_meta($product->get_id(), '_artwork_source', true);
		if (print_products_is_wp2print_type($product_type)) { $print_type = true; }
	}

	$ptemplate = '';
	switch ($template_name) {
		case 'loop/price.php':
			if ($woocommerce_disable_product_list_price == 'yes') {
				$ptemplate = 'empty.php';
			} else if ($product) {
				if (print_products_is_product_hidden_price($product->get_id())) {
					$ptemplate = 'nl-login.php';
				}
			}
		break;
		case 'single-product/product-image.php':
			if ($product_type == 'aec' || $product_type == 'aecbwc' || $product_type == 'aecsimple') {
				$ptemplate = 'product-type-aec-upload.php';
			} else if ($product_type == 'book') {
				$product_book_type = (int)get_post_meta($product->get_id(), '_book_type', true);
				if ($product_book_type > 0) {
					$ptemplate = 'product-type-book-upload.php';
				}
			}
		break;
		case 'single-product/price.php':
			if ($print_type) {
				$ptemplate = 'empty.php';
			}
		break;
		case 'single-product/add-to-cart/simple.php':
			if ($artwork_source) {
				$ptemplate = 'simple.php';
			}
			if ($print_type) {
				$ptemplate = 'product-type-'.$product_type.'.php';
			}
		break;
		case 'single-product/add-to-cart/variable.php':
			if ($artwork_source) {
				$ptemplate = 'variable.php';
			}
			if ($print_type) {
				$ptemplate = 'product-type-'.$product_type.'.php';
			}
		break;
		case 'cart/cart.php':
			if (!print_products_designer_installed()) {
				$ptemplate = 'cart.php';
			}
		break;
		case 'checkout/review-order.php':
			$ptemplate = 'review-order.php';
		break;
		case 'order/order-details.php':
			$ptemplate = 'order-details.php';
		break;
		case 'order/order-details-item.php':
			$ptemplate = 'order-details-item.php';
		break;
		case 'emails/email-order-items.php':
			$ptemplate = 'email-order-items.php';
		break;
		case 'emails/email-addresses.php':
			$ptemplate = 'email-addresses.php';
		break;
		case 'myaccount/view-order.php':
			$ptemplate = 'view-order.php';
		break;
	}
	if (strlen($ptemplate)) {
		$located = PRINT_PRODUCTS_TEMPLATES_DIR.$ptemplate;
	}
	return $located;
}

$wp2print_ptypes = print_products_get_product_types();
foreach($wp2print_ptypes as $ptype => $ptname) {
	add_action('woocommerce_'.$ptype.'_add_to_cart', 'print_products_add_to_cart_template');
}
function print_products_add_to_cart_template() {
	global $product;
	$product_type = print_products_get_type($product->get_id());
	$template = 'product-type-'.$product_type.'.php';
	include PRINT_PRODUCTS_TEMPLATES_DIR . $template;
}

add_filter('woocommerce_product_get_price', 'print_products_woocommerce_product_get_price', 11, 2);
function print_products_woocommerce_product_get_price($price, $product) {
	if (!is_cart() && !is_checkout()) {
		$product_id = $product->get_id();
		$product_type = $product->get_type();
		if (print_products_is_wp2print_type($product_type)) {
			$price = print_products_get_min_price($product_id);
		}
	}
	return $price;
}

if (!function_exists('woocommerce_show_product_images')) {
function woocommerce_show_product_images() {
	global $product;
	$product_id = $product->get_id();
	$instruction_link_type = get_post_meta($product_id, '_instruction_link_type', true);
	$instruction_link_text = get_post_meta($product_id, '_instruction_link_text', true);
	$instruction_video_url = get_post_meta($product_id, '_instruction_video_url', true);

	if (strlen($instruction_link_type) && strlen($instruction_video_url)) { $nbsp = ''; ?>
		<div class="instruction-wrap" style="margin-bottom:15px;">
			<a href="<?php echo $instruction_video_url; ?>" target="_blank" title="<?php _e('Video instruction', 'wp2print'); ?>">
			<?php if ($instruction_link_type == 'help') { ?>
				<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/help.png">
			<?php } else if ($instruction_link_type == 'video') { $nbsp = '&nbsp;'; ?>
				<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/icon-video.png">
			<?php } ?>
			<?php if (strlen($instruction_link_text)) { echo '<span>'.$nbsp.$instruction_link_text.'</span>'; } ?>
			</a>
		</div>
		<?php
	}

	wc_get_template( 'single-product/product-image.php' );
}
}

// add unique key to each cart product
add_filter('woocommerce_add_cart_item_data', 'print_products_woocommerce_add_cart_item_data', 11, 2);
function print_products_woocommerce_add_cart_item_data($cart_item_data, $product_id) {
	if (!print_products_designer_installed()) {
		$cart_item_data['unique_key'] = md5(microtime() . rand() . md5($product_id));
	}
	return $cart_item_data;
}

add_filter('woocommerce_loop_add_to_cart_link', 'print_products_woo_loop_add_to_cart_link', 10, 2);
function print_products_woo_loop_add_to_cart_link($add_to_cart_link, $product) {
	global $print_products_plugin_options;
	if (isset($print_products_plugin_options['show_add_to_cart']) && $print_products_plugin_options['show_add_to_cart'] == 1) {
		return $add_to_cart_link;
	}
	return '';
}

// hide weight fields
add_filter('wc_product_weight_enabled', 'print_products_hide_woo_weight_field');
function print_products_hide_woo_weight_field($enabled) {
	global $wpdb, $print_products_settings;
	if (is_admin() && isset($_GET['post']) && $_GET['post'] && isset($_GET['action']) && $_GET['action'] == 'edit') {
		$ptype = print_products_get_type($_GET['post']);
		if (print_products_is_wp2print_type($ptype)) {
			$material_attribute = $print_products_settings['material_attribute'];
			$product_matrix_options = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s AND mtype = 0 ORDER BY sorder", $wpdb->prefix, $_GET['post']));
			if ($product_matrix_options) {
				foreach($product_matrix_options as $pmokey => $product_matrix_option) {
					$aterms = unserialize($product_matrix_option->aterms);
					$tmaterials = (isset($aterms[$material_attribute]) ? $aterms[$material_attribute] : '');
					if ($tmaterials) {
						$enabled = false;
					}
				}
			}
		}
	}
	return $enabled;
}

add_filter('wc_product_sku_enabled', 'print_products_hide_woo_sku_field');
function print_products_hide_woo_sku_field($enabled) {
	if (is_admin() && isset($_GET['post']) && $_GET['post'] && isset($_GET['action']) && $_GET['action'] == 'edit') {
		$ptype = print_products_get_type($_GET['post']);
		if (print_products_is_wp2print_type($ptype)) {
			$enabled = false;
		}
	}
	return $enabled;
}

add_action('woocommerce_before_order_itemmeta', 'print_products_before_order_itemmeta', 10, 2);
function print_products_before_order_itemmeta($item_id, $item) {
	global $wpdb, $print_products_plugin_options;
	$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
	if ($order_item_data) {
		$item_sku = print_products_get_item_sku($order_item_data, true);
		if (current_action() == 'wpo_wcpdf_before_item_meta') {
			if ($item_sku) {
				echo '<ul><li>'.__('SKU', 'woocommerce').': ' . esc_html($item_sku).'</li></ul>';
			}
			print_products_wcpdf_order_item_data($order_item_data);
		} else {
			if ($item_sku) {
				echo ' &ndash; (' . esc_html($item_sku) . ')';
			}
		}
	}
}

add_action('woocommerce_order_item_line_item_html', 'print_products_order_item_line_item_html', 10, 3);
function print_products_order_item_line_item_html($item_id, $item, $order) {
	global $wpdb, $print_products_plugin_options;
	$download_zip_api_url = get_option('print_products_download_zip_api_url'); ?>
	<tr>
		<td>&nbsp;</td>
		<td colspan="10"><div style="margin-top:-25px;">
		<?php
		$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
		if ($order_item_data) {
			print_products_product_attributes_list_html($order_item_data);
			$additional = unserialize($order_item_data->additional);
			$artwork_files = unserialize($order_item_data->artwork_files);
			$artwork_old_files = unserialize($order_item_data->artwork_old_files);
			$artwork_rejected_files = unserialize($order_item_data->artwork_rejected_files);
			$artwork_multi_files = unserialize($order_item_data->artwork_multi_files);
			$artwork_thumbs = unserialize($order_item_data->artwork_thumbs);
			if ($artwork_files) {
				$multi_files = array();
				if ($artwork_multi_files && is_array($artwork_multi_files)) {
					foreach($artwork_multi_files as $multi_file) {
						$multi_files[$multi_file['file']] = $multi_file['name'];
					}
				}
				?>
				<div class="print-products-area">
					<ul class="product-attributes-list">
						<?php if ($order_item_data->product_type == 'aec' || $order_item_data->product_type == 'aecbwc' || $order_item_data->product_type == 'aecsimple') { ?>
							<li><?php _e('Files', 'wp2print'); ?>:</li>
						<?php } else { ?>
							<?php if (print_products_is_all_text_files($artwork_files)) { ?>
								<li><?php _e('Files', 'wp2print'); ?>:</li>
							<?php } else { ?>
								<li><?php _e('Artwork Files', 'print-products'); ?>:</li>
							<?php } ?>
						<?php } ?>
						<li><ul class="product-artwork-files-list ftpfilenames oi-files-list">
							<?php foreach($artwork_files as $af_key => $artwork_file) {
								$mflabel = ($artwork_multi_files && is_array($artwork_multi_files) && count($artwork_multi_files) && isset($artwork_multi_files[$af_key]) && $artwork_multi_files[$af_key]['file'] == $artwork_file) ? $artwork_multi_files[$af_key]['name'].':&nbsp;' : '';
								echo '<li><i class="i-check"></i> '.$mflabel.' <a href="'.print_products_get_amazon_file_url($artwork_file).'" title="'.__('Download', 'wp2print').'" target="_blank">'.basename($artwork_file).'</a><span class="af-replace"> - <a href="#replace" class="afile-replace" onclick="return order_artwork_replace('.$item_id.', '.$af_key.');"><span>'.__('Replace file', 'wp2print').'</span></a></span></li>';
							} ?>
						</ul></li>
					</ul>
					<?php if (strlen($download_zip_api_url)) { ?>
						<a href="<?php echo admin_url('/?order_download_zip=true&order_id='.$order->get_id().'&item_id='.$item_id); ?>" class="button" style="margin-bottom:15px;"><?php _e('Download ZIP', 'wp2print'); ?></a>
					<?php } ?>
				</div>
				<div class="print-products-area">
					<a href="#add-additional-file" class="button" onclick="return order_artwork_add(<?php echo $item_id; ?>);"><?php _e('Add additional files', 'wp2print'); ?></a>
				</div>
				<?php
			} else {
				?>
				<div class="print-products-area">
					<a href="#add-file" onclick="return order_artwork_replace(<?php echo $item_id; ?>, 0);"><?php _e('Add artwork file', 'wp2print'); ?></a>
				</div>
				<?php
			}
			if ($artwork_old_files) { ?>
				<div class="print-products-area old-files-container">
					<ul class="product-attributes-list">
						<li><a href="#show-old-files" class="show-old-files"><?php _e('Check old versions of files'); ?></a></li>
						<li class="old-files-list"><ul class="product-artwork-files-list ftpfilenames">
							<?php foreach($artwork_old_files as $af_key => $artwork_old_file) {
								echo '<li><i class="i-triangle"></i> <a href="'.print_products_get_amazon_file_url($artwork_old_file).'" title="'.__('Download', 'wp2print').'" target="_blank">'.basename($artwork_old_file).'</a></li>';
							} ?>
						</ul></li>
					</ul>
				</div>
				<?php
			}
			if ($artwork_rejected_files) { ?>
				<div class="print-products-area rejected-files-container">
					<ul class="product-attributes-list">
						<li style="margin-bottom:5px;"><a href="#show-rejected-files" class="show-rejected-files"><?php _e('Rejected artwork files'); ?></a></li>
						<li class="rejected-files-list"><ul class="product-artwork-files-list ftpfilenames">
							<?php foreach($artwork_rejected_files as $af_key => $artwork_rejected_file) {
								echo '<li><i class="i-rejected"></i> <a href="'.print_products_get_amazon_file_url($artwork_rejected_file).'" title="'.__('Download', 'wp2print').'" target="_blank">'.basename($artwork_rejected_file).'</a></li>';
							} ?>
						</ul></li>
					</ul>
				</div>
				<?php
			}
		}
		// Order artwork file url
		$artwork_file_url = wc_get_order_item_meta($item_id, '_artwork_file_url', true);
		$artwork_file_url_order = (int)wc_get_order_item_meta($item_id, '_artwork_file_url_order', true);
		if (strlen($artwork_file_url) && $artwork_file_url_order) {
			echo '<a href="'.$artwork_file_url.'" title="'.__('Download', 'wp2print').'" target="_blank">'.basename($artwork_file_url).'</a>';
		}

		$printess_save_token = $item->get_meta( '_printess-save-token' );
		if ($printess_save_token && function_exists('printess_order_item_meta_show')) {
			printess_order_item_meta_show( $item_id, $item );
		}

		$rp_item = wc_get_order_item_meta($item_id, '_rp_item', true);
		$rp_subtotal = wc_get_order_item_meta($item_id, '_rp_item_subtotal', true); ?>
		<div class="order-proof-container">
			<?php if (function_exists('print_products_orders_proof_order_item_approval_status')) { print_products_orders_proof_order_item_approval_status($item_id, $order); } ?>
			<div class="req-epayment" style="float:right;">
			<?php if ($rp_subtotal) { ?>
				<div style="padding:0 60px 15px 0;"><?php _e('Extra payment received', 'wp2print'); ?>: <?php echo wc_price($rp_subtotal); ?></div>
			<?php } ?>
			<?php if ($rp_item != '1') { ?>
				<div style="float:right;"><a href="admin.php?page=print-products-request-payment&order_request_epayment=true&order_id=<?php echo $order->get_id(); ?>&item_id=<?php echo $item_id; ?>&product_id=<?php echo $item->get_product_id(); ?>" class="button button-primary"><?php _e('Request extra payment', 'wp2print'); ?></a></div>
			<?php } ?>
			</div>
		</div>
		<div class="clear"></div>
		<?php
		?>
		</div></td>
	</tr>
	<?php
	// AEC table
	$dimension_unit = print_products_get_aec_dimension_unit();
	$aec_sizes = print_products_get_aec_sizes();
	$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
	if ($order_item_data) {
		$additional = unserialize($order_item_data->additional);
		if ($order_item_data->product_type == 'aec') {
			$table_values = $additional['table_values'];
			if (strlen($table_values)) {
				$table_lines = explode('|', $table_values);
				$last_size = ''; ?>
				<tr>
					<td>&nbsp;</td>
					<td colspan="10">
						<div class="low-cost-options-table"<?php if (count($table_lines) > 11) { echo ' style="height:400px;"'; } ?>>
							<table cellspacing="1" cellpadding="0" width="100%">
								<thead>
									<tr>
										<th style="text-align:left;"><?php _e('File Name', 'wp2print'); ?></th>
										<th style="text-align:center"><?php _e('Page', 'wp2print'); ?></th>
										<th style="text-align:center" nowrap><?php _e('% Coverage', 'wp2print'); ?></th>
										<th style="text-align:left"><?php _e('Print size', 'wp2print'); ?></th>
										<th style="text-align:right" nowrap><?php _e('Printed Area', 'wp2print'); ?> (<?php echo $dimension_unit; ?><sup>2</sup>)</th>
										<th style="text-align:right"><?php _e('Price', 'wp2print'); ?>/<?php echo $dimension_unit; ?><sup>2</sup></th>
										<th style="text-align:right"><?php _e('Price', 'wp2print'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($table_lines as $table_line) {
										$lvalues = explode(';', $table_line);
										$tdbg = '';
										if (($lvalues[3] != '' && $lvalues[3] != '100') || ($lvalues[3] == '' && $last_size != '100')) {
											$tdbg = 'background:#fc8c8c !important;';
										} ?>
										<tr>
											<td style="text-align:left;width:150px;word-break:break-all;<?php echo $tdbg; ?>"><?php echo $lvalues[0]; ?></td>
											<td style="text-align:center;<?php echo $tdbg; ?>"><?php echo $lvalues[1]; ?></td>
											<td style="text-align:center;<?php echo $tdbg; ?>"><?php echo $lvalues[2]; ?></td>
											<td style="text-align:left;<?php echo $tdbg; ?>" nowrap><?php echo $aec_sizes[$lvalues[3]]; ?></td>
											<td style="text-align:right;<?php echo $tdbg; ?>"><?php echo $lvalues[4]; ?> <?php echo $dimension_unit; ?><sup>2</sup></td>
											<td style="text-align:right;<?php echo $tdbg; ?>"><?php echo print_products_display_price($lvalues[5]); ?></td>
											<td style="text-align:right;<?php echo $tdbg; ?>"><?php echo print_products_display_price($lvalues[6]); ?></td>
										</tr>
										<?php if ($lvalues[3] != '') { $last_size = $lvalues[3]; } ?>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
				<?php
			}
		} else if ($order_item_data->product_type == 'aecbwc') {
			$table_values = $additional['table_values'];
			if (strlen($table_values)) {
				$table_lines = explode('|', $table_values);
				$last_size = ''; ?>
				<tr>
					<td>&nbsp;</td>
					<td colspan="10">
						<div class="low-cost-options-table"<?php if (count($table_lines) > 11) { echo ' style="height:400px;"'; } ?>>
							<table cellspacing="1" cellpadding="0" width="100%">
								<thead>
									<tr>
										<th style="text-align:left;"><?php _e('File Name', 'wp2print'); ?></th>
										<th style="text-align:center"><?php _e('Page', 'wp2print'); ?></th>
										<th style="text-align:center"><?php _e('Original color', 'wp2print'); ?></th>
										<th style="text-align:center" nowrap><?php _e('Original size', 'wp2print'); ?></th>
										<th style="text-align:center"><?php _e('Convert color', 'wp2print'); ?></th>
										<th style="text-align:left"><?php _e('Print size', 'wp2print'); ?></th>
										<th style="text-align:right"><?php _e('Price', 'wp2print'); ?>/<?php echo $dimension_unit; ?><sup>2</sup></th>
										<th style="text-align:right"><?php _e('Price', 'wp2print'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($table_lines as $table_line) {
										$lvalues = explode(';', $table_line);
										$tdbg = '';
										if (($lvalues[5] != '' && $lvalues[5] != '100') || ($lvalues[5] == '' && $last_size != '100')) {
											$tdbg = 'background:#fc8c8c !important;';
										}
										$ccolor = '&nbsp;';
										if ($lvalues[2] == 'color' && $lvalues[4] == 'bw') { $ccolor = '<img src="'.PRINT_PRODUCTS_PLUGIN_URL.'images/icon-bw.png">'; }
										?>
										<tr>
											<td style="text-align:left;width:150px;word-break:break-all;<?php echo $tdbg; ?>"><?php echo $lvalues[0]; ?></td>
											<td style="text-align:center;<?php echo $tdbg; ?>"><?php echo $lvalues[1]; ?></td>
											<td style="text-align:center;<?php echo $tdbg; ?>"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/icon-<?php echo $lvalues[2]; ?>.png"></td>
											<td style="text-align:center;<?php echo $tdbg; ?>" nowrap><?php echo $lvalues[3]; ?></td>
											<td style="text-align:center;<?php echo $tdbg; ?>"><?php echo $ccolor; ?></td>
											<td style="text-align:left;<?php echo $tdbg; ?>" nowrap><?php echo $aec_sizes[$lvalues[5]]; ?></td>
											<td style="text-align:right;<?php echo $tdbg; ?>"><?php echo print_products_display_price($lvalues[6]); ?></td>
											<td style="text-align:right;<?php echo $tdbg; ?>"><?php echo print_products_display_price($lvalues[7]); ?></td>
										</tr>
										<?php if ($lvalues[5] != '') { $last_size = $lvalues[5]; } ?>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
				<?php
			}
		} else if ($order_item_data->product_type == 'aecsimple') {
			$table_values = $additional['table_values'];
			if (strlen($table_values)) {
				$table_lines = explode('|', $table_values);
				$last_size = ''; ?>
				<tr>
					<td>&nbsp;</td>
					<td colspan="10">
						<div class="low-cost-options-table"<?php if (count($table_lines) > 11) { echo ' style="height:400px;"'; } ?>>
							<table cellspacing="1" cellpadding="0" width="100%">
								<thead>
									<tr>
										<th style="text-align:left;"><?php _e('File Name', 'wp2print'); ?></th>
										<th style="text-align:center"><?php _e('Page', 'wp2print'); ?></th>
										<th style="text-align:left"><?php _e('Print size', 'wp2print'); ?></th>
										<th style="text-align:right" nowrap><?php _e('Printed Area', 'wp2print'); ?> (<?php echo $dimension_unit; ?><sup>2</sup>)</th>
										<th style="text-align:right"><?php _e('Price', 'wp2print'); ?>/<?php echo $dimension_unit; ?><sup>2</sup></th>
										<th style="text-align:right"><?php _e('Price', 'wp2print'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($table_lines as $table_line) {
										$lvalues = explode(';', $table_line);
										$tdbg = '';
										if (($lvalues[2] != '' && $lvalues[2] != '100') || ($lvalues[2] == '' && $last_size != '100')) {
											$tdbg = 'background:#fc8c8c !important;';
										} ?>
										<tr>
											<td style="text-align:left;width:150px;word-break:break-all;<?php echo $tdbg; ?>"><?php echo $lvalues[0]; ?></td>
											<td style="text-align:center;<?php echo $tdbg; ?>"><?php echo $lvalues[1]; ?></td>
											<td style="text-align:left;<?php echo $tdbg; ?>" nowrap><?php echo $aec_sizes[$lvalues[2]]; ?></td>
											<td style="text-align:right;<?php echo $tdbg; ?>"><?php echo $lvalues[3]; ?> <?php echo $dimension_unit; ?><sup>2</sup></td>
											<td style="text-align:right;<?php echo $tdbg; ?>"><?php echo print_products_display_price($lvalues[4]); ?></td>
											<td style="text-align:right;<?php echo $tdbg; ?>"><?php echo print_products_display_price($lvalues[5]); ?></td>
										</tr>
										<?php if ($lvalues[2] != '') { $last_size = $lvalues[2]; } ?>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
				<?php
			}
		} else if ($order_item_data->product_type == 'eddm' && isset($additional['guid']) && strlen($additional['guid'])) {
			$print_products_accuzip_api_options = get_option("print_products_accuzip_api_options");
			$piece_height = '';
			$piece_length = '';
			$thickness_value = '';
			$residential_only = 1;
			if (isset($additional['psize'])) {
				$dimensions = get_term_meta($additional['psize'], '_dimensions', true);
				if ($dimensions && is_array($dimensions)) {
					$piece_length = $dimensions['width'];
					$piece_height = $dimensions['height'];
				}
			}
			if (isset($additional['pmaterial'])) {
				$thickness_value = get_term_meta($additional['pmaterial'], '_thickness', true);
			}
			if (isset($additional['eddm_target_customer']) && $additional['eddm_target_customer'] == 1) {
				$residential_only = 0;
			}
			?>
			<tr>
				<td>&nbsp;</td>
				<td colspan="10">
					<div class="eddm-purchase-wrap eddm-purchase-wrap-<?php echo $item_id; ?>">
						<?php if (isset($additional['eddm_finished']) && $additional['eddm_finished']) { ?>
							<a href="https://cloud2.iaccutrace.com/ws_360_webapps/download.jsp?guid=<?php echo $additional['guid']; ?>&ftype=maildat.zip" class="button"><?php _e('Download maildat.zip', 'wp2print'); ?></a>
						<?php } else { ?>
							<div style="padding-bottom:5px;"><input type="text" name="prepared_for_crid" class="eddm-prepared-for-crid" placeholder="prepared_for_crid" style="width:250px;padding:1px 4px;" data-emessage="<?php _e('Please fill prepared_for_crid required field.', 'wp2print'); ?>"></div>
							<div style="padding-bottom:5px;"><input type="text" name="ddu" class="eddm-ddu" placeholder="DDU" style="width:250px;padding:1px 4px;"data-emessage="<?php _e('Please fill DDU required field.', 'wp2print'); ?>"></div>
							<a href="#eddm" class="button button-primary eddm-purchase-btn" data-item-id="<?php echo $item_id; ?>" data-guid="<?php echo $additional['guid']; ?>" data-zipcode="<?php echo $print_products_accuzip_api_options['zip_code']; ?>" data-piece-height="<?php echo $piece_height; ?>" data-piece-length="<?php echo $piece_length; ?>" data-thickness-value="<?php echo $thickness_value; ?>" data-weight-value="<?php echo $additional['weight']; ?>" data-residential-only="<?php echo $residential_only; ?>"><?php _e('Purchase EDDM route list', 'wp2print'); ?></a>
							<img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/ajax-loading.gif" class="ep-loading" style="margin-top:7px; display:none;">
							<a href="https://cloud2.iaccutrace.com/ws_360_webapps/download.jsp?guid=<?php echo $additional['guid']; ?>&ftype=maildat.zip" class="button eddm-download-link" style="display:none;"><?php _e('Download maildat.zip', 'wp2print'); ?></a>
							<div class="eddm-session-id" style="padding-top:5px;">GUID: <?php echo $additional['guid']; ?></div>
						<?php } ?>
					</div>
				</td>
			</tr>
			<?php
		}
	}
}

function print_products_wcpdf_order_item_data($order_item_data) {
	print_products_product_attributes_list_html($order_item_data);
	$additional = unserialize($order_item_data->additional);
	$artwork_files = unserialize($order_item_data->artwork_files);
	$artwork_thumbs = unserialize($order_item_data->artwork_thumbs);
	if ($artwork_files) { ?>
		<div class="print-products-area">
			<ul class="product-attributes-list">
				<?php if ($order_item_data->product_type == 'aec' || $order_item_data->product_type == 'aecbwc' || $order_item_data->product_type == 'aecsimple') { ?>
					<li><?php _e('Files', 'wp2print'); ?>:</li>
				<?php } else { ?>
					<li><?php _e('Artwork Files', 'print-products'); ?>:</li>
				<?php } ?>
				<li><ul class="product-artwork-files-list ftpfilenames oi-files-list">
					<?php foreach($artwork_files as $af_key => $artwork_file) {
						echo '<li><a href="'.print_products_get_amazon_file_url($artwork_file).'" title="'.__('Download', 'wp2print').'" target="_blank">'.basename($artwork_file).'</a></li>';
					} ?>
				</ul></li>
			</ul>
		</div>
		<?php
	}
}

add_filter('woocommerce_tax_settings', 'print_products_woo_tax_settings');
function print_products_woo_tax_settings($settings) {
	$new_settings = array();
	foreach($settings as $skey => $sval) {
		if (isset($sval['id']) && $sval['id'] == 'woocommerce_price_display_suffix') {
			$sval['title'] = __('Price Display Include Suffix:', 'wp2print');
			$new_settings[] = $sval;
			$new_settings[] = array(
				'title' => __('Price Display Exclude Suffix:', 'wp2print'),
				'id' => 'woocommerce_price_display_excl_suffix',
				'default' => '',
				'placeholder' => 'N/A',
				'type' => 'text',
				'desc_tip' => __("Define text to show after your product prices (excluding tax). This could be, for example, 'excl. tax' to explain your pricing.", 'wp2print')
			);
		} else {
			$new_settings[] = $sval;
		}
	}
	return $new_settings;
}

add_filter('woocommerce_get_price_suffix', 'print_products_woo_get_price_suffix');
function print_products_woo_get_price_suffix($price_display_suffix) {
	if (is_front_page()) {
		$price_display_suffix = '';
	}
	return $price_display_suffix;
}

add_action('woocommerce_duplicate_product', 'print_products_woo_duplicate_product', 10, 2);
function print_products_woo_duplicate_product($new_id, $post) {
	global $wpdb;
	$matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s ORDER BY mtype_id", $wpdb->prefix, $post->ID));
	if ($matrix_types) {
		foreach($matrix_types as $matrix_type) {
			$mtype_id = $matrix_type->mtype_id;

			$insert = array();
			$insert['product_id'] = $new_id;
			$insert['mtype'] = $matrix_type->mtype;
			$insert['title'] = $matrix_type->title;
			$insert['attributes'] = $matrix_type->attributes;
			$insert['aterms'] = $matrix_type->aterms;
			$insert['numbers'] = $matrix_type->numbers;
			$insert['num_type'] = $matrix_type->num_type;
			$insert['sorder'] = $matrix_type->sorder;
			$insert['num_style'] = $matrix_type->num_style;
			$insert['def_quantity'] = $matrix_type->def_quantity;
			$wpdb->insert($wpdb->prefix."print_products_matrix_types", $insert);
			$new_mtype_id = $wpdb->insert_id;

			$matrix_prices = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_prices WHERE mtype_id = %s ORDER BY mtype_id", $wpdb->prefix, $mtype_id));
			if ($matrix_prices) {
				foreach($matrix_prices as $matrix_price) {
					$insert = array();
					$insert['mtype_id'] = $new_mtype_id;
					$insert['aterms'] = $matrix_price->aterms;
					$insert['number'] = $matrix_price->number;
					$insert['price'] = $matrix_price->price;
					$wpdb->insert($wpdb->prefix."print_products_matrix_prices", $insert);
				}
			}
		}
	}
}

add_filter('woocommerce_get_settings_products', 'print_products_settings_products', 10);
function print_products_settings_products($settings) {
	foreach ($settings as $skey => $setting) {
		if ($setting['id'] == 'woocommerce_dimension_unit') {
			if (!isset($setting['options']['ft'])) {
				$settings[$skey]['options']['ft'] = __('ft');
			}
		}
	}
	$settings[] = array(
		'title' => __('Product List Prices', 'wp2print'),
		'type' => 'title',
		'desc' => '',
		'id' => 'price_options'
	);
	$settings[] = array(
		'title'         => __('Disable Product List Price', 'wp2print'),
		'desc'          => 'Check checkbox if you want to hide product prices on list.',
		'id'            => 'woocommerce_disable_product_list_price',
		'default'       => 'no',
		'type'          => 'checkbox'
	);
	$settings[] = array(
		'type' 	=> 'sectionend',
		'id' 	=> 'price_options'
	);
	return $settings;
}

add_action('delete_post', 'print_products_wp_delete_product', 10);
function print_products_wp_delete_product($pid) {
	global $wpdb;
	$matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s ORDER BY mtype_id", $wpdb->prefix, $pid));
	if ($matrix_types) {
		$mtypes = array();
		foreach($matrix_types as $matrix_type) {
			$mtypes[] = $matrix_type->mtype_id;
		}
		$wpdb->query(sprintf("DELETE FROM %sprint_products_matrix_types WHERE product_id = %s", $wpdb->prefix, $pid));
		if (count($mtypes)) {
			$wpdb->query(sprintf("DELETE FROM %sprint_products_matrix_prices WHERE mtype_id IN (%s)", $wpdb->prefix, implode(',', $mtypes)));
		}
	}
}

add_filter('woocommerce_order_amount_item_subtotal', 'print_products_order_amount_item_subtotal', 11, 4);
function print_products_order_amount_item_subtotal($subtotal, $order, $item, $inc_tax) {
	$product_id = $item->get_product_id();
	$product_type = print_products_get_type($product_id);
	if (print_products_is_wp2print_type($product_type)) {
		$isubtotal = floatval($item->get_subtotal());
		$isubtotal_tax = floatval($item->get_subtotal_tax());
		$quantity = $item->get_quantity();
		if ($inc_tax) {
			$subtotal = ( $isubtotal + $isubtotal_tax ) / max( 1, $quantity );
		} else {
			$subtotal = ( $isubtotal / max( 1, $quantity ) );
		}
		$decimals = print_products_get_order_item_decimals($quantity);
		$subtotal = round($subtotal, $decimals);
	}
	return $subtotal;
}

add_filter('woocommerce_my_account_my_orders_columns', 'print_products_my_account_my_orders_columns');
function print_products_my_account_my_orders_columns($columns) {
	$new_columns = array();
	foreach($columns as $ckey => $cval) {
		if ($ckey == 'order-actions') {
			$new_columns['order-notes'] = __('Notes', 'wp2print');
		}
		$new_columns[$ckey] = $cval;
	}
	return $new_columns;
}

add_action('woocommerce_my_account_my_orders_column_order-notes', 'print_products_my_account_my_orders_column_order_notes');
function print_products_my_account_my_orders_column_order_notes($order) {
	global $wpdb;
	$is_onotes = false;
	$order_notes = $order->get_meta('_order_notes', true);
	if (strlen($order_notes) > 100) {
		$is_onotes = true;
		$order_notes = '<div class="o-notes">'.substr($order_notes, 0, 20).'... <a href="#read-more" class="o-notes-more">'.__('Read more', 'wp2print').'</a><span class="o-notes-text">'.$order_notes.'</span></div>';
	}
	echo $order_notes;
	$order_items = $order->get_items();
	foreach ($order_items as $item_id => $order_item) {
		$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
		if ($order_item_data) {
			$additional = unserialize($order_item_data->additional);
			if (isset($additional['project_name']) && $additional['project_name']) {
				echo '<div class="o-pname">'.$additional['project_name'].'</div>';
			}
		}
	}
}

add_action('wpo_wcpdf_before_item_meta', 'print_products_wpo_wcpdf_before_item_meta', 11, 3);
function print_products_wpo_wcpdf_before_item_meta($type, $item, $order) {
	echo '<br><br>';
	print_products_before_order_itemmeta($item['item_id'], $item);
}

add_filter('product_attributes_type_selector', 'print_products_woo_attributes_type_selector');
function print_products_woo_attributes_type_selector($types) {
	if (!isset($types['select'])) {
		$types['select'] = __('Select', 'woocommerce');
	}
	if (!isset($types['text'])) {
		$types['text'] = __('Text', 'woocommerce');
	}
	return $types;
}

function print_products_woo_get_order_item_meta($item_id, $meta_key) {
	$designer_image = $wpdb->get_var(sprintf("SELECT meta_value FROM %swoocommerce_order_itemmeta WHERE order_item_id = %s AND meta_key = '_image_link'", $wpdb->prefix, $item_id));
}

add_filter('get_terms_args', 'print_products_woo_product_subcategories_args', 12);
function print_products_woo_product_subcategories_args($args) {
	$uncategorized = get_option('default_product_cat');
	if (!is_admin() && $uncategorized) {
		if (is_array($args['exclude'])) {
			if (count($args['exclude'])) {
				$args['exclude'][] = $uncategorized;
			} else {
				$args['exclude'] = array($uncategorized);
			}
		} else {
			if (strlen($args['exclude'])) {
				$args['exclude'] = $args['exclude'] . ',' . $uncategorized;
			} else {
				$args['exclude'] = array($uncategorized);
			}
		}
	}
	return $args;
}

add_action('woocommerce_attribute_added', 'print_products_woo_woocommerce_attribute_added', 11, 2);
function print_products_woo_woocommerce_attribute_added($id, $data) {
	global $wpdb;
	$print_products_settings = get_option('print_products_settings');
	$finishing_attributes = unserialize($print_products_settings['finishing_attributes']);
	if (!is_array($finishing_attributes)) { $finishing_attributes = array(); }
	$finishing_attributes[] = $id;
	$print_products_settings['finishing_attributes'] = serialize($finishing_attributes);
	update_option("print_products_settings", $print_products_settings);

	$attribute_order = (int)$wpdb->get_var(sprintf("SELECT MAX(attribute_order) FROM %swoocommerce_attribute_taxonomies", $wpdb->prefix));
	$attribute_order = $attribute_order + 1;
	$wpdb->update($wpdb->prefix.'woocommerce_attribute_taxonomies', array('attribute_order' => $attribute_order), array('attribute_id' => $id));
}

add_filter('woocommerce_cart_totals_order_total_html', 'print_products_woo_cart_totals_order_total_html');
function print_products_woo_cart_totals_order_total_html($html) {
	if (!strpos($html, 'includes_tax')) {
		$woocommerce_prices_include_tax = get_option('woocommerce_prices_include_tax');
		$price_display_incl_suffix = get_option('woocommerce_price_display_suffix');
		$price_display_excl_suffix = get_option('woocommerce_price_display_excl_suffix');
		if ($woocommerce_prices_include_tax == 'yes' && strlen($price_display_incl_suffix)) {
			$html .= '<small class="includes_tax">'.$price_display_excl_suffix.'</small>';
		} else if ($woocommerce_prices_include_tax == 'no' && strlen($price_display_excl_suffix)) {
			$html .= '<small class="includes_tax">'.$price_display_incl_suffix.'</small>';
		}
	}
	return $html;
}

add_filter('woocommerce_product_is_visible', 'print_products_woo_product_is_visible', 11, 2);
function print_products_woo_product_is_visible($visible, $product_id) {
	if (print_products_is_custom_product($product_id)) {
		$visible = false;
	}
	return $visible;
}

add_filter('woocommerce_cart_item_name', 'print_products_woo_cart_item_name', 25, 2);
add_filter('woocommerce_in_cart_product_title', 'print_products_woo_cart_item_name', 25, 2);
function print_products_woo_cart_item_name($name, $cart_item) {
	if (print_products_is_custom_product($cart_item['product_id'])) {
		$name = '<strong>'.strip_tags($name).'</strong>';
	}
	return $name;
}

add_filter('woocommerce_cart_item_quantity', 'print_products_woo_cart_item_quantity', 11, 3);
function print_products_woo_cart_item_quantity($quantity, $cart_item_key, $cart_item) {
	$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
	if (print_products_is_custom_product($product_id)) {
		$quantity = $cart_item['quantity'];
	}
	return $quantity;
}

add_filter('woocommerce_product_get_weight', 'print_products_woo_product_get_weight', 11, 2);
function print_products_woo_product_get_weight($weight, $product) {
	if (!is_admin() && WC()->cart) {
		$product_id = $product->get_id();
		$cart = WC()->cart->get_cart();
		if ($cart) {
			$total_weight = 0;
			$total_prods = 0;
			foreach ($cart as $cart_item_key => $values) {
				$cart_product_id = $values['product_id'];
				if ($cart_product_id == $product_id) {
					$product_type = $product->get_type();
					if (print_products_is_wp2print_type($product_type) || print_products_is_custom_product($product_id)) {
						$weight = print_products_cart_get_product_weight($cart_item_key);
						$total_weight = $total_weight + $weight;
						$total_prods++;
					}
				}
			}
			if ($total_prods > 1 && $total_weight) {
				$weight = $total_weight / $total_prods;
			}
		}
	}
	return $weight;
}

add_filter('woocommerce_is_purchasable', 'print_products_woo_is_purchasable', 11, 2);
function print_products_woo_is_purchasable($purchasable, $product) {
	$product_id = $product->get_id();
	$product_type = $product->get_type();
	if (print_products_is_wp2print_type($product_type)) {
		if (isset($_REQUEST['print_products_checkout_process_action']) && $_REQUEST['print_products_checkout_process_action'] == 'add-to-cart' && $_REQUEST['product_id'] == $product_id) {
			if (isset($_REQUEST['pprice']) || (isset($_REQUEST['pb_amount']) && (float)$_REQUEST['pb_amount']) || (isset($_REQUEST['aec_total_price']) && (float)$_REQUEST['aec_total_price'])) {
				$purchasable = true;
			} else {
				$purchasable = false;
				if (function_exists('print_products_users_groups_is_invoice_zero')) {
					if (print_products_users_groups_is_invoice_zero()) {
						$purchasable = true;
					}
				}
			}
		} else {
			$purchasable = true;
		}
	}
	if (print_products_is_custom_product($product_id)) {
		$purchasable = true;
	}
	return $purchasable;
}

add_filter('woocommerce_hidden_order_itemmeta', 'print_products_woo_hidden_order_itemmeta');
function print_products_woo_hidden_order_itemmeta($metakeys) {
	$metakeys[] = '_sku';
	$metakeys[] = '_artwork_file_url';
	$metakeys[] = '_artwork_file_url_order';
	$metakeys[] = '_artwork_file_url_email';
	$metakeys[] = '_item_status';
	$metakeys[] = '_discount_price';
	$metakeys[] = '_approval_status';
	$metakeys[] = '_approval_type';
	$metakeys[] = '_approval_approved';
	$metakeys[] = '_approval_rejected';
	$metakeys[] = '_proof_files';
	$metakeys[] = '_item_rsdate';
	$metakeys[] = '_rp_item';
	$metakeys[] = '_rp_item_subtotal';
	$metakeys[] = '_product_attributes';
	$metakeys[] = '_preflight_rejected';
	$metakeys[] = '_media_url';
	return $metakeys;
}

add_action('template_redirect', 'print_products_woo_template_redirect');
function print_products_woo_template_redirect() {
	global $wpdb;
	if (is_checkout()) {
		$cart = WC()->cart->get_cart();
		if ($cart) {
			$goback = false;
			foreach($cart as $cart_item_key => $values) {
				$product_id = $values['product_id'];
				$product = $values['data'];
				$cart_upload_button = get_post_meta($product_id, '_cart_upload_button', true);
				$cart_upload_button_required = get_post_meta($product_id, '_cart_upload_button_required', true);
				if ($cart_upload_button && $cart_upload_button_required) {
					$prod_cart_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_cart_data WHERE cart_item_key = '%s'", $wpdb->prefix, $cart_item_key));
					if ($prod_cart_data) {
						if (!strlen($prod_cart_data->artwork_files)) {
							$cart_upload_button_text = get_post_meta($product_id, '_cart_upload_button_text', true);
							if (!strlen($cart_upload_button_text)) { $cart_upload_button_text = __('Upload your database', 'wp2print'); }

							wc_add_notice($cart_upload_button_text.' '.__('button is required for', 'wp2print').' '.$product->get_name(), 'error');

							$goback = true;
						}
					}
				}
			}
			if ($goback) {
				$cart_url = wc_get_cart_url();
				wp_redirect($cart_url);
				exit;
			}
		}
	}
}

add_action('woocommerce_thankyou', 'print_products_woo_checkout_order_processed', 25);
function print_products_woo_checkout_order_processed($order_id) {
	$order = wc_get_order($order_id);
	$payment_method = $order->get_payment_method();
	if ($payment_method == 'cheque') {
		if ($order->get_status() == 'on-hold') {
			$order->update_status('processing');
		}
	}
}

add_action('woocommerce_after_single_product_summary', 'print_products_woo_after_single_product_summary', 9);
function print_products_woo_after_single_product_summary() {
	global $post, $attribute_names, $terms_names;
	if (!print_products_is_product_hidden_price($post->ID)) {
		$unitpricetable = get_post_meta($post->ID, '_unitpricetable', true);
		$area_unit = (int)get_post_meta($post->ID, '_area_unit', true);
		$area_units = print_products_get_area_units();
		$aunit = $area_units[$area_unit];
		$upt_show = false;
		$upt_quantities = array();
		$upt_attribute = 0;
		$upt_aoptions = array();
		$upt_unitprices = 0;
		$upt_utext = '';
		if ($unitpricetable && is_array($unitpricetable)) {
			$upt_show = (isset($unitpricetable['show']) ? (int)$unitpricetable['show'] : 0);
			$upt_quantities = (isset($unitpricetable['quantities']) ? explode(',', $unitpricetable['quantities']) : array());
			$upt_attribute = (isset($unitpricetable['attribute']) ? $unitpricetable['attribute'] : '');
			$upt_aoptions = (isset($unitpricetable['aoptions']) ? $unitpricetable['aoptions'] : '');
			$upt_unitprices = (isset($unitpricetable['unitprices']) ? (int)$unitpricetable['unitprices'] : 0);
			$upt_utext = (isset($unitpricetable['utext']) ? $unitpricetable['utext'] : '');
		}
		if ($upt_show && count($upt_quantities) && $upt_attribute && is_array($upt_aoptions) && count($upt_aoptions)) { ?>
			<style>
			.upt-container{
				float:right;
				margin: 0 0 20px 0;
				clear:right;
			}
			table.unit-price-table{
				width:auto;
				margin:0;
				border-collapse:collapse !important;
			}
			table.unit-price-table td.td-hd{
				color:#000;
				padding:10px;
			}
			</style>
			<div class="upt-container entry-content">
				<table border="1" class="unit-price-table">
					<tr>
						<td class="td-hd"></td>
						<td class="td-hd" style="text-align:center;" colspan="<?php echo count($upt_quantities); ?>"><?php _e('Quantity', 'wp2print'); ?></td>
					</tr>
					<tr>
						<td class="td-hd"><?php if ($upt_attribute == 'arange') { _e('Area range', 'wp2print'); } else { echo $attribute_names[$upt_attribute]; } ?></td>
						<?php foreach($upt_quantities as $qty) { ?>
							<td class="td-hd" ><?php echo $qty; ?></td>
						<?php } ?>
					</tr>
					<?php foreach($upt_aoptions as $term_id) {
						if ($upt_attribute == 'arange') { $term_id = str_replace(':', '-', $term_id); } ?>
						<tr>
							<td class="td-hd" data-label="<?php if ($upt_attribute == 'arange') { _e('Area range', 'wp2print'); } else { echo $attribute_names[$upt_attribute]; } ?>"><?php if ($upt_attribute == 'arange') { echo $term_id.' '.$aunit; } else { echo $terms_names[$term_id]; } ?></td>
							<?php foreach($upt_quantities as $qty) { ?>
								<td class="up-<?php echo $qty; ?>-<?php echo str_replace('.', '-', $term_id); ?>" data-label="<?php echo $qty; ?>">0</td>
							<?php } ?>
						</tr>
					<?php } ?>
				</table>
			</div>
			<?php
		}
	}
}

add_shortcode('wp2print_product_unit_price_table', 'print_products_woo_product_unit_price_table');
function print_products_woo_product_unit_price_table() {
	ob_start();
	print_products_woo_after_single_product_summary();
	return ob_get_clean();
}

add_action('woocommerce_thankyou', 'print_products_woo_thankyou');
function print_products_woo_thankyou($order_id) {
	global $wpdb;
	$order = wc_get_order($order_id);
	if ($order && is_user_logged_in()) {
		$show_button = false;
		$order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
		foreach ($order_items as $item_id => $item) {
			$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
			if ($order_item_data && !$show_button) {
				$artwork_source = get_post_meta($order_item_data->product_id, '_artwork_source', true);
				if ($order_item_data->atcaction == 'artwork' && !strlen($order_item_data->artwork_files) && ($artwork_source == 'artwork' || $artwork_source == 'both')) {
					$show_button = true;
				}
			}
		}
		if ($show_button) {
			$myaccount_page_id = get_option('woocommerce_myaccount_page_id');
			$myaccount_page = get_permalink($myaccount_page_id);
			if (substr($myaccount_page, -1) != '/') { $myaccount_page .= '/'; }
			$myaccount_page .= 'orders-missing-files/?view='.$order_id;
			?>
			<script>jQuery('p.woocommerce-thankyou-order-received').html('<?php _e('Thank you. Your order has been received. If you have the files you would like us to print, please upload them now:', 'wp2print'); ?>&nbsp;&nbsp;<a href="<?php echo $myaccount_page; ?>" class="single_add_to_cart_button <?php print_products_buttons_class(); ?> alt" style="margin-bottom:20px;"><?php _e('Upload your own design', 'wp2print'); ?></a>');
			</script>
			<?php
		}
	}
}

add_action('wp_loaded', 'print_products_order_artwork_upload');
function print_products_order_artwork_upload() {
	global $wpdb;
	if (isset($_POST['artwork_upload_action']) && $_POST['artwork_upload_action'] == 'upload') {
		$item_id = (int)$_POST['item_id'];
		$au_file = (int)$_POST['au_file'];
		$au_new_file = $_POST['au_new_file'];
		$redirect_to = $_POST['redirect_to'];
		$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
		if ($order_item_data && strlen($au_new_file)) {
			$artwork_files = unserialize($order_item_data->artwork_files);
			$artwork_multi_files = unserialize($order_item_data->artwork_multi_files);
			$artwork_old_files = array();
			if ($order_item_data->artwork_old_files) {
				$artwork_old_files = unserialize($order_item_data->artwork_old_files);
			}
			if ($artwork_files) {
				$old_file = $artwork_files[$au_file];
				$artwork_old_files[] = $old_file;
				$artwork_files[$au_file] = $au_new_file;
				if ($artwork_multi_files && is_array($artwork_multi_files)) {
					foreach ($artwork_multi_files as $mfkey => $mfdata) {
						if ($mfdata['file'] == $old_file) {
							$artwork_multi_files[$mfkey]['file'] = $au_new_file;
						}
					}
				}
			} else {
				$artwork_files[] = $au_new_file;
			}

			$update = array();
			$update['artwork_files'] = serialize($artwork_files);
			$update['artwork_old_files'] = serialize($artwork_old_files);
			$update['artwork_multi_files'] = serialize($artwork_multi_files);
			$wpdb->update($wpdb->prefix.'print_products_order_items', $update, array('item_id' => $item_id));
		}
		wp_redirect($redirect_to);
		exit;
	}
	if (isset($_POST['artwork_add_upload_action']) && $_POST['artwork_add_upload_action'] == 'upload') {
		$item_id = (int)$_POST['item_id'];
		$new_files = $_POST['new_files'];
		$redirect_to = $_POST['redirect_to'];
		$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
		if ($order_item_data && strlen($new_files)) {
			$new_files = explode(';', $new_files);
			$artwork_files = unserialize($order_item_data->artwork_files);
			foreach ($new_files as $new_file) {
				$artwork_files[] = $new_file;
			}
			$update = array();
			$update['artwork_files'] = serialize($artwork_files);
			$wpdb->update($wpdb->prefix.'print_products_order_items', $update, array('item_id' => $item_id));
		}
		wp_redirect($redirect_to);
		exit;
	}
}

// add column to Users list
add_filter('manage_users_columns', 'print_products_manage_users_columns', 25);
function print_products_manage_users_columns($columns) {
	$new_columns = array();
	foreach($columns as $ckey => $cname) {
		if ($ckey == 'posts') {
			$new_columns['order-history'] = __('Order history', 'wp2print');
		}
		$new_columns[$ckey] = $cname;
	}
	return $new_columns;
}

add_filter('manage_users_custom_column', 'print_products_manage_users_custom_column', 25, 3);
function print_products_manage_users_custom_column($val, $column, $user_id) {
	if ($column == 'order-history') {
		$val = '<a href="admin.php?page=print-products-users-orders&user_id='.$user_id.'">'.__('View history', 'wp2print').'</a>';
	}
	return $val;
}

// add buttons to edit order page
add_action('woocommerce_admin_order_data_after_billing_address', 'print_products_edit_order_admin_order_data_after_billing_address', 25);
function print_products_edit_order_admin_order_data_after_billing_address($order) {
	$user_id = $order->get_customer_id();
	$user_group = (int)get_user_meta($user_id, '_user_group', true);
	$order_co_data = $order->get_meta('_order_co_data', true);
	if ($order_co_data) {
		if (isset($order_co_data['po_number']) && $order_co_data['po_number']) { ?>
			<p class="form-field"><strong><?php _e('PO Number', 'wp2print'); ?>:</strong> <?php echo $order_co_data['po_number']; ?></p>
			<?php
		}
	}
	if ($user_group || $user_id) { ?>
	<p class="form-field">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<?php if ($user_group) { ?>
					<td style="padding-right:15px;"><a href="admin.php?page=print-products-groups-orders&group_id=<?php echo $user_group; ?>" class="button"><?php _e("Group order history", 'wp2print'); ?></a></td>
				<?php } ?>
				<?php if ($user_id) { ?>
					<td style="padding-right:15px;"><a href="admin.php?page=print-products-users-orders&user_id=<?php echo $user_id; ?>" class="button"><?php _e("Order history", 'wp2print'); ?></a></td>
					<td><a href="admin.php?page=print-products-users-artwork-files&user_id=<?php echo $user_id; ?>" class="button"><?php _e("View user's files", 'wp2print'); ?></a></td>
				<?php } ?>
			</tr>
		</table>
	</p>
	<?php
	}
}

add_filter('woocommerce_product_description_heading', 'print_products_woocommerce_product_description_heading', 100);
function print_products_woocommerce_product_description_heading($heading) {
	$currtheme = get_option('stylesheet');
	if ($currtheme == 'ascend_premium' || $currtheme == 'ascend_premium_child') {
		$heading = '';
	}
	return $heading;
}

add_filter('woocommerce_admin_html_order_item_class', 'print_products_woocommerce_admin_html_order_item_class', 11, 3);
function print_products_woocommerce_admin_html_order_item_class($class, $item, $order) {
	if (strlen($class)) { $class .= ' '; }
	$class .= 'item-'.$item->get_id();
	return $class;
}

function print_products_admin_footer_edit_order_js() {
	$order_id = print_products_woocommerce_get_current_order_id();
	if ($order_id) {
		$order = wc_get_order($order_id);
		$order_co_data = $order->get_meta('_order_co_data', true);
		$line_items = $order->get_items(apply_filters('woocommerce_admin_order_item_types', 'line_item')); ?>
		<script>
		<?php foreach ($line_items as $item_id => $item) {
			$quantity = $item->get_quantity();
			$item_subtotal = $order->get_item_subtotal($item, false, true);
			$decimals = print_products_get_order_item_decimals($quantity); ?>
			jQuery('.woocommerce_order_items .item-<?php echo $item_id; ?> .item_cost .view').html('<?php echo wc_price($item_subtotal, array('decimals' => $decimals)); ?>');
		<?php } ?>
		</script>
		<?php if ($order_co_data) { ?>
		<script>var order_edit_label = '&nbsp;&nbsp;&nbsp;<?php _e('Edit', 'wp2print'); ?>&nbsp;&nbsp;&nbsp;'; order_edit_btn = true;</script>
		<form method="POST" class="co-edit-order-form">
		<input type="hidden" name="print_products_create_order_action" value="edit-order">
		<input type="hidden" name="order_edit_id" value="<?php echo $order_id; ?>">
		</form>
		<?php }
	}
}

// exclude order statuses on admin orders list
add_action('pre_get_posts', 'print_products_woocommerce_pre_get_posts');
function print_products_woocommerce_pre_get_posts($query) {
    global $post_type, $pagenow;
    if (!is_admin() && isset($query->query['post_type']) && $query->query['post_type'] == 'product') {
		$print_products_send_quote_options = get_option('print_products_send_quote_options');
		if (isset($print_products_send_quote_options['custom_product']) && $print_products_send_quote_options['custom_product']) {
			$query->set('post__not_in', array($print_products_send_quote_options['custom_product']));
		}
	}
    if (is_admin() && $pagenow == 'edit.php' && $post_type == 'shop_order' && !isset($_GET['post_status'])) {
		$include_statuses = print_products_woocommerce_get_orders_include_statuses();
		if ($include_statuses) {
			$query->query_vars['post_status'] = $include_statuses;
		}
	}
}

// hpos - exclude order statuses on admin orders list
add_filter( 'woocommerce_order_list_table_prepare_items_query_args', 'print_products_woocommerce_order_list_table_prepare_items_query_args');
function print_products_woocommerce_order_list_table_prepare_items_query_args($query_vars) {
    global $pagenow;
    if (is_admin() && $pagenow == 'admin.php' && isset($_GET['page']) && $_GET['page'] == 'wc-orders') {
		$include_statuses = print_products_woocommerce_get_orders_include_statuses();
		if ($include_statuses) {
			$query_vars['post_status'] = $include_statuses;
		}
	}
	return $query_vars;
}

function print_products_woocommerce_get_orders_include_statuses() {
	$include_statuses = false;
	$print_products_orders_options = get_option("print_products_orders_options");
	if (isset($print_products_orders_options['exclude_statuses'])) {
		$exclude_statuses = $print_products_orders_options['exclude_statuses'];
		if (is_array($exclude_statuses) && count($exclude_statuses)) {
			$include_statuses = array();
			$wc_order_statuses = wc_get_order_statuses();
			foreach($wc_order_statuses as $os_key => $os_name) {
				if (!in_array($os_key, $exclude_statuses)) {
					$include_statuses[] = $os_key;
				}
			}
			if (!count($include_statuses)) { $include_statuses[] = 'nothing'; }
		}
	}
	return $include_statuses;
}

// hide prices for non-logged users
$pp_prices_options = false;
function print_products_get_login_text() {
	global $pp_prices_options;
	$login_text = __('Login to view prices', 'wp2print');
	if (!$pp_prices_options) { $pp_prices_options = get_option("print_products_prices_options"); }
	if (is_array($pp_prices_options) && isset($pp_prices_options['login_text']) && strlen($pp_prices_options['login_text'])) {
		$login_text = $pp_prices_options['login_text'];
	}
	return $login_text;
}

function print_products_is_product_hidden_price($product_id) {
	if (!is_user_logged_in()) {
		$nl_hide_prices = (int)get_post_meta($product_id, '_nl_hide_prices', true);
		if ($nl_hide_prices == 1) {
			return true;
		}
	}
	return false;
}

function print_products_get_nl_login_link() {
	return '<a href="'.wc_get_account_endpoint_url('dashboard').'">'.print_products_get_login_text().'</a>';
}

add_action('woocommerce_after_order_details', 'print_products_woocommerce_after_order_details');
function print_products_woocommerce_after_order_details($order) {
	$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
	if (!$show_customer_details) {
		wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
	}
	$notes = $order->get_customer_order_notes();
	if ( $notes ) : ?>
		<section class="woocommerce-admin-notes" style="margin-bottom:20px;">
			<h2><?php esc_html_e( 'Admin notes', 'wp2print' ); ?></h2>
			<ol class="woocommerce-OrderUpdates commentlist notes">
				<?php foreach ( $notes as $note ) : ?>
					<li class="woocommerce-OrderUpdate comment note" style="padding:0;">
						<p class="woocommerce-OrderUpdate-meta meta"><strong><?php echo date_i18n( esc_html__( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); ?></strong></p>
						<?php echo wpautop( wptexturize( $note->comment_content ) ); ?>
					</li>
				<?php endforeach; ?>
			</ol>
		</section>
	<?php endif;
}

add_filter('woocommerce_new_customer_data', 'print_products_woocommerce_new_customer_data');
function print_products_woocommerce_new_customer_data($new_customer_data) {
	$default_role = get_option('default_role');
	if ($default_role) {
		$new_customer_data['role'] = $default_role;
	}
	return $new_customer_data;
}

add_action('woocommerce_new_order', 'print_products_woocommerce_new_order');
function print_products_woocommerce_new_order($order_id) {
	global $current_user;
	if (is_user_logged_in()) {
		if (in_array('subscriber', $current_user->roles)) {
			$current_user->set_role('customer');
		}
	}
}

function print_products_woocommerce_get_md5_order_id($oid) {
	global $wpdb;
	if (print_products_is_hpos_enabled()) {
		$order_id = $wpdb->get_var(sprintf("SELECT id FROM %swc_orders WHERE MD5(id) = '%s'", $wpdb->prefix, $oid));
	} else {
		$order_id = $wpdb->get_var(sprintf("SELECT ID FROM %sposts WHERE post_type = 'shop_order' AND MD5(ID) = '%s'", $wpdb->prefix, $oid));
	}
	return $order_id;
}

function print_products_woocommerce_get_orders_screen() {
	$orders_screen = 'shop_order';
	if (print_products_is_hpos_enabled()) {
		$orders_screen = 'woocommerce_page_wc-orders';
	}
	return $orders_screen;
}

function print_products_woocommerce_get_order_id($post) {
	if ($post instanceof WC_Order) {
		$order_id = $post->get_id();
	} else {
		$order_id = $post->ID;
	}
	return $order_id;
}

function print_products_woocommerce_get_current_order_id() {
	global $post;
	$order_id = 0;
	if ($post && is_object($post)) {
		$order_id = $post->ID;
	} else if (isset($_GET['id'])) {
		$order_id = (int)$_GET['id'];
	}
	return $order_id;
}

function print_products_woocommerce_get_order_edit_url($order_id) {
	if (print_products_is_hpos_enabled()) {
		return 'admin.php?page=wc-orders&action=edit&id='.$order_id;
	} else {
		return 'post.php?post='.$order_id.'&action=edit';
	}
}

add_filter('wp_mail_from', 'print_products_woocommerce_wp_mail_from');
function print_products_woocommerce_wp_mail_from($mail_from) {
	$woocommerce_email_from_address = get_option('woocommerce_email_from_address');
	if ($woocommerce_email_from_address) {
		$mail_from = $woocommerce_email_from_address;
	}
	return $mail_from;
}

add_action('woocommerce_before_single_product', 'print_products_woocommerce_before_single_product');
function print_products_woocommerce_before_single_product() {
	?>
	<script>
	<!--
	function matrix_single_add_to_cart_button(d) {
		if (d) {
			jQuery('.add-cart-form .single_add_to_cart_button').prop('disabled', true).addClass('disabled');
		} else {
			jQuery('.add-cart-form .single_add_to_cart_button').prop('disabled', false).removeClass('disabled');
		}
	}
	//--></script>
	<?php
}

add_action('before_woocommerce_init', 'print_products_woocommerce_before_woocommerce_init');
function print_products_woocommerce_before_woocommerce_init() {
	if (class_exists( \Automattic\WooCommerce\Utilities\FeaturesUtil::class )) {
		\Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility('custom_order_tables', 'wp2print/wp2print.php', true);
	}
}

// order item tracking number
$print_products_orders_options = get_option("print_products_orders_options");
function print_products_woocommerce_is_itn_enabled() {
	global $print_products_orders_options;
	if ($print_products_orders_options && isset($print_products_orders_options['enable-itn']) && $print_products_orders_options['enable-itn'] == 1) {
		return true;
	}
	return false;
}

function print_products_woocommerce_get_itn_label() {
	global $print_products_orders_options;
	return (isset($print_products_orders_options['itn-label']) && strlen($print_products_orders_options['itn-label'])) ? $print_products_orders_options['itn-label'] : __('Tracking number', 'wp2print');
}

add_action('woocommerce_admin_order_item_headers', 'print_products_woocommerce_admin_order_item_headers');
function print_products_woocommerce_admin_order_item_headers($order) {
	if (print_products_woocommerce_is_itn_enabled()) { ?>
		<th class="item_tracking"><?php esc_html_e('Item tracking number', 'wp2print'); ?></th>
		<?php
	}
}

add_action('woocommerce_admin_order_item_values', 'print_products_woocommerce_admin_order_item_values', 11, 3);
function print_products_woocommerce_admin_order_item_values($product, $item, $item_id) {
    global $post;
	if (print_products_woocommerce_is_itn_enabled()) {
		$item_type = $item->get_type();
		$item_tracking_number = wc_get_order_item_meta($item_id, '_item_tracking_number', true); ?>
		<td class="item_tracking">
			<?php if ($item_type == 'line_item') { ?>
				<input type="text" name="item_tracking_number[<?php echo $item_id; ?>]" value="<?php echo $item_tracking_number; ?>" class="item-tracking">
			<?php } ?>
		</td>
		<?php
	}
}

add_action('woocommerce_before_order_object_save', 'print_products_woocommerce_before_order_object_save');
function print_products_woocommerce_before_order_object_save($order_data){
	if (print_products_woocommerce_is_itn_enabled() && isset($_POST['item_tracking_number'])) {
		$order_items = $order_data->get_items('line_item');
		if ($order_items) {
			$order_items_tracking = array();
			foreach($order_items as $item_id => $item) {
				$item_tracking_number = $_POST['item_tracking_number'][$item_id];
				wc_update_order_item_meta($item_id, '_item_tracking_number', $item_tracking_number);
				if ($item_tracking_number) {
					$order_items_tracking[] = $item_tracking_number;
				}
			}
			$order_data->update_meta_data('_order_items_tracking', implode(' | ', $order_items_tracking));
		}
	}
}

add_filter('woocommerce_hidden_order_itemmeta', 'print_products_woocommerce_hidden_order_itemmeta');
function print_products_woocommerce_hidden_order_itemmeta($metakeys) {
	$metakeys[] = '_item_tracking_number';
	return $metakeys;
}

add_filter('woocommerce_order_table_search_query_meta_keys', 'print_products_woocommerce_order_table_search_query_meta_keys');
function print_products_woocommerce_order_table_search_query_meta_keys($meta_keys) {
	$meta_keys[] = '_order_items_tracking';
	return $meta_keys;
}

function print_products_woocommerce_get_item_tracking_number_orders($s) {
	global $wpdb;
	$orders = array();
	$wc_orders = $wpdb->get_results(sprintf("SELECT * FROM %swoocommerce_order_items oi LEFT JOIN %swoocommerce_order_itemmeta oim ON oim.order_item_id = oi.order_item_id AND oim.meta_key = '_item_tracking_number' WHERE oi.order_item_type = 'line_item' AND oim.meta_value LIKE '%s'", $wpdb->prefix, $wpdb->prefix, '%'.$s.'%'));
	if ($wc_orders) {
		foreach ($wc_orders as $wc_order) {
			$order_id = $wc_order->order_id;
			if (!in_array($order_id, $orders)) {
				$orders[] = $order_id;
			}
		}
	}
	return $orders;
}

function print_products_get_matrix_colours($printing_matrix_types) {
	global $print_products_settings;
	$matrix_colours = false;
	$colour_attribute = $print_products_settings['colour_attribute'];
	if ($colour_attribute && $printing_matrix_types) {
		foreach ($printing_matrix_types as $printing_matrix_type) {
			$mattributes = unserialize($printing_matrix_type->attributes);
			$materms = unserialize($printing_matrix_type->aterms);
			foreach ($mattributes as $mattribute) {
				if ($mattribute == $colour_attribute) {
					$aterms = $materms[$mattribute];
					if ($aterms) {
						return print_products_get_attribute_terms($aterms);
					}
				}
			}
		}
	}
	return $matrix_colours;
}

// my account page (Orders list) Tracking number
add_filter('woocommerce_my_account_my_orders_columns', 'print_products_woocommerce_my_account_my_orders_columns_tn', 22);
function print_products_woocommerce_my_account_my_orders_columns_tn($columns) {
	if (print_products_woocommerce_is_itn_enabled()) {
		$nckey = 'order-status';
		if (print_products_oistatus_allowed()) { $nckey = 'oistatus'; }
		$old_columns = $columns;
		$columns = array();
		foreach($old_columns as $c_key => $c_name) {
			$columns[$c_key] = $c_name;
			if ($c_key == $nckey) {
				$columns['tracknum'] = print_products_woocommerce_get_itn_label();
			}
		}
	}
	return $columns;
}

add_action('woocommerce_my_account_my_orders_column_tracknum', 'print_products_woocommerce_my_account_my_orders_column_tracknum');
function print_products_woocommerce_my_account_my_orders_column_tracknum($order) {
	$order_items = print_products_oistatus_get_order_items_tracking_nums($order);
	if ($order_items) { $oinum = count($order_items);
		foreach($order_items as $order_item) {
			if ($oinum > 1) { ?>
				<div class="ois-ma-tracknum"><?php echo $order_item['name']; ?>: <span><?php echo $order_item['itn']; ?></span></div>
			<?php } else { ?>
				<div class="ois-ma-tracknum"><span><?php echo $order_item['itn']; ?></span></div>
			<?php
			}
		}
	}
}

function print_products_oistatus_get_order_items_tracking_nums($order) {
	$tracking_nums = array();
	$order_items = $order->get_items();
	if ($order_items) {
		foreach($order_items as $order_item) {
			$item_id = $order_item->get_id();
			$item_type = $order_item->get_type();
			if ($item_type == 'line_item') {
				$item_tracking_number = wc_get_order_item_meta($item_id, '_item_tracking_number', true);
				if ($item_tracking_number) {
					$tracking_nums[] = array('name' => $order_item->get_name(), 'itn' => $item_tracking_number);
				}
			}
		}
	}
	return $tracking_nums;
}

// payment gateway
add_action('show_user_profile', 'print_products_woo_profile_field', 11);
add_action('edit_user_profile', 'print_products_woo_profile_field', 11);
function print_products_woo_profile_field($profileuser) {
	$user_invoice_payment = get_user_meta($profileuser->ID, '_user_invoice_payment', true);
	if (print_products_woo_is_allow_change_invoice_payment()) { ?>
		<h3><?php _e('Invoice Payment Method', 'wp2print'); ?></h3>
		<table class="form-table">
			<tr>
				<th><label><?php _e('Enable Invoice payment', 'wp2print'); ?></label></th>
				<td>
					<input type="checkbox" name="user_invoice_payment" value="1"<?php if ($user_invoice_payment) { echo ' CHECKED'; } ?>>
				</td>
			</tr>
		</table>
		<?php
	}
}

add_action('personal_options_update', 'print_products_woo_save_profile_field');
add_action('edit_user_profile_update', 'print_products_woo_save_profile_field');
function print_products_woo_save_profile_field($user_id) {
	if (print_products_woo_is_allow_change_invoice_payment()) {
		update_usermeta($user_id, '_user_invoice_payment', $_POST['user_invoice_payment']);
	}
}

add_filter('woocommerce_available_payment_gateways', 'print_products_woo_payment_gateway');
function print_products_woo_payment_gateway($available_gateways) {
	global $current_user_group, $current_user;

	$pgateways = WC()->payment_gateways();
	$payment_gateways = $pgateways->payment_gateways();
	$user_invoice_payment = get_user_meta($current_user->ID, '_user_invoice_payment', true);
	if (is_checkout()) {
		if ($current_user_group) {
			$payment_method = $current_user_group->payment_method;
			$invoice_zero = $current_user_group->invoice_zero;
			$cart_total = WC()->cart->total;
			if ($cart_total == 0 && $invoice_zero) {
				$available_gateways = array('cod' => $payment_gateways['cod']);
			} else if (strlen($payment_method)) {
				$available_gateways = array();
				$payment_methods = explode(';', $payment_method);
				foreach($payment_methods as $payment_method) {
					$available_gateways[$payment_method] = $payment_gateways[$payment_method];
				}
			}
		} else if ($user_invoice_payment) {
			if (!isset($available_gateways['cod'])) {
				$available_gateways = array('cod' => $payment_gateways['cod']);
			}
		}
	}
	return $available_gateways;
}

function print_products_woo_is_allow_change_invoice_payment() {
	global $current_user;
	if (in_array('administrator', $current_user->roles) || in_array('adminlite', $current_user->roles) || in_array('sales', $current_user->roles)) {
		return true;
	}
	return false;
}

add_action('woocommerce_email_header', 'print_products_woo_woocommerce_email_header', 150);
function print_products_woo_woocommerce_email_header($email_heading) {
	echo '<p>&nbsp;</p>';
}

add_action('woocommerce_email_after_order_table', 'print_products_woo_email_after_order_table', 9);
function print_products_woo_email_after_order_table($order) {
	global $wpdb;
	$show_button = false;
	$order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
	foreach ($order_items as $item_id => $item) {
		$order_item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = '%s'", $wpdb->prefix, $item_id));
		if ($order_item_data && !$show_button) {
			$artwork_source = get_post_meta($order_item_data->product_id, '_artwork_source', true);
			if ($order_item_data->atcaction == 'artwork' && !strlen($order_item_data->artwork_files) && ($artwork_source == 'artwork' || $artwork_source == 'both')) {
				$show_button = true;
			}
		}
	}
	if ($show_button) {
		$order_id = $order->get_id();
		$myaccount_page_id = get_option('woocommerce_myaccount_page_id');
		$myaccount_page = get_permalink($myaccount_page_id);
		if (substr($myaccount_page, -1) != '/') { $myaccount_page .= '/'; }
		$myaccount_page .= 'orders-missing-files/?view='.$order_id;
		?>
		<p><?php _e('If you have the files you would like us to print, please upload them now:', 'wp2print'); ?> <?php echo print_products_email_button($myaccount_page, __('Upload your own design', 'wp2print'), '#557DA1'); ?></p>
		<?php
	}
}

add_action('wp_head', 'print_products_woo_wp_head');
function print_products_woo_wp_head() {
	global $post;
	if (is_single() && $post && $post->post_type == 'product') {
		$product_type = print_products_get_type($post->ID);
		if (print_products_is_wp2print_type($product_type)) { ?>
			<style>.wp-block-column .wc-block-components-product-price{display:none !important;}</style>
			<?php
		}
	}
}
?>