<?php
add_filter('woocommerce_account_menu_items', 'print_products_user_files_account_menu_items', 40);
function print_products_user_files_account_menu_items($items) {
	$new_items = array();
	foreach($items as $ikey => $ival) {
		$inow = false;
		$new_items[$ikey] = $ival;
		if (isset($items['orders-rejected-files'])) {
			if ($ikey == 'orders-rejected-files') {
				$inow = true;
			}
		} else if (isset($items['orders-missing-files'])) {
			if ($ikey == 'orders-missing-files') {
				$inow = true;
			}
		} else  if (isset($items['orders-awaiting-approval'])) {
			if ($ikey == 'orders-awaiting-approval') {
				$inow = true;
			}
		} else {
			if ($ikey == 'orders') {
				$inow = true;
			}
		}
		if ($inow) {
			$new_items['user-files'] = __('Files', 'wp2print');
		}
	}
	return $new_items;
}

add_action('init', 'print_products_user_files_rewrite_endpoint');
function print_products_user_files_rewrite_endpoint() {
	if (print_products_my_account_is_front()) {
		add_rewrite_endpoint('user-files', EP_ROOT | EP_PAGES);
	} else {
		add_rewrite_endpoint('user-files', EP_PAGES);
	}
	flush_rewrite_rules();
}

add_filter('query_vars', 'print_products_user_files_query_vars', 10);
function print_products_user_files_query_vars($vars) {
	$vars[] = 'user-files';
	return $vars;
}

add_action('parse_request', 'print_products_user_files_parse_request', 10);
function print_products_user_files_parse_request() {
	global $wp;
	$var = 'user-files';
	if (isset($wp->query_vars['name']) && $wp->query_vars['name'] == $var) {
		unset($wp->query_vars['name']);
		$wp->query_vars[$var] = $var;
	}
}

add_action('pre_get_posts', 'print_products_user_files_pre_get_posts');
function print_products_user_files_pre_get_posts($q) {
	if ( ! $q->is_main_query() ) {
		return;
	}
	if (print_products_is_showing_page_on_front($q) && ! print_products_page_on_front_is($q->get('page_id'))) {
		$_query = wp_parse_args($q->query);
		$qv_array = array('user-files' => 'user-files');
		if (!empty($_query) && array_intersect( array_keys($_query), array_keys($qv_array))) {
			$q->is_page     = true;
			$q->is_home     = false;
			$q->is_singular = true;
			$q->set('page_id', (int)get_option( 'page_on_front'));
			add_filter('redirect_canonical', '__return_false');
		}
	}
}

add_filter('the_title', 'print_products_user_files_the_title', 12, 2);
function print_products_user_files_the_title($title, $id) {
	global $wp_query;
	if (is_account_page() && is_main_query() && in_the_loop() && isset($wp_query->query_vars['user-files']) && !is_admin()) {
		$title = __('Files', 'wp2print');
	}
	return $title;
}

add_action('woocommerce_account_user-files_endpoint', 'print_products_user_files_account_page');
function print_products_user_files_account_page() {
	include PRINT_PRODUCTS_TEMPLATES_DIR . 'user-files.php';
}

function print_products_user_files_get_types() {
	return array(__('Artwork', 'wp2print'), __('Database', 'wp2print'));
}

function print_products_user_files_get_admin_uploaded_files($user_id) {
	global $wpdb;
	return $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_user_files WHERE user_id = %s ORDER BY file_id DESC", $wpdb->prefix, $user_id));
}

function print_products_user_files_get_uploaded_files($user_id) {
	global $wpdb;
	$efiles = array();
	$uploaded_files = array();
	if (print_products_is_hpos_enabled()) {
		$user_orders = $wpdb->get_results(sprintf("SELECT oi.order_id, o.date_created_gmt as order_date, ppoi.artwork_files FROM %swoocommerce_order_items oi LEFT JOIN %swc_orders o ON o.id = oi.order_id LEFT JOIN %sprint_products_order_items ppoi ON ppoi.item_id = oi.order_item_id WHERE oi.order_item_type = 'line_item' AND o.customer_id = '%s' AND ppoi.artwork_files != '' ORDER BY oi.order_id DESC", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $user_id));
	} else {
		$user_orders = $wpdb->get_results(sprintf("SELECT oi.order_id, o.post_date as order_date, ppoi.artwork_files FROM %swoocommerce_order_items oi LEFT JOIN %sposts o ON o.ID = oi.order_id LEFT JOIN %spostmeta pm ON pm.post_id = oi.order_id LEFT JOIN %sprint_products_order_items ppoi ON ppoi.item_id = oi.order_item_id WHERE oi.order_item_type = 'line_item' AND pm.meta_key = '_customer_user' AND pm.meta_value = '%s' AND ppoi.artwork_files != '' ORDER BY oi.order_id DESC", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $user_id));
	}
	if ($user_orders) {
		foreach($user_orders as $user_order) {
			if (strlen($user_order->artwork_files)) {
				$artwork_files = unserialize($user_order->artwork_files);
				foreach($artwork_files as $afile) {
					$fname = basename($afile);
					if (!in_array($fname, $efiles)) {
						$uploaded_files[] = array('file_name' => $fname, 'file_url' => $afile, 'created' => $user_order->order_date);
						$efiles[] = $fname;
					}
				}
			}
		}
	}
	return $uploaded_files;
}

add_action('wp_loaded', 'print_products_user_files_actions_init');
function print_products_user_files_actions_init() {
	global $wpdb;
	if (isset($_POST['user_files_action'])) {
		switch($_POST['user_files_action']) {
			case 'admin-upload':
				$user_id = $_POST['user_id'];
				$ftype = $_POST['ftype'];
				$artworkfiles = $_POST['artworkfiles'];
				if (strlen($artworkfiles)) {
					$artworkfiles = explode(';', $artworkfiles);
					foreach($artworkfiles as $artworkfile) {
						$insert = array();
						$insert['file_url'] = $artworkfile;
						$insert['type'] = $ftype;
						$insert['user_id'] = $user_id;
						$insert['created'] = current_time('mysql');
						$wpdb->insert($wpdb->prefix.'print_products_user_files', $insert);
					}
				}
				$ufprocessed = 1;
			break;
			case 'admin-delete-file':
				$user_id = $_POST['user_id'];
				$file_id = (int)$_POST['file_id'];
				if ($file_id) {
					$wpdb->query(sprintf("DELETE FROM %sprint_products_user_files WHERE file_id = %s", $wpdb->prefix, $file_id));
				}
				$ufprocessed = 2;
			break;
		}
		wp_redirect('admin.php?page=print-products-users-artwork-files&user_id='.$user_id.'&ufprocessed='.$ufprocessed);
		exit;
	}
}
?>