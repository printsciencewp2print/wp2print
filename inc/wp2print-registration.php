<?php
function print_products_registration_is_validate() {
	$print_products_registration_options = get_option('print_products_registration_options');
	if ($print_products_registration_options && isset($print_products_registration_options['validate']) && $print_products_registration_options['validate'] == 1) {
		return true;
	}
	return false;
}

$is_registration_error = false;
add_filter('registration_errors', 'print_products_registration_errors', 11, 3);
function print_products_registration_errors($errors, $user_login, $user_email) {
	global $is_registration_error;
	if ($user_login && $user_email) {
		$errors = print_products_registration_check_process($errors, $user_email);
	}
	return $errors;
}

add_filter('woocommerce_process_registration_errors', 'print_products_registration_woocommerce_process_registration_errors', 20, 4);
function print_products_registration_woocommerce_process_registration_errors($errors, $user_login, $user_pass, $user_email) {
	global $is_registration_error;
	if ($user_login && $user_pass && $user_email) {
		$errors = print_products_registration_check_process($errors, $user_email);
	}
	return $errors;
}

function print_products_registration_check_process($errors, $user_email) {
	global $is_registration_error;
	if (!$errors->has_errors() && !isset($_GET['regconfirm'])) {
		if (print_products_registration_is_validate()) {
			$is_registration_error = true;
			print_products_registration_confirmation($user_email);
			$errors->add( 'confirmation_email', __( 'Please check your email box and confirm registration.', 'wp2print' ) );
		} else if (!print_products_registration_is_valid_nonce()) {
			$errors->add( 'wp2print_nonce', __( 'Registration key is incorrect. Please try again.', 'wp2print' ) );
		}
	}
	return $errors;
}

add_filter('woocommerce_add_error', 'print_products_registration_woocommerce_add_error', 11);
function print_products_registration_woocommerce_add_error($message) {
	global $is_registration_error;
	if ($is_registration_error) {
		$message = str_replace('<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ', '', $message);
	}
	return $message;
}

function print_products_registration_confirmation($user_email) {
	$print_products_registration_options = get_option('print_products_registration_options');
	$reg_key = md5($user_email);
	$confirmation_link = site_url('wp-login.php?regconfirm='.$reg_key);
	$print_products_registrations = get_option('print_products_registrations');
	if (!is_array($print_products_registrations)) { $print_products_registrations = array(); }
	$print_products_registrations[$reg_key] = $_POST;
	update_option('print_products_registrations', $print_products_registrations);

	if (strlen($print_products_registration_options['email_subject'])) {
		$email_subject = $print_products_registration_options['email_subject'];
		$email_message = $print_products_registration_options['email_message'];
		$email_message = str_replace('{CONFIRMATION-LINK}', '<a href="'.$confirmation_link.'">'.$confirmation_link.'</a>', $email_message);
		print_products_send_wc_mail($user_email, $email_subject, $email_message);
	}
}

add_action('wp_loaded', 'print_products_registration_init');
function print_products_registration_init() {
	if (isset($_GET['regconfirm']) && $_GET['regconfirm']) {
		$reg_key = $_GET['regconfirm'];
		$print_products_registrations = get_option('print_products_registrations');
		if ($print_products_registrations && isset($print_products_registrations[$reg_key])) {
			$reg_data = $print_products_registrations[$reg_key];
			$user_login = isset($reg_data['user_login']) ? $reg_data['user_login'] : '';
			$user_email = isset($reg_data['user_email']) ? $reg_data['user_email'] : '';
			$_POST = $reg_data;
			if (isset($reg_data['woocommerce-register-nonce'])) {
				WC_Form_Handler::process_registration();
			} else {
				$errors = register_new_user($user_login, $user_email);
				if (!is_wp_error($errors)) {
					unset($print_products_registrations[$reg_key]);
					update_option('print_products_registrations', $print_products_registrations);
					$redirect_to = !empty($reg_data['redirect_to']) ? $reg_data['redirect_to'] : 'wp-login.php?checkemail=registered';
					wp_safe_redirect($redirect_to);
					exit;
				} else {
					wp_die(__('Registration error.', 'wp2print'));
				}
			}
		} else {
			wp_die(__('Incorrect confirmation key.', 'wp2print'));
		}
	}
}

add_action('register_form', 'print_products_registration_register_form');
add_action('woocommerce_register_form_end', 'print_products_registration_register_form');
function print_products_registration_register_form() {
	$time_num = (int)date('YmdHis');
	$time_key1 = $time_num + 50505050505050;
	$time_key2 = $time_num + 100 + 70707070707070;
	echo '<input type="hidden" name="wp2print_registration_nonce" value="'.$time_key1.'-'.$time_key2.'">';
}

function print_products_registration_is_valid_nonce() {
	$time_key = (int)date('YmdHis');
	$start_key = 0;
	$end_key = 0;
	$wp2print_registration_nonce = $_POST['wp2print_registration_nonce'];
	if (strlen($wp2print_registration_nonce) && strpos($wp2print_registration_nonce, '-')) {
		$nonce_data = explode('-', $wp2print_registration_nonce);
		$start_key = (int)$nonce_data[0] - 50505050505050;
		$end_key = (int)$nonce_data[1] - 70707070707070;
	}
	if ($time_key < $start_key || $time_key > $end_key) {
		return false;
	}
	return true;
}
?>