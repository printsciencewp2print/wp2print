<?php
add_filter('woocommerce_account_menu_items', 'print_products_orders_missing_files_account_menu_items', 20);
function print_products_orders_missing_files_account_menu_items($items) {
	$new_items = array();
	foreach($items as $ikey => $ival) {
		$new_items[$ikey] = $ival;
		if (isset($items['orders-awaiting-approval'])) {
			if ($ikey == 'orders-awaiting-approval') {
				$new_items['orders-missing-files'] = __('Orders missing files', 'wp2print');
			}
		} else {
			if ($ikey == 'orders') {
				$new_items['orders-missing-files'] = __('Orders missing files', 'wp2print');
			}
		}
	}
	return $new_items;
}

add_action('init', 'print_products_orders_missing_files_rewrite_endpoint');
function print_products_orders_missing_files_rewrite_endpoint() {
	if (print_products_my_account_is_front()) {
		add_rewrite_endpoint('orders-missing-files', EP_ROOT | EP_PAGES);
	} else {
		add_rewrite_endpoint('orders-missing-files', EP_PAGES);
	}
	flush_rewrite_rules();
}

add_filter('query_vars', 'print_products_orders_missing_files_query_vars', 10);
function print_products_orders_missing_files_query_vars($vars) {
	$vars[] = 'orders-missing-files';
	return $vars;
}

add_action('parse_request', 'print_products_orders_missing_files_parse_request', 10);
function print_products_orders_missing_files_parse_request() {
	global $wp;
	$var = 'orders-missing-files';
	if (isset($wp->query_vars['name']) && $wp->query_vars['name'] == $var) {
		unset($wp->query_vars['name']);
		$wp->query_vars[$var] = $var;
	}
}

add_action('pre_get_posts', 'print_products_orders_missing_files_pre_get_posts');
function print_products_orders_missing_files_pre_get_posts($q) {
	if ( ! $q->is_main_query() ) {
		return;
	}
	if (print_products_is_showing_page_on_front($q) && ! print_products_page_on_front_is($q->get('page_id'))) {
		$_query = wp_parse_args($q->query);
		$qv_array = array('orders-missing-files' => 'orders-missing-files');
		if (!empty($_query) && array_intersect( array_keys($_query), array_keys($qv_array))) {
			$q->is_page     = true;
			$q->is_home     = false;
			$q->is_singular = true;
			$q->set('page_id', (int)get_option( 'page_on_front'));
			add_filter('redirect_canonical', '__return_false');
		}
	}
}

add_action('woocommerce_account_orders-missing-files_endpoint', 'print_products_orders_missing_files_account_page');
function print_products_orders_missing_files_account_page() {
	include PRINT_PRODUCTS_TEMPLATES_DIR . 'orders-missing-files.php';
}

add_action('wp_loaded', 'print_products_orders_missing_files_actions');
function print_products_orders_missing_files_actions() {
	global $wpdb, $current_user;
	if (isset($_POST['orders_missing_files_submit']) && $_POST['orders_missing_files_submit'] == 'true') {
		$order_id = $_POST['order_id'];
		$item_id = $_POST['item_id'];
		$artwork_files = $_POST['artworkfiles'];
		$uploaded_files = array();
		if (strlen($artwork_files)) {
			$uploaded_files = explode(';', $artwork_files);
			$artwork_files = serialize(explode(';', $artwork_files));
		}
		$wpdb->update($wpdb->prefix.'print_products_order_items', array('artwork_files' => $artwork_files), array('item_id' => $item_id));
		if (count($uploaded_files)) {
			$order = wc_get_order($order_id);
			// send email to admin
			$nl = '<br>';
			$admin_email = get_option('admin_email');
			$subject = __('New Order Files', 'wp2print');
			$heading = __('New Order Files', 'wp2print');

			$message  = __('Order ID', 'wp2print').': <a href="'.site_url('/wp-admin/'.print_products_woocommerce_get_order_edit_url($order_id)).'">'.$order_id.'</a>'.$nl.$nl;
			$message .= __('Files', 'wp2print').': '.$nl;
			foreach($uploaded_files as $uploaded_file) {
				$message .= '<a href="'.print_products_get_amazon_file_url($uploaded_file).'">'.basename($uploaded_file).'</a>'.$nl;
			}
			print_products_send_wc_mail($admin_email, $subject, $message, $heading);
			$co_add_files = $order->get_meta('_co_add_files', true);
			if ($co_add_files == 'sent') {
				print_products_update_order_meta($order, '_co_add_files', 'added');
			}
			echo __('Files were successfully saved.', 'wp2print');
		} else {
			echo __('Files were successfully removed.', 'wp2print');
		}

		exit;
	}
}

add_filter('the_title', 'print_products_orders_missing_files_the_title', 12, 2);
function print_products_orders_missing_files_the_title($title, $id) {
	global $wp_query;
	if (is_account_page() && is_main_query() && in_the_loop() && isset($wp_query->query_vars['orders-missing-files']) && !is_admin()) {
		$title = __('Orders missing files', 'wp2print');
	}
	return $title;
}
?>