<?php
$docket_manager_options = get_option('print_products_docket_manager_options');

add_action('woocommerce_checkout_update_order_meta', 'print_products_docketmanager_created_order');
function print_products_docketmanager_created_order($order_id) {
	global $docket_manager_options;
	if (isset($docket_manager_options['enable']) && $docket_manager_options['enable'] == 1) {
		$xmldata = print_products_docketmanager_create_xml($order_id);
		if ($xmldata) {
			print_products_docketmanager_send_xml($xmldata[1]);
		}
	}
}

function print_products_docketmanager_get_xml_filename($order_id) {
	return 'docket-manager-'.$order_id.'.xml';
}

function print_products_docketmanager_create_xml($order_id) {
	$upload_dir = wp_get_upload_dir();
	$xmlfilename = print_products_docketmanager_get_xml_filename($order_id);
	$xmlfilepath = $upload_dir['path'].'/'.$xmlfilename;
	$xmlfileurl = $upload_dir['url'].'/'.$xmlfilename;
	if (class_exists('SimpleXMLElement')) {
		$order = wc_get_order($order_id);
		$order_status = $order->get_status();
		$order_items = $order->get_items('line_item');

		$xml = new DOMDocument();
		$xml->encoding = 'utf-8';
		$xml->xmlVersion = '1.0';
		$xml->formatOutput = true;

		// ImportOrderRequest
		$root = $xml->createElement('ImportOrderRequest');
		$root->setAttributeNode(new DOMAttr('Version', '1.0'));

		// ServiceInformation
		$ServiceInformation = $xml->createElement('ServiceInformation');

		$ServiceName = $xml->createElement('ServiceName', get_bloginfo('name'));
		$ServiceInformation->appendChild($ServiceName);

		$ServiceUrl = $xml->createElement('ServiceUrl', home_url());
		$ServiceInformation->appendChild($ServiceUrl);

		$transaction_id = $order->get_transaction_id(); if (!$transaction_id) { $transaction_id = $order_id; }
		$ServiceTransactionId = $xml->createElement('ServiceTransactionId', $transaction_id);
		$ServiceInformation->appendChild($ServiceTransactionId);

		$root->appendChild($ServiceInformation);

		// Customer
		$Customer = $xml->createElement('Customer');

		$CustomerId = $xml->createElement('CustomerId', $order->get_customer_id());
		$Customer->appendChild($CustomerId);

		$Name = $xml->createElement('Name', $order->get_billing_first_name().' '.$order->get_billing_last_name());
		$Customer->appendChild($Name);

		$Phone = $xml->createElement('Phone', $order->get_billing_phone());
		$Customer->appendChild($Phone);

		$Email = $xml->createElement('Email', $order->get_billing_email());
		$Customer->appendChild($Email);

		$ContactName = $xml->createElement('ContactName', $order->get_billing_first_name().' '.$order->get_billing_last_name());
		$Customer->appendChild($ContactName);

		$ContactPhone = $xml->createElement('ContactPhone', $order->get_billing_phone());
		$Customer->appendChild($ContactPhone);

		$ContactEmail = $xml->createElement('ContactEmail', $order->get_billing_email());
		$Customer->appendChild($ContactEmail);

		$root->appendChild($Customer);

		// ShippingLocation
		$ShippingLocation = $xml->createElement('ShippingLocation');

		$AddressName = $xml->createElement('AddressName', __('Shipping Address', 'wp2print'));
		$ShippingLocation->appendChild($AddressName);

		$shipping_address = $order->get_shipping_address_1();
		if ($order->get_shipping_address_2()) { $shipping_address = $shipping_address . ' ' . $order->get_shipping_address_2(); }
		$Address = $xml->createElement('Address', $shipping_address);
		$ShippingLocation->appendChild($Address);

		$City = $xml->createElement('City', $order->get_shipping_city());
		$ShippingLocation->appendChild($City);

		$PostalZipCode = $xml->createElement('PostalZipCode', $order->get_shipping_postcode());
		$ShippingLocation->appendChild($PostalZipCode);

		$ProvinceState = $xml->createElement('ProvinceState', $order->get_shipping_state());
		$ShippingLocation->appendChild($ProvinceState);

		$Country = $xml->createElement('Country', $order->get_shipping_country());
		$ShippingLocation->appendChild($Country);

		$ShippingMethod = $xml->createElement('ShippingMethod', $order->get_shipping_method());
		$ShippingLocation->appendChild($ShippingMethod);

		$root->appendChild($ShippingLocation);

		// BillingLocation
		$BillingLocation = $xml->createElement('BillingLocation');

		$AddressName = $xml->createElement('AddressName', __('Billing Address', 'wp2print'));
		$BillingLocation->appendChild($AddressName);

		$billing_address = $order->get_billing_address_1();
		if ($order->get_billing_address_2()) { $billing_address = $shipping_address . ' ' . $order->get_billing_address_2(); }
		$Address = $xml->createElement('Address', $billing_address);
		$BillingLocation->appendChild($Address);

		$City = $xml->createElement('City', $order->get_billing_city());
		$BillingLocation->appendChild($City);

		$PostalZipCode = $xml->createElement('PostalZipCode', $order->get_billing_postcode());
		$BillingLocation->appendChild($PostalZipCode);

		$ProvinceState = $xml->createElement('ProvinceState', $order->get_billing_state());
		$BillingLocation->appendChild($ProvinceState);

		$Country = $xml->createElement('Country', $order->get_billing_country());
		$BillingLocation->appendChild($Country);

		$root->appendChild($BillingLocation);

		// PurchaseOrderNumber
		$po_number = $order_id;
		$order_co_data = $order->get_meta('_order_co_data', true);
		if ($order_co_data) {
			if (isset($order_co_data['po_number']) && $order_co_data['po_number']) {
				$po_number = $order_co_data['po_number'];
			}
		}
		$PurchaseOrderNumber = $xml->createElement('PurchaseOrderNumber', $po_number);
		$root->appendChild($PurchaseOrderNumber);

		// Order
		$Order = $xml->createElement('Order');

		$OrderId = $xml->createElement('OrderId', $order->get_order_number());
		$Order->appendChild($OrderId);

		$OrderDate = $xml->createElement('OrderDate', date('m/d/Y h:i:s A', strtotime($order->get_date_created())));
		$Order->appendChild($OrderDate);

		$OrderName = $xml->createElement('OrderName', 'Order #'.$order_id);
		$Order->appendChild($OrderName);

		$GenericNote = $xml->createElement('GenericNote', 'Order '.$order_status);
		$Order->appendChild($GenericNote);

		$ShippingTotal = $xml->createElement('ShippingTotal', print_products_docketmanager_amount($order->get_shipping_total()));
		$Order->appendChild($ShippingTotal);

		$SubTotal = $xml->createElement('SubTotal', print_products_docketmanager_amount($order->get_subtotal()));
		$Order->appendChild($SubTotal);

		$total = $order->get_total();
		$total_tax = $order->get_total_tax();

		$TaxTotal = $xml->createElement('TaxTotal', print_products_docketmanager_amount($total_tax));
		$Order->appendChild($TaxTotal);

		$tax_percentage = 0;
		if ($total_tax) {
			$tax_percentage = 100 * (($total/($total - $total_tax)) - 1);
		}
		$TaxPercentage = $xml->createElement('TaxPercentage', print_products_docketmanager_amount($tax_percentage));
		$Order->appendChild($TaxPercentage);

		$GrandTotal = $xml->createElement('GrandTotal', print_products_docketmanager_amount($total));
		$Order->appendChild($GrandTotal);

		$PaymentMethod = $xml->createElement('PaymentMethod', $order->get_payment_method_title());
		$Order->appendChild($PaymentMethod);

		$paidval = 'False';
		if ($order_status == 'processing' || $order_status == 'completed') { $paidval = 'True'; }
		$Paid = $xml->createElement('Paid', $paidval);
		$Order->appendChild($Paid);

		$Posted = $xml->createElement('Posted', 'True');
		$Order->appendChild($Posted);

		// Items
		$Items = $xml->createElement('Items');

		print_products_docketmanager_aterms();

		foreach($order_items as $order_item) {
			$item_id = $order_item->get_id();
			$product_id = $order_item->get_product_id();
			$product = $order_item->get_product();
			$order_item_data = print_products_docketmanager_get_order_item_data($item_id);

			$Item = $xml->createElement('Item');

			$ItemTotal = $xml->createElement('ItemTotal', print_products_docketmanager_amount($order_item->get_total()));
			$Item->appendChild($ItemTotal);

			$Quantity = $xml->createElement('Quantity', $order_item->get_quantity());
			$Item->appendChild($Quantity);

			$ItemName = $xml->createElement('ItemName', $order_item->get_name());
			$Item->appendChild($ItemName);

			$ItemCode = $xml->createElement('ItemCode', print_products_docketmanager_get_item_code($product_id, $product, $order_item_data));
			$Item->appendChild($ItemCode);

			$Description = $xml->createElement('Description', str_replace(array(chr(10), chr(13)), ' ', strip_tags($product->get_description())));
			$Item->appendChild($Description);

			if ($order_item_data) {
				if ($order_item_data['size']) {
					$FinishSize = $xml->createElement('FinishSize', $order_item_data['size']);
					$Item->appendChild($FinishSize);
				}

				if ($order_item_data['color']) {
					$ColorName = $xml->createElement('ColorName', $order_item_data['color']);
					$Item->appendChild($ColorName);
				}

				if ($order_item_data['pressnote']) {
					$PressNote = $xml->createElement('PressNote', $order_item_data['pressnote']);
					$Item->appendChild($PressNote);
				}

				if ($order_item_data['atrwork_file']) {
					$DownloadUrl = $xml->createElement('DownloadUrl', $order_item_data['atrwork_file']);
					$Item->appendChild($DownloadUrl);
				}
			}

			$Items->appendChild($Item);
		}

		$Order->appendChild($Items);

		$root->appendChild($Order);

		// ------------------------------------------

		$xml->appendChild($root);

		$xml->save($xmlfilepath);

		//@unlink($xmlfilepath);

		return array($xmlfileurl, $xmlfilepath);
	}
}

function print_products_docketmanager_send_xml($xmlfile) {
	global $docket_manager_options;
	$xml = file_get_contents($xmlfile);

	$dm_username = 'dmxml';
	if (isset($docket_manager_options['username']) && $docket_manager_options['username']) {
		$dm_username = $docket_manager_options['username'];
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://'.$dm_username.'.docketmanager.net/api/importorderrequest');
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}

function print_products_docketmanager_amount($amount) {
	return number_format($amount, 2);
}

$sc_terms = array();
function print_products_docketmanager_aterms() {
	global $wpdb, $print_products_settings, $sc_terms;
	$aids = array();
	if (isset($print_products_settings['size_attribute']) && $print_products_settings['size_attribute']) {
		$aids[] = $print_products_settings['size_attribute'];
	}
	if (isset($print_products_settings['colour_attribute']) && $print_products_settings['colour_attribute']) {
		$aids[] = $print_products_settings['colour_attribute'];
	}
	if (count($aids)) {
		$attributes = $wpdb->get_results(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies WHERE attribute_id IN (%s)", $wpdb->prefix, implode(',', $aids)));
		if ($attributes) {
			$taxs = array();
			foreach($attributes as $attribute) {
				$taxs[] = 'pa_'.$attribute->attribute_name;
			}
			$attr_terms = $wpdb->get_results(sprintf("SELECT t.*, tt.taxonomy FROM %sterms t LEFT JOIN %sterm_taxonomy tt ON tt.term_id = t.term_id WHERE tt.taxonomy IN ('%s') ORDER BY t.term_order, t.name", $wpdb->prefix, $wpdb->prefix, implode("','", $taxs)));
			if ($attr_terms) {
				foreach($attr_terms as $attr_term) {
					$sc_terms[$attr_term->term_id] = $attr_term->name;
				}
			}
		}
	}
}

function print_products_docketmanager_get_item_code($product_id, $product, $order_item_data) {
	global $docket_manager_options;
	$item_code = $product_id;
	if ($docket_manager_options && isset($docket_manager_options['source']) && $docket_manager_options['source']) {
		switch ($docket_manager_options['source']) {
			case 'sku':
				$item_code = get_post_meta($product_id, '_sku', true);
				if ($order_item_data && isset($order_item_data['additional'])) {
					$additional = $order_item_data['additional'];
					if (isset($additional['sku']) && $additional['sku']) {
						$item_code = $additional['sku'];
					}
				}
			break;
			case 'slug':
				$item_code = $product->get_slug();
			break;
		}
	}
	return $item_code;
}

function print_products_docketmanager_get_order_item_data($item_id) {
	global $wpdb, $print_products_settings, $sc_terms;

	$size_attribute = $print_products_settings['size_attribute'];
	$colour_attribute = $print_products_settings['colour_attribute'];

	$order_item_data = false;
	$item_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_order_items WHERE item_id = %s", $wpdb->prefix, $item_id));
	if ($item_data) {
		$size = '';
		$color = '';
		$product_attributes = unserialize($item_data->product_attributes);
		if ($product_attributes) {
			foreach($product_attributes as $product_attribute) {
				$aarray = explode(':', $product_attribute);
				if ($size_attribute && $aarray[0] == $size_attribute) {
					$size = $sc_terms[$aarray[1]];
				} else if ($aarray[0] == $colour_attribute) {
					$color = $sc_terms[$aarray[1]];
				}
			}
		}

		$pressnote = array();
		$attributes = print_products_get_product_attributes_list($item_data);
		if ($attributes) {
			foreach ($attributes as $attribute) {
				$pressnote[] = $attribute['name'].': '.$attribute['value'];
			}
		}

		$atrwork_file = '';
		$pdf_files = wc_get_order_item_meta($item_id, '_pdf_link', true);
		if (strlen($pdf_files)) {
			$pdf_files = explode(';', $pdf_files);
			$atrwork_file = $pdf_files[0];
		} else if (strlen($item_data->artwork_files)) {
			$artwork_files = unserialize($item_data->artwork_files);
			$atrwork_file = print_products_get_amazon_file_url($artwork_files[0]);
		}
		$order_item_data = array(
			'size' => $size,
			'color' => $color,
			'pressnote' => implode(' | ', $pressnote),
			'atrwork_file' => str_replace('&', '&amp;', $atrwork_file),
			'additional' => unserialize($item_data->additional)
		);
	}
	return $order_item_data;
}

// add field to user account
function print_products_docketmanager_admin_page() {
	$order_id = isset($_GET['order_id']) ? $_GET['order_id'] : '';
	?>
	<div class="wrap wp2print-docketmanager-wrap">
		<h2><?php _e('Docket Manager', 'wp2print'); ?></h2>
		<?php if (isset($_GET['dmsent']) && $_GET['dmsent']) { ?>
			<div class="notice notice-success">
				<p><?php _e('XML File was successfully submitted.', 'wp2print'); ?></p>
			</div>
		<?php } else if (isset($_GET['dmerror']) && $_GET['dmerror']) { ?>
			<div class="notice notice-error">
				<p><?php echo stripcslashes($_GET['dmerror']); ?></p>
			</div>
		<?php } ?>
		<form class="pp-search-form">
			<input type="hidden" name="page" value="print-products-docket-manager">
			<p>
				<input id="post-search-input" type="text" name="order_id" value="<?php echo $order_id; ?>" placeholder="Order ID" style="width:120px;">
				<input id="search-submit" type="submit" class="button" value="<?php _e('Search', 'wp2print'); ?>">
			</p>
		</form>
		<?php if ($order_id) {
			$xmldata = print_products_docketmanager_create_xml($order_id);
			if ($xmldata) { ?>
				<p class="pp-result">
					<?php _e('XML File', 'wp2print'); ?>: <a href="<?php echo $xmldata[0]; ?>" target="_blank"><?php echo basename($xmldata[0]); ?></a>
				</p>
				<form method="POST">
				<input type="hidden" name="docket_manager_action" value="send-xml">
				<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
				<input type="hidden" name="xml_file" value="<?php echo $xmldata[1]; ?>">
				<input type="submit" value="<?php _e('POST', 'wp2print'); ?>" class="button button-primary">
				</form>
			<?php } ?>
		<?php } ?>
	</div>
	<?php
}

add_action('wp_loaded', 'print_products_docketmanager_admin_actions');
function print_products_docketmanager_admin_actions() {
	if (isset($_POST['docket_manager_action']) && $_POST['docket_manager_action'] == 'send-xml') {
		$xml_file = $_POST['xml_file'];
		$order_id = $_POST['order_id'];
		if (file_exists($xml_file)) {
			$send_xml = print_products_docketmanager_send_xml($xml_file);
			$send_xml = json_decode($send_xml);
			if (isset($send_xml->ImportSuccessful) && $send_xml->ImportSuccessful) {
				$rparams = 'dmsent=1';
			} else {
				$rparams = 'dmerror='.urlencode($send_xml->Message);
			}
		}
		wp_redirect('admin.php?page=print-products-docket-manager&order_id='.$order_id.'&'.$rparams);
		exit;
	}
	if (isset($_GET['docket_manager_test']) && $_GET['docket_manager_test'] == 'true') {
		$order_id = 4426;
		$xmldata = print_products_docketmanager_create_xml($order_id);
		var_dump($xmldata);
		exit;
	}
}
?>