<?php
add_action('rest_api_init', function () {
	register_rest_route('wc/v1', '/inject-order/', array(
		'methods' => 'POST',
		'callback' => 'print_products_inject_order_rest_api',
		'permission_callback' => 'print_products_inject_order_rest_api_auth',
	));
});

function print_products_inject_order_rest_api() {
	global $io_user_id;
	if ($io_user_id) {
		if (isset($_POST['data'])) {
			$data = is_array($_POST['data']) ? $_POST['data'] : json_decode(stripcslashes($_POST['data']), true);
			$customer_data = get_userdata($io_user_id);
			if ($customer_data) {
				$result = print_products_inject_order_process($io_user_id, $customer_data, $data);
				if ($result['status'] == 'SUCCESS') {
					return array(
						'order_id' => $result['order_id'],
						'customer' => $customer_data->display_name,
						'billing_address' => $data['billing_address'],
						'shipping_address' => $data['shipping_address'],
						'products' => $data['products'],
						'total' => $data['total']
					);
				} else {
					return print_products_inject_order_rest_api_auth_error($result['message'], 'api_creating_order_error');
				}
			} else {
				return print_products_inject_order_rest_api_auth_error('User ID is not exist.', 'api_user_data_error');
			}
		} else {
			return print_products_inject_order_rest_api_auth_error('Order data is invalid.', 'api_user_data_error');
		}
	}
	return print_products_inject_order_rest_api_auth_error('User ID is invalid.', 'api_user_id_error');
}

function print_products_inject_order_rest_api_auth() {
	global $wpdb, $io_user_id;
	$api_key = isset($_POST['api_key']) ? $_POST['api_key'] : false;
	$api_secret = isset($_POST['api_secret']) ? $_POST['api_secret'] : false;
	if ($api_key && $api_secret) {
		$consumer_key = wc_api_hash(sanitize_text_field($api_key));
		$consumer_secret = sanitize_text_field($api_secret);
		$auth_data = $wpdb->get_row(sprintf("SELECT * FROM %swoocommerce_api_keys WHERE consumer_key = '%s'", $wpdb->prefix, $consumer_key));
		if ($auth_data) {
			if ($consumer_secret == $auth_data->consumer_secret) {
				$io_user_id = $auth_data->user_id;
				return true;
			} else {
				return print_products_inject_order_rest_api_auth_error('Secret key is invalid.');
			}
		} else {
			return print_products_inject_order_rest_api_auth_error('API key is invalid.');
		}
	} else {
		return print_products_inject_order_rest_api_auth_error('API key or Secret key is invalid.');
	}
	return print_products_inject_order_rest_api_auth_error('API authentication error.');
}

function print_products_inject_order_rest_api_auth_error($message, $code = 'api_authentication_error') {
	return new WP_Error($code, __($message, 'wp2print'), array('status' => 401));
}

function print_products_inject_order_process($customer_id, $customer_data, $data) {
	if ($data) {
		if (!$data['billing_address']['first_name'] && $data['shipping_address']['first_name']) { $data['billing_address']['first_name'] = $data['shipping_address']['first_name']; }
		if (!$data['billing_address']['last_name'] && $data['shipping_address']['last_name']) { $data['billing_address']['last_name'] = $data['shipping_address']['last_name']; }
		$required = print_products_inject_order_check_required($data);
		if (!count($required)) {
			$order_id = print_products_inject_order_create($customer_id, $data);
			if ($order_id) {
				$result = array('status' => 'SUCCESS', 'order_id' => $order_id);
			} else {
				$result = array('status' => 'FAIL', 'message' => 'Creating order error.');
			}
		} else {
			$result = array('status' => 'FAIL', 'message' => 'Required field(s): '.implode(', ', $required));
		}
	} else {
		$result = array('status' => 'FAIL', 'message' => 'JSON data is incorrect.');
	}
	return $result;
}

function print_products_inject_order_create($customer_id, $data) {
	global $wpdb;
	$products = $data['products'];
	$total = (float)$data['total'];
	$payment_method = $data['payment_method'];
	$shipping_method = $data['shipping_method'];
	$shipping_weight = $data['shipping_weight'];
	$shipping_total = $data['shipping_total'];
	$tax_total = (isset($data['tax_total']) && $data['tax_total']) ? $data['tax_total'] : 0;
	$tax_rate = (isset($data['tax_rate']) && $data['tax_rate']) ? (float)$data['tax_rate'] : 0;

	$billing_address = print_products_inject_order_get_address($data['billing_address']);
	$shipping_address = print_products_inject_order_get_address($data['shipping_address']);

	$order = wc_create_order(array('customer_id' => $customer_id));
	if ($order) {
		$order->set_address($billing_address, 'billing');
		$order->set_address($shipping_address, 'shipping');
		$order->set_status('on-hold');


		if ($products) {
			foreach($products as $product) {
				$product_id = $product['product_id'];
				$quantity = (int)$product['quantity'];
				$price = (isset($product['price']) && $product['price']) ? (float)$product['price'] : 0;
				$media_url = $product['media_url'];
				$tax = (isset($product['tax']) && $product['tax']) ? (float)$product['tax'] : 0;
				$weight = $product['weight'];

				$subtotal = $price * $quantity;

				$product = wc_get_product($product_id);

				$order_item_id = $order->add_product($product, $quantity, array('totals' => array('tax' => $tax), 'total_tax' => $tax));

				wc_update_order_item_meta($order_item_id, '_qty', $quantity);
				wc_update_order_item_meta($order_item_id, '_line_subtotal', $subtotal);
				wc_update_order_item_meta($order_item_id, '_line_total', $subtotal);
				if ($tax) {
					wc_update_order_item_meta($order_item_id, '_line_subtotal_tax', $tax);
					wc_update_order_item_meta($order_item_id, '_line_tax', $tax);
					wc_update_order_item_meta($order_item_id, '_line_tax_data', array('total' => array(1 => $tax), 'subtotal' => array(1 => $tax)));
				}
				if ($media_url) {
					$insert = array();
					$insert['item_id'] = $order_item_id;
					$insert['product_id'] = $product_id;
					$insert['product_type'] = $product->get_type();
					$insert['quantity'] = $quantity;
					$insert['price'] = $price;
					$insert['artwork_files'] = serialize(array($media_url));
					$insert['atcaction'] = 'artwork';
					$wpdb->insert($wpdb->prefix."print_products_order_items", $insert);
				}
			}
		}

		if ($payment_method) {
			$payment_method_title = $payment_method;
			$available_gateways = WC()->payment_gateways()->payment_gateways();
			if ($available_gateways) {
				foreach ($available_gateways as $pgkey => $pgateway) {
					$pgtitle = $pgateway->get_method_title();
					if ($payment_method == $pgkey || $payment_method == $pgtitle) {
						$payment_method_title = $pgtitle;
					}
				}
			}
			$order->set_payment_method($payment_method);
			$order->set_payment_method_title($payment_method_title);
		}

		$tax_rate_id = 1;
		$tax_rates = WC_Tax::get_rates();
		if ($tax_rates) {
			if (!isset($tax_rates[$tax_rate_id])) {
				$tax_rate_id = array_key_first($tax_rates);
			}
		}
		if ($tax_total) {
			$tax_item = new WC_Order_Item_Tax();
			$tax_item->set_rate_id($tax_rate_id);
			$tax_item->set_name('Tax');
			$tax_item->set_label('Tax');
			$tax_item->set_tax_total($tax_total);
			if ($tax_rate) {
				$tax_item->set_rate_percent($tax_rate);
			}
			$order->add_item($tax_item);
		}

		if ($shipping_method && $shipping_total) {
			$shipping_method_id = 'flat_rate';
			$shipping_method_title = $shipping_method;
			$shipping_methods = WC()->shipping()->get_shipping_methods();
			if ($shipping_methods) {
				foreach ($shipping_methods as $smkey => $shipping_method) {
					$smtitle = $shipping_method->get_method_title();
					if ($shipping_method == $smkey || $shipping_method == $smtitle) {
						$shipping_method_id = $smkey;
						$shipping_method_title = $smtitle;
					}
				}
			}
			$shipping_item = new WC_Order_Item_Shipping();
			$shipping_item->set_method_title($shipping_method_title);
			$shipping_item->set_method_id($shipping_method_id);
			$shipping_item->set_total($shipping_total);
			$order->add_item($shipping_item);
		}

		$order->calculate_totals(false);
		$order->set_total($total);

		$order->update_meta_data('_order_total', $total);

		$order_id = $order->save();

		return $order_id;
	}
	return false;
}

function print_products_inject_order_get_address($adata) {
	$address_data = array();
	$afields = array('first_name', 'last_name', 'company', 'email', 'phone', 'address_1', 'address_2', 'city', 'state', 'postcode', 'country');
	foreach ($afields as $afield) {
		$address_data[$afield] = isset($adata[$afield]) ? $adata[$afield] : '';
	}
	return $address_data;
}

function print_products_inject_order_check_required($data) {
	$found = array();
	$baddress = $data['billing_address'];
	$saddress = $data['shipping_address'];
	$products = $data['products'];
	$total = (float)$data['total'];

	$afields = array('first_name', 'last_name', 'email', 'phone', 'city', 'postcode', 'country');
	foreach ($afields as $afield) {
		$fval = isset($baddress[$afield]) ? trim($baddress[$afield]) : '';
		if (!strlen($fval)) {
			$found[] = 'billing '.$afield;
		} else if ($afield == 'email' && !is_email($fval)) {
			$found[] = 'billing incorrect '.$afield;
		}
	}
	foreach ($afields as $afield) {
		$fval = isset($saddress[$afield]) ? trim($saddress[$afield]) : '';
		if (!strlen($fval)) {
			$found[] = 'shipping '.$afield;
		} else if ($afield == 'email' && !is_email($fval)) {
			$found[] = 'shipping incorrect '.$afield;
		}
	}

	foreach ($products as $pkey => $product) {
		$pnum = $pkey + 1;
		$product_id = isset($product['product_id']) ? $product['product_id'] : '';
		$quantity = isset($product['quantity']) ? $product['quantity'] : '';
		$price = isset($product['price']) ? $product['price'] : '';
		if (!$product_id) {
			$found[] = 'product '.$pnum.' : product_id';
		} else {
			$wc_product = get_product($product_id);
			if (!$wc_product) {
				$found[] = 'product_id '.$product_id.' is not exist';
			}
		}
		if (!$quantity) { $found[] = 'product '.$pnum.' : quantity'; }
		//if (!$price) { $found[] = 'product '.$pnum.' : price'; }
	}

	if (!$total) {
		//$found[] = 'total';
	}

	return $found;
}

/*
https://{SITE_URL}/wp-json/wc/v1/inject-order

POST FIELDS:
api_key: REST_API_CONSUMER_KEY,
api_secret: REST_API_CONSUMER_SECRET,
data: {
  billing_address: {
    first_name: String, // required
    last_name: String, // required
    company: String,
    email: String, // required
    phone: String, // required
    address_1: String, // required
    address_2: String,
    postcode: String, // required
    city: String, // required
    state: String, // required
    country: String, // required
  },
  shipping_address: {
    first_name: String, // required
    last_name: String, // required
    company: String,
    email: String, // required
    phone: String, // required
    address_1: String, // required
    address_2: String,
    postcode: String, // required
    city: String, // required
    state: String, // required
    country: String, // required
  },
  products: [
    {
      product_id: Number, // required
      quantity: Number, // required
      price: 0.0, // required
	  media_url: String,
	  tax: 0.0
    },
  ],
  payment_method: String,
  shipping_method: String,
  shipping_total: 0.0,
  tax_rate: 0.0,
  tax_total: 0.0,
  total: 0.0 // required
}
*/
?>