<?php
class WC_Product_Fixed extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
	}

	public function get_type() {
		return 'fixed';
	}

}

class WC_Product_Book extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
	}

	public function get_type() {
		return 'book';
	}

}

class WC_Product_Box extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
	}

	public function get_type() {
		return 'box';
	}

}

class WC_Product_Area extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
	}

	public function get_type() {
		return 'area';
	}

}

class WC_Product_Aec extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
	}

	public function get_type() {
		return 'aec';
	}

}

class WC_Product_Aecbwc extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
	}

	public function get_type() {
		return 'aecbwc';
	}

}

class WC_Product_Aecsimple extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
	}

	public function get_type() {
		return 'aecsimple';
	}

}

class WC_Product_Paybill extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
		$this->set_virtual(true);
	}

	public function get_type() {
		return 'paybill';
	}

}

class WC_Product_Eddm extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
		$this->set_virtual(true);
	}

	public function get_type() {
		return 'eddm';
	}

}

class WC_Product_Quote extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
		$this->set_virtual(true);
	}

	public function get_type() {
		return 'quote';
	}

}

class WC_Product_Sticker extends WC_Product {

	public function __construct( $product = 0 ) {
		$this->supports[]   = 'ajax_add_to_cart';
		parent::__construct( $product );
	}

	public function get_type() {
		return 'sticker';
	}

}
?>