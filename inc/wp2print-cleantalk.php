<?php
require_once 'lib/Cleantalk.php';
require_once 'lib/CleantalkRequest.php';
require_once 'lib/CleantalkResponse.php';
require_once 'lib/CleantalkHelper.php';

use Cleantalk\CleantalkRequest;
use Cleantalk\Cleantalk;
use Cleantalk\CleantalkAPI;

add_filter('registration_errors', 'print_products_cleantalk_registration_errors', 10, 3);
function print_products_cleantalk_registration_errors($errors, $user_login, $user_email) {
	if ($user_login && $user_email) {
		$errors = print_products_cleantalk_check($errors, $user_email, $user_login);
	}
	return $errors;
}

add_filter('woocommerce_process_registration_errors', 'print_products_cleantalk_registration_woocommerce_process_registration_errors', 10, 4);
function print_products_cleantalk_registration_woocommerce_process_registration_errors($errors, $user_login, $user_pass, $user_email) {
	if ($user_login && $user_pass && $user_email) {
		$errors = print_products_cleantalk_check($errors, $user_email, $user_login);
	}
	return $errors;
}

add_action('woocommerce_after_checkout_validation', 'print_products_cleantalk_woocommerce_after_checkout_validation', 100, 2);
function print_products_cleantalk_woocommerce_after_checkout_validation($data, $errors) {
	$user_email = $data['billing_email'];
	$user_login = sanitize_title($data['billing_email']);
	$errors = print_products_cleantalk_check($errors, $user_email, $user_login);
}

function print_products_cleantalk_check($errors, $user_email, $user_login) {
	$cleantalk_auth_key = 'epydebuda5upa3u';
	$print_products_registration_options = get_option('print_products_registration_options');

	if (!$errors->has_errors() && !isset($_GET['regconfirm']) && $print_products_registration_options && isset($print_products_registration_options['cleantalk']) && $print_products_registration_options['cleantalk'] == 1) {
		$ct_request = new CleantalkRequest();
		$ct_request->auth_key = $cleantalk_auth_key;
		$ct_request->agent = 'php-api';
		$ct_request->sender_email = $user_email;
		$ct_request->sender_ip = $_SERVER['REMOTE_ADDR'];
		$ct_request->sender_nickname = $user_login;
		$ct_request->js_on = 0;
		$ct_request->submit_time = time();
		 
		$ct = new Cleantalk();
		$ct->server_url = 'http://moderate.cleantalk.org/api2.0/';
		 
		// check
		$ct_result = $ct->isAllowUser($ct_request);
		if ($ct_result && isset($ct_result->allow)) {
			if ($ct_result->allow != 1) {
				$comment = str_replace('*', '', $ct_result->comment);
				$errors->add( 'wp2print_cleantalk', __( trim($comment), 'wp2print' ) );
			}
		}
	}

	return $errors;
}

?>