<?php
$print_products_request_payment_options = get_option("print_products_request_payment_options");

function print_products_request_payment_admin_page() {
	include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-request-payment.php';
}

function print_products_request_payment_history_admin_page() {
	include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-request-payment-history.php';
}

add_action('wp_loaded', 'print_products_request_payment_actions');
function print_products_request_payment_actions() {
	global $print_products_request_payment_options, $wpdb;
	if (isset($_GET['order_request_epayment']) && $_GET['order_request_epayment'] == 'true') {
		$order_id = (int)$_GET['order_id'];
		if ($order_id) {
			$order = wc_get_order($order_id);
			$request_payment_data = array('order_id' => $order_id, 'item_id' =>(int)$_GET['item_id'], 'customer' => $order->get_customer_id(), 'product_id' => (int)$_GET['product_id'], 'tax' => 0);
			print_products_request_payment_set_order_data($request_payment_data);
		}
	}
	if (isset($_POST['print_products_request_payment_action']) && $_POST['print_products_request_payment_action'] == 'process') {
		$request_payment_data = print_products_request_payment_get_order_data();
		switch ($_POST['process_step']) {
			case '1':
				$request_payment_data = print_products_request_payment_get_order_data();
				$request_payment_data['sender'] = $_POST['order_sender'];
				$request_payment_data['customer'] = $_POST['order_customer'];
				print_products_request_payment_set_order_data($request_payment_data);
			break;
			case '2':
				$request_payment_data = print_products_request_payment_get_order_data();
				$request_payment_data['invoice_number'] = $_POST['order_invoice_number'];
				$request_payment_data['invoice_number_label'] = $_POST['order_invoice_number_label'];
				$request_payment_data['amount'] = $_POST['order_amount'];
				$request_payment_data['amount_label'] = $_POST['order_amount_label'];
				$request_payment_data['tax'] = (int)$_POST['tax'];
				$request_payment_data['pdffile'] = $_POST['order_pdffile'];
				print_products_request_payment_set_order_data($request_payment_data);
			break;
			case 'send':
				print_products_request_payment_save_order();
			break;
		}
	}
	if (isset($_POST['rph_action'])) {
		switch ($_POST['rph_action']) {
			case 'resend':
				$order_id = (int)$_POST['order_id'];
				if ($order_id) {
					$email_subject = $print_products_request_payment_options['email_subject'];
					$email_message = $print_products_request_payment_options['email_message'];
					print_products_request_payment_order_email($order_id, $email_subject, $email_message);
					$redirect = 'admin.php?page=print-products-request-payment-history';
					if (isset($_GET['s'])) { $redirect .= '&s='.$_GET['s']; }
					if (isset($_GET['qopage'])) { $redirect .= '&qopage='.$_GET['qopage']; }
					$redirect .= '&resent=true';
					wp_redirect($redirect);
					exit;
				}
			break;
			case 'duplicate':
				$order_id = (int)$_POST['order_id'];
				if ($order_id) {
					print_products_request_payment_duplicate_order($order_id);
					wp_redirect('admin.php?page=print-products-request-payment');
					exit;
				}
			break;
			case 'delete':
				$order_id = (int)$_POST['order_id'];
				if ($order_id) {
					print_products_request_payment_delete_order($order_id);
					wp_redirect('admin.php?page=print-products-request-payment-history');
					exit;
				}
			break;
		}
	}
	if (isset($_GET['rporder']) && $_GET['rporder'] == 'true') {
		print_products_request_payment_order_process();
	}
	if (isset($_REQUEST['AjaxAction']) && $_REQUEST['AjaxAction'] == 'request-payment-select-user') {
		$users_data = array();
		if (isset($_REQUEST['q']) && strlen($_REQUEST['q'])) {
			$users = $wpdb->get_results(sprintf("SELECT u.*, um.meta_value FROM %susers u LEFT JOIN %susermeta um ON um.user_id = u.ID AND um.meta_key = 'billing_email' WHERE u.display_name LIKE '%s' OR u.user_login LIKE '%s' OR u.user_email LIKE '%s' OR um.meta_value LIKE '%s' ORDER BY u.display_name LIMIT 0, 50", $wpdb->prefix, $wpdb->prefix, '%'.$_REQUEST['q'].'%', '%'.$_REQUEST['q'].'%', '%'.$_REQUEST['q'].'%', '%'.$_REQUEST['q'].'%'));
			if ($users) {
				foreach($users as $user) {
					$user_email = $user->user_email;
					if (strlen($user->meta_value)) { $user_email = $user->meta_value; }
					$users_data[] = array('id' => $user->ID, 'text' => $user->display_name.' ('.__('Email', 'wp2print').': '.$user_email.')');
					$exusers[] = $user->ID;
				}
			}
		}
		echo json_encode(array('results' => $users_data));
		exit;
	}
}

function print_products_request_payment_get_order_data() {
	global $print_products_request_payment_options;
	$request_payment_data = array();
	if (isset($_SESSION['request_payment_data'])) { $request_payment_data = $_SESSION['request_payment_data']; }
	if (!isset($request_payment_data['product_id'])) { $request_payment_data['product_id'] = $print_products_request_payment_options['product']; }
	return $request_payment_data;
}

function print_products_request_payment_set_order_data($request_payment_data) {
	$_SESSION['request_payment_data'] = $request_payment_data;
}

function print_products_request_payment_get_order($order_id) {
	global $wpdb;
	return $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_request_payments WHERE order_id = %s", $wpdb->prefix, $order_id));
}

function print_products_request_payment_save_order() {
	global $wpdb, $print_products_request_payment_options;
	$request_payment_data = print_products_request_payment_get_order_data();

	$additional = array(
		'invoice_number' => $request_payment_data['invoice_number'],
		'invoice_number_label' => $request_payment_data['invoice_number_label'],
		'amount_label' => $request_payment_data['amount_label']
	);
	if (isset($request_payment_data['order_id'])) {
		$additional['order_id'] = $request_payment_data['order_id'];
		$additional['item_id'] = $request_payment_data['item_id'];
	}

	$email_subject = trim($_POST['email_subject']);
	$email_message = trim($_POST['email_message']);

	// add record to db
	$insert = array();
	$insert['user_id'] = $request_payment_data['customer'];
	$insert['sender'] = $request_payment_data['sender'];
	$insert['product_id'] = $request_payment_data['product_id'];
	$insert['price'] = $request_payment_data['amount'];
	$insert['additional'] = serialize($additional);
	$insert['pdffile'] = $request_payment_data['pdffile'];
	$insert['created'] = current_time('mysql');
	$insert['tax'] = $request_payment_data['tax'];
	$wpdb->insert($wpdb->prefix."print_products_request_payments", $insert);
	$order_id = $wpdb->insert_id;

	// send email
	print_products_request_payment_order_email($order_id, $email_subject, $email_message);

	unset($_SESSION['request_payment_data']);

	wp_redirect('admin.php?page=print-products-request-payment&step=completed&order='.$order_id);
	exit;
}

function print_products_request_payment_duplicate_order($order_id) {
	global $wpdb;
	$order_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_request_payments WHERE order_id = '%s' AND status = 0", $wpdb->prefix, $order_id));
	if ($order_data) {
		$additional = unserialize($order_data->additional);
		$request_payment_data = array();
		$request_payment_data['sender'] = $order_data->sender;
		$request_payment_data['customer'] = $order_data->user_id;
		$request_payment_data['product_id'] = $order_data->product_id;
		$request_payment_data['invoice_number'] = $additional['invoice_number'];
		$request_payment_data['invoice_number_label'] = $additional['invoice_number_label'];
		$request_payment_data['amount'] = $order_data->price;
		$request_payment_data['amount_label'] = $additional['amount_label'];
		$request_payment_data['tax'] = $order_data->tax;
		$request_payment_data['pdffile'] = $order_data->pdffile;
		print_products_request_payment_set_order_data($request_payment_data);
	}
}

function print_products_request_payment_delete_order($order_id) {
	global $wpdb;
	$wpdb->query(sprintf("DELETE FROM %sprint_products_request_payments WHERE order_id = '%s'", $wpdb->prefix, $order_id));
}

function print_products_request_payment_order_email($order_id, $email_subject, $email_message) {
	global $wpdb, $print_products_request_payment_options;

	$request_payment_data = print_products_request_payment_get_order_data();

	$order_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_request_payments WHERE order_id = '%s'", $wpdb->prefix, $order_id));
	if ($order_data) {
		$customer_id = $order_data->user_id;
		$customer_data = get_userdata($customer_id);

		$pay_now_text = __('Pay now', 'wp2print');
		if (strlen($print_products_request_payment_options['pay_now_text'])) {
			$pay_now_text = $print_products_request_payment_options['pay_now_text'];
		}
		$rpkey = md5(time());
		$wpdb->update($wpdb->prefix.'print_products_request_payments', array('rpkey' => $rpkey), array('order_id' => $order_id));

		$pay_now_link = '<a href="'.site_url('?rporder=true&uid='.md5($customer_id).'&oid='.md5($order_id)).'&rpkey='.$rpkey.'" style="background:#0085ba; border:1px solid #006799; border-color:#0073aa #006799 #006799; border-radius:3px; color:#fff; font-size:13px; line-height:26px; height:26px; text-decoration:none;font-family:Arial; display:inline-block; padding:0 10px 1px;">'.$pay_now_text.'</a>';

		$request_detail = print_products_request_payment_quote_detail($order_data);

		$email_message = '<p style="font-size:13px;">'.__('Customer', 'wp2print').': <strong>'.$customer_data->first_name.' '.$customer_data->last_name.' ('.$customer_data->user_email.')</strong></p>'.$email_message;
		$email_message = str_replace('{REQUEST-DETAIL}', $request_detail, $email_message);
		$email_message = str_replace('{PAY-NOW-LINK}', $pay_now_link, $email_message);

		$email_message = print_products_request_payment_billing_info($customer_id, $email_message);

		if (isset($request_payment_data['sender']) && strlen($request_payment_data['sender'])) {
			add_filter('wp_mail_from', function($email_from){
				$request_payment_data = print_products_request_payment_get_order_data();
				return $request_payment_data['sender'];
			}, 11);
		}

		$user_email = get_user_meta($customer_id, 'billing_email', true);
		if (!strlen($user_email)) { $user_email = $customer_data->user_email; }

		print_products_send_wc_mail($user_email, $email_subject, $email_message);
		if (strlen($print_products_request_payment_options['bcc_email'])) {
			print_products_send_wc_mail($print_products_request_payment_options['bcc_email'], $email_subject, $email_message);
		}
	}
}

function print_products_request_payment_billing_info($customer_id, $email_message) {
	$billing_infos = array(
		'{billing-first-name}' => get_user_meta($customer_id, 'billing_first_name', true),
		'{billing-last-name}'  => get_user_meta($customer_id, 'billing_last_name', true),
		'{billing-company}'    => get_user_meta($customer_id, 'billing_company', true),
		'{billing-address1}'   => get_user_meta($customer_id, 'billing_address_1', true),
		'{billing-address2}'   => get_user_meta($customer_id, 'billing_address_2', true),
		'{billing-city}'       => get_user_meta($customer_id, 'billing_city', true),
		'{billing-state}'      => get_user_meta($customer_id, 'billing_state', true),
		'{billing-zip-code}'   => get_user_meta($customer_id, 'billing_postcode', true)
	);

	foreach($billing_infos as $bi_key => $bi_val) {
		if (strlen($bi_val)) {
			$email_message = str_replace($bi_key, $bi_val, $email_message);
		} else {
			$email_message = str_replace($bi_key.'<br />'."\r\n", '', $email_message);
			$email_message = str_replace($bi_key.' ', '', $email_message);
			$email_message = str_replace($bi_key, '', $email_message);
		}
	}

	return $email_message;
}

function print_products_request_payment_order_process() {
	global $wpdb;

	$uid = $_GET['uid'];
	$oid = $_GET['oid'];
	$rpkey = $_GET['rpkey'];

	if (strlen($uid) && strlen($oid)) {
		$user_data = $wpdb->get_row(sprintf("SELECT * FROM %susers WHERE MD5(ID) = '%s'", $wpdb->prefix, $uid));
		$order_data = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_request_payments WHERE MD5(order_id) = '%s'", $wpdb->prefix, $oid));

		if ($user_data && $order_data) {
			$order_id = $order_data->order_id;
			$order_rpkey = $order_data->rpkey;
			$order_rpcall = (int)$order_data->rpcall;
			if ($order_rpkey == $rpkey) {
				if ($order_rpcall < 2) {
					// login user
					$user_id = $order_data->user_id;
					$user_data = get_userdata($user_id);
					$user_login = $user_data->user_login;
					wp_set_current_user($user_id, $user_login);
					wp_set_auth_cookie($user_id);
					do_action('wp_login', $user_login, $user);

					// add product to cart
					WC()->cart->empty_cart();

					$product_id = $order_data->product_id;
					$additional = unserialize($order_data->additional);

					$_REQUEST['print_products_checkout_process_action'] = 'add-to-cart';
					$_REQUEST['quantity'] = 1;
					$_REQUEST['product_type'] = 'paybill';
					$_REQUEST['product_id'] = $product_id;
					$_REQUEST['add-to-cart'] = $product_id;
					$_REQUEST['pb_amount'] = $order_data->price;
					$_REQUEST['pb_invoice_number'] = $additional['invoice_number'];
					$_REQUEST['pb_invoice_number_label'] = $additional['invoice_number_label'];
					$_REQUEST['atcaction'] = 'artwork';

					unset($_REQUEST['rporder']);
					unset($_REQUEST['uid']);
					unset($_REQUEST['oid']);

					$cart_item_data = array();
					$cart_item_data['unique_key'] = md5(microtime() . rand() . md5($product_id));
					$cart_item_data['rp_order_id'] = $order_id;
					if (isset($additional['order_id']) && $additional['order_id']) {
						$cart_item_data['rp_item_id'] = $additional['item_id'];
						$cart_item_data['rp_item_name'] = __('Extra payment for Order', 'wp2print').' '.$additional['order_id'];
					}
					if ($order_data->tax == 1) {
						$cart_item_data['rp_tax'] = 1;
					}

					WC()->cart->add_to_cart($product_id, 1, 0, array(), $cart_item_data);

					$order_rpcall = $order_rpcall + 1;
					$update = array('rpcall' => $order_rpcall);
					if ($order_rpcall >= 2) {
						$update['rpkey'] = '';
					}
					$wpdb->update($wpdb->prefix.'print_products_request_payments', $update, array('order_id' => $order_id));

					// redirect to checkout page
					$checkout_url = WC()->cart->get_checkout_url();
					wp_redirect($checkout_url);
					exit;
				}
			} else {
				wp_die(__('Invalid security key.', 'wp2print'));
			}
		} else {
			wp_die(__('Invalid customer or order data.', 'wp2print'));
		}
	} else {
		wp_die(__('Invalid payment data.', 'wp2print'));
	}
}

function print_products_request_payment_quote_detail($order_data) {
	global $wpdb;

	$product_id = $order_data->product_id;
	$product_name = get_the_title($product_id);
	$additional = unserialize($order_data->additional);
	if (isset($additional['order_id']) && $additional['order_id']) { $product_name = __('Extra payment for Order', 'wp2print').' '.$additional['order_id']; }

	$quote_detail .= '<p>'.__('Product', 'wp2print').': <strong>'.$product_name.'</strong></p>';
	$quote_detail .= '<p>'.$additional['invoice_number_label'].': <strong>'.$additional['invoice_number'].'</strong></p>';
	$quote_detail .= '<p>'.$additional['amount_label'].': <strong>'.wc_price($order_data->price).'</strong></p>';
	if (strlen($order_data->pdffile)) {
		$quote_detail .= '<p>'.__('PDF of invoice', 'wp2print').': <a href="'.print_products_get_amazon_file_url($order_data->pdffile).'">'.__('Download invoice', 'wp2print').'</a></p>';
	}

	return $quote_detail;
}

add_action('woocommerce_checkout_order_processed', 'print_products_request_payment_woocommerce_checkout_order_processed');
add_action('woocommerce_payment_complete', 'print_products_request_payment_woocommerce_checkout_order_processed');
function print_products_request_payment_woocommerce_checkout_order_processed($order_id) {
	global $wpdb;
	$cart_contents = WC()->cart->cart_contents;
	if ($cart_contents) {
		foreach ($cart_contents as $cart_item_key => $cart_item) {
			if (isset($cart_item['rp_order_id']) && $cart_item['rp_order_id']) {
				$wpdb->update($wpdb->prefix.'print_products_request_payments', array('status' => '1', 'wc_order_id' => $order_id), array('order_id' => $cart_item['rp_order_id']));
			}
		}
	}
}

add_filter('woocommerce_cart_item_name', 'print_products_request_payment_woocommerce_cart_item_name', 11, 3);
function print_products_request_payment_woocommerce_cart_item_name($item_name, $cart_item, $cart_item_key) {
	if (isset($cart_item['rp_item_name'])) {
		$item_name = $cart_item['rp_item_name'];
	}
	return $item_name;
}

add_action('woocommerce_before_calculate_totals', 'print_products_request_payment_woocommerce_before_calculate_totals');
function print_products_request_payment_woocommerce_before_calculate_totals($cart) {
	$cart_contents = WC()->cart->cart_contents;
	if ($cart_contents) {
		foreach ($cart_contents as $cart_item_key => $cart_item) {
			$rp_tax = true;
			$product = $cart_item['data'];
			if ($product && $product->get_type() == 'paybill') {
				$rp_tax = false;
			}
			if (isset($cart_item['rp_order_id'])) {
				$rp_tax = false;
				if (isset($cart_item['rp_tax']) && (int)$cart_item['rp_tax'] == 1) {
					$rp_tax = true;
				}
			}
			if (!$rp_tax) {
				$product->set_tax_status('none');
			}
		}
	}
}

add_action('woocommerce_checkout_create_order_line_item', 'print_products_request_payment_woocommerce_checkout_create_order_line_item', 11, 3);
function print_products_request_payment_woocommerce_checkout_create_order_line_item($item, $cart_item_key, $cart_item) {
	if (isset($cart_item['rp_item_name'])) {
		$item->set_name($cart_item['rp_item_name']);
	}
}

add_action('woocommerce_new_order_item', 'print_products_request_payment_woocommerce_new_order_item', 11, 2);
function print_products_request_payment_woocommerce_new_order_item($item_id, $item) {
	if (!print_products_is_wp_json()) {
		$cart_contents = WC()->cart->cart_contents;
		if ($cart_contents) {
			$cart_item_key = $item->legacy_cart_item_key;
			$cart_item = $cart_contents[$cart_item_key];
			if ($cart_item) {
				if (isset($cart_item['rp_item_id']) && $cart_item['rp_item_id']) {
					wc_add_order_item_meta($item_id, '_rp_item', '1');

					$o_item_id = (int)$cart_item['rp_item_id'];
					wc_add_order_item_meta($o_item_id, '_rp_item_subtotal', $item->get_subtotal());
					wc_add_order_item_meta($o_item_id, '_rp_item_tax', $item->get_total_tax());
				}
			}
		}
	}
}

add_filter('woocommerce_order_item_quantity_html', 'print_products_request_payment_woocommerce_order_item_quantity_html', 11, 2);
function print_products_request_payment_woocommerce_order_item_quantity_html($quantity_html, $item) {
	$item_id = $item->get_id();
	$rp_item = (int)wc_get_order_item_meta($item_id, '_rp_item', true);
	if ($rp_item) {
		$quantity_html = '';
	}
	return $quantity_html;
}
?>