<?php
add_filter('woocommerce_account_menu_items', 'print_products_sales_quotes_account_menu_items', 20);
function print_products_sales_quotes_account_menu_items($items) {
	$new_items = array();
	foreach($items as $ikey => $ival) {
		$new_items[$ikey] = $ival;
		if ($ikey == 'orders') {
			$new_items['sales-quotes'] = __('Sales quotes', 'wp2print');
		}
	}
	return $new_items;
}

add_action('init', 'print_products_sales_quotes_rewrite_endpoint');
function print_products_sales_quotes_rewrite_endpoint() {
	if (print_products_my_account_is_front()) {
		add_rewrite_endpoint('sales-quotes', EP_ROOT | EP_PAGES);
	} else {
		add_rewrite_endpoint('sales-quotes', EP_PAGES);
	}
	flush_rewrite_rules();
}

add_filter('query_vars', 'print_products_sales_quotes_query_vars', 10);
function print_products_sales_quotes_query_vars($vars) {
	$vars[] = 'sales-quotes';
	return $vars;
}

add_action('parse_request', 'print_products_sales_quotes_parse_request', 10);
function print_products_sales_quotes_parse_request() {
	global $wp;
	$var = 'sales-quotes';
	if (isset($wp->query_vars['name']) && $wp->query_vars['name'] == $var) {
		unset($wp->query_vars['name']);
		$wp->query_vars[$var] = $var;
	}
}

add_action('pre_get_posts', 'print_products_sales_quotes_pre_get_posts');
function print_products_sales_quotes_pre_get_posts($q) {
	if ( ! $q->is_main_query() ) {
		return;
	}
	if (print_products_is_showing_page_on_front($q) && ! print_products_page_on_front_is($q->get('page_id'))) {
		$_query = wp_parse_args($q->query);
		$qv_array = array('sales-quotes' => 'sales-quotes');
		if (!empty($_query) && array_intersect( array_keys($_query), array_keys($qv_array))) {
			$q->is_page     = true;
			$q->is_home     = false;
			$q->is_singular = true;
			$q->set('page_id', (int)get_option( 'page_on_front'));
			add_filter('redirect_canonical', '__return_false');
		}
	}
}

add_action('woocommerce_account_sales-quotes_endpoint', 'print_products_sales_quotes_account_page');
function print_products_sales_quotes_account_page() {
	include PRINT_PRODUCTS_TEMPLATES_DIR . 'sales-quotes.php';
}

add_filter('the_title', 'print_products_sales_quotes_the_title', 12, 2);
function print_products_sales_quotes_the_title($title, $id) {
	global $wp_query;
	if (is_account_page() && is_main_query() && in_the_loop() && isset($wp_query->query_vars['sales-quotes']) && !is_admin()) {
		$title = __('Sales quotes', 'wp2print');
	}
	return $title;
}
?>