<?php
add_action('show_user_profile', 'print_products_user_artwork_files_user_profile', 100);
add_action('edit_user_profile', 'print_products_user_artwork_files_user_profile', 100);
function print_products_user_artwork_files_user_profile($profileuser) {
	?>
	<h3><?php _e('User artwork files', 'wp2print'); ?></h3>
	<table class="form-table">
		<tr>
			<th><label><?php _e('User artwork files', 'wp2print'); ?></label></th>
			<td><a href="admin.php?page=print-products-users-artwork-files&user_id=<?php echo $profileuser->ID; ?>" class="button"><?php _e("View user's files", 'wp2print'); ?></a></td>
		</tr>
	</table>
	<?php
}

function print_products_user_artwork_files_admin_page() {
	global $wpdb;
	$user_id = 0;
	$user_name = '';
	if (isset($_GET['user_id'])) { $user_id = (int)$_GET['user_id']; }

	$user_files = false;
	if ($user_id) {
		if (print_products_is_hpos_enabled()) {
			$user_orders = $wpdb->get_results(sprintf("SELECT oi.order_id, o.date_created_gmt as order_date, ppoi.artwork_files FROM %swoocommerce_order_items oi LEFT JOIN %swc_orders o ON o.id = oi.order_id LEFT JOIN %sprint_products_order_items ppoi ON ppoi.item_id = oi.order_item_id WHERE oi.order_item_type = 'line_item' AND o.customer_id = '%s' AND ppoi.artwork_files != '' ORDER BY oi.order_id DESC", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $user_id));
		} else {
			$user_orders = $wpdb->get_results(sprintf("SELECT oi.order_id, o.post_date as order_date, ppoi.artwork_files FROM %swoocommerce_order_items oi LEFT JOIN %spostmeta pm ON pm.post_id = oi.order_id LEFT JOIN %sposts o ON o.ID = oi.order_id LEFT JOIN %sprint_products_order_items ppoi ON ppoi.item_id = oi.order_item_id WHERE oi.order_item_type = 'line_item' AND pm.meta_key = '_customer_user' AND pm.meta_value = '%s' AND ppoi.artwork_files != '' ORDER BY oi.order_id DESC", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $user_id));
		}
		if ($user_orders) {
			foreach($user_orders as $user_order) {
				if (strlen($user_order->artwork_files)) { $afiles = array();
					$artwork_files = unserialize($user_order->artwork_files);
					foreach($artwork_files as $afile) {
						$afiles[] = '<a href="'.print_products_get_amazon_file_url($afile).'" target="_blank">'.basename($afile).'</a>';
					}
					$user_files[$user_order->order_id] = array('date' => $user_order->order_date, 'afiles' => $afiles);
				}
			}
		}
		if (print_products_is_hpos_enabled()) {
			$user_designer_orders = $wpdb->get_results(sprintf("SELECT oi.order_id, o.date_created_gmt as order_date, oim.meta_value FROM %swoocommerce_order_items oi LEFT JOIN %swc_orders o ON o.id = oi.order_id LEFT JOIN %swoocommerce_order_itemmeta oim ON oim.order_item_id = oi.order_item_id WHERE oi.order_item_type = 'line_item' AND o.customer_id = '%s' AND oim.meta_key = '_pdf_link' ORDER BY oi.order_id DESC", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $user_id));
		} else {
			$user_designer_orders = $wpdb->get_results(sprintf("SELECT oi.order_id, o.post_date as order_date, oim.meta_value FROM %swoocommerce_order_items oi LEFT JOIN %spostmeta pm ON pm.post_id = oi.order_id LEFT JOIN %sposts o ON o.ID = oi.order_id LEFT JOIN %swoocommerce_order_itemmeta oim ON oim.order_item_id = oi.order_item_id WHERE oi.order_item_type = 'line_item' AND pm.meta_key = '_customer_user' AND pm.meta_value = '%s' AND oim.meta_key = '_pdf_link' ORDER BY oi.order_id DESC", $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $wpdb->prefix, $user_id));
		}
		if ($user_designer_orders) {
			foreach($user_designer_orders as $user_designer_order) {
				if (strlen($user_designer_order->meta_value)) { $afiles = array();
					$pdf_links = explode(',', $user_designer_order->meta_value);
					foreach($pdf_links as $pdf_link) {
						$afiles[] = '<a href="'.$pdf_link.'" target="_blank">'.basename($pdf_link).'</a>';
					}
					$user_files[$user_designer_order->order_id] = array('date' => $user_designer_order->order_date, 'afiles' => $afiles);
				}
			}
		}
		$admin_uploaded_files = print_products_user_files_get_admin_uploaded_files($user_id);
	}
	$ftypes = print_products_user_files_get_types();
	$rapid_user_search = print_products_is_rapid_user_search();
	?>
	<style>
	.uf-title { margin-bottom:5px; font-size:14px; font-weight:700; }
	.uf-upload-wrap { margin-bottom:15px; }
	</style>
	<div class="wrap wp2print-uaf-wrap">
		<h2><?php _e('Users artwork files', 'wp2print'); ?></h2>
		<?php if (isset($_GET['ufprocessed'])) { ?><div id="message" class="updated fade"><p><?php if ($_GET['ufprocessed'] == 1) { _e('File was successfully added.', 'wp2print'); } else if ($_GET['ufprocessed'] == 2) { _e('File was successfully deleted.', 'wp2print'); } ?></p></div><?php } ?>
		<div class="tablenav top" style="margin-bottom:15px;">
			<form>
				<?php if (isset($_GET['post_type'])) { ?><input type="hidden" name="post_type" value="<?php echo $_GET['post_type']; ?>"><?php } ?>
				<input type="hidden" name="page" value="print-products-users-artwork-files">
				<div class="alignleft actions bulkactions">
					<select name="user_id" class="order-customer">
						<option value="">-- <?php _e('Select user', 'wp2print'); ?> --</option>
						<?php if ($rapid_user_search == 1) { ?>
							<?php if ($user_id) {
								$userdata = get_userdata($user_id);
								if ($userdata) { $user_name = $userdata->display_name; ?>
									<option value="<?php echo $userdata->ID; ?>" selected="selected"><?php echo $userdata->display_name; ?></option>
								<?php } ?>
							<?php } ?>
						<?php } else { ?>
							<?php $wp_users = get_users(array('orderby' => 'display_name'));
							foreach($wp_users as $wp_user) { ?>
								<option value="<?php echo $wp_user->ID; ?>"<?php if ($wp_user->ID == $user_id) { echo ' SELECTED'; $user_name = $wp_user->display_name; } ?>><?php echo $wp_user->display_name; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
					<input type="submit" class="button" value="<?php _e('Filter', 'wp2print'); ?>">
					<script>
					<!--
					jQuery(document).ready(function() {
					<?php if ($rapid_user_search == 1) { ?>
						jQuery('select.order-customer').select2({ajax: {url:'<?php echo site_url('/?AjaxAction=rapid-select-users'); ?>', dataType: 'json'}});
					<?php } else { ?>
						jQuery('select.order-customer').select2();
					<?php } ?>
					});
					//--></script>
				</div>
			</form>
		</div>
		<?php if ($user_id) { ?>
			<div class="uf-title"><?php _e('Add new file', 'wp2print'); ?></div>
			<div class="uf-upload-wrap" style="width:100%;">
				<form method="POST" class="user-files-form">
				<input type="hidden" name="user_files_action" value="admin-upload">
				<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
				<?php include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-user-files-upload.php'; ?>
				</form>
			</div>
			<?php if ($admin_uploaded_files) { ?>
				<div class="uf-title"><?php _e('Admin uploaded files', 'wp2print'); ?></div>
				<table class="wp-list-table widefat uf-list" width="100%" style="margin-bottom:15px;">
					<thead>
						<tr>
							<th style="width:180px;"><?php _e('Customer', 'wp2print'); ?></th>
							<th style="width:120px;"><?php _e('File type', 'wp2print'); ?></th>
							<th style="width:150px;"><?php _e('Date added', 'wp2print'); ?></th>
							<th><?php _e('File', 'wp2print'); ?></th>
							<th style="width:50px;"><?php _e('Delete', 'wp2print'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($admin_uploaded_files as $admin_uploaded_file) { ?>
							<tr>
								<td><?php echo $user_name; ?></td>
								<td><?php echo $ftypes[$admin_uploaded_file->type]; ?></td>
								<td><?php echo $admin_uploaded_file->created; ?></td>
								<td><a href="<?php echo print_products_get_amazon_file_url($admin_uploaded_file->file_url); ?>" target="_blank"><?php echo basename($admin_uploaded_file->file_url); ?></a></td>
								<td><a href="#delete" style="color:#d63638;" class="uf-delete" data-file_id="<?php echo $admin_uploaded_file->file_id; ?>"><?php _e('Delete', 'wp2print'); ?></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			<?php } ?>
			<div class="uf-title"><?php _e('User uploaded files', 'wp2print'); ?></div>
			<table class="wp-list-table widefat" width="100%">
				<thead>
					<tr>
						<th style="width:180px;"><?php _e('Customer', 'wp2print'); ?></th>
						<th style="width:120px;"><?php _e('OrderID', 'wp2print'); ?></th>
						<th style="width:150px;"><?php _e('Date added', 'wp2print'); ?></th>
						<th><?php _e('Artwork Files', 'wp2print'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if ($user_files) { ?>
						<?php foreach ($user_files as $order_id => $fdata) { ?>
							<tr>
								<td><?php echo $user_name; ?></td>
								<td><a href="<?php echo print_products_woocommerce_get_order_edit_url($order_id); ?>"><?php echo $order_id; ?></a></td>
								<td><?php echo $fdata['date']; ?></td>
								<td><?php echo implode('<br>', $fdata['afiles']); ?></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td colspan="4"><?php _e('Nothing found.', 'wp2print'); ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		<?php } ?>
	</div>
	<form method="POST" class="user-files-delete-form" data-emessage="<?php _e('Are you sure?', 'wp2print'); ?>">
	<input type="hidden" name="user_files_action" value="admin-delete-file">
	<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
	<input type="hidden" name="file_id" class="file-id">
	</form>
	<?php
}
?>