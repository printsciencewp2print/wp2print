<?php
// ----------------------------------------------------------------
// add additional attributes fields
// ----------------------------------------------------------------
add_action('woocommerce_after_add_attribute_fields', 'print_products_attribute_add_attribute_fields', 12);
function print_products_attribute_add_attribute_fields() {
	?>
	<div class="form-field">
		<label for="attribute_img"><?php _e('Display attribute images', 'wp2print') ?></label>
		<select name="attribute_img">
			<option value="0"><?php _e('No', 'wp2print') ?></option>
			<option value="1"><?php _e('Yes', 'wp2print') ?></option>
		</select>
	</div>
	<div class="form-field">
		<label for="attribute_orderby"><?php _e('Help text', 'woocommerce'); ?></label>
		<?php wp_editor('', 'attribute_help_text', 'textarea_rows=5&media_buttons=0'); ?>
	</div>
	<?php
}

add_action('woocommerce_after_edit_attribute_fields', 'print_products_attribute_edit_attribute_fields', 12);
function print_products_attribute_edit_attribute_fields() {
	$attr_id = absint($_GET['edit']);
	$attribute_data = print_products_attribute_data($attr_id);
	?>
	<tr class="form-field">
		<th scope="row" valign="top">
			<label for="help_text"><?php _e('Display attribute images', 'wp2print'); ?></label>
		</th>
		<td><select name="attribute_img">
			<option value="0"><?php _e('No', 'wp2print') ?></option>
			<option value="1"<?php if ($attribute_data->attribute_img == 1) { echo ' SELECTED'; } ?>><?php _e('Yes', 'wp2print') ?></option>
		</select></td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top">
			<label for="help_text"><?php _e('Help text', 'wp2print'); ?></label>
		</th>
		<td><?php wp_editor($attribute_data->attribute_help_text, 'attribute_help_text', 'textarea_rows=5&media_buttons=0'); ?></td>
	</tr>
	<?php
}

add_action('woocommerce_attribute_added', 'print_products_attribute_save_fields', 10, 2);
add_action('woocommerce_attribute_updated', 'print_products_attribute_save_fields', 10, 2);
function print_products_attribute_save_fields($attribute_id, $attribute) {
	global $wpdb;
	$update = array();
	$update['attribute_img'] = (int)$_POST['attribute_img'];
	$update['attribute_help_text'] = $_POST['attribute_help_text'];
	$wpdb->update($wpdb->prefix.'woocommerce_attribute_taxonomies', $update, array( 'attribute_id' => $attribute_id));
}

function print_products_attribute_data($attribute_id) {
	global $wpdb;
	return $wpdb->get_row(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies WHERE attribute_id = %s", $wpdb->prefix, $attribute_id));
}

// ----------------------------------------------------------------
// attribute images
// ----------------------------------------------------------------
$wp2print_attribute_images = get_option('wp2print_attribute_images');

add_action('init', 'print_products_attribute_images_init');
function print_products_attribute_images_init() {
	$attributes = print_products_get_registered_attributes();
	if (count($attributes)) {
		foreach($attributes as $aslug) {
			add_action($aslug.'_add_form_fields', 'print_products_attribute_image_field_add');
			add_action($aslug.'_edit_form_fields', 'print_products_attribute_image_field_edit', 10, 2);
			add_action('create_'.$aslug, 'print_products_attribute_image_save', 10, 2);
			add_action('edited_'.$aslug, 'print_products_attribute_image_save', 10, 2);
			add_filter('manage_edit-'.$aslug.'_columns', 'print_products_attribute_image_column_field');
			add_filter('manage_'.$aslug.'_custom_column', 'print_products_attribute_image_column_content', 10, 3);
		}
	}
}

function print_products_attribute_image_field_add($tag) {
	global $print_products_settings, $wpdb;
	$dimshow = false;
	$thicknessshow = false;
	$size_attribute = $print_products_settings['size_attribute'];
	if ($size_attribute) {
		$size_slug = $wpdb->get_var(sprintf("SELECT attribute_name FROM %swoocommerce_attribute_taxonomies WHERE attribute_id = %s", $wpdb->prefix, $size_attribute));
		if (strlen($size_slug) && isset($_GET['taxonomy']) && $_GET['taxonomy'] == 'pa_'.$size_slug) {
			$dimshow = true;
		}
	}
	$material_attribute = $print_products_settings['material_attribute'];
	if ($material_attribute) {
		$material_slug = $wpdb->get_var(sprintf("SELECT attribute_name FROM %swoocommerce_attribute_taxonomies WHERE attribute_id = %s", $wpdb->prefix, $material_attribute));
		if (strlen($material_slug) && isset($_GET['taxonomy']) && $_GET['taxonomy'] == 'pa_'.$material_slug) {
			$thicknessshow = true;
		}
	}
	?>
	<?php if ($dimshow) { ?>
		<div class="form-field term-dimensions-wrap" data-dimm-unit="<?php echo print_products_get_dimension_unit(); ?>">
			<label for="dimensions"><?php _e('Dimensions', 'wp2print'); ?> (<?php echo print_products_get_dimension_unit(); ?>)</label>
			<input type="text" name="dimensions[width]" style="width:60px;" placeholder="<?php _e('Width', 'wp2print'); ?>" class="dimm-width"> X <input type="text" name="dimensions[height]" style="width:60px;" placeholder="<?php _e('Height', 'wp2print'); ?>" class="dimm-height">
		</div>
		<div class="form-field term-dimensions-pts-wrap">
			<label for="dimensions"><?php _e('Dimensions', 'wp2print'); ?> (points)</label>
			<input type="text" name="dimensions_pts[width]" style="width:60px;" placeholder="<?php _e('Width', 'wp2print'); ?>" class="pts-width"> X <input type="text" name="dimensions_pts[height]" style="width:60px;" placeholder="<?php _e('Height', 'wp2print'); ?>" class="pts-height"> <input type="button" value="<?php _e('Calculate', 'wp2print'); ?>" class="button" onclick="wp2print_pts_calculate();">
		</div>
	<?php } ?>
	<?php if ($thicknessshow) { ?>
		<div class="form-field term-thickness-wrap">
			<label for="thickness"><?php _e('Thickness', 'wp2print'); ?> (<?php echo print_products_get_dimension_unit(); ?>)</label>
			<input type="text" name="thickness" style="width:120px;">
		</div>
	<?php } ?>
	<div class="form-field term-image-wrap">
		<label for="customfield"><?php _e('Image', 'wp2print'); ?></label>
		<div class="uploader"></div>
		<input class="button" type="button" value="<?php _e('Upload', 'wp2print') ?>" onclick="open_media_uploader_image()" />
		<input type="hidden" name="attribute_image" class="attribute-image-id">
		<p><?php _e('Image size: 100px x 80px', 'wp2print') ?></p>

		<script>
		var media_uploader = null;
		function open_media_uploader_image()
		{
			media_uploader = wp.media({
				frame:    "post",
				state:    "insert",
				multiple: false
			});

			media_uploader.on("insert", function(){
				var json = media_uploader.state().get("selection").first().toJSON();

				var image_id = json.id;
				var image_url = json.url;
				jQuery('.uploader').html('<img src="'+image_url+'" style="width:100px;">');
				jQuery('.attribute-image-id').val(image_id);
			});

			media_uploader.open();
		}
		</script>
	</div>
	<div class="form-field term-colour-wrap">
		<label><?php _e('Color', 'wp2print'); ?></label>
		<input type="text" name="attribute_color" class="fld-color">
	</div>
	<?php
}

function print_products_attribute_image_field_edit($tag) {
	global $wp2print_attribute_images, $print_products_settings, $wpdb;
    $term_id = $tag->term_id;
	$wp2print_attribute_images = get_option('wp2print_attribute_images');
	$attribute_image = $wp2print_attribute_images[$term_id];
	$dimensions = get_term_meta($term_id, '_dimensions', true);
	$dimensions_pts = get_term_meta($term_id, '_dimensions_pts', true);
	$thickness = get_term_meta($term_id, '_thickness', true);
	$dimension_unit = print_products_get_dimension_unit();
	$term_color = get_term_meta($term_id, '_color', true);

	if (!$dimensions_pts && is_array($dimensions)) {
		$dimensions_pts = array('width' => '', 'height' => '');
		if ($dimensions['width']) {
			$dimensions_pts['width'] = print_products_unit_to_pts($dimensions['width'], $dimension_unit);
		}
		if ($dimensions['height']) {
			$dimensions_pts['height'] = print_products_unit_to_pts($dimensions['height'], $dimension_unit);
		}
	}

	$dimshow = false;
	$thicknessshow = false;
	$size_attribute = $print_products_settings['size_attribute'];
	if ($size_attribute) {
		$size_slug = $wpdb->get_var(sprintf("SELECT attribute_name FROM %swoocommerce_attribute_taxonomies WHERE attribute_id = %s", $wpdb->prefix, $size_attribute));
		if (strlen($size_slug) && isset($_GET['taxonomy']) && $_GET['taxonomy'] == 'pa_'.$size_slug) {
			$dimshow = true;
		}
	}
	$material_attribute = $print_products_settings['material_attribute'];
	if ($material_attribute) {
		$material_slug = $wpdb->get_var(sprintf("SELECT attribute_name FROM %swoocommerce_attribute_taxonomies WHERE attribute_id = %s", $wpdb->prefix, $material_attribute));
		if (strlen($material_slug) && isset($_GET['taxonomy']) && $_GET['taxonomy'] == 'pa_'.$material_slug) {
			$thicknessshow = true;
		}
	}
	?>
	<?php if ($dimshow) { ?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="presenter_id"><?php _e('Dimensions', 'wp2print'); ?> (<?php echo $dimension_unit; ?>)</label>
			</th>
			<td class="term-dimensions-wrap" data-dimm-unit="<?php echo $dimension_unit; ?>"><input type="text" name="dimensions[width]" value="<?php if (isset($dimensions['width'])) { echo $dimensions['width']; } ?>" style="width:60px;" placeholder="<?php _e('Width', 'wp2print'); ?>" class="dimm-width"> X <input type="text" name="dimensions[height]" value="<?php if (isset($dimensions['height'])) { echo $dimensions['height']; } ?>" style="width:60px;" placeholder="<?php _e('Height', 'wp2print'); ?>" class="dimm-height"></td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="presenter_id"><?php _e('Dimensions', 'wp2print'); ?> (points)</label>
			</th>
			<td class="term-dimensions-pts-wrap"><input type="text" name="dimensions_pts[width]" value="<?php if (isset($dimensions_pts['width'])) { echo $dimensions_pts['width']; } ?>" style="width:60px;" placeholder="<?php _e('Width', 'wp2print'); ?>" class="pts-width"> X <input type="text" name="dimensions_pts[height]" value="<?php if (isset($dimensions_pts['height'])) { echo $dimensions_pts['height']; } ?>" style="width:60px;" placeholder="<?php _e('Height', 'wp2print'); ?>" class="pts-height"> <input type="button" value="<?php _e('Calculate', 'wp2print'); ?>" class="button" onclick="wp2print_pts_calculate();"></td>
		</tr>
	<?php } ?>
	<?php if ($thicknessshow) { ?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="presenter_id"><?php _e('Thickness', 'wp2print'); ?> (<?php echo $dimension_unit; ?>)</label>
			</th>
			<td><input type="text" name="thickness" value="<?php echo $thickness; ?>" style="width:120px;"></td>
		</tr>
	<?php } ?>
	<tr class="form-field">
		<th scope="row" valign="top">
			<label for="presenter_id"><?php _e('Image', 'wp2print'); ?></label>
		</th>
		<td>
			<div class="uploader"><?php if ($attribute_image) { echo '<img src="'.print_products_get_thumb($attribute_image, 100, 80, true).'">'; } ?></div>
			<input class="button" type="button" value="<?php _e('Upload', 'wp2print') ?>" onclick="open_media_uploader_image()" />
			<input class="button del-button" type="button" value="<?php _e('Delete', 'wp2print') ?>" onclick="delete_media_uploader_image()"<?php if (!$attribute_image) { echo ' style="display:none;"'; } ?> />
			<input type="hidden" name="attribute_image" value="<?php echo $attribute_image; ?>" class="attribute-image-id">
			<p class="description"><?php _e('Image size: 100px x 80px', 'wp2print') ?></p>

			<script>
			var media_uploader = null;
			function open_media_uploader_image()
			{
				media_uploader = wp.media({
					frame:    "post",
					state:    "insert",
					multiple: false
				});

				media_uploader.on("insert", function(){
					var json = media_uploader.state().get("selection").first().toJSON();

					var image_id = json.id;
					var image_url = json.url;
					jQuery('.uploader').html('<img src="'+image_url+'" style="width:100px;">');
					jQuery('.attribute-image-id').val(image_id);
					jQuery('.del-button').show();
				});

				media_uploader.open();
			}
			function delete_media_uploader_image() {
				var d = confirm('<?php _e('Are you sure?', 'wp2print'); ?>');
				if (d) {
					jQuery('.uploader').html('');
					jQuery('.attribute-image-id').val('');
					jQuery('.del-button').hide();
				}
			}
			</script>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top">
			<label><?php _e('Color', 'wp2print'); ?></label>
		</th>
		<td>
			<input type="text" name="attribute_color" value="<?php echo $term_color; ?>" class="fld-color">
		</td>
	</tr>
	<?php
}

function print_products_attribute_image_save($term_id) {
	global $wp2print_attribute_images;
    if (isset($_POST['attribute_image'])) {
		$wp2print_attribute_images[$term_id] = $_POST['attribute_image'];
        update_option('wp2print_attribute_images', $wp2print_attribute_images);
    }
    if (isset($_POST['dimensions'])) {
		$dimensions = $_POST['dimensions'];
		$dimensions_pts = $_POST['dimensions_pts'];
		if ($dimensions['width'] && $dimensions['height']) {
			$dimension_unit = print_products_get_dimension_unit();
			if (!$dimensions_pts['width']) {
				$dimensions_pts['width'] = print_products_unit_to_pts($dimensions['width'], $dimension_unit);
			}
			if (!$dimensions_pts['height']) {
				$dimensions_pts['height'] = print_products_unit_to_pts($dimensions['height'], $dimension_unit);
			}
		}
		update_term_meta($term_id, '_dimensions', $dimensions);
		update_term_meta($term_id, '_dimensions_pts', $dimensions_pts);
		
    }
    if (isset($_POST['thickness'])) {
		update_term_meta($term_id, '_thickness', $_POST['thickness']);
    }
    if (isset($_POST['attribute_color'])) {
		update_term_meta($term_id, '_color', $_POST['attribute_color']);
    }
}

function print_products_attribute_image_column_field($columns){
	$new_columns = array();
	foreach($columns as $ckey => $cval) {
		$new_columns[$ckey] = $cval;
		if ($ckey == 'name') {
			$new_columns['aimage'] = __('Image', 'wp2print');
		}
	}
    return $new_columns;
}

function print_products_attribute_image_column_content($content, $column_name, $term_id) {
	global $wp2print_attribute_images;
	if ($column_name == 'aimage') {
		$attribute_image = $wp2print_attribute_images[$term_id];
		if ($attribute_image) {
			echo '<img src="'.print_products_get_thumb($attribute_image, 30, 30, true).'" style="border:1px solid #EEE;">';
		}
	}
	return $content;
}

function print_products_get_registered_attributes() {
	global $wpdb;
	$registered_attributes = array();
	$pa_taxonomies = $wpdb->get_results(sprintf("SELECT * FROM %sterm_taxonomy WHERE taxonomy LIKE '%s'", $wpdb->prefix, 'pa_%'));
	if ($pa_taxonomies) {
		foreach($pa_taxonomies as $pa_taxonomy) {
			$registered_attributes[] = $pa_taxonomy->taxonomy;
		}
	}
	return $registered_attributes;
}
?>