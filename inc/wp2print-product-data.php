<?php
// Add tabs to woocommerce product edit page
add_action('woocommerce_product_write_panel_tabs', 'print_products_price_matrix_tabs');
function print_products_price_matrix_tabs() {
    global $post;
	$product_id = $post->ID;
	$product_type = print_products_get_type($product_id);
	if ($product_type == 'eddm') { ?>
	    <li class="shipping_options show_if_eddm"><a href="#eddm_shipping"><span><?php _e('Shipping', 'wp2print'); ?></span></a></li>
	<?php } ?>
    <li class="file_source_tab hide_if_aec hide_if_paybill hide_if_quote"><a href="#file_source"><span><?php _e('File source', 'wp2print'); ?></span></a></li>
    <li class="attribute_options printing_attributes_tab hide_if_simple hide_if_grouped hide_if_external hide_if_variable hide_if_paybill hide_if_quote"><a href="#printingattributes"><span><?php _e('Printing attributes', 'wp2print'); ?></span></a></li>
    <li class="attribute_options finishing_attributes_tab hide_if_simple hide_if_grouped hide_if_external hide_if_variable hide_if_paybill hide_if_quote"><a href="#finishingattributes"><span><?php _e('Finishing attributes', 'wp2print'); ?></span></a></li>
    <li class="attribute_options attribute_labels_tab hide_if_simple hide_if_grouped hide_if_external hide_if_variable hide_if_paybill hide_if_quote"><a href="#attributelabels"><span><?php _e('Attribute labels', 'wp2print'); ?></span></a></li>
    <li class="attribute_options color_swatches_tab hide_if_simple hide_if_grouped hide_if_external hide_if_variable hide_if_paybill hide_if_quote"><a href="#colorswatches"><span><?php _e('Color swatches', 'wp2print'); ?></span></a></li>
	<li class="attribute_options price_matrix_tab hide_if_simple hide_if_grouped hide_if_external hide_if_variable hide_if_paybill hide_if_quote"><a href="#pricematrix"><span><?php _e('Prices', 'wp2print'); ?></span></a></li>
	<li class="variations_options hide_if_simple hide_if_grouped hide_if_external hide_if_variable hide_if_area hide_if_book hide_if_box hide_if_aec hide_if_aecbwc hide_if_aecsimple hide_if_paybill hide_if_eddm hide_if_quote"><a href="#unitpricetable"><span><?php _e('Unit price table', 'wp2print'); ?></span></a></li>
	<li class="linked_product_options hide_if_simple hide_if_virtual hide_if_grouped hide_if_external hide_if_variable hide_if_aec hide_if_aecbwc hide_if_aecsimple hide_if_paybill hide_if_eddm hide_if_quote hide_if_sticker"><a href="#add-on-products"><span><?php _e('Add-on products', 'wp2print'); ?></span></a></li>
	<li class="paybill_options hide_if_simple hide_if_virtual hide_if_grouped hide_if_external hide_if_variable hide_if_aec hide_if_aecbwc hide_if_aecsimple hide_if_fixed hide_if_area hide_if_book hide_if_box hide_if_eddm hide_if_quote hide_if_sticker"><a href="#paybill-options"><span><?php _e('Pay bill options', 'wp2print'); ?></span></a></li>
	<li class="quote_options hide_if_simple hide_if_grouped hide_if_external hide_if_variable hide_if_aec hide_if_aecbwc hide_if_aecsimple hide_if_fixed hide_if_area hide_if_book hide_if_box hide_if_eddm hide_if_sticker"><a href="#quote-options"><span><?php _e('Quote options', 'wp2print'); ?></span></a></li>
    <?php
		
}

add_action('woocommerce_product_data_panels', 'print_products_price_matrix_tabs_output');
function print_products_price_matrix_tabs_output() {
    global $post, $wpdb, $attribute_names, $terms_names, $print_products_settings;
	$product_id = $post->ID;
	$product_type = print_products_get_type($product_id);
	$dimension_unit = print_products_get_dimension_unit();
	$dimension_unit_aec = print_products_get_aec_dimension_unit();

	$area_min_width = get_post_meta($product_id, '_area_min_width', true);
	$area_min_height = get_post_meta($product_id, '_area_min_height', true);
	$area_min_length = get_post_meta($product_id, '_area_min_length', true);
	$area_max_width = get_post_meta($product_id, '_area_max_width', true);
	$area_max_height = get_post_meta($product_id, '_area_max_height', true);
	$area_max_length = get_post_meta($product_id, '_area_max_length', true);
	$area_width_round = get_post_meta($product_id, '_area_width_round', true);
	$area_height_round = get_post_meta($product_id, '_area_height_round', true);
	$area_length_round = get_post_meta($product_id, '_area_length_round', true);
	$area_min_quantity = get_post_meta($product_id, '_area_min_quantity', true);
	$area_unit = (int)get_post_meta($product_id, '_area_unit', true);
	$area_transposable = (int)get_post_meta($product_id, '_area_transposable', true);
	$auval = print_products_get_area_unit($area_unit);

	if (!$area_min_quantity) { $area_min_quantity = 1; }

	$num_types = print_products_get_num_types($auval);
	$num_type_labels = print_products_get_num_type_labels($auval);

	$attributes = $wpdb->get_results(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies ORDER BY attribute_order, attribute_label", $wpdb->prefix));
	print_products_price_matrix_attr_names_init($attributes);
	$mtype_names = print_products_price_matrix_get_types();
	$rounding_vals = array(125 => 0.125, 250 => 0.25, 500 => 0.5, 1000 => 1);

	$product_type_printing_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s AND mtype = 0 ORDER BY mtype, sorder", $wpdb->prefix, $product_id));
	$product_type_finishing_matrix_types = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s AND mtype = 1 ORDER BY mtype, sorder", $wpdb->prefix, $product_id));
	$product_type_matrix_types = array_merge($product_type_printing_matrix_types, $product_type_finishing_matrix_types);

    $personalize = get_post_meta($product_id, 'personalize', true);
	$artwork_source = get_post_meta($product_id, '_artwork_source', true);
	$artwork_allow_later = get_post_meta($product_id, '_artwork_allow_later', true);
	$artwork_file_url = get_post_meta($product_id, '_artwork_file_url', true);
	$artwork_file_url_order = (int)get_post_meta($product_id, '_artwork_file_url_order', true);
	$artwork_file_url_email = (int)get_post_meta($product_id, '_artwork_file_url_email', true);
	$artwork_file_count = (int)get_post_meta($product_id, '_artwork_file_count', true);
	$artwork_afile_types = get_post_meta($product_id, '_artwork_afile_types', true);

	$show_attribute_color = (int)get_post_meta($product_id, '_show_attribute_color', true);
	$color_modify_image = (int)get_post_meta($product_id, '_color_modify_image', true);
	$color_product_main_images = get_post_meta($product_id, '_color_product_main_images', true);
	$cart_upload_button = (int)get_post_meta($product_id, '_cart_upload_button', true);
	$cart_upload_button_text = get_post_meta($product_id, '_cart_upload_button_text', true);
	$cart_upload_stage = get_post_meta($product_id, '_cart_upload_stage', true);
	$cart_upload_artwork_label = get_post_meta($product_id, '_cart_upload_artwork_label', true);
	$cart_upload_artwork_required = get_post_meta($product_id, '_cart_upload_artwork_required', true);
	$cart_upload_artwork_ftypes = get_post_meta($product_id, '_cart_upload_artwork_ftypes', true);
	$cart_upload_artwork_uploaded_files = get_post_meta($product_id, '_cart_upload_artwork_uploaded_files', true);
	$cart_upload_database_label = get_post_meta($product_id, '_cart_upload_database_label', true);
	$cart_upload_database_required = get_post_meta($product_id, '_cart_upload_database_required', true);
	$cart_upload_database_ftypes = get_post_meta($product_id, '_cart_upload_database_ftypes', true);
	$cart_upload_database_uploaded_files = get_post_meta($product_id, '_cart_upload_database_uploaded_files', true);

	$use_production_speed = (int)get_post_meta($product_id, '_use_production_speed', true);
	$production_speed_label = get_post_meta($product_id, '_production_speed_label', true);
	$production_speed_options = get_post_meta($product_id, '_production_speed_options', true);
	$order_min_price = get_post_meta($product_id, '_order_min_price', true);
	$order_max_price = get_post_meta($product_id, '_order_max_price', true);
	$unitpricetable = get_post_meta($product_id, '_unitpricetable', true);
	$postage_attribute = (int)$print_products_settings['postage_attribute'];
	$ieditor_product_id = get_post_meta($product_id, '_ieditor_product_id', true);
	$show_uploaded_files = get_post_meta($product_id, '_show_uploaded_files', true);
	$multiple_file_uploads = get_post_meta($product_id, '_multiple_file_uploads', true);
	$multiple_file_data = get_post_meta($product_id, '_multiple_file_data', true);
	$reorder_color = (int)get_post_meta($product_id, '_reorder_color', true);
	$reorder_color_1 = get_post_meta($product_id, '_reorder_color_1', true);
	$preflight_options = get_post_meta($product_id, '_preflight_options', true);

	$print_products_preflight_profiles = get_option("print_products_preflight_profiles");

	$colour_matrix_options = print_products_get_matrix_colours($product_type_printing_matrix_types);

	$size_attribute = (int)$print_products_settings['size_attribute'];
	$colour_attribute = (int)$print_products_settings['colour_attribute'];

	if (!$artwork_file_count) { $artwork_file_count = 25; }
	if (!is_array($artwork_afile_types)) { $artwork_afile_types = array(); }
	if (!strlen($cart_upload_button_text)) { $cart_upload_button_text = __('Upload your database', 'wp2print'); }
	if (!is_array($cart_upload_artwork_ftypes)) { $cart_upload_artwork_ftypes = array(); }
	if (!is_array($cart_upload_database_ftypes)) { $cart_upload_database_ftypes = array(); }

	$ftypes = print_products_get_file_types();
	$aunits = print_products_get_area_units();
	$show_printing_matrix = true;
	if (($product_type == 'area' || $product_type == 'box') && !$product_type_printing_matrix_types) {
		$show_printing_matrix = false;
		if (strlen($area_min_width) && strlen($area_min_height) && strlen($area_min_quantity) && strlen($area_unit)) {
			$show_printing_matrix = true;
		}
	}

	$artwsources = array('artwork' => __('Customer uploads file', 'wp2print'));
	$artwsources = apply_filters('print_products_file_source_options', $artwsources);
	if (function_exists('fie_load_textdomain') && in_array($product_type, array('fixed', 'area', 'simple'))) {
		$artwsources['ieditor'] = __('Photo editor', 'wp2print');
	}
	?>
	<script>
	<!--
	jQuery('li.attribute_tab').addClass('<?php echo print_products_tab_classes(); ?>');
	<?php if ($product_type == 'aec' || $product_type == 'aecbwc' || $product_type == 'aecsimple') { ?>jQuery('li.file_source_tab').remove();<?php } ?>
	jQuery(document).ready(function(){
		setTimeout(function(){
			<?php if (isset($_GET['matrixopt']) && $_GET['matrixopt'] == '1' && isset($_GET['mtype'])) {
				if ($_GET['mtype'] == '1') {
					echo "jQuery('.finishing_attributes_tab a').trigger('click');";
				} else {
					echo "jQuery('.printing_attributes_tab a').trigger('click');";
				}
			} else if (isset($_GET['matrixp']) && $_GET['matrixp'] == '1') {
				echo "jQuery('.price_matrix_tab a').trigger('click');";
			} ?>
		}, 1000);
	});
	//--></script>
    <div id="file_source" class="panel woocommerce_options_panel">
        <div class="options_group">
			<p class="form-field" style="margin-bottom:0px;">
				<label><?php _e('File source', 'wp2print'); ?>:</label>
				<select name="artwork_source" onchange="artwork_source_change()" class="artwork-source">
					<option value=""><?php _e('No file upload required', 'wp2print'); ?></option>
					<?php foreach($artwsources as $awkey => $awval) { $s = ''; if ($awkey == $artwork_source) { $s = ' SELECTED'; } ?>
						<option value="<?php echo $awkey; ?>"<?php echo $s; ?>><?php echo $awval; ?></option>
					<?php } ?>
				</select>
			</p>
			<p class="form-field no-upload-option" style="margin-top:0px;">
				<label><?php _e('Full URL to file', 'wp2print'); ?>:</label>
				<input type="text" name="artwork_file_url" value="<?php echo $artwork_file_url; ?>" style="width:100%;">
				<input type="checkbox" name="artwork_file_url_order" value="1"<?php if ($artwork_file_url_order == 1) { echo ' CHECKED'; } ?>>&nbsp;&nbsp;<?php _e("Display 'URL to file' in admin Order detail page", 'wp2print'); ?><br>
				<input type="checkbox" name="artwork_file_url_email" value="1"<?php if ($artwork_file_url_email == 1) { echo ' CHECKED'; } ?>>&nbsp;&nbsp;<?php _e("Display 'URL to file' in order confirmation emails", 'wp2print'); ?>
			</p>
			<p class="form-field artwork-option" style="padding-top:0px !important; margin-top:0px;">
				<label><strong><?php _e('Uploads configuration', 'wp2print'); ?></strong>:</label>
			</p>
			<p class="form-field artwork-option" style="padding-top:0px !important; margin:0px;">
				<label><?php _e('Maximum file count', 'wp2print'); ?>:</label>
				<input type="number" name="artwork_file_count" value="<?php echo $artwork_file_count; ?>" style="width:60px;">
			</p>
			<p class="form-field artwork-option allowed-types" style="padding-top:0px !important; margin:0px;">
				<label><?php _e('Allowed file types', 'wp2print'); ?>:</label>
				<input type="checkbox" name="artwork_afile_types[]" value="all" class="artwork-afile-type-all" onclick="artwork_aftypes_change(1)"<?php if (!count($artwork_afile_types) || (is_array($artwork_afile_types) && in_array('all', $artwork_afile_types))) { echo ' CHECKED'; } ?>> <?php _e('All file types', 'wp2print'); ?>
				<span class="artwork-afile-types-list"><br>
					<?php foreach($ftypes as $ftype) { ?>
						<input type="checkbox" name="artwork_afile_types[]" value="<?php echo $ftype; ?>" class="artwork-afile-type" onclick="artwork_aftypes_change(0)"<?php if (is_array($artwork_afile_types) && in_array($ftype, $artwork_afile_types)) { echo ' CHECKED'; } ?>> <?php echo $ftype; ?>&nbsp;
					<?php } ?>
				</span>
			</p>
			<p class="form-field artwork-option" style="padding-top:0px !important; margin:0;">
				<label><?php _e('Allow upload later', 'wp2print'); ?>:</label>
				<input type="checkbox" name="artwork_allow_later" value="1"<?php if ($artwork_allow_later) { echo ' CHECKED'; } ?>>
			</p>
			<p class="form-field artwork-option" style="padding-top:0px !important; margin:0 0 5px;">
				<label style="width:275px;"><?php _e('Display previously uploaded files to customer', 'wp2print'); ?>: <input type="checkbox" name="show_uploaded_files" value="1" class="show-uploaded-files"<?php if ($show_uploaded_files == 1) { echo ' CHECKED'; } ?>></label>
			</p>
			<div class="artwork-option" style="border-top:1px solid #C1C1C1;">
				<p class="form-field" style="padding-top:3px !important; margin:0 0 5px;">
					<label><?php _e('Labeled file upload fields', 'wp2print'); ?>:</label>
					<input type="checkbox" name="multiple_file_uploads" value="1" class="multiple-file-uploads" onclick="artwork_multiple_file_uploads_change();"<?php if ($multiple_file_uploads == 1) { echo ' CHECKED'; } ?>>
				</p>
				<div class="multiple-file-option" style="display:none;">
					<?php $mfd_nums = array(1, 2, 3, 4); $mfd_types = array('simple' => __('Simple use', 'wp2print'), 'colour' => __('Use Color attribute', 'wp2print')); ?>
					<?php if ($colour_matrix_options) { ?>
						<p class="form-field" style="padding-top:0px !important; margin:0 0 5px;">
							<label><?php _e('Usage', 'wp2print'); ?>:</label>
							<select name="multiple_file_data[usage]" class="mfu-usage" onchange="artwork_multiple_file_uploads_usage()"><?php foreach ($mfd_types as $mfut_key => $mfut_name) { ?><option value="<?php echo $mfut_key; ?>"<?php if ($multiple_file_data && isset($multiple_file_data['usage']) && $multiple_file_data['usage'] == $mfut_key) { echo ' SELECTED'; } ?>><?php echo $mfut_name; ?></option><?php } ?></select>
						</p>
						<div class="mfu-usage-colour" style="padding:0 0 15px 160px; display:none;">
							<table style="width:auto;">
								<tr>
									<td style="border-top: 1px solid #EEE;border-bottom: 1px solid #EEE;padding:4px 0;"><?php _e('Color', 'wp2print'); ?></td>
									<td style="border-top: 1px solid #EEE;border-bottom: 1px solid #EEE;padding:4px 0;"><?php _e('Fields', 'wp2print'); ?></td>
									<?php foreach ($mfd_nums as $num) { ?>
									<td style="border-top: 1px solid #EEE;border-bottom: 1px solid #EEE;padding:4px 0;min-width:163px;"><?php _e('Upload file '.$num.' label', 'wp2print'); ?>&nbsp;</td>
									<?php } ?>
								</tr>
								<?php foreach ($colour_matrix_options as $ctid => $ctname) { ?>
								<tr class="mfu-cline-<?php echo $ctid; ?>">
									<td style="padding-top:4px;"><?php echo $ctname; ?>&nbsp;</td>
									<td style="padding-top:4px;"><select name="multiple_file_data[<?php echo $ctid; ?>][num]" class="mfu-num" onchange="artwork_multiple_file_uploads_num(this)" data-tp="colour" data-cln="<?php echo $ctid; ?>"><?php foreach ($mfd_nums as $num) { ?><option value="<?php echo $num; ?>"<?php if ($multiple_file_data && isset($multiple_file_data[$ctid]) && isset($multiple_file_data[$ctid]['num']) && $multiple_file_data[$ctid]['num'] == $num) { echo ' SELECTED'; } ?>><?php echo $num; ?></option><?php } ?></select></td>
									<?php foreach ($mfd_nums as $num) { ?>
									<td class="mfu-file-<?php echo $num; ?>" style="padding-top:4px;"><input type="text" name="multiple_file_data[<?php echo $ctid; ?>][label<?php echo $num; ?>]" value="<?php if ($multiple_file_data && isset($multiple_file_data[$ctid]) && isset($multiple_file_data[$ctid]['label'.$num])) { echo $multiple_file_data[$ctid]['label'.$num]; } ?>" style="width:160px;"></td>
									<?php } ?>
								</tr>
								<?php } ?>
							</table>
						</div>
					<?php } else { ?>
						<input type="hidden" name="multiple_file_data[usage]" value="simple" class="mfu-usage">
					<?php } ?>
					<div class="mfu-usage-simple" style="padding:0 0 15px 0; display:none;">
						<p class="form-field" style="padding-top:0px !important; margin:0 0 5px;">
							<label><?php _e('Fields', 'wp2print'); ?>:</label>
							<select name="multiple_file_data[num]" class="mfu-num" onchange="artwork_multiple_file_uploads_num(this)" data-tp="simple" data-cln="0"><?php foreach ($mfd_nums as $num) { ?><option value="<?php echo $num; ?>"<?php if ($multiple_file_data && isset($multiple_file_data['num']) && $multiple_file_data['num'] == $num) { echo ' SELECTED'; } ?>><?php echo $num; ?></option><?php } ?></select>
						</p>
						<?php foreach ($mfd_nums as $num) { ?>
						<p class="form-field mfu-file-<?php echo $num; ?>" style="padding-top:0px !important; margin:0 0 5px;">
							<label><?php _e('Upload file '.$num.' label', 'wp2print'); ?>:</label>
							<input type="text" name="multiple_file_data[label<?php echo $num; ?>]" value="<?php if ($multiple_file_data && isset($multiple_file_data['label'.$num])) { echo $multiple_file_data['label'.$num]; } ?>" style="width:200px;">
						</p>
						<?php } ?>
					</div>
				</div>
			</div>
			<div style="border-top:1px solid #C1C1C1; border-bottom:1px solid #C1C1C1;">
				<p class="form-field" style="margin-top:3px;">
					<label><?php _e('Display cart upload button', 'wp2print'); ?>:</label>
					<input type="checkbox" name="cart_upload_button" value="1" class="cart-upload-button" onclick="artwork_cart_upload_button_change()"<?php if ($cart_upload_button) { echo ' CHECKED'; } ?>>
				</p>
				<p class="form-field cart-upload-button-option" style="padding-top:0px !important; margin:0px;">
					<label><?php _e('Cart upload button text', 'wp2print'); ?>:</label>
					<input type="text" name="cart_upload_button_text" value="<?php echo $cart_upload_button_text; ?>" style="width:80%;">
				</p>
				<!-- ------------------------------------------------------------------- -->
				<?php $sup_vals = array(0 => __('Artwork only', 'wp2print'), 1 => __('Database only', 'wp2print'), 2 => __('Artwork and Database', 'wp2print')); ?>
				<p class="form-field cart-upload-button-option" style="padding-top:0px !important; margin:0 0 10px;">
					<label><?php _e('Use upload stage', 'wp2print'); ?>:</label>
					<select name="cart_upload_stage" onchange="artwork_cart_upload_stage_change()" class="cart-upload-stage">
						<?php foreach($sup_vals as $okey => $oval) { ?>
							<option value="<?php echo $okey; ?>"<?php if ($okey == $cart_upload_stage) { echo ' SELECTED'; } ?>><?php echo $oval; ?></option>
						<?php } ?>
					</select>
				</p>
				<p class="form-field cart-upload-button-option cart-upload-artwork-option" style="padding-top:10px !important; margin:0; border-top:1px solid #C1C1C1;">
					<label><?php _e('Text label for Artwork', 'wp2print'); ?>:</label>
					<input type="text" name="cart_upload_artwork_label" value="<?php echo $cart_upload_artwork_label; ?>" style="width:80%;">
				</p>
				<p class="form-field cart-upload-button-option cart-upload-artwork-option" style="padding-top:0px !important; margin:0;">
					<label><?php _e('Artwork upload is required', 'wp2print'); ?>:</label>
					<input type="checkbox" name="cart_upload_artwork_required" value="1"<?php if ($cart_upload_artwork_required) { echo ' CHECKED'; } ?>>
				</p>
				<p class="form-field cart-upload-button-option cart-upload-artwork-option" style="padding-top:0px !important; margin:0;">
					<label><?php _e('Artwork upload file types', 'wp2print'); ?>:</label>
					<span>
						<?php $dftypes = array('csv', 'xls', 'xlsx', 'txt'); ?>
						<?php foreach($ftypes as $ftype) { ?>
							<input type="checkbox" name="cart_upload_artwork_ftypes[]" value="<?php echo $ftype; ?>"<?php if (is_array($cart_upload_artwork_ftypes) && in_array($ftype, $cart_upload_artwork_ftypes)) { echo ' CHECKED'; } ?>> <?php echo $ftype; ?>&nbsp;&nbsp;
						<?php } ?>
					</span>
				</p>
				<p class="form-field cart-upload-button-option cart-upload-artwork-option" style="padding-top:0px !important; margin:0;">
					<label style="width:100%"><?php _e('Display previously uploaded files for Artwork', 'wp2print'); ?>: <input type="checkbox" name="cart_upload_artwork_uploaded_files" value="1"<?php if ($cart_upload_artwork_uploaded_files == 1) { echo ' CHECKED'; } ?>></label>
				</p>
				<!-- ------------------------------------------------------------------- -->
				<p class="form-field cart-upload-button-option cart-upload-database-option" style="padding-top:10px !important; margin:0; border-top:1px solid #C1C1C1;">
					<label><?php _e('Text label for Database', 'wp2print'); ?>:</label>
					<input type="text" name="cart_upload_database_label" value="<?php echo $cart_upload_database_label; ?>" style="width:80%;">
				</p>
				<p class="form-field cart-upload-button-option cart-upload-database-option" style="padding-top:0px !important; margin:0;">
					<label><?php _e('Database upload is required', 'wp2print'); ?>:</label>
					<input type="checkbox" name="cart_upload_database_required" value="1"<?php if ($cart_upload_database_required) { echo ' CHECKED'; } ?>>
				</p>
				<p class="form-field cart-upload-button-option cart-upload-database-option" style="padding-top:0px !important; margin:0;">
					<label><?php _e('Database upload file types', 'wp2print'); ?>:</label>
					<span>
						<?php $dftypes = array('csv', 'xls', 'xlsx', 'txt'); ?>
						<?php foreach($dftypes as $ftype) { ?>
							<input type="checkbox" name="cart_upload_database_ftypes[]" value="<?php echo $ftype; ?>"<?php if (is_array($cart_upload_database_ftypes) && in_array($ftype, $cart_upload_database_ftypes)) { echo ' CHECKED'; } ?>> <?php echo $ftype; ?>&nbsp;&nbsp;
						<?php } ?>
					</span>
				</p>
				<p class="form-field cart-upload-button-option cart-upload-database-option" style="padding-top:0px !important; margin:0;">
					<label style="width:100%"><?php _e('Display previously uploaded files for Database', 'wp2print'); ?>: <input type="checkbox" name="cart_upload_database_uploaded_files" value="1"<?php if ($cart_upload_database_uploaded_files == 1) { echo ' CHECKED'; } ?>></label>
				</p>
				<?php if (print_products_license_allow_flexo_plate()) { ?>
					<div style="padding-bottom:12px;">
						<p class="form-field" style="padding-top:10px !important; margin:0; border-top:1px solid #C1C1C1;">
							<label><?php _e('Force re-orders to 1-color', 'wp2print'); ?>:</label>
							<input type="checkbox" name="reorder_color" class="reorder-color" value="1" onclick="artwork_reorder_color_change()"<?php if ($reorder_color == 1) { echo ' CHECKED'; } ?>>
						</p>
						<?php if ($colour_matrix_options) { ?>
						<p class="form-field reorder-color-option" style="padding-bottom:10px !important; margin:0;">
							<label><?php _e('1-Color attribute item', 'wp2print'); ?>:</label>
							<select name="reorder_color_1">
								<?php foreach ($colour_matrix_options as $aci_id => $aci_name) { ?>
								<option value="<?php echo $aci_id; ?>"<?php if ($reorder_color_1 == $aci_id) { echo ' SELECTED'; } ?>><?php echo $aci_name; ?></option>
								<?php } ?>
							</select>
						</p>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<div style="border-bottom:1px solid #C1C1C1;padding-bottom:12px;">
				<p class="form-field" style="padding-top:10px !important; margin:0;">
					<label style="width:215px;"><?php _e('Customer-side preflight process enable', 'wp2print'); ?>:</label>
					<input type="checkbox" name="preflight_options[enable]" value="1"<?php if ($preflight_options && isset($preflight_options['enable']) && $preflight_options['enable'] == 1) { echo ' CHECKED'; } ?>>
				</p>
				<p class="form-field" style="padding-top:0px !important; margin:0;">
					<label style="width:297px;"><?php _e('Prevent add to cart with customer-side preflight failure', 'wp2print'); ?>:</label>
					<input type="checkbox" name="preflight_options[prevent_cart]" value="1"<?php if ($preflight_options && isset($preflight_options['prevent_cart']) && $preflight_options['prevent_cart'] == 1) { echo ' CHECKED'; } ?>>
				</p>
				<?php if ($print_products_preflight_profiles && is_array($print_products_preflight_profiles)) { $pprofile = isset($preflight_options['profile']) ? $preflight_options['profile'] : 0; ?>
				<p class="form-field" style="padding-top:0px !important; margin:0;">
					<label><?php _e('Preflight profile', 'wp2print'); ?>:</label>
					<select name="preflight_options[profile]">
					<?php foreach ($print_products_preflight_profiles as $pfpid => $preflight_profile) { if (!$pprofile && $preflight_profile['def'] == 1) { $pprofile = $pfpid; } ?>
					<option value="<?php echo $pfpid; ?>"<?php if ($pfpid == $pprofile) { echo ' SELECTED'; } ?>><?php echo $preflight_profile['name']; ?></option>
					<?php } ?>
					</select>
				</p>
				<?php } ?>
			</div>
			<?php if (function_exists('personalize_designer_fields')) { ?>
				<div class="personalize-fields" style="display:none;">
					<p class="form-field">
						<label><strong><?php _e('Designer configuration', 'wp2print'); ?></strong>:</label>
					</p>
					<?php personalize_designer_fields(); ?>
					<input type="hidden" name="personalize" value="<?php echo $personalize; ?>" class="personalize-fld">
				</div>
			<?php } ?>
			<?php if (function_exists('printess_product_fields')) { ?>
				<div class="printess-fields" style="display:none;">
					<p class="form-field">
						<label><strong><?php _e('Flex Designer configuration', 'wp2print'); ?></strong>:</label>
					</p>
					<?php printess_product_fields(); ?>
				</div>
			<?php } ?>
			<?php if (function_exists('fie_load_textdomain') && in_array($product_type, array('fixed', 'area', 'simple'))) { ?>
				<?php $sc_fields = false; ?>
				<?php if ($product_type == 'fixed') {
					if ($size_attribute && $colour_attribute) {
						$product_type_matrix_type = $wpdb->get_row(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s AND mtype = 0 ORDER BY sorder LIMIT 0, 1", $wpdb->prefix, $product_id));
						if ($product_type_matrix_type) {
							$mattributes = unserialize($product_type_matrix_type->attributes);
							$materms = unserialize($product_type_matrix_type->aterms);
							if ($mattributes) {
								$tsizes = array(); $tcolours = array();
								if (isset($materms[$size_attribute])) {
									$tsizes = $materms[$size_attribute];
								}
								if (isset($materms[$colour_attribute])) {
									$tcolours = $materms[$colour_attribute];
								}
								if (count($tsizes) && count($tcolours)) {
									$sc_fields = true;
								}
							}
						}
					}
				} ?>
				<?php if ($sc_fields) { ?>
					<div class="ieditor-option" style="margin:10px 0 10px 8px;">
						<table>
							<tr>
								<td colspan="3"><?php _e('Image Editor Product ID', 'wp2print'); ?>:</td>
							</tr>
							<tr>
								<td><strong><?php _e('Size', 'wp2print'); ?>&nbsp;&nbsp;</strong></td>
								<td><strong><?php _e('Colour', 'wp2print'); ?>&nbsp;&nbsp;</strong></td>
								<td><strong><?php _e('Product ID', 'personalize'); ?></strong></td>
							</tr>
							<?php foreach($tsizes as $tsize) { ?>
								<?php foreach($tcolours as $tcolour) { ?>
									<tr>
										<td><?php echo $terms_names[$tsize]; ?>&nbsp;&nbsp;</td>
										<td><?php echo $terms_names[$tcolour]; ?>&nbsp;&nbsp;</td>
										<td><input type="text" name="ieditor_product_id[<?php echo $tsize; ?>][<?php echo $tcolour; ?>]" value="<?php if (is_array($ieditor_product_id) && isset($ieditor_product_id[$tsize][$tcolour])) { echo $ieditor_product_id[$tsize][$tcolour]; } ?>" style="width:120px;"></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</table>
					</div>
				<?php } else { ?>
					<p class="form-field ieditor-option" style="padding-top:0px !important;">
						<label><?php _e('Image Editor Product ID', 'wp2print'); ?>:</label>
						<input type="text" name="ieditor_product_id" value="<?php echo $ieditor_product_id; ?>" style="width:80%;">
					</p>
				<?php } ?>
			<?php } ?>
        </div>	
    </div>
	<script>
	<!--
	function artwork_source_change() {
		var asval = jQuery('.artwork-source').val();
		if (asval == 'design' || asval == 'both') {
			jQuery('.personalize-fields').show();
			jQuery('.personalize-fld').val('y');
		} else {
			jQuery('.personalize-fields').hide();
			jQuery('.personalize-fld').val('n');
		}
		if (asval == 'artwork' || asval == 'both' || asval == 'artwork-pritess') {
			jQuery('.artwork-option').show();
		} else {
			jQuery('.artwork-option').hide();
		}
		if (asval == 'printess' || asval == 'artwork-printess') {
			jQuery('.printess-fields').show();
		} else {
			jQuery('.printess-fields').hide();
		}
		if (asval != '') {
			jQuery('p.no-upload-option').hide();
		} else {
			jQuery('p.no-upload-option').show();
		}
		if (asval == 'ieditor') {
			jQuery('.ieditor-option').show();
		} else {
			jQuery('.ieditor-option').hide();
		}
	}
	function artwork_aftypes_change(a) {
		if (a == 1) {
			if (jQuery('.artwork-afile-type-all').is(':checked')) {
				jQuery('.artwork-afile-type').removeAttr('checked');
				jQuery('.artwork-afile-types-list').hide();
			} else {
				jQuery('.artwork-afile-types-list').show();
			}
		} else {
			jQuery('.artwork-afile-type-all').removeAttr('checked');
		}
	}
	function artwork_cart_upload_button_change() {
		var ubch = jQuery('input.cart-upload-button').is(':checked');
		if (ubch) {
			jQuery('.cart-upload-button-option').show();
			artwork_cart_upload_stage_change();
		} else {
			jQuery('.cart-upload-button-option').hide();
		}
	}
	function artwork_cart_upload_stage_change() {
		var ubch = jQuery('input.cart-upload-button').is(':checked');
		if (ubch) {
			var oval = jQuery('select.cart-upload-stage').val();
			if (oval == 1) {
				jQuery('.cart-upload-artwork-option').hide();
			} else {
				jQuery('.cart-upload-artwork-option').show();
			}
			if (oval == 1 || oval == 2) {
				jQuery('.cart-upload-database-option').show();
			} else {
				jQuery('.cart-upload-database-option').hide();
			}
		}
	}
	function artwork_multiple_file_uploads_change() {
		var mfuch = jQuery('input.multiple-file-uploads').is(':checked');
		if (mfuch) {
			jQuery('.multiple-file-option').show();
		} else {
			jQuery('.multiple-file-option').hide();
		}
	}
	function artwork_multiple_file_uploads_usage() {
		var mfu_usage = jQuery('.mfu-usage').val();
		if (mfu_usage == 'colour') {
			jQuery('.mfu-usage-simple').hide();
			jQuery('.mfu-usage-colour').show();
		} else {
			jQuery('.mfu-usage-colour').hide();
			jQuery('.mfu-usage-simple').show();
		}
	}
	function artwork_multiple_file_uploads_num(o) {
		var nval = parseInt(jQuery(o).val());
		var t = jQuery(o).data('tp');
		var n = jQuery(o).data('cln');
		if (t == 'colour') {
			for (var f=1; f<=4; f++) {
				if (f <= nval) {
					jQuery('.mfu-usage-colour .mfu-cline-'+n+' .mfu-file-'+f+' input').show();
				} else {
					jQuery('.mfu-usage-colour .mfu-cline-'+n+' .mfu-file-'+f+' input').hide();
				}
			}
		} else {
			for (var f=1; f<=4; f++) {
				if (f <= nval) {
					jQuery('.mfu-usage-simple .mfu-file-'+f).show();
				} else {
					jQuery('.mfu-usage-simple .mfu-file-'+f).hide();
				}
			}
		}
	}
	function artwork_multiple_file_uploads_nums() {
		if (jQuery('.multiple-file-option .mfu-num').length) {
			jQuery('.multiple-file-option .mfu-num').each(function(){
				artwork_multiple_file_uploads_num(this);
			});
		}
	}
	function artwork_reorder_color_change() {
		var rcch = jQuery('input.reorder-color').is(':checked');
		if (rcch) {
			jQuery('.reorder-color-option').show();
		} else {
			jQuery('.reorder-color-option').hide();
		}
	}
	artwork_source_change();
	artwork_aftypes_change(1);
	artwork_cart_upload_button_change();
	artwork_cart_upload_stage_change();
	artwork_multiple_file_uploads_change();
	artwork_multiple_file_uploads_usage();
	artwork_multiple_file_uploads_nums();
	artwork_reorder_color_change();
	//--></script>
    <div id="printingattributes" class="panel woocommerce_options_panel" style="padding:10px;">
		<?php if ($post->post_status == 'auto-draft') { ?>
			<p style="color:#FF0000;"><?php _e('Please publish product before adding matrix options.', 'wp2print'); ?></p>
		<?php } else {
			if ($product_type == 'book') {
				$product_book_type = (int)get_post_meta($product_id, '_book_type', true);
				$book_types = array(
					__('No size or count detection', 'wp2print'),
					__('Size and count autodetection mixed BW/Color', 'wp2print'),
					__('Size and count autodetection  BW only', 'wp2print'),
					__('Count detection only', 'wp2print')
				);
				?>
				<p class="form-field">
					<label><?php _e('Book count detection', 'wp2print'); ?>:</label>
					<select name="_book_type" class="select short">
						<?php foreach($book_types as $btkey => $book_type) { ?>
							<option value="<?php echo $btkey; ?>"<?php if ($btkey == $product_book_type) { echo ' SELECTED'; } ?>><?php echo $book_type; ?></option>
						<?php } ?>
					</select>
				</p>
				<?php
			}
			if ($product_type == 'area' || $product_type == 'sticker') { ?>
				<table class="wp-list-table widefat striped">
					<tr>
						<td style="padding-right:0px;"><?php _e('Min Width', 'wp2print'); ?> (<?php echo $dimension_unit; ?>): <span style="color:#FF0000;">*</span></td>
						<td style="padding-left:0px;"><input type="text" name="area_min_width" value="<?php echo $area_min_width; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Max Width', 'wp2print'); ?> (<?php echo $dimension_unit; ?>):</td>
						<td style="padding-left:0px;"><input type="text" name="area_max_width" value="<?php echo $area_max_width; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Rounding', 'wp2print'); ?>:</td>
						<td style="padding-left:0px;"><select name="area_width_round">
							<option value="0">None</option>
							<?php foreach($rounding_vals as $rkey => $rval) { $s = ''; if ($rkey == $area_width_round) { $s = ' SELECTED'; } ?>
								<option value="<?php echo $rkey; ?>"<?php echo $s; ?>><?php echo $rval; ?></option>
							<?php } ?>
						</select></td>
					</tr>
					<tr>
						<td style="padding-right:0px;"><?php _e('Min Height', 'wp2print'); ?> (<?php echo $dimension_unit; ?>): <span style="color:#FF0000;">*</span></td>
						<td style="padding-left:0px;"><input type="text" name="area_min_height" value="<?php echo $area_min_height; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Max Height', 'wp2print'); ?> (<?php echo $dimension_unit; ?>):</td>
						<td style="padding-left:0px;"><input type="text" name="area_max_height" value="<?php echo $area_max_height; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Rounding', 'wp2print'); ?>:</td>
						<td style="padding-left:0px;"><select name="area_height_round">
							<option value="0">None</option>
							<?php foreach($rounding_vals as $rkey => $rval) { $s = ''; if ($rkey == $area_height_round) { $s = ' SELECTED'; } ?>
								<option value="<?php echo $rkey; ?>"<?php echo $s; ?>><?php echo $rval; ?></option>
							<?php } ?>
						</select></td>
					</tr>
					<tr>
						<td style="padding-right:0px;"><?php _e('Min Quantity', 'wp2print'); ?>: <span style="color:#FF0000;">*</span></td>
						<td style="padding-left:0px;"><input type="text" name="area_min_quantity" value="<?php echo $area_min_quantity; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Total Areas', 'wp2print'); ?>: <span style="color:#FF0000;">*</span></td>
						<td style="padding-left:0px;">
							<select name="area_unit">
								<?php foreach($aunits as $aukey => $auval) { ?>
									<option value="<?php echo $aukey; ?>"<?php if ($aukey == $area_unit) { echo ' SELECTED'; } ?>><?php echo $auval; ?></option>
								<?php } ?>
							</select>
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-right:0px;" colspan="6"><input type="checkbox" name="area_transposable" value="1"<?php if ($area_transposable == 1) { echo ' CHECKED'; } ?>> <?php _e('Transposable dimension limits', 'wp2print'); ?></td>
					</tr>
				</table><br />
			<?php } ?>
			<?php if ($product_type == 'sticker') {
				$dm_value = '';
				if ($aunits && isset($aunits[$area_unit])) {
					$dm_value = $aunits[$area_unit];
				} else {
					foreach($aunits as $aukey => $auval) {
						if (!strlen($dm_value)) { $dm_value = $auval; }
					}
				}
				$area_ranges = get_post_meta($product_id, '_area_ranges', true);
				?>
				<table class="wp-list-table widefat striped area-ranges-table" style="width:auto;">
					<tr>
						<td colspan="3" style="text-align:center;"><?php _e('Area ranges', 'wp2print'); ?></td>
					</tr>
					<tr>
						<td><?php _e('Min Area', 'wp2print'); ?> (<?php echo $dm_value; ?>)</td>
						<td><?php _e('Max Area', 'wp2print'); ?> (<?php echo $dm_value; ?>)</td>
						<td>&nbsp;</td>
					</tr>
					<?php if ($area_ranges && is_array($area_ranges) && count($area_ranges)) { ?>
					<?php foreach ($area_ranges as $arnum => $ardata) { ?>
						<tr class="ar-row ar-row-<?php echo $arnum; ?>">
							<td><input type="text" name="area_ranges[<?php echo $arnum; ?>][min]" value="<?php echo $ardata['min']; ?>" style="width:70px;"></td>
							<td><input type="text" name="area_ranges[<?php echo $arnum; ?>][max]" value="<?php echo $ardata['max']; ?>" style="width:70px;"></td>
							<td style="vertical-align:bottom;"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/icon-delete.png" style="width:16px;cursor:pointer;" onclick="area_ranges_delete(<?php echo $arnum; ?>);"></td>
						</tr>
					<?php } ?>
					<?php } ?>
				</table>
				<table class="area-ranges-row" style="display:none;">
					<tr class="ar-row-{N}">
						<td><input type="text" name="area_ranges[{N}][min]" style="width:70px;"></td>
						<td><input type="text" name="area_ranges[{N}][max]" style="width:70px;"></td>
						<td style="vertical-align:bottom;"><img src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>images/icon-delete.png" style="width:16px;cursor:pointer;" onclick="area_ranges_delete({N});"></td>
					</tr>
				</table>
				<input type="button" value="<?php _e('Add new row', 'wp2print'); ?>" class="button" style="margin:10px 0 20px 0;" onclick="area_ranges_add()">
				<script>
				<!--
				var arnumber = jQuery('.area-ranges-table .ar-row').length;
				function area_ranges_add() {
					var trhtml = jQuery('table.area-ranges-row tbody').html();
					trhtml = wp2print_replace(trhtml, '{N}', arnumber);
					jQuery('.area-ranges-table tbody').append(trhtml);
					arnumber = arnumber + 1;
				}
				function area_ranges_delete(trn) {
					var d = confirm('<?php _e('Are you sure?', 'wp2print'); ?>');
					if (d) {
						jQuery('.area-ranges-table .ar-row-'+trn).remove();
					}
				}
				//--></script>
			<?php } ?>
			<?php if ($product_type == 'box') { ?>
				<table class="wp-list-table widefat striped">
					<tr>
						<td style="padding-right:0px;"><?php _e('Min Width', 'wp2print'); ?> (<?php echo $dimension_unit; ?>): <span style="color:#FF0000;">*</span></td>
						<td style="padding-left:0px;"><input type="text" name="area_min_width" value="<?php echo $area_min_width; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Max Width', 'wp2print'); ?> (<?php echo $dimension_unit; ?>):</td>
						<td style="padding-left:0px;"><input type="text" name="area_max_width" value="<?php echo $area_max_width; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Rounding', 'wp2print'); ?>:</td>
						<td style="padding-left:0px;"><select name="area_width_round">
							<option value="0">None</option>
							<?php foreach($rounding_vals as $rkey => $rval) { $s = ''; if ($rkey == $area_width_round) { $s = ' SELECTED'; } ?>
								<option value="<?php echo $rkey; ?>"<?php echo $s; ?>><?php echo $rval; ?></option>
							<?php } ?>
						</select></td>
					</tr>
					<tr>
						<td style="padding-right:0px;"><?php _e('Min Height', 'wp2print'); ?> (<?php echo $dimension_unit; ?>): <span style="color:#FF0000;">*</span></td>
						<td style="padding-left:0px;"><input type="text" name="area_min_height" value="<?php echo $area_min_height; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Max Height', 'wp2print'); ?> (<?php echo $dimension_unit; ?>):</td>
						<td style="padding-left:0px;"><input type="text" name="area_max_height" value="<?php echo $area_max_height; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Rounding', 'wp2print'); ?>:</td>
						<td style="padding-left:0px;"><select name="area_height_round">
							<option value="0">None</option>
							<?php foreach($rounding_vals as $rkey => $rval) { $s = ''; if ($rkey == $area_height_round) { $s = ' SELECTED'; } ?>
								<option value="<?php echo $rkey; ?>"<?php echo $s; ?>><?php echo $rval; ?></option>
							<?php } ?>
						</select></td>
					</tr>
					<tr>
						<td style="padding-right:0px;"><?php _e('Min Length', 'wp2print'); ?> (<?php echo $dimension_unit; ?>): <span style="color:#FF0000;">*</span></td>
						<td style="padding-left:0px;"><input type="text" name="area_min_length" value="<?php echo $area_min_length; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Max Length', 'wp2print'); ?> (<?php echo $dimension_unit; ?>):</td>
						<td style="padding-left:0px;"><input type="text" name="area_max_length" value="<?php echo $area_max_length; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Rounding', 'wp2print'); ?>:</td>
						<td style="padding-left:0px;"><select name="area_length_round">
							<option value="0">None</option>
							<?php foreach($rounding_vals as $rkey => $rval) { $s = ''; if ($rkey == $area_length_round) { $s = ' SELECTED'; } ?>
								<option value="<?php echo $rkey; ?>"<?php echo $s; ?>><?php echo $rval; ?></option>
							<?php } ?>
						</select></td>
					</tr>
					<tr>
						<td style="padding-right:0px;"><?php _e('Min Quantity', 'wp2print'); ?>: <span style="color:#FF0000;">*</span></td>
						<td style="padding-left:0px;"><input type="text" name="area_min_quantity" value="<?php echo $area_min_quantity; ?>" style="width:70px;"></td>
						<td style="padding-right:0px;"><?php _e('Total Areas', 'wp2print'); ?>: <span style="color:#FF0000;">*</span></td>
						<td style="padding-left:0px;">
							<select name="area_unit">
								<?php foreach($aunits as $aukey => $auval) { ?>
									<option value="<?php echo $aukey; ?>"<?php if ($aukey == $area_unit) { echo ' SELECTED'; } ?>><?php echo $auval; ?></option>
								<?php } ?>
							</select>
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
				</table><br />
			<?php } ?>
			<?php if ($show_printing_matrix) { ?>
				<?php if ($product_type_printing_matrix_types) { ?>
					<table class="wp-list-table widefat striped">
						<tr>
							<td><strong><?php _e('Attributes', 'wp2print'); ?></strong></td>
							<td><strong><?php _e('Quantities', 'wp2print'); ?></strong></td>
							<td align="center"><strong><?php _e('Display Sort Order', 'wp2print'); ?></strong></td>
							<td align="center"><strong><?php _e('Action', 'wp2print'); ?></strong></td>
						</tr>
						<?php foreach($product_type_printing_matrix_types as $product_type_matrix_type) {
							$mtype_id = $product_type_matrix_type->mtype_id;
							$mtattributes = unserialize($product_type_matrix_type->attributes);
							$numbers = $product_type_matrix_type->numbers;
							$sorder = $product_type_matrix_type->sorder;
							$num_type = $product_type_matrix_type->num_type;
							$mtattr_names = print_products_price_matrix_attr_names($mtattributes);
							?>
							<tr>
								<td><?php echo implode(', ', $mtattr_names); ?></td>
								<td><div style="max-width:300px;"><?php echo $num_type_labels[$num_type]; ?>: <?php echo $numbers; ?></div></td>
								<td align="center"><?php echo $sorder; ?></td>
								<td align="center"><a href="edit.php?post_type=product&page=print-products-price-matrix-options&pid=<?php echo $product_id; ?>&mtid=<?php echo $mtype_id; ?>" class="button-primary" style="width:60px;margin-bottom:4px;"><?php _e('edit', 'wp2print'); ?></a><br><a href="edit.php?post_type=product&page=print-products-price-matrix-options&pid=<?php echo $product_id; ?>&mtid=<?php echo $mtype_id; ?>&mtype=0&print_products_price_matrix_types_action=delete" class="button-primary" onclick="return confirm('<?php _e('Are you sure?', 'wp2print'); ?>');" style="width:60px;"><?php _e('delete', 'wp2print'); ?></a></td>
							</tr>
						<?php } ?>
					</table><br>
				<?php } else {
					echo '<hr><p>'.__('No Printing attributes', 'wp2print').'</p><hr>';
				} ?>
				<a href="edit.php?post_type=product&page=print-products-price-matrix-options&pid=<?php echo $product_id; ?>&pmtaction=add&mtype=0" class="button-primary"><?php _e('Add new printing price matrix', 'wp2print'); ?></a>&nbsp;
				<?php if ($product_type_printing_matrix_types) { ?>
					<a href="edit.php?post_type=product&page=print-products-unavailability&product_id=<?php echo $product_id; ?>" class="button-primary"><?php _e('Adjust availability', 'wp2print'); ?></a>
				<?php } ?>
				<?php } else { ?>
					<p style="color:#FF0000;"><?php _e('Please fill required area data before adding printing attributes.', 'wp2print'); ?></p>
			<?php } ?>
		<?php } ?>
    </div>
    <div id="finishingattributes" class="panel woocommerce_options_panel" style="padding:10px;">
		<?php if ($post->post_status == 'auto-draft') { ?>
			<p style="color:#FF0000;"><?php _e('Please publish product before adding matrix options.', 'wp2print'); ?></p>
		<?php } else {
			if ($product_type_finishing_matrix_types) { ?>
				<table class="wp-list-table widefat striped">
					<tr>
						<td><strong><?php _e('Attributes', 'wp2print'); ?></strong></td>
						<td><strong><?php _e('Quantities', 'wp2print'); ?></strong></td>
						<td align="center"><strong><?php _e('Display Sort Order', 'wp2print'); ?></strong></td>
						<td align="center"><strong><?php _e('Action', 'wp2print'); ?></strong></td>
					</tr>
					<?php foreach($product_type_finishing_matrix_types as $product_type_matrix_type) {
						$mtype_id = $product_type_matrix_type->mtype_id;
						$mtattributes = unserialize($product_type_matrix_type->attributes);
						$numbers = $product_type_matrix_type->numbers;
						$sorder = $product_type_matrix_type->sorder;
						$num_type = $product_type_matrix_type->num_type;
						$mtattr_names = print_products_price_matrix_attr_names($mtattributes);
						?>
						<tr>
							<td><?php echo implode(', ', $mtattr_names); ?></td>
							<td><div style="max-width:300px;"><?php echo $num_type_labels[$num_type]; ?>: <?php echo $numbers; ?></div></td>
							<td align="center"><?php echo $sorder; ?></td>
							<td align="center"><a href="edit.php?post_type=product&page=print-products-price-matrix-options&pid=<?php echo $product_id; ?>&mtid=<?php echo $mtype_id; ?>" class="button-primary" style="width:60px;margin-bottom:4px;"><?php _e('edit', 'wp2print'); ?></a><br><a href="edit.php?post_type=product&page=print-products-price-matrix-options&pid=<?php echo $product_id; ?>&mtid=<?php echo $mtype_id; ?>&mtype=1&print_products_price_matrix_types_action=delete" class="button-primary" onclick="return confirm('<?php _e('Are you sure?', 'wp2print'); ?>');" style="width:60px;"><?php _e('delete', 'wp2print'); ?></a></td>
						</tr>
					<?php } ?>
				</table><br>
			<?php } else {
				echo '<hr><p>'.__('No Finishing attributes', 'wp2print').'</p><hr>';
			} ?>
			<a href="edit.php?post_type=product&page=print-products-price-matrix-options&pid=<?php echo $product_id; ?>&pmtaction=add&mtype=1" class="button-primary"><?php _e('Add new finishing price matrix', 'wp2print'); ?></a>&nbsp;
			<?php if ($product_type_finishing_matrix_types) { ?>
				<a href="edit.php?post_type=product&page=print-products-unavailability&product_id=<?php echo $product_id; ?>" class="button-primary"><?php _e('Adjust availability', 'wp2print'); ?></a>
			<?php } ?>
		<?php } ?>
    </div>
    <div id="attributelabels" class="panel woocommerce_options_panel" style="padding:10px;">
		<?php 
		$attribute_labels = get_post_meta($product_id, '_attribute_labels', true);
		$attribute_display = get_post_meta($product_id, '_attribute_display', true);
		$attribute_as_radio = get_post_meta($product_id, '_attribute_as_radio', true);
		if (!is_array($attribute_labels)) { $attribute_labels = array(); }
		if (!is_array($attribute_display)) { $attribute_display = array(); }
		if (!is_array($attribute_as_radio)) { $attribute_as_radio = array(); }
		if ($product_type == 'book') { ?>
			<table class="wp-list-table widefat striped">
				<tr>
					<td style="width:30%;vertical-align:middle;"><?php _e('Books quantity label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[bquantity]" value="<?php if (isset($attribute_labels['bquantity'])) { echo $attribute_labels['bquantity']; } ?>" style="width:200px;" placeholder="<?php _e('Quantity of bound books', 'wp2print'); ?>"></td>
				</tr>
				<tr>
					<td style="vertical-align:middle;"><?php _e('Pages quantity label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[pquantity]" value="<?php if (isset($attribute_labels['pquantity'])) { echo $attribute_labels['pquantity']; } ?>" style="width:200px;" placeholder="<?php _e('Pages Quantity', 'wp2print'); ?>"></td>
				</tr>
			</table><br>
		<?php } else if ($product_type == 'area') { ?>
			<table class="wp-list-table widefat striped">
				<tr>
					<td style="width:30%;vertical-align:middle;"><?php _e('Quantity label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[quantity]" value="<?php if (isset($attribute_labels['quantity'])) { echo $attribute_labels['quantity']; } ?>" style="width:200px;" placeholder="<?php _e('Quantity', 'wp2print'); ?>"></td>
				</tr>
				<tr>
					<td style="vertical-align:middle;"><?php _e('Width label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[width]" value="<?php if (isset($attribute_labels['width'])) { echo $attribute_labels['width']; } ?>" style="width:200px;" placeholder="<?php _e('Width', 'wp2print'); ?>"></td>
				</tr>
				<tr>
					<td style="vertical-align:middle;"><?php _e('Height label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[height]" value="<?php if (isset($attribute_labels['height'])) { echo $attribute_labels['height']; } ?>" style="width:200px;" placeholder="<?php _e('Height', 'wp2print'); ?>"></td>
				</tr>
			</table><br>
		<?php } else if ($product_type == 'box') { ?>
			<table class="wp-list-table widefat striped">
				<tr>
					<td style="width:30%;vertical-align:middle;"><?php _e('Quantity label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[quantity]" value="<?php if (isset($attribute_labels['quantity'])) { echo $attribute_labels['quantity']; } ?>" style="width:200px;" placeholder="<?php _e('Quantity', 'wp2print'); ?>"></td>
				</tr>
				<tr>
					<td style="vertical-align:middle;"><?php _e('Width label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[width]" value="<?php if (isset($attribute_labels['width'])) { echo $attribute_labels['width']; } ?>" style="width:200px;" placeholder="<?php _e('Box width', 'wp2print'); ?>"></td>
				</tr>
				<tr>
					<td style="vertical-align:middle;"><?php _e('Height label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[height]" value="<?php if (isset($attribute_labels['height'])) { echo $attribute_labels['height']; } ?>" style="width:200px;" placeholder="<?php _e('Box height', 'wp2print'); ?>"></td>
				</tr>
				<tr>
					<td style="vertical-align:middle;"><?php _e('Length label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[length]" value="<?php if (isset($attribute_labels['length'])) { echo $attribute_labels['length']; } ?>" style="width:200px;" placeholder="<?php _e('Box length', 'wp2print'); ?>"></td>
				</tr>
			</table><br>
		<?php } else if ($product_type == 'eddm') { ?>
			<table class="wp-list-table widefat striped">
				<tr>
					<td style="vertical-align:middle;"><?php _e('EDDM Select routes button label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[eddmsrbl]" value="<?php if (isset($attribute_labels['eddmsrbl'])) { echo $attribute_labels['eddmsrbl']; } ?>" style="width:200px;" placeholder="<?php _e('Select routes', 'wp2print'); ?>"></td>
				</tr>
				<tr>
					<td style="vertical-align:middle;"><?php _e('EDDM Target customer label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[eddmtcl]" value="<?php if (isset($attribute_labels['eddmtcl'])) { echo $attribute_labels['eddmtcl']; } ?>" style="width:200px;" placeholder="<?php _e('EDDM Target customer', 'wp2print'); ?>"></td>
				</tr>
			</table>
		<?php } else { ?>
			<table class="wp-list-table widefat striped">
				<tr>
					<td style="width:30%;vertical-align:middle;"><?php _e('Quantity label', 'wp2print'); ?></td>
					<td><input type="text" name="attribute_labels[quantity]" value="<?php if (isset($attribute_labels['quantity'])) { echo $attribute_labels['quantity']; } ?>" style="width:200px;" placeholder="<?php _e('Quantity', 'wp2print'); ?>"></td>
				</tr>
			</table><br>
		<?php } ?>
		<?php if ($product_type_printing_matrix_types || $product_type_finishing_matrix_types) {
			$pattributes = array();
			if ($product_type_printing_matrix_types) {
				foreach($product_type_printing_matrix_types as $product_type_matrix_type) {
					$mtattributes = unserialize($product_type_matrix_type->attributes);
					if ($mtattributes) {
						foreach($mtattributes as $mtattribute) {
							if (!in_array($mtattribute, $pattributes)) {
								$pattributes[] = $mtattribute;
							}
						}
					}
				}
			}
			if ($product_type_finishing_matrix_types) {
				foreach($product_type_finishing_matrix_types as $product_type_matrix_type) {
					$mtattributes = unserialize($product_type_matrix_type->attributes);
					if ($mtattributes) {
						foreach($mtattributes as $mtattribute) {
							if (!in_array($mtattribute, $pattributes)) {
								$pattributes[] = $mtattribute;
							}
						}
					}
				}
			}
			print_products_price_matrix_attr_names_init($pattributes);
			?>
			<table class="wp-list-table widefat striped">
				<tr>
					<td style="width:30%;"><strong><?php _e('Attribute', 'wp2print'); ?></strong></td>
					<td><strong><?php _e('Label', 'wp2print'); ?></strong></td>
					<td align="center"><strong><?php _e('Display as radio buttons', 'wp2print'); ?></strong></td>
					<td align="center"><strong><?php _e('Do not display', 'wp2print'); ?></strong></td>
				</tr>
				<?php foreach($pattributes as $pattribute) { ?>
					<tr>
						<td style="vertical-align:middle;"><?php echo $attribute_names[$pattribute]; ?></td>
						<td><input type="text" name="attribute_labels[<?php echo $pattribute; ?>]" value="<?php if (isset($attribute_labels[$pattribute])) { echo $attribute_labels[$pattribute]; } ?>" style="width:200px;" placeholder="<?php if (isset($attribute_names[$pattribute])) { echo $attribute_names[$pattribute]; } ?>"></td>
						<td align="center"><input type="checkbox" name="attribute_as_radio[<?php echo $pattribute; ?>]" value="1" <?php if (isset($attribute_as_radio[$pattribute]) && $attribute_as_radio[$pattribute] == 1) { echo ' CHECKED'; } ?>></td>
						<td align="center"><input type="checkbox" name="attribute_display[<?php echo $pattribute; ?>]" value="1" <?php if (isset($attribute_display[$pattribute]) && $attribute_display[$pattribute] == 1) { echo ' CHECKED'; } ?>></td>
					</tr>
					<?php if ($pattribute == $postage_attribute) { ?>
						<tr>
							<td style="vertical-align:middle;"><?php _e('Quantity Mailed', 'wp2print'); ?></td>
							<td><input type="text" name="attribute_labels[qm]" value="<?php if (isset($attribute_labels['qm'])) { echo $attribute_labels['qm']; } ?>" style="width:200px;" placeholder="<?php _e('Quantity Mailed', 'wp2print'); ?>"></td>
							<td align="center">&nbsp;</td>
							<td align="center">&nbsp;</td>
						</tr>
					<?php } ?>
				<?php } ?>
			</table><br>
		<?php } else {
			echo '<hr><p>'.__('No selected attributes', 'wp2print').'</p><hr>';
		} ?>
    </div>
	<?php
	$selected_color_attributes = array();
	if ($product_type_matrix_types) {
		foreach ($product_type_matrix_types as $product_type_matrix_type) {
			$mattributes = unserialize($product_type_matrix_type->attributes);
			$materms = unserialize($product_type_matrix_type->aterms);
			if ($mattributes) {
				foreach($mattributes as $mattribute) {
					if ($mattribute == $colour_attribute) {
						$aterms = $materms[$mattribute];
						$aterms = print_products_get_attribute_terms($aterms);
						foreach($aterms as $aterm_id => $aterm_name) {
							$selected_color_attributes[$aterm_id] = $aterm_name;
						}
					}
				}
			}
		}
	}
	if (!is_array($color_product_main_images)) { $color_product_main_images = array(); }
	?>
    <div id="colorswatches" class="panel woocommerce_options_panel" style="padding:10px;">
		<?php if (count($selected_color_attributes)) { ?>
			<table class="wp-list-table">
				<tr>
					<td><input type="checkbox" name="show_attribute_color" value="1"<?php if ($show_attribute_color) { echo ' CHECKED'; } ?>> <?php _e('Display color swatches', 'wp2print'); ?></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="color_modify_image" value="1" class="color-modify-image" onclick="color_modify_image_click()"<?php if ($color_modify_image) { echo ' CHECKED'; } ?>> <?php _e('Modify main product image', 'wp2print'); ?></td>
				</tr>
			</table>
			<table class="wp-list-table color-images-table" style="border:1px solid #EEE;margin-top:10px;display:none;">
				<?php foreach ($selected_color_attributes as $aid => $aname) {
					$acpm_image = '';
					if (isset($color_product_main_images[$aid]) && $color_product_main_images[$aid]) { $acpm_image = $color_product_main_images[$aid]; } ?>
					<tr>
						<td><?php echo $aname; ?>&nbsp;&nbsp;</td>
						<td class="color-image-<?php echo $aid; ?>"><?php if ($acpm_image) { echo '<a href="'.$acpm_image.'" target="_blank"><img src="'.$acpm_image.'" style="width:80px;"></a>'; } else { _e('No image', 'wp2print'); } ?></td>
						<td><input type="button" value="<?php _e('Add image', 'wp2print'); ?>" class="button" onclick="color_add_image(<?php echo $aid; ?>)"><input type="hidden" name="color_product_main_images[<?php echo $aid; ?>]" value="<?php echo $acpm_image; ?>" class="color-product-main-image-<?php echo $aid; ?>"></td>
					</tr>
				<?php } ?>
			</table>
			<script>
			<!--
			var color_media_uploader = null;
			function color_modify_image_click() {
				var cmi_checked = jQuery('input.color-modify-image').is(':checked');
				if (cmi_checked) {
					jQuery('.color-images-table').show();
				} else {
					jQuery('.color-images-table').hide();
				}
			}
			function color_add_image(aid) {
				color_media_uploader = wp.media({
					frame:    "post",
					state:    "insert",
					multiple: false
				});

				color_media_uploader.on("insert", function(){
					var json = color_media_uploader.state().get("selection").first().toJSON();
					var image_url = json.url;
					jQuery('.color-image-'+aid).html('<a href="'+image_url+'" target="_blank"><img src="'+image_url+'" style="width:80px;"></a>');
					jQuery('.color-product-main-image-'+aid).val(image_url);
				});

				color_media_uploader.open();
			}
			color_modify_image_click();
			//--></script>
		<?php } else { ?>
			<p><?php _e('Please select color attributes fist.', 'wp2print'); ?></p>
		<?php } ?>
    </div>
    <div id="pricematrix" class="panel woocommerce_options_panel" style="padding:15px;">
		<?php if ($post->post_status == 'auto-draft') { ?>
			<p style="color:#FF0000;"><?php _e('Please publish product before adding price matrix.', 'wp2print'); ?></p>
		<?php } else {
			if ($product_type == 'aec') {
				$inc_coverage_prices = (array)get_post_meta($product_id, '_inc_coverage_prices', true);
				$apply_round_up = (int)get_post_meta($product_id, '_apply_round_up', true);
				$round_up_discounts = (array)get_post_meta($product_id, '_round_up_discounts', true);
				$aec_coverage_ranges = print_products_get_aec_coverage_ranges();
				$price_decimals = wc_get_price_decimals();
				$lcrange = 0;
				$icp_vals = array();
				foreach($aec_coverage_ranges as $crange) {
					$icp_vals[$crange] = $lcrange.'% - '.$crange.'%';
					$lcrange = $crange;
				}
				if ($product_type_printing_matrix_types) {
					$material_attribute = $print_products_settings['material_attribute'];
					$material_attrs = array();
					foreach($product_type_printing_matrix_types as $product_type_printing_matrix_type) {
						$mattributes = unserialize($product_type_printing_matrix_type->attributes);
						$materms = unserialize($product_type_printing_matrix_type->aterms);
						if (in_array($material_attribute, $mattributes)) {
							$material_attrs = $materms[$material_attribute];
						}
					}
					$disc_numbers = array(1, 10, 50, 100, 1000);
					if (count($material_attrs)) { ?>
						<div class="inc-coverage-box">
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td><strong><?php _e('Paper Type', 'wp2print'); ?></strong>&nbsp;&nbsp;</td>
									<td><strong><?php _e('Coverage', 'wp2print'); ?> %</strong>&nbsp;&nbsp;</td>
									<td><strong><?php _e('Price', 'wp2print'); ?>/<?php echo $dimension_unit_aec; ?><sup>2</sup></strong></td>
								</tr>
								<?php foreach($material_attrs as $material_attr) { $material_attr = (int)$material_attr; ?>
									<?php foreach($icp_vals as $icp_val => $icp_label) { ?>
										<tr>
											<td><?php echo $terms_names[$material_attr]; ?>&nbsp;&nbsp;</td>
											<td align="center"><?php echo $icp_label; ?></td>
											<td><div style="float:left;padding:3px 3px 0 0;">$</div><input type="text" name="inc_coverage_prices[<?php echo $material_attr; ?>][<?php echo $icp_val; ?>]" value="<?php echo number_format((float)$inc_coverage_prices[$material_attr][$icp_val], $price_decimals); ?>" style="width:50px;"></td>
										</tr>
									<?php } ?>
								<?php } ?>
							</table>
						</div>
						<div class="round-up-box">
							<table>
								<tr>
									<td><input type="checkbox" name="apply_round_up" value="1" class="apply-round-up"<?php if ($apply_round_up == 1) { echo ' CHECKED'; } ?>></td>
									<td><?php _e('Round-up area to nearest', 'wp2print'); ?> <?php echo $dimension_unit_aec; ?><sup>2</sup></td>
								</tr>
							</table>
							<table cellpadding="0" cellspacing="0" class="round-up-discount"<?php if ($apply_round_up != 1) { echo ' style="display:none;"'; } ?>>
								<tr>
									<td colspan="5" style="text-align:center;"><?php _e('Volume discount', 'wp2print'); ?></td>
								</tr>
								<tr>
									<?php foreach($disc_numbers as $disc_number) { ?>
										<td><strong><?php echo $disc_number; ?><?php echo $dimension_unit_aec; ?><sup>2</sup></strong>&nbsp;&nbsp;</td>
									<?php } ?>
								</tr>
								<tr>
									<?php foreach($disc_numbers as $disc_number) { ?>
										<td><input type="text" name="round_up_discounts[<?php echo $disc_number; ?>]" value="<?php echo $round_up_discounts[$disc_number]; ?>"><div>%</div></td>
									<?php } ?>
								</tr>
							</table>
						</div>
					<?php
					} else {
						echo '<p style="color:#FF0000;">'.__('No Paper Type selected.', 'wp2print').'</p>';
					}
				} else {
					echo '<p style="color:#FF0000;">'.__('You must create Price Matrix Options first.', 'wp2print').'</p>';
				}
				if ($product_type_finishing_matrix_types) { $nmb = 1; ?>
					<div style="border-top:1px solid #eee; margin-top:10px; padding-top:10px;">
						<table width="100%">
							<tr>
								<td width="60%"><strong><?php _e('Select price matrix type', 'wp2print'); ?>:</strong></td>
								<td><strong><?php _e('Proportional quantity', 'wp2print'); ?></strong></td>
							</tr>
							<?php
							foreach($product_type_finishing_matrix_types as $product_type_finishing_matrix_type) {
								$mtattributes = unserialize($product_type_finishing_matrix_type->attributes);
								$mtattr_names = print_products_price_matrix_attr_names($mtattributes);
								?>
								<tr>
									<td><?php echo $nmb; ?>. <a href="edit.php?post_type=product&page=print-products-price-matrix-values&mtype_id=<?php echo $product_type_finishing_matrix_type->mtype_id; ?>"><?php echo $mtype_names[$product_type_finishing_matrix_type->mtype]; ?> (<?php echo implode(', ', $mtattr_names); ?>)</a></td>
									<td><?php echo $num_types[$product_type_finishing_matrix_type->num_type]; ?></td>
								</tr>
							<?php $nmb++; } ?>
						</table>
					</div>
					<?php
				}
			} else if ($product_type == 'aecbwc') {
				$inc_coverage_prices = (array)get_post_meta($product_id, '_inc_coverage_prices', true);
				$apply_round_up = (int)get_post_meta($product_id, '_apply_round_up', true);
				$round_up_discounts = (array)get_post_meta($product_id, '_round_up_discounts', true);
				$price_decimals = wc_get_price_decimals();
				if ($product_type_printing_matrix_types) {
					$material_attribute = $print_products_settings['material_attribute'];
					$material_attrs = array();
					foreach($product_type_printing_matrix_types as $product_type_printing_matrix_type) {
						$mattributes = unserialize($product_type_printing_matrix_type->attributes);
						$materms = unserialize($product_type_printing_matrix_type->aterms);
						if (in_array($material_attribute, $mattributes)) {
							$material_attrs = $materms[$material_attribute];
						}
					}
					$disc_numbers = array(1, 10, 50, 100, 1000);
					if (count($material_attrs)) { ?>
						<div class="inc-coverage-box">
							<h3 class="hndle" style="padding:0 0 8px;border:none;"><?php _e('Black/White pages', 'wp2print'); ?>:</h3>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td><strong><?php _e('Paper Type', 'wp2print'); ?></strong>&nbsp;&nbsp;</td>
									<td><strong><?php _e('Price', 'wp2print'); ?>/<?php echo $dimension_unit_aec; ?><sup>2</sup></strong></td>
								</tr>
								<?php foreach($material_attrs as $material_attr) { $material_attr = (int)$material_attr; ?>
									<tr>
										<td><?php echo $terms_names[$material_attr]; ?>&nbsp;&nbsp;</td>
										<td><div style="float:left;padding:3px 3px 0 0;">$</div><input type="text" name="inc_coverage_prices[0][<?php echo $material_attr; ?>]" value="<?php echo number_format((float)$inc_coverage_prices[0][$material_attr], $price_decimals); ?>" style="width:50px;"></td>
									</tr>
								<?php } ?>
							</table>
							<h3 class="hndle" style="padding:10px 0 8px;border:none;"><?php _e('Color pages', 'wp2print'); ?>:</h3>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td><strong><?php _e('Paper Type', 'wp2print'); ?></strong>&nbsp;&nbsp;</td>
									<td><strong><?php _e('Price', 'wp2print'); ?>/<?php echo $dimension_unit_aec; ?><sup>2</sup></strong></td>
								</tr>
								<?php foreach($material_attrs as $material_attr) { $material_attr = (int)$material_attr; ?>
									<tr>
										<td><?php echo $terms_names[$material_attr]; ?>&nbsp;&nbsp;</td>
										<td><div style="float:left;padding:3px 3px 0 0;">$</div><input type="text" name="inc_coverage_prices[1][<?php echo $material_attr; ?>]" value="<?php echo number_format((float)$inc_coverage_prices[1][$material_attr], $price_decimals); ?>" style="width:50px;"></td>
									</tr>
								<?php } ?>
							</table>
						</div>
						<div class="round-up-box">
							<table>
								<tr>
									<td><input type="checkbox" name="apply_round_up" value="1" class="apply-round-up"<?php if ($apply_round_up == 1) { echo ' CHECKED'; } ?>></td>
									<td><?php _e('Round-up area to nearest', 'wp2print'); ?> <?php echo $dimension_unit_aec; ?><sup>2</sup></td>
								</tr>
							</table>
							<table cellpadding="0" cellspacing="0" class="round-up-discount"<?php if ($apply_round_up != 1) { echo ' style="display:none;"'; } ?>>
								<tr>
									<td colspan="5" style="text-align:center;"><?php _e('Volume discount', 'wp2print'); ?></td>
								</tr>
								<tr>
									<?php foreach($disc_numbers as $disc_number) { ?>
										<td><strong><?php echo $disc_number; ?><?php echo $dimension_unit_aec; ?><sup>2</sup></strong>&nbsp;&nbsp;</td>
									<?php } ?>
								</tr>
								<tr>
									<?php foreach($disc_numbers as $disc_number) { ?>
										<td><input type="text" name="round_up_discounts[<?php echo $disc_number; ?>]" value="<?php echo $round_up_discounts[$disc_number]; ?>"><div>%</div></td>
									<?php } ?>
								</tr>
							</table>
						</div>
					<?php
					} else {
						echo '<p style="color:#FF0000;">'.__('No Paper Type selected.', 'wp2print').'</p>';
					}
				} else {
					echo '<p style="color:#FF0000;">'.__('You must create Price Matrix Options first.', 'wp2print').'</p>';
				}
				if ($product_type_finishing_matrix_types) { $nmb = 1; ?>
					<div style="border-top:1px solid #eee; margin-top:10px; padding-top:10px;">
						<table width="100%">
							<tr>
								<td width="60%"><strong><?php _e('Select price matrix type', 'wp2print'); ?>:</strong></td>
								<td><strong><?php _e('Proportional quantity', 'wp2print'); ?></strong></td>
							</tr>
							<?php
							foreach($product_type_finishing_matrix_types as $product_type_finishing_matrix_type) {
								$mtattributes = unserialize($product_type_finishing_matrix_type->attributes);
								$mtattr_names = print_products_price_matrix_attr_names($mtattributes);
								?>
								<tr>
									<td><?php echo $nmb; ?>. <a href="edit.php?post_type=product&page=print-products-price-matrix-values&mtype_id=<?php echo $product_type_finishing_matrix_type->mtype_id; ?>"><?php echo $mtype_names[$product_type_finishing_matrix_type->mtype]; ?> (<?php echo implode(', ', $mtattr_names); ?>)</a></td>
									<td><?php echo $num_types[$product_type_finishing_matrix_type->num_type]; ?></td>
								</tr>
							<?php $nmb++; } ?>
						</table>
					</div>
					<?php
				}
			} else if ($product_type == 'aecsimple') {
				$inc_coverage_prices = (array)get_post_meta($product_id, '_inc_coverage_prices', true);
				$apply_round_up = (int)get_post_meta($product_id, '_apply_round_up', true);
				$round_up_discounts = (array)get_post_meta($product_id, '_round_up_discounts', true);
				$price_decimals = wc_get_price_decimals();
				if ($product_type_printing_matrix_types) {
					$material_attribute = $print_products_settings['material_attribute'];
					$material_attrs = array();
					foreach($product_type_printing_matrix_types as $product_type_printing_matrix_type) {
						$mattributes = unserialize($product_type_printing_matrix_type->attributes);
						$materms = unserialize($product_type_printing_matrix_type->aterms);
						if (in_array($material_attribute, $mattributes)) {
							$material_attrs = $materms[$material_attribute];
						}
					}
					$disc_numbers = array(1, 10, 50, 100, 1000);
					if (count($material_attrs)) { ?>
						<div class="inc-coverage-box">
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td><strong><?php _e('Paper Type', 'wp2print'); ?></strong>&nbsp;&nbsp;</td>
									<td><strong><?php _e('Price', 'wp2print'); ?>/<?php echo $dimension_unit_aec; ?><sup>2</sup></strong></td>
								</tr>
								<?php foreach($material_attrs as $material_attr) { $material_attr = (int)$material_attr; ?>
									<tr>
										<td><?php echo $terms_names[$material_attr]; ?>&nbsp;&nbsp;</td>
										<td><div style="float:left;padding:3px 3px 0 0;">$</div><input type="text" name="inc_coverage_prices[<?php echo $material_attr; ?>]" value="<?php echo number_format((float)$inc_coverage_prices[$material_attr], $price_decimals); ?>" style="width:50px;"></td>
									</tr>
								<?php } ?>
							</table>
						</div>
						<div class="round-up-box">
							<table>
								<tr>
									<td><input type="checkbox" name="apply_round_up" value="1" class="apply-round-up"<?php if ($apply_round_up == 1) { echo ' CHECKED'; } ?>></td>
									<td><?php _e('Round-up area to nearest', 'wp2print'); ?> <?php echo $dimension_unit_aec; ?><sup>2</sup></td>
								</tr>
							</table>
							<table cellpadding="0" cellspacing="0" class="round-up-discount"<?php if ($apply_round_up != 1) { echo ' style="display:none;"'; } ?>>
								<tr>
									<td colspan="5" style="text-align:center;"><?php _e('Volume discount', 'wp2print'); ?></td>
								</tr>
								<tr>
									<?php foreach($disc_numbers as $disc_number) { ?>
										<td><strong><?php echo $disc_number; ?><?php echo $dimension_unit_aec; ?><sup>2</sup></strong>&nbsp;&nbsp;</td>
									<?php } ?>
								</tr>
								<tr>
									<?php foreach($disc_numbers as $disc_number) { ?>
										<td><input type="text" name="round_up_discounts[<?php echo $disc_number; ?>]" value="<?php echo $round_up_discounts[$disc_number]; ?>"><div>%</div></td>
									<?php } ?>
								</tr>
							</table>
						</div>
					<?php
					} else {
						echo '<p style="color:#FF0000;">'.__('No Paper Type selected.', 'wp2print').'</p>';
					}
				} else {
					echo '<p style="color:#FF0000;">'.__('You must create Price Matrix Options first.', 'wp2print').'</p>';
				}
				if ($product_type_finishing_matrix_types) { $nmb = 1; ?>
					<div style="border-top:1px solid #eee; margin-top:10px; padding-top:10px;">
						<table width="100%">
							<tr>
								<td width="60%"><strong><?php _e('Select price matrix type', 'wp2print'); ?>:</strong></td>
								<td><strong><?php _e('Proportional quantity', 'wp2print'); ?></strong></td>
							</tr>
							<?php
							foreach($product_type_finishing_matrix_types as $product_type_finishing_matrix_type) {
								$mtattributes = unserialize($product_type_finishing_matrix_type->attributes);
								$mtattr_names = print_products_price_matrix_attr_names($mtattributes);
								?>
								<tr>
									<td><?php echo $nmb; ?>. <a href="edit.php?post_type=product&page=print-products-price-matrix-values&mtype_id=<?php echo $product_type_finishing_matrix_type->mtype_id; ?>"><?php echo $mtype_names[$product_type_finishing_matrix_type->mtype]; ?> (<?php echo implode(', ', $mtattr_names); ?>)</a></td>
									<td><?php echo $num_types[$product_type_finishing_matrix_type->num_type]; ?></td>
								</tr>
							<?php $nmb++; } ?>
						</table>
					</div>
					<?php
				}
			} else {
				if ($product_type_matrix_types) { $nmb = 1; ?>
					<table width="100%">
						<tr>
							<td width="60%"><strong><?php _e('Select price matrix type', 'wp2print'); ?>:</strong></td>
							<td><strong><?php _e('Proportional quantity', 'wp2print'); ?></strong></td>
						</tr>
						<?php
						foreach($product_type_matrix_types as $product_type_matrix_type) {
							$mtattributes = unserialize($product_type_matrix_type->attributes);
							$mtattr_names = print_products_price_matrix_attr_names($mtattributes);
							?>
							<tr>
								<td><?php echo $nmb; ?>. <a href="edit.php?post_type=product&page=print-products-price-matrix-values&mtype_id=<?php echo $product_type_matrix_type->mtype_id; ?>"><?php echo $mtype_names[$product_type_matrix_type->mtype]; ?> (<?php echo implode(', ', $mtattr_names); ?>)</a></td>
								<td><?php echo $num_types[$product_type_matrix_type->num_type]; ?></td>
							</tr>
						<?php $nmb++; } ?>
					</table>
				<?php } else {
					echo '<p style="color:#FF0000;">'.__('You must create Price Matrix Options first.', 'wp2print').'</p>';
				}
			}
			$product_display_price = get_post_meta($product_id, '_product_display_price', true);
			$dpvals = array(
				'excl' => __('Exclude tax price', 'wp2print'),
				'incl' => __('Include tax price', 'wp2print'),
				'both' => __('Include tax and Exclude tax prices', 'wp2print')
			);
			?>
			<div style="border-top:1px solid #eee; margin-top:10px; padding-top:15px;">
				<table style="width:auto;">
					<tr>
						<td><?php _e('Minimum price', 'wp2print'); ?>:</td>
						<td><input type="text" name="order_min_price" value="<?php echo $order_min_price; ?>" style="width:100px;"></td>
					</tr>
					<tr>
						<td><?php _e('Maximum price', 'wp2print'); ?>:</td>
						<td><input type="text" name="order_max_price" value="<?php echo $order_max_price; ?>" style="width:100px;"></td>
					</tr>
				</table>
			</div>
			<div style="margin-top:5px; padding-top:10px;">
				<?php
				if (!$production_speed_options) {
					$production_speed_options = array(
						array('label' => __('Slow', 'wp2print'), 'percent' => 0),
						array('label' => __('Fast', 'wp2print'), 'percent' => 20),
						array('label' => __('Very fast', 'wp2print'), 'percent' => 50)
					);
				}
				if (!strlen($production_speed_label)) { $production_speed_label = __('Production speed', 'wp2print'); }
				$production_speed_sd_data = get_post_meta($product_id, '_production_speed_sd_data', true);
				if (!$production_speed_sd_data) {
					$production_speed_sd_data = array(
						'show' => 0,
						'label' => __('Shipment date', 'wp2print'),
						'weekend' => 1,
						'time' => '13:00'
					);
				}
			?>
				<div class="pso-box">
					<table>
						<tr>
							<td><input type="checkbox" name="use_production_speed" value="1" onclick="wp2print_pso_check(this)" style="margin-top:-3px;"<?php if ($use_production_speed) { echo ' CHECKED'; } ?>>&nbsp;<?php _e("Apply 'Production speed' multiplier", 'wp2print'); ?></td>
						</tr>
					</table>
					<div class="pso-list"<?php if (!$use_production_speed) { echo ' style="display:none;"'; } ?>>
						<table class="pso-label-table">
							<tr>
								<td><?php _e('Dropdown label', 'wp2print'); ?>:</td>
								<td><input type="text" name="production_speed_label" value="<?php echo $production_speed_label; ?>"></td>
							</tr>
						</table>
						<table cellspacing="0" cellpadding="0" class="pso-list-table">
							<tr class="tr-h">
								<td colspan="5"><?php _e('Production speed options list', 'wp2print'); ?></td>
							</tr>
							<tr class="tr-h">
								<td><?php _e('N', 'wp2print'); ?></td>
								<td><?php _e('Label', 'wp2print'); ?></td>
								<td><?php _e('Percent', 'wp2print'); ?>, %</td>
								<td><?php _e('Days', 'wp2print'); ?></td>
								<td><?php _e('Remove', 'wp2print'); ?></td>
							</tr>
							<?php foreach($production_speed_options as $psokey => $pso_option) { ?>
								<tr class="pso-tr pso-tr<?php echo $psokey; ?>">
									<td><?php echo $psokey + 1; ?></td>
									<td><input type="text" name="production_speed_options[<?php echo $psokey; ?>][label]" value="<?php echo $pso_option['label']; ?>"></td>
									<td><input type="text" name="production_speed_options[<?php echo $psokey; ?>][percent]" value="<?php echo $pso_option['percent']; ?>"></td>
									<td><input type="text" name="production_speed_options[<?php echo $psokey; ?>][days]" value="<?php echo (int)$pso_option['days']; ?>"></td>
									<td><a href="#remove" class="pso-rem" onclick="wp2print_pso_remove(<?php echo $psokey; ?>); return false;">X</a></td>
								</tr>
							<?php } ?>
						</table>
						<a href="#add" onclick="wp2print_pso_add(); return false;"><?php _e('Add new row', 'wp2print'); ?></a>
						<div class="pso-data" style="display:none;" data-rmessage="<?php _e('Are you sure?', 'wp2print'); ?>" data-num="<?php echo $psokey + 1; ?>">
							<table>
								<tr class="pso-tr pso-tr{N}">
									<td>0</td>
									<td><input type="text" name="production_speed_options[{N}][label]"></td>
									<td><input type="text" name="production_speed_options[{N}][percent]"></td>
									<td><input type="text" name="production_speed_options[{N}][days]"></td>
									<td><a href="#remove" class="pso-rem" onclick="wp2print_pso_remove({N}); return false;">X</a></td>
								</tr>
							</table>
						</div>
						<table class="pso-sd-ch">
							<tr>
								<td><input type="checkbox" name="production_speed_sd_data[show]" value="1" onclick="wp2print_pso_sd_check(this)" style="margin-top:-3px;"<?php if ($production_speed_sd_data['show']) { echo ' CHECKED'; } ?>>&nbsp;<?php _e("Display Shipment date", 'wp2print'); ?></td>
							</tr>
						</table>
						<div class="pso-sd-data"<?php if (!$production_speed_sd_data['show']) { echo ' style="display:none;"'; } ?>>
							<table>
								<tr>
									<td><?php _e('Text label for shipment date', 'wp2print'); ?>:&nbsp;</td>
									<td><input type="text" name="production_speed_sd_data[label]" value="<?php echo $production_speed_sd_data['label']; ?>"></td>
								</tr>
								<tr>
									<td><?php _e('Exclude weekend days', 'wp2print'); ?>:</td>
									<td><input type="checkbox" name="production_speed_sd_data[weekend]" value="1"<?php if ($production_speed_sd_data['weekend']) { echo ' CHECKED'; } ?>></td>
								</tr>
								<tr>
									<td><?php _e('Order cutoff time', 'wp2print'); ?>:</td>
									<td><select name="production_speed_sd_data[time]">
										<?php for ($h=0; $h<=23; $h++) { ?>
											<?php for ($m=0; $m<60; $m=$m+30) {
												$hm = sprintf("%02d", $h).':'.sprintf("%02d", $m); ?>
												<option value="<?php echo $hm; ?>"<?php if ($production_speed_sd_data['time'] == $hm) { echo ' SELECTED'; } ?>><?php echo $hm; ?></option>
											<?php } ?>
										<?php } ?>
									</select></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<table>
					<tr>
						<td><strong><?php _e('Display Price', 'wp2print'); ?>:&nbsp;&nbsp;</strong></td>
						<td>
							<select name="product_display_price" class="select short">
								<?php foreach($dpvals as $dpkey => $dpval) { $s = ''; if ($product_display_price == $dpkey) { $s = ' SELECTED'; } ?>
									<option value="<?php echo $dpkey; ?>"<?php echo $s; ?>><?php echo $dpval; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
				</table>
				<?php $nl_hide_prices = get_post_meta($product_id, '_nl_hide_prices', true); ?>
				<div style="border-top:1px solid #EEE; margin-top:15px; padding-top:10px;"><input type="checkbox" name="nl_hide_prices" value="1"<?php if ($nl_hide_prices == 1) { echo ' CHECKED'; } ?>> <?php _e('Hide prices for users that are not logged-in', 'wp2print'); ?></div>
			</div>
		<?php } ?>
    </div>
    <div id="unitpricetable" class="panel woocommerce_options_panel" style="padding:15px;">
		<?php
		if (!is_array($unitpricetable)) {
			$unitpricetable = array(
				'show' => 0,
				'quantities' => '',
				'attribute' => '',
				'aoptions' => array(),
				'unitprices' => 0,
				'utext' => ''
			);
		}
		if (isset($unitpricetable['aoptions']) && !is_array($unitpricetable['aoptions'])) { $unitpricetable['aoptions'] = array(); }
		$ppattributes = array();
		if ($product_type_printing_matrix_types) {
			foreach($product_type_printing_matrix_types as $product_type_printing_matrix_type) {
				$mattributes = unserialize($product_type_printing_matrix_type->attributes);
				$materms = unserialize($product_type_printing_matrix_type->aterms);
				if ($mattributes) {
					foreach($mattributes as $attr_id) {
						$aterms = array();
						if ($materms && isset($materms[$attr_id])) {
							foreach($materms[$attr_id] as $term_id) {
								$aterms[$term_id] = $term_id;
							}
						}
						$ppattributes[$attr_id] = array('name' => $attribute_names[$attr_id], 'aterms' => $aterms);
					}
				}
			}
		} ?>
		<?php if (!$product_type_printing_matrix_types) { ?>
			<p style="color:#FF0000;"><?php _e('Please add printing attributes firstly.', 'wp2print'); ?></p>
		<?php } else { ?>
			<table class="unit-price-table">
				<tr>
					<td><strong><?php _e('Display Unit price', 'wp2print'); ?>:&nbsp;&nbsp;</strong></td>
					<td><input type="checkbox" name="unitpricetable[show_price]" value="1"<?php if (isset($unitpricetable['show_price']) && $unitpricetable['show_price'] == 1) { echo ' CHECKED'; } ?>></td>
				</tr>
				<tr>
					<td><strong><?php _e('Show unit price table', 'wp2print'); ?>:&nbsp;&nbsp;</strong></td>
					<td><input type="checkbox" name="unitpricetable[show]" value="1"<?php if (isset($unitpricetable['show']) && $unitpricetable['show'] == 1) { echo ' CHECKED'; } ?>></td>
				</tr>
				<tr>
					<td><strong><?php _e('Quantity values in columns', 'wp2print'); ?>:&nbsp;&nbsp;</strong></td>
					<td><input type="text" name="unitpricetable[quantities]" value="<?php if (isset($unitpricetable['quantities'])) { echo $unitpricetable['quantities']; } ?>" style="width:350px;"></td>
				</tr>
				<tr>
					<td><strong><?php _e('Row attribute', 'wp2print'); ?>:&nbsp;&nbsp;</strong></td>
					<td><select name="unitpricetable[attribute]" class="upt-attribute" onchange="upt_attribute_change()">
						<option value="">-- <?php _e('Select attribute', 'wp2print'); ?> --</option>
						<?php foreach($ppattributes as $aid => $adata) { ?>
							<option value="<?php echo $aid; ?>"<?php if (isset($unitpricetable['attribute']) && $aid == $unitpricetable['attribute']) { echo ' SELECTED'; } ?>><?php echo $adata['name']; ?></option>
						<?php } ?>
						<?php if ($product_type == 'sticker') { ?>
							<option value="arange"<?php if (isset($unitpricetable['attribute']) && $unitpricetable['attribute'] == 'arange') { echo ' SELECTED'; } ?>><?php _e('Area range', 'wp2print'); ?></option>
						<?php } ?>
					</select></td>
				</tr>
				<tr>
					<td><strong><?php _e('Row attribute items', 'wp2print'); ?>:&nbsp;&nbsp;</strong></td>
					<td>
						<div class="upt-aoptions upt-aoptions-"<?php if ($unitpricetable['attribute']) { echo ' style="display:none;"'; } ?>><?php _e('Select attribute', 'wp2print'); ?></div>
						<?php foreach($ppattributes as $aid => $adata) { ?>
						<div class="upt-aoptions upt-aoptions-<?php echo $aid; ?>"<?php if ($aid != $unitpricetable['attribute']) { echo ' style="display:none;"'; } ?>>
							<?php foreach($adata['aterms'] as $aterm) { ?>
								<input type="checkbox" name="unitpricetable[aoptions][]" value="<?php echo $aterm; ?>"<?php if (isset($unitpricetable['aoptions']) && is_array($unitpricetable['aoptions']) && in_array($aterm, $unitpricetable['aoptions'])) { echo ' CHECKED'; } ?>> <?php echo $terms_names[$aterm]; ?><br>
							<?php } ?>
						</div>
						<?php } ?>
						<?php if ($product_type == 'sticker') {
							$area_ranges = get_post_meta($product_id, '_area_ranges', true);
							if ($area_ranges && is_array($area_ranges) && count($area_ranges)) { ?>
								<div class="upt-aoptions upt-aoptions-arange"<?php if ($unitpricetable['attribute'] != 'arange') { echo ' style="display:none;"'; } ?>>
									<?php foreach ($area_ranges as $arnum => $ardata) { $arkey = $ardata['min'].':'.$ardata['max']; ?>
										<input type="checkbox" name="unitpricetable[aoptions][]" value="<?php echo $arkey; ?>"<?php if (isset($unitpricetable['aoptions']) && is_array($unitpricetable['aoptions']) && in_array($arkey, $unitpricetable['aoptions'])) { echo ' CHECKED'; } ?>> <?php echo $ardata['min'].'-'.$ardata['max']; ?><br>
									<?php } ?>
								</div>
							<?php } ?>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td><strong><?php _e('Display unit prices', 'wp2print'); ?>:&nbsp;&nbsp;</strong></td>
					<td><input type="checkbox" name="unitpricetable[unitprices]" value="1"<?php if (isset($unitpricetable['unitprices']) && $unitpricetable['unitprices'] == 1) { echo ' CHECKED'; } ?>></td>
				</tr>
				<tr>
					<td><strong><?php _e('Unit text', 'wp2print'); ?>:&nbsp;&nbsp;</strong></td>
					<td><input type="text" name="unitpricetable[utext]" value="<?php echo $unitpricetable['utext']; ?>" style="width:150px;"></td>
				</tr>
			</table>
			<script>
			function upt_attribute_change() {
				var attribute = jQuery('.unit-price-table .upt-attribute').val();
				jQuery('.unit-price-table .upt-aoptions').hide();
				jQuery('.unit-price-table .upt-aoptions input').removeAttr('checked');
				jQuery('.unit-price-table .upt-aoptions-'+attribute).fadeIn();
			}
			</script>
		<?php } ?>
    </div>
    <div id="add-on-products" class="panel woocommerce_options_panel" style="padding:15px;">
		<h2 style="font-weight:700; padding:0 0 5px;"><?php _e('Select Add-on products', 'wp2print'); ?>:</h2>
		<?php
		$wc_products = wc_get_products(array('type' => 'simple'));
		$addon_products = get_post_meta($product_id, '_addon_products', true);
		if (!is_array($addon_products)) { $addon_products = array(); }
		if ($wc_products) { ?>
			<table style="width:auto;">
				<?php foreach($wc_products as $wc_product) {
					$pid = $wc_product->get_id(); ?>
					<tr>
						<td><input type="checkbox" name="addon_products[]" value="<?php echo $pid; ?>"<?php if (in_array($pid, $addon_products)) { echo ' CHECKED'; } ?>></td>
						<td><?php echo $wc_product->get_name(); ?></td>
					</tr>
				<?php } ?>
			</table>
		<?php } ?>
    </div>
    <div id="paybill-options" class="panel woocommerce_options_panel" style="padding:15px;">
		<?php
		$paybill_options = get_post_meta($product_id, '_paybill_options', true);
		if (!is_array($paybill_options)) { $paybill_options = array('invoice-number-label' => __('Invoice number', 'wp2print'), 'amount-label' => __('Amount', 'wp2print')); }
		?>
		<p class="form-field">
			<label><?php _e('Invoice number label', 'wp2print'); ?></label>
			<input type="text" name="paybill_options[invoice-number-label]" value="<?php echo $paybill_options['invoice-number-label']; ?>">
		</p>
		<p class="form-field">
			<label><?php _e('Amount label', 'wp2print'); ?></label>
			<input type="text" name="paybill_options[amount-label]" value="<?php echo $paybill_options['amount-label']; ?>">
		</p>
    </div>
    <div id="quote-options" class="panel woocommerce_options_panel" style="padding:15px;">
		<?php
		$quote_options = get_post_meta($product_id, '_quote_options', true);
		if (!is_array($quote_options)) { $quote_options = array('button-text' => __('Request quote', 'wp2print'), 'button-link' => ''); }
		?>
		<p class="form-field">
			<label style="width:170px;"><?php _e('Text for call to action button', 'wp2print'); ?>: </label>
			<input type="text" name="quote_options[button-text]" value="<?php echo $quote_options['button-text']; ?>" class="short">
		</p>
		<p class="form-field">
			<label style="width:170px;"><?php _e('URL for request quote page', 'wp2print'); ?>: </label>
			<input type="text" name="quote_options[button-link]" value="<?php echo $quote_options['button-link']; ?>" class="short">
		</p>
    </div>
	<?php if ($product_type == 'eddm') { ?>
		<div id="eddm_shipping" class="panel woocommerce_options_panel" style="padding:15px;">
			<?php
			$size_attribute = $print_products_settings['size_attribute'];
			$material_attribute = $print_products_settings['material_attribute'];
			$page_count_attribute = $print_products_settings['page_count_attribute'];

			$weight_unit = print_products_get_weight_unit();
			$square_unit = print_products_get_square_unit();
			$product_shipping_weights = unserialize(get_post_meta($product_id, '_product_shipping_weights', true));
			$product_shipping_base_quantity = get_post_meta($product_id, '_product_shipping_base_quantity', true);
			$product_display_weight = get_post_meta($product_id, '_product_display_weight', true);
			$product_max_weight_per_box = get_post_meta($product_id, '_product_max_weight_per_box', true);

			if (!$product_shipping_base_quantity) { $product_shipping_base_quantity = 1; }

			$product_matrix_options = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s AND mtype = 0 ORDER BY sorder", $wpdb->prefix, $product_id));
			if ($product_matrix_options) {
				foreach($product_matrix_options as $pmokey => $product_matrix_option) {
					$title = $product_matrix_option->title;
					$aterms = unserialize($product_matrix_option->aterms);
					$tsizes = $aterms[$size_attribute];
					$tmaterials = $aterms[$material_attribute];
					$tpagecounts = $aterms[$page_count_attribute];
					if (!is_array($tsizes)) { $tsizes = array(); }
					if (!is_array($tpagecounts)) { $tpagecounts = array(); }
					if ($tmaterials) {
						$terms_names = array();
						$attr_terms = $wpdb->get_results(sprintf("SELECT * FROM %sterms WHERE term_id IN (%s)", $wpdb->prefix, implode(",", array_merge($tmaterials, $tsizes, $tpagecounts))));
						if ($attr_terms) {
							foreach($attr_terms as $attr_term) {
								$terms_names[$attr_term->term_id] = $attr_term->name;
							}
						}
						?>
						<div class="options_group">
							<?php if ($tsizes) { ?>
								<?php if ($tpagecounts) { ?>
									<div style="width:100%; overflow:auto; margin-bottom:10px;">
										<table>
											<tr>
												<td><strong><?php _e('Material', 'wp2print'); ?>&nbsp;&nbsp;</strong></td>
												<td><strong><?php _e('Size', 'wp2print'); ?>&nbsp;&nbsp;</strong></td>
												<?php foreach($tpagecounts as $tpagecount) { ?>
													<td><strong><?php echo $terms_names[$tpagecount]; ?></strong></td>
												<?php } ?>
												<td>&nbsp;</td>
											</tr>
											<?php foreach($tmaterials as $tmaterial) { ?>
												<?php foreach($tsizes as $tsize) { ?>
													<tr>
														<td nowrap><?php echo $terms_names[$tmaterial]; ?>&nbsp;&nbsp;</td>
														<td nowrap><?php echo $terms_names[$tsize]; ?>&nbsp;&nbsp;</td>
														<?php foreach($tpagecounts as $tpagecount) { ?>
															<td><input type="text" name="product_shipping_weights[<?php echo $tmaterial; ?>][<?php echo $tsize; ?>][<?php echo $tpagecount; ?>]" value="<?php echo $product_shipping_weights[$tmaterial][$tsize][$tpagecount]; ?>" style="width:70px;"></td>
														<?php } ?>
														<td><?php _e('oz.', 'wp2print'); ?></td>
													</tr>
												<?php } ?>
											<?php } ?>
										</table>
									</div>
								<?php } else { ?>
									<table>
										<tr>
											<td style="width:145px;"><strong><?php _e('Material', 'wp2print'); ?></strong></td>
											<?php foreach($tsizes as $tsize) { ?>
												<td align="center"><strong><?php echo $terms_names[$tsize]; ?></strong></td>
											<?php } ?>
											<td>&nbsp;</td>
										</tr>
										<?php foreach($tmaterials as $tmaterial) { ?>
											<tr>
												<td><?php echo $terms_names[$tmaterial]; ?>&nbsp;&nbsp;</td>
												<?php foreach($tsizes as $tsize) { ?>
													<td><input type="text" name="product_shipping_weights[<?php echo $tmaterial; ?>][<?php echo $tsize; ?>]" value="<?php echo $product_shipping_weights[$tmaterial][$tsize]; ?>" style="width:90px;"></td>
												<?php } ?>
												<td><?php _e('oz.', 'wp2print'); ?></td>
											</tr>
										<?php } ?>
									</table>
								<?php } ?>
								<table>
									<tr>
										<td style="width:145px;"><strong><?php _e('Base Quantity', 'wp2print'); ?></strong></td>
										<td><input type="text" name="product_shipping_base_quantity" value="1" style="width:90px;" readonly></td>
									</tr>
								</table>
							<?php } else { ?>
								<table>
									<tr>
										<td style="width:145px;"><strong><?php _e('Material', 'wp2print'); ?></strong></td>
										<td><strong><?php _e('Weight', 'wp2print'); ?></strong></td>
										<td></td>
									</tr>
									<?php foreach($tmaterials as $tmaterial) { ?>
										<tr>
											<td><?php echo $terms_names[$tmaterial]; ?>&nbsp;&nbsp;</td>
											<td><input type="text" name="product_shipping_weights[<?php echo $tmaterial; ?>]" value="<?php echo $product_shipping_weights[$tmaterial]; ?>" style="width:100px;"></td>
											<td>(<?php echo $weight_unit; ?>/<?php echo $square_unit; ?>)</td>
										</tr>
									<?php } ?>
								</table>
							<?php } ?>
						</div>
						<?php
					}
				}
			}
			?>
		</div>
	<?php } ?>
    <?php
}

// product shipping (material base)
add_action('woocommerce_product_options_dimensions', 'print_products_product_shipping_options');
function print_products_product_shipping_options() {
	global $post, $wpdb, $print_products_settings;
	$product_id = $post->ID;
	$product_type = print_products_get_type($product_id);
	$size_attribute = $print_products_settings['size_attribute'];
	$material_attribute = $print_products_settings['material_attribute'];
	$page_count_attribute = $print_products_settings['page_count_attribute'];

	if (print_products_is_wp2print_type($product_type) && $product_type != 'eddm') {
		$weight_unit = print_products_get_weight_unit();
		$square_unit = print_products_get_square_unit();
		$aunits = print_products_get_area_units();
		$product_shipping_weights = unserialize(get_post_meta($product_id, '_product_shipping_weights', true));
		$product_shipping_base_quantity = get_post_meta($product_id, '_product_shipping_base_quantity', true);
		$product_display_weight = get_post_meta($product_id, '_product_display_weight', true);
		$product_max_weight_per_box = get_post_meta($product_id, '_product_max_weight_per_box', true);

		$area_unit = (int)get_post_meta($product_id, '_area_unit', true);
		if ($area_unit == 0) { $square_unit = $aunits[0]; }

		if ($product_type == 'book') { $product_shipping_base_quantity = unserialize($product_shipping_base_quantity); }

		$product_matrix_options = $wpdb->get_results(sprintf("SELECT * FROM %sprint_products_matrix_types WHERE product_id = %s AND mtype = 0 ORDER BY sorder", $wpdb->prefix, $product_id));
		if ($product_matrix_options) {
			foreach($product_matrix_options as $pmokey => $product_matrix_option) {
				$title = $product_matrix_option->title;
				$aterms = unserialize($product_matrix_option->aterms);
				$tsizes = (isset($aterms[$size_attribute]) ? $aterms[$size_attribute] : array());
				$tmaterials = (isset($aterms[$material_attribute]) ? $aterms[$material_attribute] : array());
				$tpagecounts = (isset($aterms[$page_count_attribute]) ? $aterms[$page_count_attribute] : array());
				if (!is_array($tsizes)) { $tsizes = array(); }
				if (!is_array($tpagecounts)) { $tpagecounts = array(); }
				if ($tmaterials) {
					$terms_names = array();
					$attr_terms = $wpdb->get_results(sprintf("SELECT * FROM %sterms WHERE term_id IN (%s)", $wpdb->prefix, implode(",", array_merge($tmaterials, $tsizes, $tpagecounts))));
					if ($attr_terms) {
						foreach($attr_terms as $attr_term) {
							$terms_names[$attr_term->term_id] = $attr_term->name;
						}
					}
					?>
					<div class="options_group" style="padding:10px;">
						<?php if ($product_type == 'area' || $product_type == 'sticker' || $product_type == 'box') { ?>
							<table>
								<tr>
									<td style="width:145px;"><strong><?php _e('Material', 'wp2print'); ?></strong></td>
									<td><strong><?php _e('Weight', 'wp2print'); ?></strong></td>
									<td></td>
								</tr>
								<?php foreach($tmaterials as $tmaterial) { ?>
									<tr>
										<td><?php echo $terms_names[$tmaterial]; ?>&nbsp;&nbsp;</td>
										<td><input type="text" name="product_shipping_weights[<?php echo $tmaterial; ?>]" value="<?php if (isset($product_shipping_weights[$tmaterial])) { echo $product_shipping_weights[$tmaterial]; } ?>" style="width:100px;"></td>
										<td>(<?php echo $weight_unit; ?>/<?php echo $square_unit; ?>)</td>
									</tr>
								<?php } ?>
							</table>
						<?php } else if ($tsizes) { ?>
							<?php if ($product_type == 'book') {
								if (strlen($title)) { $title = '('.$title.') '; } ?>
								<table>
									<tr>
										<td style="width:145px;"><strong><?php echo $title; ?><?php _e('Material', 'wp2print'); ?></strong></td>
										<?php foreach($tsizes as $tsize) { ?>
											<td align="center"><strong><?php echo $terms_names[$tsize]; ?></strong></td>
										<?php } ?>
									</tr>
									<?php foreach($tmaterials as $tmaterial) { ?>
										<tr>
											<td><?php echo $terms_names[$tmaterial]; ?>&nbsp;&nbsp;</td>
											<?php foreach($tsizes as $tsize) { ?>
												<td><input type="text" name="product_shipping_weights[<?php echo $pmokey; ?>][<?php echo $tmaterial; ?>][<?php echo $tsize; ?>]" value="<?php if ($product_shipping_weights && isset($product_shipping_weights[$pmokey]) && isset($product_shipping_weights[$pmokey][$tmaterial]) && isset($product_shipping_weights[$pmokey][$tmaterial][$tsize])) { echo $product_shipping_weights[$pmokey][$tmaterial][$tsize]; } ?>" style="width:90px;"></td>
											<?php } ?>
										</tr>
									<?php } ?>
								</table>
								<table>
									<tr>
										<td style="width:145px;"><strong><?php echo $title; ?><?php _e('Base Quantity', 'wp2print'); ?></strong></td>
										<td><input type="text" name="product_shipping_base_quantity[<?php echo $pmokey; ?>]" value="<?php if (isset($product_shipping_base_quantity[$pmokey])) { echo $product_shipping_base_quantity[$pmokey]; } ?>" style="width:90px;"></td>
									</tr>
								</table>
							<?php } else { ?>
								<?php if ($tpagecounts) { ?>
									<div style="width:100%; overflow:auto; margin-bottom:10px;">
									<table>
										<tr>
											<td><strong><?php _e('Material', 'wp2print'); ?>&nbsp;&nbsp;</strong></td>
											<td><strong><?php _e('Size', 'wp2print'); ?>&nbsp;&nbsp;</strong></td>
											<?php foreach($tpagecounts as $tpagecount) { ?>
												<td><strong><?php echo $terms_names[$tpagecount]; ?></strong></td>
											<?php } ?>
										</tr>
										<?php foreach($tmaterials as $tmaterial) { ?>
											<?php foreach($tsizes as $tsize) { ?>
												<tr>
													<td nowrap><?php echo $terms_names[$tmaterial]; ?>&nbsp;&nbsp;</td>
													<td nowrap><?php echo $terms_names[$tsize]; ?>&nbsp;&nbsp;</td>
													<?php foreach($tpagecounts as $tpagecount) { ?>
														<td><input type="text" name="product_shipping_weights[<?php echo $tmaterial; ?>][<?php echo $tsize; ?>][<?php echo $tpagecount; ?>]" value="<?php if ($product_shipping_weights && isset($product_shipping_weights[$tmaterial]) && isset($product_shipping_weights[$tmaterial][$tsize]) && isset($product_shipping_weights[$tmaterial][$tsize][$tpagecount])) { echo $product_shipping_weights[$tmaterial][$tsize][$tpagecount]; } ?>" style="width:70px;"></td>
													<?php } ?>
												</tr>
											<?php } ?>
										<?php } ?>
									</table>
									</div>
								<?php } else { ?>
									<table>
										<tr>
											<td style="width:145px;"><strong><?php _e('Material', 'wp2print'); ?></strong></td>
											<?php foreach($tsizes as $tsize) { ?>
												<td align="center"><strong><?php echo $terms_names[$tsize]; ?></strong></td>
											<?php } ?>
										</tr>
										<?php foreach($tmaterials as $tmaterial) { ?>
											<tr>
												<td><?php echo $terms_names[$tmaterial]; ?>&nbsp;&nbsp;</td>
												<?php foreach($tsizes as $tsize) { ?>
													<td><input type="text" name="product_shipping_weights[<?php echo $tmaterial; ?>][<?php echo $tsize; ?>]" value="<?php if ($product_shipping_weights && isset($product_shipping_weights[$tmaterial]) && isset($product_shipping_weights[$tmaterial][$tsize])) { echo $product_shipping_weights[$tmaterial][$tsize]; } ?>" style="width:90px;"></td>
												<?php } ?>
											</tr>
										<?php } ?>
									</table>
								<?php } ?>
								<table>
									<tr>
										<td style="width:145px;"><strong><?php _e('Base Quantity', 'wp2print'); ?></strong></td>
										<td><input type="text" name="product_shipping_base_quantity" value="<?php echo $product_shipping_base_quantity; ?>" style="width:90px;"></td>
									</tr>
								</table>
							<?php } ?>
						<?php } else { ?>
							<table>
								<tr>
									<td style="width:145px;"><strong><?php _e('Material', 'wp2print'); ?></strong></td>
									<td><strong><?php _e('Weight', 'wp2print'); ?></strong></td>
									<td></td>
								</tr>
								<?php foreach($tmaterials as $tmaterial) { ?>
									<tr>
										<td><?php echo $terms_names[$tmaterial]; ?>&nbsp;&nbsp;</td>
										<td><input type="text" name="product_shipping_weights[<?php echo $tmaterial; ?>]" value="<?php if (isset($product_shipping_weights[$tmaterial])) { echo $product_shipping_weights[$tmaterial]; } ?>" style="width:100px;"></td>
										<td>(<?php echo $weight_unit; ?>/<?php echo $square_unit; ?>)</td>
									</tr>
								<?php } ?>
							</table>
						<?php } ?>
						<table>
							<tr>
								<td style="width:145px;"><strong><?php _e('Max. weight per box', 'wp2print'); ?></strong></td>
								<td><input type="text" name="product_max_weight_per_box" value="<?php echo $product_max_weight_per_box; ?>" style="width:90px;"></td>
							</tr>
						</table>
						<table>
							<tr>
								<td style="width:145px;"><strong><?php _e('Display Weight', 'wp2print'); ?></strong></td>
								<td><input type="checkbox" name="product_display_weight" value="1"<?php if ($product_display_weight) { echo ' CHECKED'; } ?>></td>
							</tr>
						</table>
					</div>
					<?php
				}
			}
		}
	}
}

add_action('woocommerce_product_options_sku', 'print_products_product_options_sku');
function print_products_product_options_sku() {
	global $post, $thepostid, $wpdb, $print_products_settings, $terms_names;
	$product_type = print_products_get_type($thepostid);
	if (print_products_is_wp2print_type($product_type)) {
		$printing_mtype_id = $wpdb->get_var(sprintf("SELECT mtype_id FROM %sprint_products_matrix_types WHERE product_id = %s AND mtype = 0 ORDER BY sorder ASC LIMIT 0, 1", $wpdb->prefix, $thepostid));
		if ($printing_mtype_id) {
			?>
			<div style="border-bottom:1px solid #eee;">
				<p class="form-field _sku_field">
					<label><?php _e('SKU Matrix', 'wp2print'); ?></label>
					<a href="edit.php?post_type=product&page=print-products-price-matrix-sku&mtype_id=<?php echo $printing_mtype_id; ?>"><?php _e('Printing SKU Matrix', 'wp2print'); ?></a>
				</p>
			</div>
			<?php
		}
	}
}

add_action('woocommerce_product_options_advanced', 'print_products_woocommerce_product_options_advanced');
function print_products_woocommerce_product_options_advanced() {
	global $post;
	$product_id = $post->ID;
	$instruction_link_type = get_post_meta($product_id, '_instruction_link_type', true);
	$instruction_link_text = get_post_meta($product_id, '_instruction_link_text', true);
	$instruction_video_url = get_post_meta($product_id, '_instruction_video_url', true);
	$project_name_field = get_post_meta($product_id, '_project_name_field', true);
	$iltypes = array('text' => __('Text link only', 'wp2print'), 'help' => __('Help icon and text link', 'wp2print'), 'video' => __('Video icon and text link', 'wp2print'));
	?>
	<div class="options_group hide_if_external hide_if_grouped">
		<p class="form-field">
			<label><?php _e('Instruction link type', 'wp2print'); ?>:</label>
			<select name="instruction_link_type" onchange="artwork_instruction_link_type_change()">
				<option value="">-- <?php _e('Select', 'wp2print'); ?> --</option>
				<?php foreach($iltypes as $ltkey => $ltval) { ?>
					<option value="<?php echo $ltkey; ?>"<?php if ($ltkey == $instruction_link_type) { echo ' SELECTED'; } ?>><?php echo $ltval; ?></option>
				<?php } ?>
			</select>
		</p>
		<p class="form-field instruction-link-text">
			<label><?php _e('Link text', 'wp2print'); ?>:</label>
			<input type="text" name="instruction_link_text" value="<?php echo $instruction_link_text; ?>" style="width:80%;">
		</p>
		<p class="form-field">
			<label><?php _e('URL to instruction video', 'wp2print'); ?>:</label>
			<input type="text" name="instruction_video_url" value="<?php echo $instruction_video_url; ?>" style="width:80%;">
		</p>
	</div>
	<div class="options_group hide_if_external hide_if_grouped">
		<p class="form-field">
			<label><?php _e('Display Project Name field', 'wp2print'); ?>:</label>
			<input type="checkbox" name="project_name_field" value="1"<?php if ($project_name_field == 1) { echo ' CHECKED'; } ?>>
		</p>
	</div>
	<?php
}

add_action('woocommerce_process_product_meta', 'print_products_process_product_meta');
function print_products_process_product_meta($post_id) {
	$product_type = print_products_get_type($post_id);
	if ($product_type == 'area' || $product_type == 'box' || $product_type == 'sticker') {
		update_post_meta($post_id, '_area_min_width', $_POST['area_min_width']);
		update_post_meta($post_id, '_area_max_width', $_POST['area_max_width']);
		update_post_meta($post_id, '_area_min_height', $_POST['area_min_height']);
		update_post_meta($post_id, '_area_max_height', $_POST['area_max_height']);
		update_post_meta($post_id, '_area_min_quantity', $_POST['area_min_quantity']);
		update_post_meta($post_id, '_area_width_round', $_POST['area_width_round']);
		update_post_meta($post_id, '_area_height_round', $_POST['area_height_round']);
		update_post_meta($post_id, '_area_unit', $_POST['area_unit']);
		update_post_meta($post_id, '_area_transposable', (int)$_POST['area_transposable']);
		if (isset($_POST['area_min_length'])) {
			update_post_meta($post_id, '_area_min_length', $_POST['area_min_length']);
			update_post_meta($post_id, '_area_max_length', $_POST['area_max_length']);
			update_post_meta($post_id, '_area_length_round', $_POST['area_length_round']);
		}
		if ($product_type == 'sticker') {
			$area_ranges = array();
			if (isset($_POST['area_ranges']) && $_POST['area_ranges']) {
				foreach($_POST['area_ranges'] as $ardata) {
					if (strlen($ardata['min']) && $ardata['min'] != '{N}' && strlen($ardata['max']) && $ardata['max'] != '{N}') {
						$area_ranges[] = array('min' => $ardata['min'], 'max' => $ardata['max']);
					}
				}
			}
			update_post_meta($post_id, '_area_ranges', $area_ranges);
			
		}
	} else if ($product_type == 'paybill') {
		update_post_meta($post_id, '_paybill_options', $_POST['paybill_options']);
	} else if ($product_type == 'book') {
		update_post_meta($post_id, '_book_type', $_POST['_book_type']);
	}

	if (isset($_POST['quote_options'])) {
		update_post_meta($post_id, '_quote_options', $_POST['quote_options']);
	}

	update_post_meta($post_id, '_artwork_source', $_POST['artwork_source']);
	update_post_meta($post_id, '_artwork_allow_later', $_POST['artwork_allow_later']);
	update_post_meta($post_id, '_artwork_file_count', $_POST['artwork_file_count']);
	update_post_meta($post_id, '_artwork_afile_types', $_POST['artwork_afile_types']);
	update_post_meta($post_id, '_show_attribute_color', (int)$_POST['show_attribute_color']);
	update_post_meta($post_id, '_color_modify_image', (int)$_POST['color_modify_image']);
	update_post_meta($post_id, '_color_product_main_images', $_POST['color_product_main_images']);
	update_post_meta($post_id, '_cart_upload_button', $_POST['cart_upload_button']);
	update_post_meta($post_id, '_cart_upload_button_text', $_POST['cart_upload_button_text']);
	update_post_meta($post_id, '_cart_upload_stage', $_POST['cart_upload_stage']);
	update_post_meta($post_id, '_cart_upload_artwork_label', $_POST['cart_upload_artwork_label']);
	update_post_meta($post_id, '_cart_upload_artwork_required', $_POST['cart_upload_artwork_required']);
	update_post_meta($post_id, '_cart_upload_artwork_ftypes', $_POST['cart_upload_artwork_ftypes']);
	update_post_meta($post_id, '_cart_upload_artwork_uploaded_files', $_POST['cart_upload_artwork_uploaded_files']);
	update_post_meta($post_id, '_cart_upload_database_label', $_POST['cart_upload_database_label']);
	update_post_meta($post_id, '_cart_upload_database_required', $_POST['cart_upload_database_required']);
	update_post_meta($post_id, '_cart_upload_database_ftypes', $_POST['cart_upload_database_ftypes']);
	update_post_meta($post_id, '_cart_upload_database_uploaded_files', $_POST['cart_upload_database_uploaded_files']);
	update_post_meta($post_id, '_attribute_labels', $_POST['attribute_labels']);
	update_post_meta($post_id, '_attribute_display', $_POST['attribute_display']);
	update_post_meta($post_id, '_attribute_as_radio', $_POST['attribute_as_radio']);
	update_post_meta($post_id, '_ieditor_product_id', $_POST['ieditor_product_id']);
	update_post_meta($post_id, '_show_uploaded_files', (int)$_POST['show_uploaded_files']);
	update_post_meta($post_id, '_show_uploaded_files_only', $_POST['show_uploaded_files_only']);
	update_post_meta($post_id, '_show_uploaded_files_types', $_POST['show_uploaded_files_types']);
	update_post_meta($post_id, '_multiple_file_uploads', (int)$_POST['multiple_file_uploads']);
	update_post_meta($post_id, '_multiple_file_data', $_POST['multiple_file_data']);

	// production speed options
	$use_production_speed = (int)$_POST['use_production_speed'];
	$production_speed_label = $_POST['production_speed_label'];
	$production_speed_sd_data = $_POST['production_speed_sd_data'];
	$production_speed_options = '';
	if ($use_production_speed) {
		$pso_options = $_POST['production_speed_options'];
		unset($pso_options['{N}']);
		if ($pso_options && count($pso_options)) {
			$production_speed_options = array();
			foreach($pso_options as $pso_option) {
				$production_speed_options[] = array('label' => $pso_option['label'], 'percent' => $pso_option['percent'], 'days' => $pso_option['days']);
			}
		} else {
			$use_production_speed = 0;
		}
	}
	update_post_meta($post_id, '_use_production_speed', $use_production_speed);
	update_post_meta($post_id, '_production_speed_options', $production_speed_options);
	update_post_meta($post_id, '_production_speed_label', $production_speed_label);
	update_post_meta($post_id, '_production_speed_sd_data', $production_speed_sd_data);
	update_post_meta($post_id, '_order_min_price', $_POST['order_min_price']);
	update_post_meta($post_id, '_order_max_price', $_POST['order_max_price']);
	update_post_meta($post_id, '_unitpricetable', $_POST['unitpricetable']);
	update_post_meta($post_id, '_addon_products', $_POST['addon_products']);
	update_post_meta($post_id, '_preflight_options', $_POST['preflight_options']);

	$artwork_file_url = trim($_POST['artwork_file_url']);
	if (strlen($artwork_file_url) && substr($artwork_file_url, 0, 4) != 'http') {
		$artwork_file_url = 'http://' . $artwork_file_url;
	}
	update_post_meta($post_id, '_artwork_file_url', $artwork_file_url);
	update_post_meta($post_id, '_artwork_file_url_order', (int)$_POST['artwork_file_url_order']);
	update_post_meta($post_id, '_artwork_file_url_email', (int)$_POST['artwork_file_url_email']);

	update_post_meta($post_id, '_instruction_link_type', $_POST['instruction_link_type']);
	update_post_meta($post_id, '_instruction_link_text', $_POST['instruction_link_text']);
	update_post_meta($post_id, '_instruction_video_url', $_POST['instruction_video_url']);
	update_post_meta($post_id, '_project_name_field', $_POST['project_name_field']);
	if (print_products_license_allow_flexo_plate()) {
		update_post_meta($post_id, '_reorder_color', (int)$_POST['reorder_color']);
		update_post_meta($post_id, '_reorder_color_1', $_POST['reorder_color_1']);
	}
}

add_action('woocommerce_process_product_meta_aec', 'print_products_process_aec_product_meta');
add_action('woocommerce_process_product_meta_aecbwc', 'print_products_process_aec_product_meta');
add_action('woocommerce_process_product_meta_aecsimple', 'print_products_process_aec_product_meta');
function print_products_process_aec_product_meta($post_id) {
	if (isset($_POST['inc_coverage_prices'])) {
		update_post_meta($post_id, '_inc_coverage_prices', $_POST['inc_coverage_prices']);
		update_post_meta($post_id, '_apply_round_up', (int)$_POST['apply_round_up']);
		update_post_meta($post_id, '_round_up_discounts', $_POST['round_up_discounts']);

		// update price
		$inc_coverage_prices = $_POST['inc_coverage_prices'];
		if (is_array($inc_coverage_prices) && count($inc_coverage_prices)) {
			$product_price = 1000000000000000000;
			foreach($inc_coverage_prices as $icprices) {
				if (is_array($icprices)) {
					foreach($icprices as $icprice) {
						$icprice = (float)$icprice;
						if ($icprice < $product_price) {
							$product_price = $icprice;
						}
					}
				} else {
					$icprice = (float)$icprices;
					if ($icprice < $product_price) {
						$product_price = $icprice;
					}
				}
			}
			update_post_meta($post_id, '_price', $product_price);
			update_post_meta($post_id, '_regular_price', $product_price);
		}
	}
}

// save custom data
add_action('save_post', 'print_products_product_custom_options_save', 10, 2);
function print_products_product_custom_options_save($post_id, $post) {
	if (isset($_POST['action']) && $_POST['action'] == 'inline-save') { return; }
	if ($post->post_type == 'product' && isset($_POST['action']) && $_POST['action'] == 'editpost') {
		$product_type = print_products_get_type($post_id);
		// update shipping weights
		$product_shipping_weights = isset($_POST['product_shipping_weights']) ? serialize($_POST['product_shipping_weights']) : '';
		$product_shipping_base_quantity = isset($_POST['product_shipping_base_quantity']) ? $_POST['product_shipping_base_quantity'] : '';
		$product_display_weight = isset($_POST['product_display_weight']) ? (int)$_POST['product_display_weight'] : 0;
		$product_max_weight_per_box = isset($_POST['product_max_weight_per_box']) ? $_POST['product_max_weight_per_box'] : '';
		$product_display_price = isset($_POST['product_display_price']) ? $_POST['product_display_price'] : '';
		$nl_hide_prices = isset($_POST['nl_hide_prices']) ? (int)$_POST['nl_hide_prices'] : 0;

		if ($product_type == 'book') { $product_shipping_base_quantity = serialize($product_shipping_base_quantity); }

		update_post_meta($post_id, '_product_shipping_weights', $product_shipping_weights);
		update_post_meta($post_id, '_product_shipping_base_quantity', $product_shipping_base_quantity);
		update_post_meta($post_id, '_product_display_weight', $product_display_weight);
		update_post_meta($post_id, '_product_display_price', $product_display_price);
		update_post_meta($post_id, '_product_max_weight_per_box', $product_max_weight_per_box);
		update_post_meta($post_id, '_nl_hide_prices', $nl_hide_prices);
	}
}
?>