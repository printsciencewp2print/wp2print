<?php
/*
Plugin Name: wp2print
Description: wp2print brings full Web-to-Print functions to Wordpress/WooCommerce
Version: 3.7.478
Author: Print Science
Tested up to: 6.4
Bitbucket Plugin URI: printsciencewp2print/wp2print
WC tested up to: 8.4
*/
$wp2print_version = '3.7.478';

if (!defined('ABSPATH')) exit;

if (!session_id()) { @session_start(); }

// settings
$siteurl = get_option('siteurl');
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
	$siteurl = str_replace('http:', 'https:', $siteurl);
}

ini_set('max_execution_time', '7200');
ini_set('memory_limit', '512M');
ini_set('post_max_size', '1024M');
ini_set('max_input_vars', '5000');

define('PRINT_PRODUCTS_PLUGIN_DIR', dirname(__FILE__));
define('PRINT_PRODUCTS_PLUGIN_URL', $siteurl.'/wp-content/plugins/wp2print/');
define('PRINT_PRODUCTS_TEMPLATES_DIR', PRINT_PRODUCTS_PLUGIN_DIR . '/templates/');
define('PRINT_PRODUCTS_API_SERVER_URL', 'https://license.printscience.net');
define('PRINT_PRODUCTS_API_SECRET_KEY', 'ZKu9g9cKmNZhXuY5Jy6M');
define('PRINT_PRODUCTS_BLITLINE_API_KEY', '1qerRZ8IELX5qG7H7DK1vUg');

if (class_exists('WooCommerce')) {
	define('IS_WOOCOMMERCE', true);
} else {
	define('IS_WOOCOMMERCE', false);
}

$designer_installed = false;
$print_products_settings = get_option('print_products_settings');
$print_products_plugin_options = get_option('print_products_plugin_options');
$print_products_plugin_aec = get_option('print_products_plugin_aec');
$print_products_file_upload_target = get_option('print_products_file_upload_target');
$print_products_amazon_s3_settings = get_option('print_products_amazon_s3_settings');
$woocommerce_custom_orders_table_enabled = get_option('woocommerce_custom_orders_table_enabled');

include(PRINT_PRODUCTS_PLUGIN_DIR . '/wp2print-functions.php');

include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-settings.php');

if (a12() && IS_WOOCOMMERCE) {
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-woo.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-product-data.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-product-classes.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-product-unavail.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-checkout-process.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-price-matrix-types.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-price-matrix-values.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-price-matrix-sku.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-price-matrix-settings.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-sort-order.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-orders-missing-files.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-orders-rejected-files.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-attribute-fields.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-simple-submit.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-polylang.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-sso.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-orders-export.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-wwof.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-email-quote.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-vendor.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-create-order.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-request-payment.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-blitline.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-validation-address.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-zapier.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-order-item-status.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-order-item-rsdate.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-myaccount-orders-search.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-recaptcha.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-user-discount.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-cf7-upload.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-rest-api.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-user-artwork-files.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-user-orders.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-user-files.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-printers-plan.php');
	//include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-printvis.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-paybill.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-fixed-import.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-ieditor.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-registration.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-cleantalk.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-orders-backup.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-production-view.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-docket-manager.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-sales-quotes.php');
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-inject-order.php');
	if (isset($_GET['cadratin_test'])) {
		include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-cadratin.php');
	}
	if (print_products_ifv()) { // mini subscription level
		include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-preflight.php');
		include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-send-quote.php');
		include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-users-groups.php');
		include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-users-groups-orders.php');
		include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-orders-proof.php');
	}
	include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-spam.php');
	if (print_products_is_allow_aec()) {
		include(PRINT_PRODUCTS_PLUGIN_DIR . '/inc/wp2print-aec-orders.php');
	}
}

// plugin translation
add_action('plugins_loaded', 'print_products_load_textdomain');
function print_products_load_textdomain() {
	load_plugin_textdomain('wp2print', false, trailingslashit( WP_LANG_DIR ) . 'plugins/');
	load_plugin_textdomain('wp2print', false, plugin_basename( PRINT_PRODUCTS_PLUGIN_DIR ) . '/languages'); 
	if (is_dir(trailingslashit( WP_LANG_DIR ) . 'loco/plugins')) {
		load_plugin_textdomain('wp2print', false, trailingslashit( WP_LANG_DIR ) . 'loco/plugins'); 
	}
}

// install plugin data
register_activation_hook(__FILE__, 'print_products_install');
function print_products_install() {
	global $wpdb, $wp2print_version;
	include(dirname(__FILE__).'/wp2print-install.php');
}

// front-end part
add_action('wp_head', 'print_products_header');
function print_products_header() {
	if (a12()) {
		echo '<link href="' . PRINT_PRODUCTS_PLUGIN_URL . 'css/wp2print.css?ver='.time().'" rel="stylesheet" type="text/css" />' . "\n";
		if (is_single()) {
			echo '<style>.select2-search { display: none !important; }</style>' . "\n";
		}
	}
}

add_action('wp_footer', 'print_products_footer');
function print_products_footer() {
	if (a12()) {
		echo '<link href="' . PRINT_PRODUCTS_PLUGIN_URL . 'css/colorbox.css" rel="stylesheet" type="text/css" />' . "\n";
		echo '<script type="text/javascript">var wp2print_siteurl = "'.home_url('/').'";</script>' . "\n";
		echo '<script type="text/javascript" src="' . PRINT_PRODUCTS_PLUGIN_URL . 'js/jquery.colorbox.min.js"></script>' . "\n";
		echo '<script type="text/javascript" src="' . PRINT_PRODUCTS_PLUGIN_URL . 'js/wp2print.js?ver='.time().'"></script>' . "\n"; // 11117
	}
	if (is_account_page()) { ?>
		<script type="text/javascript" src="<?php echo PRINT_PRODUCTS_PLUGIN_URL; ?>js/plupload/plupload.full.min.js?ver=3.1.2"></script>
		<?php include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-order-artwork-upload.php';
	}
}

add_action('wp_enqueue_scripts', 'print_products_enqueue_scripts', 11);
function print_products_enqueue_scripts() {
	global $woocommerce, $post;
	if (a12() && IS_WOOCOMMERCE) {
		wp_enqueue_script('prettyPhoto', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto.min.js', array('jquery'), $woocommerce->version, true);
		wp_enqueue_script('prettyPhoto-init', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto.init.min.js', array('jquery'), $woocommerce->version, true);
		wp_enqueue_style('woocommerce_prettyPhoto_css', $woocommerce->plugin_url() . '/assets/css/prettyPhoto.css');

		$currtheme = get_option('stylesheet');
		if ($currtheme == 'ascend_premium' && is_single() && $post->post_type == 'product') {
			wp_deregister_script( 'select2' );
		}
	}
}

// admin part
add_action('admin_menu', 'print_products_admin_menu', 50);
function print_products_admin_menu() {
	global $current_user;
	if (a12() && IS_WOOCOMMERCE) {
		if (current_user_can('create_users')) {
			// wp2print
			add_menu_page(__('wp2print', 'wp2print'), __('wp2print', 'wp2print'), 'create_users', 'print-products-production-view', 'print_products_oistatus_admin_page', '', 57);
			add_submenu_page('print-products-production-view', __('Production View', 'wp2print'), __('Production View', 'wp2print'), 'create_users', 'print-products-production-view', 'print_products_oistatus_admin_page');
			if (class_exists('WOO_MSTORE_MULTI_INIT')) {
				add_submenu_page('print-products-production-view', __('Network Production View', 'wp2print'), __('Network Production View', 'wp2print'), 'create_users', 'print-products-network-production-view', 'print_products_oistatus_network_production_view_page');
			}
			add_submenu_page('print-products-production-view', __('Create Order', 'wp2print'), __('Create Order', 'wp2print'), 'create_users', 'print-products-create-order', 'print_products_create_order_admin_page');
			if (print_products_ifv()) {
				add_submenu_page('print-products-production-view', __('Send Quote', 'wp2print'), __('Send Quote', 'wp2print'), 'create_users', 'print-products-send-quote', 'print_products_send_quote_admin_page');
				add_submenu_page('print-products-production-view', __('Send Quote history', 'wp2print'), __('Send Quote history', 'wp2print'), 'create_users', 'print-products-send-quote-history', 'print_products_send_quote_history_admin_page');
				add_submenu_page('print-products-production-view', __('Preflight', 'wp2print'), __('Preflight', 'wp2print'), 'create_users', 'print-products-preflight', 'print_products_preflight_admin_page');
			}
			add_submenu_page('print-products-production-view', __('Request payment', 'wp2print'), __('Request payment', 'wp2print'), 'create_users', 'print-products-request-payment', 'print_products_request_payment_admin_page');
			add_submenu_page('print-products-production-view', __('Request payment history', 'wp2print'), __('Request payment history', 'wp2print'), 'create_users', 'print-products-request-payment-history', 'print_products_request_payment_history_admin_page');
			add_submenu_page('print-products-production-view', __('Users artwork files', 'wp2print'), __('Users artwork files', 'wp2print'), 'create_users', 'print-products-users-artwork-files', 'print_products_user_artwork_files_admin_page');
			add_submenu_page('print-products-production-view', __('Users Orders', 'wp2print'), __('Users Orders', 'wp2print'), 'create_users', 'print-products-users-orders', 'print_products_user_orders_admin_page');
			add_submenu_page('print-products-production-view', __('Group order history', 'wp2print'), __('Group order history', 'wp2print'), 'create_users', 'print-products-groups-orders', 'print_products_groups_orders_admin_page');
			if (!print_products_is_hpos_enabled()) {
				add_submenu_page('print-products-production-view', __('Order archiving', 'wp2print'), __('Order archiving', 'wp2print'), 'create_users', 'print-products-order-archiving', 'print_products_orders_backup_admin_page');
			}
			add_submenu_page('print-products-production-view', __('Settings', 'wp2print'), __('Settings', 'wp2print'), 'manage_options', 'print-products-settings', 'print_products_settings');

			// woocommerce
			$pslug = 'edit.php?post_type=shop_order';
			if (current_user_can('manage_options')) { $pslug = 'woocommerce'; }
			add_submenu_page($pslug, __('Create Order', 'wp2print'), __('Create Order', 'wp2print'), 'create_users', 'print-products-create-order', 'print_products_create_order_admin_page');
			if (print_products_ifv()) {
				add_submenu_page($pslug, __('Send Quote', 'wp2print'), __('Send Quote', 'wp2print'), 'create_users', 'print-products-send-quote', 'print_products_send_quote_admin_page');
				add_submenu_page($pslug, __('Send Quote history', 'wp2print'), __('Send Quote history', 'wp2print'), 'create_users', 'print-products-send-quote-history', 'print_products_send_quote_history_admin_page');
				add_submenu_page($pslug, __('Preflight', 'wp2print'), __('Preflight', 'wp2print'), 'create_users', 'print-products-preflight', 'print_products_preflight_admin_page');
			}
			add_submenu_page($pslug, __('Request payment', 'wp2print'), __('Request payment', 'wp2print'), 'create_users', 'print-products-request-payment', 'print_products_request_payment_admin_page');
			add_submenu_page($pslug, __('Request payment history', 'wp2print'), __('Request payment history', 'wp2print'), 'create_users', 'print-products-request-payment-history', 'print_products_request_payment_history_admin_page');
			add_submenu_page($pslug, __('Production View', 'wp2print'), __('Production View', 'wp2print'), 'create_users', 'print-products-production-view', 'print_products_oistatus_admin_page');
			add_submenu_page($pslug, __('Printers Plan', 'wp2print'), __('Printers Plan', 'wp2print'), 'create_users', 'print-products-printers-plan', 'print_products_printers_plan_admin_page');
			//add_submenu_page($pslug, __('PrintVIS', 'wp2print'), __('PrintVIS', 'wp2print'), 'create_users', 'print-products-printvis', 'print_products_printvis_admin_page');
			add_submenu_page($pslug, __('Docket Manager', 'wp2print'), __('Docket Manager', 'wp2print'), 'create_users', 'print-products-docket-manager', 'print_products_docketmanager_admin_page');
			if (class_exists('WOO_MSTORE_MULTI_INIT')) {
				add_submenu_page($pslug, __('Network Production View', 'wp2print'), __('Network Production View', 'wp2print'), 'create_users', 'print-products-network-production-view', 'print_products_oistatus_network_production_view_page');
			}
		}
		if (in_array('vendor', $current_user->roles)) {
			if (print_products_vendor_allow_access('pview')) { add_submenu_page('woocommerce', __('Production View', 'wp2print'), __('Production View', 'wp2print'), 'edit_shop_orders', 'print-products-production-view', 'print_products_oistatus_admin_page'); }
			if (print_products_vendor_allow_access('uafiles')) { add_submenu_page('woocommerce', __('Users artwork files', 'wp2print'), __('Users artwork files', 'wp2print'), 'edit_shop_orders', 'print-products-users-artwork-files', 'print_products_user_artwork_files_admin_page'); }
			if (print_products_vendor_allow_access('uorders')) { add_submenu_page('woocommerce', __('Users Orders', 'wp2print'), __('Users Orders', 'wp2print'), 'edit_shop_orders', 'print-products-users-orders', 'print_products_user_orders_admin_page'); }
		} else if (in_array('adminlite', $current_user->roles)) {
			add_submenu_page('woocommerce', __('Create Order', 'wp2print'), __('Create Order', 'wp2print'), 'adminlite_access', 'print-products-create-order', 'print_products_create_order_admin_page');
			add_submenu_page('woocommerce', __('Send Quote', 'wp2print'), __('Send Quote', 'wp2print'), 'adminlite_access', 'print-products-send-quote', 'print_products_send_quote_admin_page');
			add_submenu_page('woocommerce', __('Production View', 'wp2print'), __('Production View', 'wp2print'), 'adminlite_access', 'print-products-production-view', 'print_products_oistatus_admin_page');
			add_submenu_page('woocommerce', __('Request payment', 'wp2print'), __('Request payment', 'wp2print'), 'adminlite_access', 'print-products-request-payment', 'print_products_request_payment_admin_page');
			add_submenu_page('woocommerce', __('Users artwork files', 'wp2print'), __('Users artwork files', 'wp2print'), 'adminlite_access', 'print-products-users-artwork-files', 'print_products_user_artwork_files_admin_page');
			add_submenu_page('woocommerce', __('Users Orders', 'wp2print'), __('Users Orders', 'wp2print'), 'adminlite_access', 'print-products-users-orders', 'print_products_user_orders_admin_page');
		}

		add_options_page(__('wp2print', 'wp2print'), __('wp2print', 'wp2print'), 'manage_options', 'print-products-settings-fs', 'print_products_settings');

		add_submenu_page('edit.php?post_type=product', __('Price Matrix Options', 'wp2print'), __('Price Matrix Options', 'wp2print'), 'manage_options', 'print-products-price-matrix-options', 'print_products_price_matrix_types');
		add_submenu_page('edit.php?post_type=product', __('Price Matrix Values', 'wp2print'), __('Price Matrix Values', 'wp2print'), 'manage_options', 'print-products-price-matrix-values', 'print_products_price_matrix_values');
		add_submenu_page('edit.php?post_type=product', __('Unavailability', 'wp2print'), __('Unavailability', 'wp2print'), 'manage_options', 'print-products-unavailability', 'print_products_unavailability_page');
		add_submenu_page('edit.php?post_type=product', __('Printing SKU Matrix', 'wp2print'), __('Printing SKU Matrix', 'wp2print'), 'manage_options', 'print-products-price-matrix-sku', 'print_products_price_matrix_sku');
		add_submenu_page('edit.php?post_type=product', __('Attributes Options', 'wp2print'), __('Attributes Options', 'wp2print'), 'manage_options', 'print-products-attributes-options', 'print_products_attributes_options');
		add_submenu_page('edit.php?post_type=product', __('Attribute sort order', 'wp2print'), __('Attribute sort order', 'wp2print'), 'manage_options', 'print-products-sort-order', 'print_products_sort_order_page');
		add_submenu_page('edit.php?post_type=product', __('Fixed-size Import', 'wp2print'), __('Fixed-size Import', 'wp2print'), 'manage_options', 'print-products-fixed-import', 'print_products_fixed_import_page');
		add_submenu_page('edit.php?post_type=product', __('Fixed-size Export', 'wp2print'), __('Fixed-size Export', 'wp2print'), 'manage_options', 'print-products-fixed-export', 'print_products_fixed_export_page');
	} else {
		add_options_page(__('wp2print', 'wp2print'), __('wp2print', 'wp2print'), 'manage_options', 'print-products-settings', 'print_products_settings');
	}
}

add_action('admin_print_scripts', 'print_products_admin_print_scripts');
function print_products_admin_print_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_media();
	if (isset($_GET['page']) && (($_GET['page'] == 'print-products-settings' && isset($_GET['tab']) && $_GET['tab'] == 'vendor') || ($_GET['page'] == 'print-products-create-order' && isset($_GET['step']) && $_GET['step'] == '2'))) {
		$default_location = wc_get_customer_default_location();
		wp_enqueue_script('wc-admin-order-meta-boxes', WC()->plugin_url() . '/assets/js/admin/meta-boxes-order.min.js', array('wc-admin-meta-boxes', 'wc-backbone-modal', 'selectWoo', 'wc-clipboard'), WC_VERSION);
		wp_localize_script('wc-admin-order-meta-boxes', 'woocommerce_admin_meta_boxes_order', array(
			'countries'              => json_encode( array_merge( WC()->countries->get_allowed_country_states(), WC()->countries->get_shipping_country_states() ) ),
			'i18n_select_state_text' => esc_attr__( 'Select an option&hellip;', 'woocommerce' ),
			'default_country'        => isset( $default_location['country'] ) ? $default_location['country'] : '',
			'default_state'          => isset( $default_location['state'] ) ? $default_location['state'] : '',
			'placeholder_name'       => esc_attr__( 'Name (required)', 'woocommerce' ),
			'placeholder_value'      => esc_attr__( 'Value (required)', 'woocommerce' ),
		));
		wp_register_style('woocommerce_admin_styles', WC()->plugin_url() . '/assets/css/admin.css', array(), WC_VERSION);
		wp_enqueue_style('woocommerce_admin_styles');
	}
    wp_enqueue_style('jquery-ui-timepicker-addon', PRINT_PRODUCTS_PLUGIN_URL.'css/jquery-ui-timepicker-addon.css', array());
	if (isset($_GET['page']) && $_GET['page'] == 'print-products-orders-backup') {
		wp_enqueue_script('jquery-ui-datepicker');
		wp_register_style('bo-jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
		wp_enqueue_style('bo-jquery-ui');
	}
}

add_action('admin_footer', 'print_products_admin_footer');
function print_products_admin_footer() {
	global $pagenow, $post, $current_user;
	$screen_id = get_current_screen()->id;
	$ver = time();
	if (a12()) {
		echo '<link href="' . plugins_url('css/colorbox.css', __FILE__) . '" rel="stylesheet" type="text/css" />' . "\n";
		echo '<link href="' . plugins_url('css/select2.min.css', __FILE__) . '" rel="stylesheet" type="text/css" />' . "\n";
		echo '<link href="' . plugins_url('css/jquery.minicolors.css', __FILE__) . '" rel="stylesheet" type="text/css" />' . "\n";
		echo '<link href="' . plugins_url('css/wp2print-admin.css', __FILE__) . '?ver='.$ver.'" rel="stylesheet" type="text/css" />' . "\n";

		echo '<script type="text/javascript">var wp2print_adminurl = "'.admin_url('/').'"; var order_print_label = "'.__('Print', 'wp2print').'"; var order_edit_btn = false;</script>';
		echo '<script type="text/javascript" src="' . plugins_url('js/jquery.colorbox.min.js', __FILE__) . '"></script>' . "\n";
		echo '<script type="text/javascript" src="' . plugins_url('js/select2.min.js', __FILE__) . '"></script>' . "\n";
		echo '<script type="text/javascript" src="' . plugins_url('js/jquery.minicolors.min.js', __FILE__) . '"></script>' . "\n";
		print_products_jquery_ui();
		echo '<script type="text/javascript" src="' . plugins_url('js/wp2print-admin.js?ver='.$ver.'', __FILE__) . '"></script>' . "\n";
		?>
		<script type="text/javascript">
			jQuery('#menu-posts-product ul li').each(function(){
				var ahtml = jQuery(this).find('a').html();
				if (ahtml == '<?php _e('Price Matrix Options', 'wp2print'); ?>' || ahtml == '<?php _e('Price Matrix Values', 'wp2print'); ?>' || ahtml == '<?php _e('Printing SKU Matrix', 'wp2print'); ?>' || ahtml == '<?php _e('Unavailability', 'wp2print'); ?>') {
					jQuery(this).hide();
				}
			});
			jQuery('#adminmenu ul li').each(function(){
				var ahref = jQuery(this).find('a').attr('href');
				if (ahref == 'admin.php?page=print-products-send-quote-history' || ahref == 'edit.php?post_type=shop_order&page=print-products-send-quote-history' || ahref == 'admin.php?page=print-products-request-payment-history' || ahref == 'edit.php?post_type=shop_order&page=print-products-request-payment-history') {
					jQuery(this).hide();
				}
			});
			jQuery('div.notice-error[data-dismissible="401-error-1"]').remove();
			if (jQuery('input#woocommerce_feature_product_block_editor_enabled').length) {
				jQuery('input#woocommerce_feature_product_block_editor_enabled').parents('tr').remove();
			}
		</script>
		<?php
		if ($pagenow == 'edit.php' && isset($_GET['post_type']) && $_GET['post_type'] == 'product' && isset($_GET['page']) && $_GET['page'] == 'product_attributes' && !isset($_GET['adelete'])) {
			$print_products_settings = get_option('print_products_settings'); ?>
			<script>
			jQuery('.attributes-table .row-actions a.delete').each(function(){
				var ahref = jQuery(this).attr('href');
				if (ahref.indexOf('&delete=<?php echo $print_products_settings['size_attribute']; ?>&') > 0 || ahref.indexOf('&delete=<?php echo $print_products_settings['colour_attribute']; ?>&') > 0 || ahref.indexOf('&delete=<?php echo $print_products_settings['material_attribute']; ?>&') > 0 || ahref.indexOf('&delete=<?php echo $print_products_settings['page_count_attribute']; ?>&') > 0) {
					jQuery(this).remove();
				}
			});
			</script>
			<?php
		}
		if ($screen_id == 'shop_order' || $screen_id == 'woocommerce_page_wc-orders') {
			$print_products_jobticket_options = get_option("print_products_jobticket_options");
			$exclude_prices = 0;
			$order_id = print_products_woocommerce_get_current_order_id();
			if ($print_products_jobticket_options && isset($print_products_jobticket_options['exclude_prices']) && $print_products_jobticket_options['exclude_prices'] == 1) { $exclude_prices = 1; }
			include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-order-proof-upload.php';
			include PRINT_PRODUCTS_TEMPLATES_DIR . 'admin-order-artwork-upload.php';
			if ($exclude_prices == 1) {
				echo '<script>jQuery("body").addClass("print-no-prices");</script>';
			}
			echo '<form class="sva-form" method="POST"><input type="hidden" name="validaddressaction" value="save"><input type="hidden" name="order_id" value="'.$order_id.'"></form>';
			print_products_admin_footer_edit_order_js();

		}
		if ($screen_id == 'edit-shop_order' || $screen_id == 'woocommerce_page_wc-orders') {
			echo '<style>.wrap a.page-title-action{ display:none; }</style>';
		}
		if ($screen_id == 'shop_order' || $screen_id == 'edit-shop_order' || $screen_id == 'woocommerce_page_wc-orders' || ($pagenow == 'admin.php' && isset($_GET['page']) && $_GET['page'] == 'print-products-production-view')) {
			print_products_oistatus_popup_html();
			print_products_vendor_popup_html();
			if (function_exists('print_products_preflight_popup_html')) { print_products_preflight_popup_html(); }
		}
		if (isset($_GET['action']) && $_GET['action'] == 'edit') {
			print_products_oirsdate_popup_html();
			print_products_vendor_assign_to_me_popup_html();
		}
		if ($pagenow == 'admin.php' && isset($_GET['page']) && $_GET['page'] == 'woonet-woocommerce-production-view') {
			print_products_oistatus_network_popup_html();
		}
		$pt_classes = array();
		$product_types = print_products_get_product_types();
		foreach ($product_types as $ptkey => $ptype) {
			$pt_classes[] = 'show_if_'.$ptkey;
		}
		?>
		<script>
		jQuery(document).ready(function() {
			jQuery('#woocommerce-product-data label[for="_virtual"]').addClass('<?php echo implode(' ', $pt_classes); ?>').show();
			if (jQuery('.wrap ul.subsubsub li.trash')) {
			}
		});
		</script>
		<?php if (in_array('adminlite', $current_user->roles)) { ?>
		<style>
		.wrap ul.subsubsub li.trash,
		#wc-orders-filter #bulk-action-selector-top option[value="trash"],
		#wc-orders-filter #bulk-action-selector-top option[value="untrash"],
		#wc-orders-filter #bulk-action-selector-top option[value="delete"],
		#wc-orders-filter #delete_all{
			display:none !important;
		}
		</style>
		<script>
		jQuery(document).ready(function() {
			if (jQuery('.wrap ul.subsubsub li.trash').length) {
				jQuery('.wrap ul.subsubsub li.trash').remove();
			}
		});
		</script>
		<?php
		}
	}
}

add_action('admin_notices', 'print_products_admin_notices');
function print_products_admin_notices() {
	$notices = array();
	$all_plugins = get_plugins();
	if (!isset($all_plugins['woocommerce/woocommerce.php'])) {
		$notices[] = __('`Woocommerce plugin` is required for `wp2print` plugin. Please install it.', 'wp2print');
	} else if (!class_exists( 'WooCommerce' )) {
		$notices[] = __('`Woocommerce plugin` is required for `wp2print` plugin. Please activate it.', 'wp2print');
	}
	if (count($notices)) { ?>
		<div id="message" class="error fade">
			<p><?php echo implode('</p><p>', $notices); ?></p>
		</div>
	<?php
	}
	if (!a12()) {
		if ($_GET['page'] != 'print-products-activation') { ?>
			<div id="message" class="error fade">
				<p><?php echo sprintf(__("Please visit <a href='%s'>this page</a> and enter your license code.", 'wp2print'), 'admin.php?page=print-products-settings'); ?></p>
			</div>
			<?php
		}
	}
	$session_data = get_transient('wp2print-expired');
	if ($session_data && $session_data == 'true') {
		delete_transient('wp2print-expired'); ?>
		<div id="message" class="error fade">
			<p><?php _e("Your license for wp2print is not eligible to receive updates. Please contact Print Science at info@printscience.com", 'wp2print'); ?></p>
		</div>
		<?php
	}
}

add_action('after_setup_theme', 'print_products_remove_admin_bar');
function print_products_remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}

// init actions
add_action('init', 'print_products_init');
function print_products_init() {
	global $wp2print_version;

	// check plugin version
	$last_wp2print_version = get_option('wp2print_version');
	if ($wp2print_version != $last_wp2print_version) {
		print_products_install();
	}

	// add admin lite role for woo orders
	$adminlite = get_role('adminlite');
	$administrator = get_role('administrator');
	if ($adminlite) {
		$alcaps = array('delete_shop_order', 'delete_shop_orders', 'delete_private_shop_orders', 'delete_published_shop_orders', 'delete_others_shop_orders');
		foreach ($alcaps as $alcap) {
			if ($adminlite->has_cap($alcap)) {
				$adminlite->remove_cap($alcap);
			}
		}
	} else {
		$capabilities = array(
			'read' => true,
			'edit_files' => true,
			'edit_shop_order' => true,
			'read_shop_order' => true,
			'edit_shop_orders' => true,
			'edit_others_shop_orders' => true,
			'publish_shop_orders' => true,
			'read_private_shop_orders' => true,
			'edit_private_shop_orders' => true,
			'edit_published_shop_orders' => true,
			'edit_others_posts' => true,
			'list_users' => true,
			'edit_users' => true
		);
		add_role('adminlite', __('Admin Lite'), $capabilities);
		$adminlite = get_role('adminlite');
	}
	if (!$adminlite->has_cap('rapid_quotes')) {
		$adminlite->add_cap('rapid_quotes');
	}
	if (!$adminlite->has_cap('adminlite_access')) {
		$adminlite->add_cap('adminlite_access');
	}
	if (!$adminlite->has_cap('list_users')) {
		$adminlite->add_cap('list_users');
	}
	if (!$adminlite->has_cap('edit_users')) {
		$adminlite->add_cap('edit_users');
	}
	if (!$adminlite->has_cap('edit_others_posts')) {
		$adminlite->add_cap('edit_others_posts');
	}
	if (!$administrator->has_cap('rapid_quotes')) {
		$administrator->add_cap('rapid_quotes');
	}
	// add Sales role
	$sales = get_role('sales');
	if (!$sales) {
		$capabilities = array(
			'read' => true,
			'create_users' => true,
			'edit_shop_order' => true,
			'edit_others_shop_orders' => true,
			'edit_private_shop_orders' => true,
			'edit_published_shop_orders' => true,
			'edit_shop_orders' => true,
			'edit_users' => true,
			'list_users' => true,
			'read_private_shop_orders' => true,
			'read_shop_order' => true
		);
		add_role('sales', __('Sales'), $capabilities);
	}

	// add Vendor role
	$vendor = get_role('vendor');
	if (!$vendor) {
		$capabilities = array(
			'read' => true,
			'edit_shop_order' => true,
			'read_shop_order' => true,
			'edit_shop_orders' => true,
			'edit_others_shop_orders' => true,
			'read_private_shop_orders' => true
		);
		add_role('vendor', __('Vendor'), $capabilities);
	}
}

add_action('wp_loaded', 'print_products_wp_loaded');
function print_products_wp_loaded() {
	global $designer_installed, $pagenow;

	if ($pagenow == 'admin-ajax.php' && $_REQUEST['action'] == 'update-plugin' && $_REQUEST['slug'] == 'wp2print') {
		$check_license_expiry = print_products_check_license_expiry();
		if (!$check_license_expiry) {
			_e('Your license for wp2print is not eligible to receive updates. Please contact Print Science at info@printscience.com', 'wp2print'); exit;
		}
	} else if (isset($_POST['wppusher']) && is_array($_POST['wppusher'])) {
		if (isset($_POST['wppusher']['action']) && $_POST['wppusher']['action'] == 'update-plugin' && isset($_POST['wppusher']['file']) && $_POST['wppusher']['file'] == 'wp2print/wp2print.php') {
			$check_license_expiry = print_products_check_license_expiry();
			if (!$check_license_expiry) {
				set_transient('wp2print-expired', 'true', 60);
				$redirect_to = $_SERVER['REQUEST_URI'];
				wp_redirect($redirect_to);
				exit;
			}
		}
	}
	print_products_clear_cart_data();
	// check print-science-designer plugin
	if (function_exists('personalize_init')) {
		$designer_installed = true;
	}

	print_products_check_dfincart();

	// disable add new product wizard
	update_option('woocommerce_feature_product_block_editor_enabled', 'no');

	// update cart / checkout page content
	print_products_woocommerce_cart_checkout_shortcode();

	// product price with tax
	if (isset($_POST['AjaxAction'])) {
		$aexit = false;
		switch ($_POST['AjaxAction']) {
			case 'product-get-price-with-tax':
				print_products_ajax_get_price_with_tax(); $aexit = true;
			break;
			case 'email-quote-send':
				print_products_ajax_email_quote_send(); $aexit = true;
			break;
			case 'check-product-config':
				print_products_ajax_check_product_config(); $aexit = true;
			break;
			case 'check-product-send-result':
				print_products_ajax_check_product_send_result(); $aexit = true;
			break;
		}
		if ($aexit) { exit; }
	}
	if (isset($_REQUEST['AjaxAction']) && $_REQUEST['AjaxAction'] == 'rapid-select-users') {
		print_products_ajax_rapid_select_users();
		exit;
	}
	if (isset($_GET['wp2printaction']) && $_GET['wp2printaction'] == 'clear-install') {
		update_option('print_products_installed', '0');
		echo 'Install parameter is cleared.';
		exit;
	}
	if ($pagenow == 'options-general.php' && isset($_GET['page']) && $_GET['page'] == 'print-products-settings-fs') {
		wp_redirect(admin_url('admin.php?page=print-products-settings'));
		exit;
	}
	print_products_rapid_user_search_check();
}

add_action('wp_loaded', 'print_products_close_session', 100);
function print_products_close_session() {
	session_write_close();
}

add_action('before_woocommerce_init', function(){
    if ( class_exists( \Automattic\WooCommerce\Utilities\FeaturesUtil::class ) ) {
        \Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility( 'custom_order_tables', __FILE__, true );
    }
});

// Amazon S3 Classes
$amazonS3Client = false;
if ($print_products_file_upload_target && $print_products_amazon_s3_settings) {
	if ($print_products_file_upload_target == 'amazon' && isset($print_products_amazon_s3_settings['s3_access']) && $print_products_amazon_s3_settings['s3_access'] == 'private' && $print_products_amazon_s3_settings['s3_access_key'] && isset($print_products_amazon_s3_settings['s3_secret_key']) && $print_products_amazon_s3_settings['s3_secret_key']) {
		require PRINT_PRODUCTS_PLUGIN_DIR . '/vendor/aws-autoloader.php';
		if (isset($print_products_amazon_s3_settings['s3_region']) && strlen($print_products_amazon_s3_settings['s3_region'])) {
			$amazonS3Client = @Aws\S3\S3Client::factory(array('region' => $print_products_amazon_s3_settings['s3_region'], 'signature' => 'v4', 'key' => $print_products_amazon_s3_settings['s3_access_key'], 'secret' => $print_products_amazon_s3_settings['s3_secret_key'], 'credentials' => new Aws\Common\Credentials\Credentials($print_products_amazon_s3_settings['s3_access_key'], $print_products_amazon_s3_settings['s3_secret_key'])));
		} else {
			$amazonS3Client = @Aws\S3\S3Client::factory(array('credentials' => new Aws\Common\Credentials\Credentials($print_products_amazon_s3_settings['s3_access_key'], $print_products_amazon_s3_settings['s3_secret_key'])));
		}
	}
}
?>