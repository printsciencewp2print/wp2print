![wp2print]

# wp2print

* Tags: product,customize,personalize,customization,personalization,design,online
* Requires at least: 4.6
* Requires PHP: 5.6
* Tested up to: 5.7
* Stable tag: master
* License: GPLv2 or later
* License URI: <http://www.gnu.org/licenses/gpl-2.0.html>

wp2print brings full Web-to-Print functions to Wordpress/WooCommerce

## Description

wp2print provides everything your printshop needs to begin selling online. Your customers will be able to get a price quote, upload their file or create a new design online and then make a payment.

wp2print can handle the complicated products that you print every day including:

* Brochures and Flyers
* Postcards
* Business cards
* Saddle-stitched catalogs
* Books with perfect binding and wire binding
* Wide-format banners

You can host wp2print on your own servers or receive managed-hosting from Print Science.
